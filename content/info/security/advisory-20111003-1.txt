KDE Security Advisory: KSSL and Rekonq Input Validation Failure
Original Release Date: 2011-10-03
URL: http://www.kde.org/info/security/advisory-20111003-1.txt

0. References:
    CVE-2011-3365 KSSL
    CVE-2011-3366 Rekonq

1. Systems affected:

    KSSL as shipped with KDE SC 4.6.0 up to and including KDE SC 4.7.1.
    Earlier versions of KDE SC may also be affected. Rekonq versions up to at
    least 0.7.0 are also affected.

2. Overview:

    The default rendering type for a QLabel is QLabel::AutoText, which uses
    heuristics to determine whether to render the given content as plain text
    or rich text.

    When displaying a security dialog with a certificate, KSSL does not
    properly force its QLabels to use QLabel::PlainText. As a result, if given
    a certificate containing rich text in its fields, it will render the rich
    text.

    Specifically, a certificate containing a common name (CN) that has a table
    element will cause the second line of the table to be displayed. This can
    allow spoofing of the certificate's common name.

    The vulnerability and technical information about the exploit were
    provided by Tim Brown of Nth Dimension. We thank them for their 
    responsible disclosure and cooperative handling of the matter.

3. Impact:

    Exploitation may trick the user into beliving a certificate is legitimate
    when in fact it is invalid, and simply displayed incorrectly.

4. Solution:

    Source code patches have been made available which fix these
    vulnerabilities. At the time of this writing most OS vendor / binary
    package providers should have updated binary packages. Contact your OS
    vendor / binary package provider for information about how to obtain
    updated binary packages.

5. Patch:

    Patches for this issue were provided by David Faure (KSSL) and Andrea
    Diamantini (Rekonq). In addition, a patch was provied to Qt by Peter
    Hartmann.

    Patches have been committed to the kdelibs Git repository in the
    following commit IDs:

    4.6 branch: 9ca2b26f 90607b28
    4.7 branch: bd70d4e5 86622e4d
    frameworks: bd70d4e5 86622e4d

    (Note: the second commit for each branch above is a fix for kio_http that
    fixes a similar issue, but with only very minor security implications.)

    Patches can be retrieved by cloning the kdelibs repository at
    git://anongit.kde.org/kdelibs.git and running "git show <commit-id>"

    Patches have been committed to the Rekonq Git repository in the following
    commit IDs:

    85f454fa
    526ce56f
    d1711fff

    These are three separate, but related patches.

    Patches can be retrieved by cloning the rekonq repository at
    git://anongit.kde.org/rekonq.git and running "git show <commit-id>"

    Qt has also received a patch to warn users about sanitizing their QLabel
    input:

    https://qt.gitorious.org/qt/qt/commit/31f7ecbdcdbafbac5bbfa693e4d060757244941b
