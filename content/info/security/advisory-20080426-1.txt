KDE Security Advisory: KHTML PNG Loader Buffer Overflow
Original Release Date: 2008-04-26
URL: http://www.kde.org/info/security/advisory-20080426-1.txt

0. References
        CVE-2008-1670
        http://bugs.kde.org/show_bug.cgi?id=156623


1. Systems affected:

	KHTML, as shipped with KDE 4.0 or newer. KDE 3.x is not affected.


2. Overview:

	The new progressive PNG Image loader in KHTML of KDE 4.0 and newer
	can be tricked into overrunning a heap allocated memory buffer
	by loading a specially encoded image.

3. Impact:

	A remote site can cause a denial of service and possibly execute
	arbitrary code in the context of the user.

4. Solution:

        Source code patches have been made available which fix these
        vulnerabilities. Contact your OS vendor / binary package provider
        for information about how to obtain updated binary packages.


5. Patch:

        A patch for KDE 4.0 and newer is available from
        ftp://ftp.kde.org/pub/kde/security_patches :

        f31a4bb0429149e27b4436f138eea471  post-kde-4.0.3-khtml.diff
