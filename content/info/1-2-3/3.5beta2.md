---
title: KDE 3.5 Beta 2 Info Page
---

<p>
KDE 3.5 Beta 2 was released on October 18th, 2005. Read the <a
href="/announcements/announce-3.5beta2">official announcement</a>.</p>

<h2><a name="bugs">Bugs</a></h2>

<p>This is a list of grave bugs and common pitfalls
surfacing after the release date:</p>

<ul>
<li>None known yet</li>
</ul>

<p>Please check the <a href="http://bugs.kde.org/">bug database</a>
before filing any bug reports. Also check for possible updates on this page
that might describe or fix your problem.</p>

<h2>FAQ</h2>

See the <a href="https://userbase.kde.org/Asking_Questions">KDE FAQ</a> for any specific
questions you may have.

<h2>Download and Installation</h2>

<p>
<u>Library Requirements</u>.
 <a href="/info/1-2-3/requirements/3.5">KDE 3.5 requires or benefits</a>
 from the given list of libraries, most of which should be already installed
 on your system or available from your OS CD or your vendor's website.
</p>
<p>
  The complete source code for KDE 3.5 Beta 2 is available for download:
</p>

{{< readfile "/content/info/1-2-3/source-3.5beta2.inc" >}}

<!-- Comment the following if Konstruct is not up-to-date -->
<p>The <a href="http://developer.kde.org/build/konstruct/">Konstruct</a> build toolset can help you
downloading and installing these tarballs.</p>

<u><a name=binary>Binary packages</a></u>

<p>
  Some Linux/UNIX OS vendors have kindly provided binary packages of
  KDE 3.5 Beta 2 for some versions of their distribution, and in other cases
  community volunteers have done so.
  Some of these binary packages are available for free download from KDE's
  <a href="http://download.kde.org/binarydownload.html?url=/unstable/3.5-beta2/">http</a> or
  <a href="/mirrors/ftp.php">FTP mirrors</a>.
</p>

<p>
  Currently pre-compiled packages are available for:
</p>

{{< readfile "/content/info/1-2-3/binary-3.5beta2.inc" >}}

<p>
Additional binary packages might become available in the coming weeks,
as well as updates to the current packages.
</p>

<h2>Developer Info</h2>

If you need help porting your application to KDE 3.x see the <a
href="http://websvn.kde.org/*checkout*/branches/KDE/3.5/kdelibs/KDE3PORTING.html">
porting guide</a> or subscribe to the
<a href="http://mail.kde.org/mailman/listinfo/kde-devel">KDE Devel Mailinglist</a>
to ask specific questions about porting your applications.

<p>There is also info on the <a
href="http://developer.kde.org/documentation/library/kdeqt/kde3arch/index.html">architecture</a>
and the <a href="http://developer.kde.org/documentation/library/3.5-api.php">
programming interface of KDE 3.5</a>.
</p>
