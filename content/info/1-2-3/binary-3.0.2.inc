<ul>
<!--   CONECTIVA LINUX -->
<li><a href="http://www.conectiva.com/">Conectiva Linux</a>:
<!--
  (<a href="http://download.kde.org/stable/3.0.2/Conectiva/8.0/README">README</a>
  /
  <a href="http://download.kde.org/stable/3.0.2/Conectiva/8.0/LEIAME">LEIAME</a>)
-->
  <ul type="disc">
    <li>
      8.0:
      <a href="http://download.kde.org/stable/3.0.2/Conectiva/conectiva/RPMS.kde/">Intel
      i386</a>
    </li>
  </ul>
  <p />
</li>

<!--   FREEBSD -->
<!--
  <a href="http://www.freebsd.org/">FreeBSD</a>:  
  <a href="http://download.kde.org/stable/3.0.2/FreeBSD/">4.5-STABLE</a>
  <p />
-->

<!--   MAC OS X (FINK PROJECT) -->
<li>
  <a href="http://www.apple.com/macosx/">Mac OS</a> /
  <a href="http://www.opendarwin.org/">OpenDarwin</a>
  (downloads via the <a href="http://fink.sourceforge.net/">Fink</a> project):
  <ul type="disc">
    <li>
      <a href="http://fink.sourceforge.net/news/kde.php">X</a>
    </li>
  </ul>
  <p />
</li>

<!--   MANDRAKE LINUX -->
<li>
  <a href="http://www.linux-mandrake.com/">Mandrake Linux</a>:
  <ul type="disc">
    <li>
      8.2:
      <a href="http://download.kde.org/stable/3.0.2/Mandrake/8.2/">Intel
      i586</a>
    </li>
    <li>
      8.1:
      <a href="http://download.kde.org/stable/3.0.2/Mandrake/8.1/">Intel
      i586</a>
    </li>
    <li>
      8.0:
      <a href="http://download.kde.org/stable/3.0.2/Mandrake/8.0/">Intel
      i586</a>
    </li>
  </ul>
  Please see also the
  <a href="http://download.kde.org/stable/3.0.2/Mandrake/noarch/">noarch</a>
  directory for i18n packages.
  <p />
</li>

<!--   REDHAT LINUX -->
<!--
<li>
  <a href="http://www.redhat.com/">Red Hat</a>
  (<a href="http://download.kde.org/stable/3.0.2/Red%20Hat/README">README</a>):
  <p />
  <a href="http://www.slackware.org/">Slackware</a>:
  <a href="http://download.kde.org/stable/3.0.2/Slackware/">8.0</a>
  <p />
</li>
-->

<!--   SUSE LINUX -->
<li>
  <a href="http://www.suse.com/">SuSE Linux</a>
  (<a href="http://download.kde.org/stable/3.0.2/SuSE/README">README</a>,
   <a href="http://download.kde.org/stable/3.0.2/SuSE/SORRY">SORRY</a>):
  <ul type="disc">
    <li>
      8.0:
      <a href="http://download.kde.org/stable/3.0.2/SuSE/i386/8.0/">Intel
      i386</a>
    </li>
    <li>
      7.3:
      <a href="http://download.kde.org/stable/3.0.2/SuSE/i386/7.3/">Intel
      i386</a> and
      <a href="http://download.kde.org/stable/3.0.2/SuSE/ppc/7.3/">IBM
      PowerPC</a>
    </li>
    <li>
      7.2:
      <a href="http://download.kde.org/stable/3.0.2/SuSE/i386/7.2/">Intel
      i386</a>
    </li>
    <li>
      7.1:
      <a href="http://download.kde.org/stable/3.0.2/SuSE/i386/7.1/">Intel
      i386</a>
    </li>
    <li>
      7.0:
      <a href="http://download.kde.org/stable/3.0.2/SuSE/i386/7.0/">Intel
      i386</a>
    </li>
  </ul>
  Please see also the
  <a href="http://download.kde.org/stable/3.0.2/SuSE/noarch/">noarch</a>
  directory for i18n packages.
  <p />
</li>

<li>
    <a href="http://www.turbolinux.com/">Turbolinux</a>
    (<a href="http://download.kde.org/stable/3.0.2/Turbo/README">README</a>):
    <ul>
      <li>
        <a href="http://download.kde.org/stable/3.0.2/Turbo/noarch/">Language
        packages</a> (all versions and architectures)</li>
      <li>
       7.0:
       <a href="http://download.kde.org/stable/3.0.2/Turbo/7/i586/">Intel i586</a>
      </li>
      <li>
       8.0: 
       <a href="http://download.kde.org/stable/3.0.2/Turbo/8/i586/">Intel i586</a>
      </li>
    </ul>
   </li>

<!--   TRU64 -->
<li>
  <a href="http://www.tru64unix.compaq.com/">Tru64</a>
  (<a href="http://download.kde.org/stable/3.0.2/Tru64/README.Tru64">README</a>):
  <ul type="disc">
    <li>
      4.0d, e, f and g and 5.x:
      <a href="http://download.kde.org/stable/3.0.2/Tru64/">Compaq Alpha</a>
    </li>
  </ul>
  <p />
</li>
<!--   YELLOWDOG LINUX -->
<!--
  <a href="http://www.yellowdoglinux.com/">YellowDog</a>:
  <a href="http://download.kde.org/stable/3.0.2/YellowDog">2.2</a>
-->
</ul>
