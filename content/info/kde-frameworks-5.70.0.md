---
version: "5.70.0"
title: "KDE Frameworks 5.70.0 Source Info and Download"
type: info/frameworks
date: 2020-05-02
bugs:
- Crash in plasma-framework (<a href="http://bugs.kde.org/421170">bug 421170</a>).
- KTextEditor global view setting changes ignored after session reopening (Kate, KDevelop) (<a href="https://bugs.kde.org/show_bug.cgi?id=421375">bug 421375</a>).
- 'KIO: Cannot copy/move files to a symlink to a directory (<a href="https://bugs.kde.org/show_bug.cgi?id=421213">bug 421213</a>).'
patches:
- 5.70.1
---
