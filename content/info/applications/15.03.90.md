---
title: "KDE Applications 15.03.90 Info Page"
announcement: /announcements/announce-applications-15.04-beta2
layout: applications
build_instructions: https://techbase.kde.org/Getting_Started/Build/Historic/KDE4
---
