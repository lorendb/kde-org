---
title: Suund mobiilsusele kahandab KDE platvormi kaalu
date: "2011-01-26"
hidden: true
---

<h2>KDE laskis välja arendusplatvormi 4.6.0</h2>
<p>
 KDE teatab uhkuse KDE platvormi 4.6 väljalaskest. Plasma töötsoonide ja KDE rakenduste alus on saanud olulisi parandusi paljudes valdkondades. Lisaks uue tehnoloogia pakkumisele on tublisti tööd tehtud platvormi põhialuste jõudluse parandamisel ja stabiilsuse tugevdamisel.
</p>
<h2>Vähenõudlik mobiilne profiil kahandab sõltuvusahelaid</h2>
<p>
KDE teekide jätkuva modulariseerimisega on nüüd võimalik kasutada mõningaid KDE platvormi osi mobiilsetes ja sardsüsteemides. Vähenenud teekidevahelised sõltuvused ja võimalus teatavaid omadusi keelata lubavad KDE raamistikke edaspidi hõlpsasti kasutada mobiilsetes seadmetes. Mobiilset profiili pruugivad juba mõned KDE rakenduste mobiiltelefonidele ja tahvelarvutitele mõeldud versioonid, näiteks <a href="http://userbase.kde.org/Kontact_Touch">Kontact Touch</a>, KDE kontoritöö pakett mobiilidele ning Plasma kasutajaliidesed <a href="http://www.notmart.org/index.php/Software/Plasma:_now_comes_in_tablets">tahvelarvutitele</a> ja pihuseadmetele.
</p>

<h2>Plasma jõu rakendamine QML-i teenistusse</h2>
<p>Uue väljalaskega toetab Plasma raamistik nüüd ka Qt Quicki deklaratiivses kasutajaliidese programmeerimiskeeles <b>QML</b> kirjutatud Plasma vidinaid. Olemasolevad vidinad töötavad endiselt edasi, kuid nüüdsest on just QML eelistatud uute vidinate loomise meetod. Plasma andmemootorid on saanud uusi omadusi, sealhulgas võime jagada faile JavaScripti plugina vahendusel ning salvestusteenuse, mis lubab andmemootoritel puhverdada andmeid, et neid saaks kasutada ka ajal, mil võrguühendus puudub. Loodud on uus Plasma KPart, mis muudab arendajatel lihtsamaks põimida nii mainitud uued kui ka mõistagi senised Plasma tehnoloogiad oma rakendustesse - juba praegu käib näiteks töö Plasma raamistiku tarvitamiseks Kontactis ja Skrooges.
</p>

<h2>UPoweri, UDevi ja UDisksi toetus, metaandmete varundamine</h2>
<p>
Solidi uute <b>UPoweri, UDevi ja UDisksi</b> taustaprogrammide tõttu ei ole enam Linuxis vajalik pruukida riistvara haldamiseks iganenud HAL-i. Rakendused ei vaja uute taustaprogrammide kasutamiseks mingeid muudatusi. Süsteemide jaoks, mis ei toeta UPowerit, on siiski endiselt saadaval ka HAL-i taustaprogramm.
</p>
<p>
KDE platvormi semantilise töölaua tehnoloogia <b>Nepomuk</b> toetab nüüd <a href="http://vhanda.in/blog/2010/11/nepomuk-backup/">varundamist ja sünkroonimist</a>, mis muudab metaandmete vahetamise seadmete vahel palju lihtsamaks. Kasutajad võivad oma semantilisi andmeid varundada graafilise liidese abil. Täiendavad sünkroonimisvõimalused on esialgu kasutatavad ainult käsurealt.
</p>

<div class="text-center">
	<a href="/announcements/4/4.6.0/46-p01.png">
	<img src="/announcements/4/4.6.0/thumbs/46-p01.png" class="img-fluid" alt="Semantilisi andmeid saab nüüd hõlpsasti graafiliselt varundada ja taastada">
	</a> <br/>
	<em>Semantilisi andmeid saab nüüd hõlpsasti graafiliselt varundada ja taastada</em>
</div>
<br/>

<h2>KWin muutus skriptitavaks, Oxygen-GTK parandab töökeskkondade vahelist lõimimist</h2>
<p>
KDE aknahalduril <b>Kwin</b> on nüüd <a href="http://rohanprabhu.com/?p=116">skriptiliides</a>. See annab kogenud kasutajatele ja distributsioonide valmistajatele märksa suuremaid võimalusi määrata akende käitumist Plasma töötsoonis - nii võib näiteks lasta aknal olla alati teiste peal, kui see pole maksimeeritud, ja kohelda seda tavalise aknana, mida saab ka teise aknaga katta, kui see on maksimeeritud. Kui aken taas väiksemaks muudetakse, omandab ta automaatselt taas teiste peal olemise omaduse. KWini meeskond töötab praegu agaralt OpenGL-ES toetuse kallal, mis loodetavasti saab valmis 4.7 ilmumisajaks 2011. aasta suvel, andes siis KWinile võimaluse töötada ka mobiilsetes seadmetes.
</p>
<p>
KDE platvormi välimuse eest hoolitsevat <b>Oxygeni</b> on samuti palju edendatud, sealhulgas on täielikult muudetud kõiki MIME tüüpide ikoone ning võetud kasutusele <a href="http://hugo-kde.blogspot.com/2010/11/oxygen-gtk.html">Oxygen-GTK teemamootor</a>, mis võimaldab GTK rakenduste paremat lõimimist KDE Plasma töötsoonidesse, pakkudes ka näiteks üleminekuid ja veel mitmeid võimalusi, mida kasutajad Oxygeni puhul enesestmõistetavaks peavad.
</p>

<div class="text-center">
	<a href="/announcements/4/4.6.0/46-p02.png">
	<img src="/announcements/4/4.6.0/thumbs/46-p02.png" class="img-fluid" alt="Oxygen-GTK teema lubab KDE ja GTK rakendustel teineteise moodi välja näha">
	</a> <br/>
	<em>Oxygen-GTK teema lubab KDE ja GTK rakendustel teineteise moodi välja näha</em>
</div>
<br/>

<h2>Uus Bluetoothi raamistik laseb seadmetel paremini läbi saada</h2>
<p>
Tuliuus Bluetoothi raamistik <b>BlueDevil</b> võimaldab seadmetel üksteist paremini ära tunda ja neid mugavamalt hallata. BlueDevil asendab senise KBluetoothi ja toetub BlueZile. See võimaldab muu hulgas järgmist:
</p>
<ul>
    <li>seadmete paaritamiseks saab kasutada nõustajat,</li>
    <li>Bluetoothi seadmete faile võib sirvida Dolphinis või Konqueroris,</li>
    <li>Bluetoothi seadistustele pääseb ligi nii KDE Süsteemi seadistustes kui ka otse süsteemisalvest,</li>
    <li>uus raamistik jälgib ka saabuvaid soove, näiteks failide vastuvõtmiseks või PIN-i sisestamiseks.</li>
</ul>

<h4>KDE arendusplatvormi paigaldamine</h4>
<p align="justify">
KDE tarkvara, sealhulgas kõik teegid ja rakendused, on vabalt saadaval vastavalt avatud lähtekoodiga tarkvara litsentsidele. KDE tarkvara töötab väga mitmesugusel riistvaral, operatsioonisüsteemides ning igasuguste aknahaldurite ja töökeskkondadega. Lisaks Linuxile ja teistele UNIX-il põhinevatele süsteemidele leiab enamiku KDE rakenduste Microsoft Windowsi versioonid leheküljelt <a href="http://windows.kde.org">KDE software on Windows</a> ja Apple Mac OS X versioonid leheküljelt <a href="http://mac.kde.org/">KDE software on Mac</a>. Veebist võib leida KDE rakenduste eksperimentaalseid versioone mitmele mobiilsele platvormile, näiteks MeeGo, MS Windows Mobile ja Symbian, kuid need on esialgu ametliku toetuseta.
<br />
KDE tarkvara saab hankida lähtekoodina või mitmesugustes binaarvormingutes aadressilt <a
href="http://download.kde.org/stable/4.6.0/">download.kde.org</a>, samuti
<a href="/download">CD-ROM-il</a>
või ka mis tahes tänapäevasest <a href="/distributions">
GNU/Linuxi ja UNIX-i süsteemist</a>.
</p>
<p align="justify">
  <a id="packages"><em>Paketid</em></a>.
  Mõned Linux/UNIX OS-i tootjad on lahkelt valmistanud 4.6.0 
binaarpaketid mõnele oma distributsiooni versioonile, mõnel juhul on sama teinud
kogukonna vabatahtlikud. <br />
  Mõned binaarpaketid on vabalt allalaaditavad KDE saidilt <a
href="http://download.kde.org/binarydownload.html?url=/stable/4.6.0/">download.kde.org</a>.
  Lähinädalatel võib lisanduda teisigi binaarpakette, samuti praegu
saadaolevate pakettide uuendusi.
<a id="package_locations"><em>Pakettide asukohad</em></a>.
Praegu saadaolevate binaarpakettide nimekirja, millest KDE väljalaskemeeskond on teadlik,
näeb vastaval <a href="/info/4/4.6.0">4.6infoleheküljel</a>.
</p>
<p align="justify">
  <a id="source_code"></a>
  Täieliku 4.6.0 lähtekoodi võib vabalt alla laadida <a
href="http://download.kde.org/stable/4.6.0/src/">siit</a>.
Juhiseid KDE tarkvarakomplekti 4.6.0 kompileerimiseks ja paigaldamiseks
  leiab samuti <a href="/info/4/4.6.0#binary">4.6.0infoleheküljelt</a>.
</p>

<h4>
Nõuded süsteemile
</h4>
<p align="justify">
Võimaldaks ära kasutada väljalasete täit võimsust, soovitame tungivalt pruukida Qt uusimat versiooni, milleks praegu on 4.7.2. See on hädavajalik stabiilsuse kindlustamiseks, sest mõnedki KDE tarkvara parandused toetuvad tegelikult aluseks olevale Qt raamistikule.<br />
Graafikadraiverid võivad teatud olukorras komposiitvõimaluste kasutamisel eelistada OpenGL-ile hoopis XRenderit. Kui täheldate tõsiseid graafikaprobleeme, võib usutavasti aidata töölauaefektide väljalülitamine, sõltuvalt küll konkreetsest graafikadraiverist ja selle seadistustest. KDE tarkvara kõigi võimaluste täielikuks ärakasutamiseks soovitame kasutada ka uusimaid süsteemile mõeldud graafikadraivereid, mis võivad tunduvalt parandada süsteemi kasutamist nii funktsioonide mõttes kui ka eriti üldise jõudluse mõttes. 
</p>

<h4>
Autorid
</h4>
<p>
Käesoleva väljalasketeate koostasid Vivek Prakash, Stefan Majewsky, Guillaume De Bure, Nikhil Marathe, Markus Slopianka, Stuart Jarvis, Jos Poortvliet, Nuno Pinheiro, Carl Symons, Marco Martin, Sebastian Kügler, Nick P ja veel paljud KDE propageerimise meeskonna liikmed. Eesti keelde tõlkis väljalasketeate Marek Laane.
</p>




<h2>Täna ilmusid veel:</h2>
<h3>
<a href="../plasma">
Plasma töötsoonid annavad juhtohjad kasutaja kätte
</a>
</h3>

<p>
<a href="../plasma">
<img src="/announcements/4/4.6.0/images/plasma.png" class="app-icon float-left m-3" alt="KDE Plasma töötsoonid 4.6.0" />
</a>

<b>KDE Plasma töötsoonidele</b> lisandus uus tegevuste süsteem, mille abil on palju hõlpsam seostada rakendusi konkreetse tegevusega, näiteks tööülesannete või koduste tegemistega. Uuendatud toitehaldus pakub uusi võimalusi, aga ka senisest lihtsamat seadistamist. Plasma töötsooni aknahaldur KWin sai skriptide kasutamise võimaluse ning töötsoonid tervikuna mitmeid visuaalseid parandusi. Mobiilsetele seadmetele kohandatud <b>Plasma Netbook</b> on senisest tunduvalt kiirem ning seda on lihtsam kasutada ka puuteekraaniga seadmete puhul. Täpsemalt kõneleb kõigest <a href="../plasma">KDE Plasma töötsoonide 4.6 teadaanne</a>.

</p>

<h3>
<a href="../applications">
KDE Dolphinile lisandus täpsustav otsing
</a>
</h3>

<p>

<a href="../applications">
<img src="/announcements/4/4.6.0/images/applications.png" class="app-icon float-left m-3" alt="KDE rakendused 4.6.0"/>
</a>
</a>
Paljude <b>KDE rakenduste</b> meeskonnad lasksid samuti välja uued versioonid. Eriti tasub tähele panna KDE virtuaalse gloobuse Marble tunduvalt parandatud teekondade väljaarvutamise ja näitamise võimalusi ning KDE failihalduri Dolphin täiustatud filtreerimist ja otsimist metaandmete abil, niinimetatud fassettsirvimist. KDE mängud said hulganisti parandusi ning pildinäitaja Gwenview ja ekraanipiltide tegemise rakendus KSnapshot võimaluse saata pilte aega viitmata mitmesse levinud sotsiaalvõrgustikku. Täpsemalt kõneleb kõigest <a href="../applications">KDE rakenduste 4.6 teadaanne</a>.<br /><br />
</p>
