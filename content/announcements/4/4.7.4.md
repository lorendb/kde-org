---
aliases:
- ../announce-4.7.4
date: '2011-12-07'
description: KDE Ships 4.7.4 Workspaces, Applications and Platform.
title: KDE's December Updates Conclude 4.7 Series
---

<p align="justify">
Today KDE released updates for its Workspaces, Applications, and Development Platform.
These updates are the fourth in a series of monthly stabilization updates to the 4.7 series. 4.7.4 updates bring many bugfixes and translation updates on top of the latest edition in the 4.7 series and are recommended updates for everyone running 4.7.0 or earlier versions. As the release only contains bugfixes and translation updates, it will be a safe and pleasant update for everyone. KDE’s software is already translated into more than 55 languages, with more to come.
The December updates contain many performance improvements and bugfixes for applications using the Nepomuk semantic framework.
<br /><br />
To  download source code or packages to install go to the <a href="/info/4/4.7.4">4.7.4 Info Page</a>. The <a href="/announcements/changelogs/changelog4_7_3to4_7_4">changelog</a> and <a href="https://bugs.kde.org/buglist.cgi?query_format=advanced&short_desc_type=allwordssubstr&short_desc=&long_desc_type=substring&long_desc=&bug_file_loc_type=allwordssubstr&bug_file_loc=&keywords_type=allwords&keywords=&bug_status=RESOLVED&bug_status=VERIFIED&bug_status=CLOSED&emailtype1=substring&email1=&emailassigned_to2=1&emailreporter2=1&emailcc2=1&emailtype2=substring&email2=&bugidtype=include&bug_id=&votes=&chfieldfrom=2011-06-01&chfieldto=Now&chfield=cf_versionfixedin&chfieldvalue=4.7.4&cmdtype=doit&order=Bug+Number&field0-0-0=noop&type0-0-0=noop&value0-0-0=">Bugzilla</a> list more, but not all improvements since 4.7.3.
Note that these changelogs are incomplete. For a complete list of changes that went into 4.7.4, you can browse the Subversion and Git logs. 4.7.4 also ships a more complete set of translations for many of the 55+ supported languages.
To find out more about the KDE Workspace and Applications 4.7, please refer to the <a href="../4.7.0">4.7 release notes</a> and its earlier versions.
</p>

<div class="text-center">
	<a href="/announcements/4/4.7.0/dolphin-gwenview.png">
	<img src="/announcements/4/4.7.0/thumbs/dolphin-gwenview.png" class="img-fluid" alt="Plasma Desktop with Dolphin and Gwenview">
	</a> <br/>
	<em>Plasma Desktop with Dolphin and Gwenview</em>
</div>
<br/>
<p align="justify">
The KDE Software Compilation, including all its libraries and its applications, is available for free
under Open Source licenses. KDE's software can be obtained in source and various binary
formats from <a
href="http://download.kde.org/stable/4.7.4/">download.kde.org</a>
or with any of the <a href="/distributions">major
GNU/Linux and UNIX systems</a> shipping today.
</p>

<h4>
  Installing 4.7.4 Binary Packages
</h4>
<p align="justify">
  <em>Packages</em>.
  Some Linux/UNIX OS vendors have kindly provided binary packages of 4.7.4
for some versions of their distribution, and in other cases community volunteers
have done so.
  Additional binary packages, as well as updates to the packages now available,
may become available over the coming weeks.
</p>

<p align="justify">
  <a id="package_locations"></a><em>Package Locations</em>.
  For a current list of available binary packages of which the KDE Project has
been informed, please visit the <a href="/info/4/4.7.4#binary">4.7.4 Info
Page</a>.
</p>

<h4>
  Compiling 4.7.4
</h4>
<p align="justify">
  <a id="source_code"></a>
  The complete source code for 4.7.4 may be <a
href="http://download.kde.org/stable/4.7.4/src/">freely downloaded</a>.
Instructions on compiling and installing 4.7.4
  are available from the <a href="/info/4/4.7.4">4.7.4 Info
Page</a>.
</p>

<h4>
  Supporting KDE
</h4>

<p align="justify">
 KDE is a <a href="http://www.gnu.org/philosophy/free-sw.html">Free Software</a>
community that exists and grows only because of the help of many volunteers that
donate their time and effort. KDE is always looking for new volunteers and
contributions, whether it is help with coding, bug fixing or reporting, writing
documentation, translations, promotion, money, etc. All contributions are
gratefully appreciated and eagerly accepted. Please read through the <a
href="/community/donations/">Supporting KDE page</a> for further information or 
become a KDE e.V. supporting member through our new 
<a href="http://jointhegame.kde.org/">Join the Game</a> initiative. </p>


