------------------------------------------------------------------------
r1097026 | tokoe | 2010-02-28 22:04:33 +1300 (Sun, 28 Feb 2010) | 2 lines

Backport bugfix #228372

------------------------------------------------------------------------
r1097155 | ppenz | 2010-03-01 04:02:30 +1300 (Mon, 01 Mar 2010) | 1 line

Backport of SVN commit 1097154: Fix possible memory leak when invoking KLoadMetaDataThread::cancelAndDelete().
------------------------------------------------------------------------
r1097198 | anschneider | 2010-03-01 06:14:11 +1300 (Mon, 01 Mar 2010) | 6 lines

kio_sftp: Don't set the username in the kio_sftp module.

libssh should set it to be able to support ~/.ssh/config options.

BUG: 228440

------------------------------------------------------------------------
r1097312 | ppenz | 2010-03-01 11:03:13 +1300 (Mon, 01 Mar 2010) | 7 lines

Backport of SVN commit 1097289: Only access the data of the thread that has been created most recently. Data of older threads will just get ignored.

The fix will be part of KDE SC 4.4.2 (4.4.1 has been tagged already).

BUG: 224848
BUG: 226706
BUG: 222324
------------------------------------------------------------------------
r1097313 | hindenburg | 2010-03-01 11:04:42 +1300 (Mon, 01 Mar 2010) | 4 lines

Revert a fix for 199161 which is causing an ASSERT to crash.

CCBUG: 228443

------------------------------------------------------------------------
r1097380 | hindenburg | 2010-03-01 17:26:43 +1300 (Mon, 01 Mar 2010) | 4 lines

Correct where new Hotspots are calculated upon resize.

CCBUG: 228443

------------------------------------------------------------------------
r1097413 | scripty | 2010-03-01 23:31:14 +1300 (Mon, 01 Mar 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1097465 | gkiagia | 2010-03-02 02:10:15 +1300 (Tue, 02 Mar 2010) | 2 lines

Fix compilation on systems without strsignal()

------------------------------------------------------------------------
r1097599 | aseigo | 2010-03-02 07:40:23 +1300 (Tue, 02 Mar 2010) | 2 lines

need to return a QIcon otherwise the poor little binding's head gets confused

------------------------------------------------------------------------
r1097686 | abinader | 2010-03-02 10:37:58 +1300 (Tue, 02 Mar 2010) | 2 lines

Added support for passing QEasingCurve::Type enumerator literal string as valid argument on type() js binding

------------------------------------------------------------------------
r1097696 | scripty | 2010-03-02 11:13:17 +1300 (Tue, 02 Mar 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1097876 | mart | 2010-03-02 22:51:58 +1300 (Tue, 02 Mar 2010) | 4 lines

backport userClosed forwarding on notification deletion

BUG:228743

------------------------------------------------------------------------
r1097888 | mart | 2010-03-02 23:42:44 +1300 (Tue, 02 Mar 2010) | 2 lines

backport tooltip hiding after drag start, fixes the probably mayor complaint with the explorer

------------------------------------------------------------------------
r1097917 | mart | 2010-03-03 01:23:12 +1300 (Wed, 03 Mar 2010) | 2 lines

backport focus issues fix

------------------------------------------------------------------------
r1098020 | mart | 2010-03-03 06:24:10 +1300 (Wed, 03 Mar 2010) | 3 lines

final solution, that is way uglier than the solution in 4.5, due to the old architecture
CCBUG:228743

------------------------------------------------------------------------
r1098188 | hindenburg | 2010-03-03 16:20:55 +1300 (Wed, 03 Mar 2010) | 1 line

update version
------------------------------------------------------------------------
r1098192 | aseigo | 2010-03-03 17:21:16 +1300 (Wed, 03 Mar 2010) | 3 lines

looks like something i started and then didn't finish; this actually works, however.
CCBUG:229204

------------------------------------------------------------------------
r1098275 | dfaure | 2010-03-04 01:07:11 +1300 (Thu, 04 Mar 2010) | 3 lines

Port web_module away from QT3_SUPPORT.
Move QT3_SUPPORT define (oops, had no idea it was still there) into the only dir that still needs it.

------------------------------------------------------------------------
r1098402 | lueck | 2010-03-04 05:58:23 +1300 (Thu, 04 Mar 2010) | 1 line

doc backport from trunk
------------------------------------------------------------------------
r1098511 | aseigo | 2010-03-04 09:30:11 +1300 (Thu, 04 Mar 2010) | 2 lines

backport fixes to applet list widget

------------------------------------------------------------------------
r1098537 | lbeltrame | 2010-03-04 10:22:45 +1300 (Thu, 04 Mar 2010) | 5 lines

Backport:

 Apply the applet script fix to runners, i.e. use __dict__ to call up main scripts that have different names than "main"


------------------------------------------------------------------------
r1098600 | pino | 2010-03-04 12:53:43 +1300 (Thu, 04 Mar 2010) | 4 lines

Add support for the Renesas SH(sh4) CPU.

Patch kindly provided by Nobuhiro Iwamatsu.

------------------------------------------------------------------------
r1098666 | scripty | 2010-03-04 15:16:51 +1300 (Thu, 04 Mar 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1098985 | darioandres | 2010-03-05 10:22:04 +1300 (Fri, 05 Mar 2010) | 5 lines

Backport svn rev. 1098983
Dear stupid me: don't commit stupid changes without testing them.... Thanks
- Fix KFind not being able to search in file contents..
BUG: 229348

------------------------------------------------------------------------
r1099350 | lueck | 2010-03-06 03:12:04 +1300 (Sat, 06 Mar 2010) | 1 line

rename to plasma-desktop to get the documentation in the right place
------------------------------------------------------------------------
r1099501 | reed | 2010-03-06 06:47:02 +1300 (Sat, 06 Mar 2010) | 1 line

this should only be pulled in for Qt/X11
------------------------------------------------------------------------
r1099503 | reed | 2010-03-06 06:48:40 +1300 (Sat, 06 Mar 2010) | 1 line

hide knotify from the OSX dock
------------------------------------------------------------------------
r1099507 | reed | 2010-03-06 06:51:33 +1300 (Sat, 06 Mar 2010) | 1 line

missing include directories
------------------------------------------------------------------------
r1099510 | reed | 2010-03-06 06:54:58 +1300 (Sat, 06 Mar 2010) | 6 lines

test for sys/proc_info.h and sys/proc.h

proc_info.h was introduced in OSX 10.5
10.4 (and perhaps earlier, although untested)
contained the necessary defines in sys/proc.h.

------------------------------------------------------------------------
r1099513 | reed | 2010-03-06 07:04:22 +1300 (Sat, 06 Mar 2010) | 1 line

allow / in DISPLAY (OSX X11)
------------------------------------------------------------------------
r1099515 | reed | 2010-03-06 07:06:38 +1300 (Sat, 06 Mar 2010) | 1 line

these apply to Q_WS_MAC, not Q_OS_MAC (Q_WS_X11 on OSX uses the same ordering as on other platforms)
------------------------------------------------------------------------
r1099528 | jmhoffmann | 2010-03-06 07:26:14 +1300 (Sat, 06 Mar 2010) | 8 lines

fix geolocation dataengine build against gpsd 2.92
Even if API version is bumped, it does not mean that the code will cease to
build or work.
Patch contributed by Modestas Vainius <modax@debian.org>
Backport of r1099523 from trunk
CCMAIL: modax@debian.org
CCMAIL: damu@iki.fi

------------------------------------------------------------------------
r1099530 | reed | 2010-03-06 07:27:23 +1300 (Sat, 06 Mar 2010) | 1 line

add some missing include_directories
------------------------------------------------------------------------
r1099731 | scripty | 2010-03-06 15:24:42 +1300 (Sat, 06 Mar 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1100193 | scripty | 2010-03-07 15:23:43 +1300 (Sun, 07 Mar 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1100595 | hpereiradacosta | 2010-03-08 13:33:12 +1300 (Mon, 08 Mar 2010) | 4 lines

Backport r1100594
Removed use of kglobalsettings to decide whether animations should be enabled or not.


------------------------------------------------------------------------
r1100596 | hpereiradacosta | 2010-03-08 13:44:25 +1300 (Mon, 08 Mar 2010) | 6 lines

Backport r1088959
Added checkbox to enable/disable decoration animations
CCMAIL: kde-i18n-doc@kde.org
CCBUG: 226634 
CCBUG: 226668

------------------------------------------------------------------------
r1100945 | scripty | 2010-03-09 15:21:36 +1300 (Tue, 09 Mar 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1100969 | aseigo | 2010-03-09 17:34:50 +1300 (Tue, 09 Mar 2010) | 5 lines

revert http://websvn.kde.org/?view=revision&revision=1066297
not sure what this was supposed to fix (the commit log message was too vague to tell) but the commit was obviously wrong as it calls resize() outside of the constructor.
CCMAIL:notmart@gmail.com
CCBUG:227777

------------------------------------------------------------------------
r1100972 | aseigo | 2010-03-09 17:55:19 +1300 (Tue, 09 Mar 2010) | 4 lines

allow up to 5s between the removal and creation of the file backing the icon

CCBUG:229970

------------------------------------------------------------------------
r1101129 | trueg | 2010-03-10 00:39:56 +1300 (Wed, 10 Mar 2010) | 1 line

Backport:use correct string replace indices
------------------------------------------------------------------------
r1101217 | bettio | 2010-03-10 05:41:45 +1300 (Wed, 10 Mar 2010) | 2 lines

Backport: missing showdesktop line in monitorLoadAction. 

------------------------------------------------------------------------
r1101269 | aseigo | 2010-03-10 09:03:05 +1300 (Wed, 10 Mar 2010) | 3 lines

support for Svg* marshalling, used with e.g. Plasma::SvgWidget
BUG:229668

------------------------------------------------------------------------
r1101335 | aseigo | 2010-03-10 11:30:51 +1300 (Wed, 10 Mar 2010) | 2 lines

IntervalAlignment

------------------------------------------------------------------------
r1101466 | scripty | 2010-03-10 15:49:47 +1300 (Wed, 10 Mar 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1101677 | trueg | 2010-03-11 03:55:22 +1300 (Thu, 11 Mar 2010) | 4 lines

Backport: Fixed filter creation for removing unwanted entries. This fixes the bug of files being re-indexed all the time in case one has configured more than one top-level index folder.

CCMAIL: nlecureuil@mandriva.com

------------------------------------------------------------------------
r1101731 | trueg | 2010-03-11 08:06:09 +1300 (Thu, 11 Mar 2010) | 2 lines

Backport: Properly ignore empty entries in the include and exclude folders when creating the query filter to remove unwanted entries.
CCMAIL: nlecureuil@mandriva.com
------------------------------------------------------------------------
r1101760 | hpereiradacosta | 2010-03-11 10:02:57 +1300 (Thu, 11 Mar 2010) | 3 lines

backport r1101759
Removed customized default buttons layout

------------------------------------------------------------------------
r1101831 | scripty | 2010-03-11 15:23:53 +1300 (Thu, 11 Mar 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1101849 | mleupold | 2010-03-11 18:40:20 +1300 (Thu, 11 Mar 2010) | 3 lines

Backport of r1101847:
Fix pamOpen hash size: it's 20, 40 or 56 depending on the password length.

------------------------------------------------------------------------
r1101856 | lueck | 2010-03-11 19:36:52 +1300 (Thu, 11 Mar 2010) | 1 line

documentation was renamed to plasma-desktop, backport from trunk
------------------------------------------------------------------------
r1101925 | johnflux | 2010-03-12 00:46:01 +1300 (Fri, 12 Mar 2010) | 1 line

Fix problem with updating the process list if the interval time was sent while the list was hidden
------------------------------------------------------------------------
r1102004 | hpereiradacosta | 2010-03-12 04:26:29 +1300 (Fri, 12 Mar 2010) | 5 lines

Backport: r1102002
Revert r1101759, after discussion with Lubos and Nuno. Will work on having stronger glow for close 
button, and possibly reduce button hit area as an alternative


------------------------------------------------------------------------
r1102044 | trueg | 2010-03-12 06:15:08 +1300 (Fri, 12 Mar 2010) | 4 lines

Backport: Take the exclude filters and the hidden folders config into account.

CCMAIL: nlecureuil@mandriva.com

------------------------------------------------------------------------
r1102112 | camuffo | 2010-03-12 10:07:36 +1300 (Fri, 12 Mar 2010) | 3 lines

backport of r1102110
the labels must be transparent to mouse click events

------------------------------------------------------------------------
r1102188 | scripty | 2010-03-12 15:34:35 +1300 (Fri, 12 Mar 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1102554 | chani | 2010-03-13 10:06:22 +1300 (Sat, 13 Mar 2010) | 3 lines

backport the panel code cleanup that *appears* to have fixed the windows-can-cover bug.
BUG: 223786

------------------------------------------------------------------------
r1102558 | sengels | 2010-03-13 10:13:18 +1300 (Sat, 13 Mar 2010) | 1 line

fix build on non-x11 as has been done in r1088793
------------------------------------------------------------------------
r1102694 | ossi | 2010-03-13 23:06:37 +1300 (Sat, 13 Mar 2010) | 2 lines

backport: make UnInhibit not enable a disabled screensaver

------------------------------------------------------------------------
r1102701 | ossi | 2010-03-13 23:40:17 +1300 (Sat, 13 Mar 2010) | 3 lines

backport: no point in calling utmpname() with the default path


------------------------------------------------------------------------
r1102704 | ossi | 2010-03-13 23:47:04 +1300 (Sat, 13 Mar 2010) | 10 lines

backport: Xservers is long dead, and so is sessreg

kdm does not use an Xservers config file any more, so don't mention it
in the README.

the lack of the file would also make BSD-style sessreg invocations fail.
kdm has built-in sessreg anyway, so don't write config examples using
the external one.


------------------------------------------------------------------------
r1102705 | ossi | 2010-03-13 23:48:02 +1300 (Sat, 13 Mar 2010) | 5 lines

backport: don't include utmp.h when using utmpx

something will probably blow up, but wth ... :)


------------------------------------------------------------------------
r1102706 | ossi | 2010-03-13 23:48:33 +1300 (Sat, 13 Mar 2010) | 4 lines

backport: utmpx is guaranteed to have ut_user, so don't alias it to
ut_name


------------------------------------------------------------------------
r1102707 | ossi | 2010-03-13 23:49:08 +1300 (Sat, 13 Mar 2010) | 3 lines

backport: freebsd 9 doesn't need lastlog


------------------------------------------------------------------------
r1102711 | ossi | 2010-03-13 23:53:08 +1300 (Sat, 13 Mar 2010) | 7 lines

backport: make ConsoleKit failure non-fatal and reset disposed pointer

libck-connector sucks at error reporting, so we cannot tell whether no
ck daemon is running (which would be kinda fine) or whether talking with
it failed (which would be a bit worse). so choose the less disruptive
whole-sale solution.

------------------------------------------------------------------------
r1102715 | ossi | 2010-03-13 23:55:38 +1300 (Sat, 13 Mar 2010) | 3 lines

backport: support both grub-set-default and grub-once patch, after all


------------------------------------------------------------------------
r1102716 | ossi | 2010-03-13 23:56:33 +1300 (Sat, 13 Mar 2010) | 5 lines

backport: better i/o error reporting

use %m more


------------------------------------------------------------------------
r1102717 | ossi | 2010-03-13 23:57:05 +1300 (Sat, 13 Mar 2010) | 3 lines

backport: beautify error messages :)


------------------------------------------------------------------------
r1102718 | ossi | 2010-03-13 23:58:07 +1300 (Sat, 13 Mar 2010) | 6 lines

backport: even more random temp file names

this makes temp file names yet more resilient against local DoS - not
that it would be a particularly useful attack vector ...


------------------------------------------------------------------------
r1102719 | ossi | 2010-03-13 23:58:50 +1300 (Sat, 13 Mar 2010) | 3 lines

backport: too much fprintf_()


------------------------------------------------------------------------
r1102720 | ossi | 2010-03-13 23:59:15 +1300 (Sat, 13 Mar 2010) | 3 lines

backport: remove spurious ATTR_UNUSED


------------------------------------------------------------------------
r1102721 | ossi | 2010-03-13 23:59:39 +1300 (Sat, 13 Mar 2010) | 4 lines

backport: fix off-by one causing the lowest priority updaters not being
called


------------------------------------------------------------------------
r1102724 | ossi | 2010-03-14 00:04:26 +1300 (Sun, 14 Mar 2010) | 6 lines

backport: make a note of inaccessible DataDir in the log

make no attempt at fixing it or being more visible. such a low-level
installation failure can reasonably require low-level debugging ...


------------------------------------------------------------------------
r1102799 | mart | 2010-03-14 06:07:52 +1300 (Sun, 14 Mar 2010) | 2 lines

backport the signal emission after  notification expire with the proper reason

------------------------------------------------------------------------
r1102939 | scripty | 2010-03-14 15:33:34 +1300 (Sun, 14 Mar 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1103089 | ossi | 2010-03-14 23:10:03 +1300 (Sun, 14 Mar 2010) | 7 lines

don't kill locker when krunner suddenly exits

do this by making the locker kprocess parent-less. this means it will
just leak at exit.

BUG: 185714

------------------------------------------------------------------------
r1103104 | graesslin | 2010-03-14 23:58:13 +1300 (Sun, 14 Mar 2010) | 5 lines

Backport rev: 1103103
Do not paint windows as if it is being moved when only clicking on it in desktop grid.
This prevents that the window flickers on clicking them.
CCBUG: 229741

------------------------------------------------------------------------
r1103209 | ppenz | 2010-03-15 05:22:24 +1300 (Mon, 15 Mar 2010) | 5 lines

Backport of SVN commit 1103207: Fix issue that the current terminal line does not get cleared before sending a cd command.

The terminal interface does not provide an API for this, so as workaround backspaces are send. Not nice, but as the issue can lead to data loss, this solution is better than having nothing.

CCBUG: 161637
------------------------------------------------------------------------
r1103210 | graesslin | 2010-03-15 05:26:09 +1300 (Mon, 15 Mar 2010) | 5 lines

Backport rev 1103206:
Rearrange windows when the gemoetry changes in both pw and dg in pw mode.
CCBUG: 228829


------------------------------------------------------------------------
r1103404 | ereslibre | 2010-03-15 13:23:13 +1300 (Mon, 15 Mar 2010) | 2 lines

Backport commit 1103377. Fix X11 pixmap leaking.

------------------------------------------------------------------------
r1103493 | graesslin | 2010-03-15 21:41:24 +1300 (Mon, 15 Mar 2010) | 5 lines

Backport rev 1103492:
Correctly handle mouse clicks in TabBox when there is an additional view.
CCBUG: 226877


------------------------------------------------------------------------
r1103521 | trueg | 2010-03-15 22:57:17 +1300 (Mon, 15 Mar 2010) | 2 lines

Backport: Stat URLs before opening them to make sure they are actually dirs we can list. Otherwise use KRun.

------------------------------------------------------------------------
r1103577 | darioandres | 2010-03-16 01:26:46 +1300 (Tue, 16 Mar 2010) | 7 lines

Backport to 4.4:
SVN commit 1103576 by darioandres:

- Fix StatusWidget height. Duplicates list will use all the available height now

CCBUG: 230025

------------------------------------------------------------------------
r1103856 | scripty | 2010-03-16 15:43:09 +1300 (Tue, 16 Mar 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1103921 | lunakl | 2010-03-16 22:41:18 +1300 (Tue, 16 Mar 2010) | 4 lines

backport r1103920
exit code 0 when the user rejected the kdesu dialog is not that good idea


------------------------------------------------------------------------
r1104130 | dfaure | 2010-03-17 09:07:21 +1300 (Wed, 17 Mar 2010) | 2 lines

Backport fix for 213876, crash when closing okularpart tab

------------------------------------------------------------------------
r1104234 | scripty | 2010-03-17 16:06:17 +1300 (Wed, 17 Mar 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1104235 | mmrozowski | 2010-03-17 16:24:33 +1300 (Wed, 17 Mar 2010) | 9 lines

(More) gracefully handle building runtime/phonon without alsa:
- move version checks to phonon toplevel directory (there were two copies, one commented out)
- guard linking with ALSA_FOUND
- move include_directories to phonon toplevel dir (used in multiple places)
- make building tests optional (they require OpenGL)

Still there's chicken-egg problem caused by FindAlsa.cmake, (macro_optional_find_package cannot be used because of alsa_configure_file invocation)

CCMAIL: kde-buildsystem@kde.org
------------------------------------------------------------------------
r1104461 | anschneider | 2010-03-18 08:04:31 +1300 (Thu, 18 Mar 2010) | 2 lines

kio_sftp: Set type for symlink correctly.

------------------------------------------------------------------------
r1104487 | anschneider | 2010-03-18 08:15:16 +1300 (Thu, 18 Mar 2010) | 2 lines

kio_sftp: Probably fix bug #230621.

------------------------------------------------------------------------
r1104490 | anschneider | 2010-03-18 08:18:00 +1300 (Thu, 18 Mar 2010) | 2 lines

kio_sftp: Fixed a typo.

------------------------------------------------------------------------
r1104540 | scripty | 2010-03-18 15:41:01 +1300 (Thu, 18 Mar 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1104736 | trueg | 2010-03-19 00:55:26 +1300 (Fri, 19 Mar 2010) | 5 lines

Backported all fixes and improvements to the strigi file index service.
The only thing that has not been backported is the inotify integration.

CCMAIL: nlecureuil@mandriva.com

------------------------------------------------------------------------
r1104743 | mart | 2010-03-19 01:37:23 +1300 (Fri, 19 Mar 2010) | 3 lines

revert r1098020, was quite a wrong solutio.
the truth is that with the 4.4 architecture is almost impossible to nitice a change of a notification

------------------------------------------------------------------------
r1104799 | dfaure | 2010-03-19 05:43:52 +1300 (Fri, 19 Mar 2010) | 2 lines

Backport: point to qt.doc.nokia.com

------------------------------------------------------------------------
r1104851 | mart | 2010-03-19 07:36:52 +1300 (Fri, 19 Mar 2010) | 2 lines

backport panel visibility fix

------------------------------------------------------------------------
r1104968 | scripty | 2010-03-19 15:59:53 +1300 (Fri, 19 Mar 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1105290 | abizjak | 2010-03-20 08:20:45 +1300 (Sat, 20 Mar 2010) | 5 lines

backport revision 1105289
Before adding an item to a requested position. check if it would meet the visibility criterion. If not, 
use the positioning algorithm instead. 
BUG: 229616

------------------------------------------------------------------------
r1105379 | whiting | 2010-03-20 13:19:52 +1300 (Sat, 20 Mar 2010) | 1 line

backport fix of temperature value when using Imperial measuring system to 4.4 branch
------------------------------------------------------------------------
r1105399 | scripty | 2010-03-20 15:31:09 +1300 (Sat, 20 Mar 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1105476 | graesslin | 2010-03-21 00:08:30 +1300 (Sun, 21 Mar 2010) | 5 lines

Backport rev 1105473: Prevent modal dialogs to be shown twice in alt+tab list.
CCBUG: 230807
CCBUG: 228186


------------------------------------------------------------------------
r1105572 | scripty | 2010-03-21 05:53:38 +1300 (Sun, 21 Mar 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1105594 | graesslin | 2010-03-21 06:54:54 +1300 (Sun, 21 Mar 2010) | 4 lines

Backport rev 1105593: Don't hover sticky windows as that cannot work.
CCBUG: 203086


------------------------------------------------------------------------
r1105633 | scripty | 2010-03-21 09:14:09 +1300 (Sun, 21 Mar 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1105657 | darioandres | 2010-03-21 10:07:46 +1300 (Sun, 21 Mar 2010) | 8 lines

Backport to 4.4:
SVN commit 1105648 by darioandres:

Implement http://reviewboard.kde.org/r/3322
- Improve Style KCM loading time by delaying the Desktop (Plasma) Themes load

CCBUG: 222745

------------------------------------------------------------------------
r1105823 | scripty | 2010-03-22 02:22:03 +1300 (Mon, 22 Mar 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1105870 | jacopods | 2010-03-22 03:40:40 +1300 (Mon, 22 Mar 2010) | 3 lines

Backport fix to correct action handling.
BUG:229321

------------------------------------------------------------------------
r1105871 | jacopods | 2010-03-22 03:43:29 +1300 (Mon, 22 Mar 2010) | 2 lines

Handle actions correctly

------------------------------------------------------------------------
r1106167 | scripty | 2010-03-22 15:47:30 +1300 (Mon, 22 Mar 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1106213 | mlaurent | 2010-03-22 21:35:24 +1300 (Mon, 22 Mar 2010) | 3 lines

Backport:
Add missing i18n

------------------------------------------------------------------------
r1106246 | trueg | 2010-03-23 00:04:38 +1300 (Tue, 23 Mar 2010) | 1 line

Backport: do not crash if the system setup is broken and Nepomuk cannot find the Virtuoso Soprano backend.
------------------------------------------------------------------------
r1106512 | scripty | 2010-03-23 15:42:08 +1300 (Tue, 23 Mar 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1106612 | kossebau | 2010-03-24 01:54:41 +1300 (Wed, 24 Mar 2010) | 2 lines

backport of 1106610: fixed: also use estimated type postfix for mimetype (default "unknown"), not just the name searched

------------------------------------------------------------------------
r1106617 | kossebau | 2010-03-24 01:56:58 +1300 (Wed, 24 Mar 2010) | 2 lines

backport of 1106615: fixed: inode/vnd.kde.service.ftps for FTPS, not *.sftp

------------------------------------------------------------------------
r1106789 | aseigo | 2010-03-24 11:50:44 +1300 (Wed, 24 Mar 2010) | 2 lines

isEmpty() is what we really want

------------------------------------------------------------------------
r1106885 | scripty | 2010-03-24 15:28:35 +1300 (Wed, 24 Mar 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1107069 | trueg | 2010-03-25 07:39:34 +1300 (Thu, 25 Mar 2010) | 2 lines

Backport: cleanup unused code and do not cache all results twice.

------------------------------------------------------------------------
r1107071 | hpereiradacosta | 2010-03-25 07:44:23 +1300 (Thu, 25 Mar 2010) | 4 lines

Backport r1107068
Fixed hover and focus for Q3ListViews, like in e.g. konversation.


------------------------------------------------------------------------
r1107167 | aseigo | 2010-03-25 13:01:30 +1300 (Thu, 25 Mar 2010) | 2 lines

QSizePolicy, need to make size policy setting possible

------------------------------------------------------------------------
r1107301 | trueg | 2010-03-25 22:22:23 +1300 (Thu, 25 Mar 2010) | 1 line

Backport: Do not perform the second query if the first one yielded less than 10 results.
------------------------------------------------------------------------
r1107308 | mzanetti | 2010-03-25 22:37:34 +1300 (Thu, 25 Mar 2010) | 4 lines

Backport commit 1106285:
fixed: no need to redefine the signals already present in Solid::Control::Ifaces::RemoteControlManager


------------------------------------------------------------------------
r1107481 | hpereiradacosta | 2010-03-26 08:37:08 +1300 (Fri, 26 Mar 2010) | 3 lines

Backport: r1107480
properly reset lastKey and lastValue when unregistering widget

------------------------------------------------------------------------
r1107590 | harshj | 2010-03-26 17:16:21 +1300 (Fri, 26 Mar 2010) | 10 lines

For Comic Book Thumbnails:
 * Add support for the 'rar' executable. Shareware version sometimes distributed as 'unrar' also. Thumbnailing breaks in this case.
 * Instead of checking for the 'freeware' keyword in unrar output, lets do it like Okular - check for 'UNRAR'/'RAR' words 
(case-sensitive).

CCBUG:232062

Backport of 1107365


------------------------------------------------------------------------
r1107637 | segato | 2010-03-26 23:46:39 +1300 (Fri, 26 Mar 2010) | 3 lines

backport r1107627
 no need to create the theme directory here, it's done by kemoticons now

------------------------------------------------------------------------
r1107743 | mueller | 2010-03-27 06:18:40 +1300 (Sat, 27 Mar 2010) | 2 lines

KDE 4.4.2

------------------------------------------------------------------------
r1107744 | mueller | 2010-03-27 06:18:57 +1300 (Sat, 27 Mar 2010) | 2 lines

KDE 4.4.2

------------------------------------------------------------------------
