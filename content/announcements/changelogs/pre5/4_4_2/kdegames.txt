------------------------------------------------------------------------
r1096848 | jobermayr | 2010-02-28 09:20:46 +1300 (Sun, 28 Feb 2010) | 5 lines

Icon "fifteenpieces" is in plasma-addons package and not installed on all systems by default.

Icon "preferences-plugin" is in oxygen package and so usually installed on all systems by default.

Icon "palapeli" exits only in oxygen package since development of KDE SC 4.5.
------------------------------------------------------------------------
r1097414 | scripty | 2010-03-01 23:31:29 +1300 (Mon, 01 Mar 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1097697 | scripty | 2010-03-02 11:13:26 +1300 (Tue, 02 Mar 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1097786 | scripty | 2010-03-02 15:33:11 +1300 (Tue, 02 Mar 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1097996 | gpelladi | 2010-03-03 05:08:14 +1300 (Wed, 03 Mar 2010) | 2 lines

BUG: 228583
Add icons for bovo, like other games do.
------------------------------------------------------------------------
r1098005 | gpelladi | 2010-03-03 05:44:00 +1300 (Wed, 03 Mar 2010) | 1 line

Add scalable icon for bovo.
------------------------------------------------------------------------
r1100590 | joselb | 2010-03-08 12:04:09 +1300 (Mon, 08 Mar 2010) | 2 lines

- Fixed bug for the 4.4 branch (missed to backport the fix of trunk)
BUG:217926
------------------------------------------------------------------------
r1100606 | scripty | 2010-03-08 15:27:23 +1300 (Mon, 08 Mar 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1101832 | scripty | 2010-03-11 15:24:10 +1300 (Thu, 11 Mar 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1103857 | scripty | 2010-03-16 15:43:21 +1300 (Tue, 16 Mar 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1104969 | scripty | 2010-03-19 16:00:03 +1300 (Fri, 19 Mar 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1105824 | scripty | 2010-03-22 02:22:12 +1300 (Mon, 22 Mar 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1106513 | scripty | 2010-03-23 15:42:19 +1300 (Tue, 23 Mar 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1107212 | scripty | 2010-03-25 15:29:39 +1300 (Thu, 25 Mar 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
