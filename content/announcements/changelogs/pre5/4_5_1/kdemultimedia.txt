------------------------------------------------------------------------
r1167202 | mpyne | 2010-08-24 01:29:17 +0100 (dt, 24 ago 2010) | 3 lines

This fix for the too-transparent Juk track popup works, but makes the track
popup show up in the taskbar. Revert from 4.5.1.

------------------------------------------------------------------------
r1166839 | mpyne | 2010-08-23 04:52:27 +0100 (dl, 23 ago 2010) | 2 lines

Backport fix for JuK transparent track announcement popup background to KDE SC 4.5.1.

------------------------------------------------------------------------
r1166293 | esken | 2010-08-21 13:32:49 +0100 (ds, 21 ago 2010) | 5 lines

Fix  Bug 245737:  kmix with pulseaudio-support messes up global
shortcut settings
This is the fix for KDE4.5 branch.
BUGS: 

------------------------------------------------------------------------
r1163407 | scripty | 2010-08-14 03:17:26 +0100 (ds, 14 ago 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1161881 | scripty | 2010-08-11 03:15:13 +0100 (dc, 11 ago 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
