2005-03-06 21:14 +0000 [r395340]  coolo

	* kpat/fortyeight.cpp: backporting

2005-03-07 16:10 +0000 [r395537]  coolo

	* kpat/pwidget.cpp: backporting "do not show again" - no new
	  strings

2005-03-07 20:08 +0000 [r395601]  coolo

	* kpat/pwidget.cpp: do not give abort warning when the game is lost
	  anyway (dedicated to the best beta tester there is ;)

2005-03-19 14:48 +0000 [r398978]  aacid

	* ksame/KSameWidget.h, ksame/KSameWidget.cpp: Backport fix for bug
	  101061

2005-03-28 19:41 +0000 [r401374]  aacid

	* kreversi/board.cpp: Backporting fix for bug 94069

2005-03-28 21:07 +0000 [r401404]  aacid

	* kreversi/kreversi.cpp: Backport Fix faulty connect CCMAIL:
	  inge@lysator.liu.se

2005-03-28 21:41 +0000 [r401420]  aacid

	* kmahjongg/kmahjongg.cpp: Backport fix for bug 101436

2005-03-28 21:52 +0000 [r401425]  aacid

	* kmahjongg/kmahjongg.cpp, kmahjongg/kmahjongg.h,
	  kmahjongg/prefs.kcfgc, kmahjongg/kmahjongg.kcfg: Save "Settings
	  -> Show Matching Tiles" between program starts Fixes bug 101058
	  Thanks Ronny for reporting Will be fixed in KDE >= 3.4.1 BUGS:
	  101058

2005-04-01 20:11 +0000 [r402499]  ingwa

	* kreversi/kreversi.h, kreversi/ChangeLog: Fix bug 102297: I am
	  playing in KReversi as "expert" but it saves statistics to the
	  "beginner" records. - let KReversi::m_lowestStrength be an uint
	  instead of bool. This was a rather embarrasing cut & paste bug.
	  BUGS: 102297

2005-04-01 23:44 +0000 [r402549]  ingwa

	* kreversi/ChangeLog, kreversi/kreversi.cpp: Fix bug 102890: The
	  result is not put into the higscore if not all squares are filled
	  at the end of the game

2005-04-10 20:08 +0000 [r404632]  orlovich

	* kasteroids/toplevel.cpp: Backport the workaround for duplicate TI
	  symbols crashing.

2005-04-12 15:47 +0000 [r405073]  akrille

	* libksirtet/common/Makefile.am: Build on srcdir != builddir

2005-04-15 17:26 +0000 [r405764]  aacid

	* kmahjongg/boardwidget.cpp: Update number of remaining pairs after
	  a shuffle BUGS: 103976

2005-05-08 09:03 +0000 [r410715]  binner

	* kwin4/grafix/Makefile.am: fix for .svn/

2005-05-10 10:32 +0000 [r411876]  binner

	* kdegames.lsm: update lsm for release

2005-05-22 15:35 +0000 [r416935]  coolo

	* kolf/courses.list: backport i18n fix

