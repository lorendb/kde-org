------------------------------------------------------------------------
r1043211 | scripty | 2009-11-01 04:08:35 +0000 (Sun, 01 Nov 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1044965 | scripty | 2009-11-05 04:19:38 +0000 (Thu, 05 Nov 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1047719 | aacid | 2009-11-11 22:04:28 +0000 (Wed, 11 Nov 2009) | 4 lines

Backport r1047718 | aacid | 2009-11-11 23:03:16 +0100 (Wed, 11 Nov 2009) | 2 lines

use confirm overwrite!

------------------------------------------------------------------------
r1047726 | aacid | 2009-11-11 22:21:07 +0000 (Wed, 11 Nov 2009) | 3 lines

Backport aacid * r1047724 trunk/KDE/kdegames/ktuberling/ (playground.cpp todraw.cpp) 
do explicit versioning of the datastream, should have done this earlier :-/ 

------------------------------------------------------------------------
r1047741 | aacid | 2009-11-11 23:54:15 +0000 (Wed, 11 Nov 2009) | 5 lines

backport r1047740 | aacid | 2009-11-12 00:52:26 +0100 (Thu, 12 Nov 2009) | 3 lines

Do not use QIODevice::Text with QDataStream, is not a good idea
BUGS: 214090

------------------------------------------------------------------------
r1049792 | aacid | 2009-11-15 21:08:37 +0000 (Sun, 15 Nov 2009) | 2 lines

4.3 does not have this flag, thanks André for noticing

------------------------------------------------------------------------
r1051239 | scripty | 2009-11-19 04:15:23 +0000 (Thu, 19 Nov 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
