------------------------------------------------------------------------
r853758 | mueller | 2008-08-28 10:58:47 +0200 (Thu, 28 Aug 2008) | 2 lines

bump version

------------------------------------------------------------------------
r853986 | orlovich | 2008-08-28 17:51:14 +0200 (Thu, 28 Aug 2008) | 3 lines

Erk. Fix last-minute regression from a microoptimization :(
Would be nice to see it in 4.1.1

------------------------------------------------------------------------
r854142 | ilic | 2008-08-29 00:11:13 +0200 (Fri, 29 Aug 2008) | 1 line

Dummy Messages.sh to prevent extraction of messages from kfile/ into kdelibs4.po, as kio4.po includes it. (bport: 854139)
------------------------------------------------------------------------
r854183 | porten | 2008-08-29 03:24:39 +0200 (Fri, 29 Aug 2008) | 2 lines

Merged revision 854182:
Event handlers were still accessing the debugger even if it was disabled.
------------------------------------------------------------------------
r854449 | ereslibre | 2008-08-29 15:48:58 +0200 (Fri, 29 Aug 2008) | 2 lines

Backport of rev 854447

------------------------------------------------------------------------
r854499 | dfaure | 2008-08-29 18:56:06 +0200 (Fri, 29 Aug 2008) | 2 lines

Backport fix for missing items, reported by jpwhiting.

------------------------------------------------------------------------
r854554 | vandenoever | 2008-08-29 21:12:18 +0200 (Fri, 29 Aug 2008) | 1 line

Fix Strigi detection to use a version from the 0.5 branch and to not use a newer version.
------------------------------------------------------------------------
r854568 | skilling | 2008-08-29 22:07:30 +0200 (Fri, 29 Aug 2008) | 1 line

Fixing a few old errors
------------------------------------------------------------------------
r854600 | aacid | 2008-08-29 23:55:30 +0200 (Fri, 29 Aug 2008) | 2 lines

Approved i18n change, add context to Value: for HSV

------------------------------------------------------------------------
r854967 | orlovich | 2008-08-30 22:40:14 +0200 (Sat, 30 Aug 2008) | 11 lines

Pickup Seli's revision r458979 of the old loader_jpeg.cpp (man did that file 
get forked a lot..), as it also fixes  Unai Garro's artifacts bug.

   r458979 | lunakl | 2005-09-09 10:12:34 -0400 (Fri, 09 Sep 2005) | 4 lines

   Don't use JDCT_FASTEST, it's causing artifacts and I can't
   actually measure any speed difference. (#108903)


BUG:169820

------------------------------------------------------------------------
r854972 | orlovich | 2008-08-30 22:59:31 +0200 (Sat, 30 Aug 2008) | 4 lines

Pick up infrastructure for supporting getComputedStyle on display:none 
things from WebCore. Not hooked up yet --- RenderStyleDeclarationImpl 
needs to be taught not to use the renderer when not available.

------------------------------------------------------------------------
r855025 | porten | 2008-08-31 01:31:57 +0200 (Sun, 31 Aug 2008) | 3 lines

Merged revision 855024:
Optimized parser for CSS color values. Gives me 3.5% speedup on the
first skeleton movie compared to the version using QString::toInt().
------------------------------------------------------------------------
r855037 | porten | 2008-08-31 02:12:18 +0200 (Sun, 31 Aug 2008) | 4 lines

Merged revision 855035:
Fixed parsing of multi-line FTP responses. It was stricter
than required by RFC 959. Fixes login on ftp://download.nvidia.com
as reported in bug #157857.
------------------------------------------------------------------------
r855183 | ossi | 2008-08-31 11:40:00 +0200 (Sun, 31 Aug 2008) | 3 lines

backport: do not crash in expandString() if KGlobal::mainComponent() is
not inited yet.

------------------------------------------------------------------------
r855352 | ereslibre | 2008-08-31 17:02:14 +0200 (Sun, 31 Aug 2008) | 2 lines

Backport rev 855350. Do not force to change the order of calls

------------------------------------------------------------------------
r855359 | ggarand | 2008-08-31 17:25:19 +0200 (Sun, 31 Aug 2008) | 4 lines

automatically merged revision 855319:
must have a renderer

BUG: 167239
------------------------------------------------------------------------
r855360 | ggarand | 2008-08-31 17:26:00 +0200 (Sun, 31 Aug 2008) | 5 lines

automatically merged revision 855320:
revert part of r784843
-> copy/paste error

BUG: 169447
------------------------------------------------------------------------
r855361 | ggarand | 2008-08-31 17:26:35 +0200 (Sun, 31 Aug 2008) | 4 lines

automatically merged revision 855321:
do proper error handling inside @media blocks

BUG: 169629
------------------------------------------------------------------------
r855364 | ggarand | 2008-08-31 17:32:19 +0200 (Sun, 31 Aug 2008) | 5 lines

automatically merged revision 855322:
add missing compatibility bit
<table border=NaN> translates to <table border=1>

CCBUG:167567
------------------------------------------------------------------------
r855365 | ggarand | 2008-08-31 17:33:13 +0200 (Sun, 31 Aug 2008) | 5 lines

automatically merged revision 855323:
properly compute width of containing block of inline elements
as per CSS 2.1 - 10.1.4.1

BUG: 170095
------------------------------------------------------------------------
r855371 | ereslibre | 2008-08-31 17:51:24 +0200 (Sun, 31 Aug 2008) | 2 lines

Backport rev 855369

------------------------------------------------------------------------
r855373 | ereslibre | 2008-08-31 17:53:39 +0200 (Sun, 31 Aug 2008) | 2 lines

Mark as internal so it is hidden for docu

------------------------------------------------------------------------
r855413 | ereslibre | 2008-08-31 20:32:21 +0200 (Sun, 31 Aug 2008) | 2 lines

Backport rev 855412

------------------------------------------------------------------------
r855524 | ggarand | 2008-09-01 02:15:06 +0200 (Mon, 01 Sep 2008) | 7 lines

automatically merged revision 855521:
Consider the real root background box when computing some background
properties - not the one that is extended on the whole canvas.

Patch based on work by Anatoli Papirovski <apapirovski mac dot com>

BUG: 169608
------------------------------------------------------------------------
r855930 | pino | 2008-09-01 20:28:53 +0200 (Mon, 01 Sep 2008) | 2 lines

add missing entity for KGPG

------------------------------------------------------------------------
r856379 | porten | 2008-09-02 20:34:55 +0200 (Tue, 02 Sep 2008) | 3 lines

Merged revision 856375:
Fixed crash on setting cookies on empty domains (like the file system).
Bug report #170147 can be closed.
------------------------------------------------------------------------
r856403 | ppenz | 2008-09-02 21:43:16 +0200 (Tue, 02 Sep 2008) | 4 lines

Backport of 856401: assure that the URL navigator gets activated when the path gets the focus

CCBUG: 169497
CCBUG: 170211
------------------------------------------------------------------------
r856535 | staniek | 2008-09-03 09:46:19 +0200 (Wed, 03 Sep 2008) | 6 lines

missing commit:
activate existing unique app's process on Windows (as in enterprise4 branch)

CCMAIL:till@kde.org
CCMAIL:kde-windows@kde.org

------------------------------------------------------------------------
r856558 | trueg | 2008-09-03 10:57:16 +0200 (Wed, 03 Sep 2008) | 1 line

backport: handle uris that do not use fragments
------------------------------------------------------------------------
r856659 | ereslibre | 2008-09-03 15:42:24 +0200 (Wed, 03 Sep 2008) | 2 lines

Backport (Created commit 694afd9: If the service has noDisplay() set, do not list this plugin)

------------------------------------------------------------------------
r856701 | ereslibre | 2008-09-03 17:51:36 +0200 (Wed, 03 Sep 2008) | 2 lines

Backport (Let's check if entries are immutable. This lets kiosk disable the user being able to enable/disable plugins."

------------------------------------------------------------------------
r856719 | ereslibre | 2008-09-03 18:40:59 +0200 (Wed, 03 Sep 2008) | 2 lines

Backport, prevent crash if service is null

------------------------------------------------------------------------
r856935 | lueck | 2008-09-04 10:35:18 +0200 (Thu, 04 Sep 2008) | 1 line

DBus entity added
------------------------------------------------------------------------
r856959 | wstephens | 2008-09-04 11:40:52 +0200 (Thu, 04 Sep 2008) | 3 lines

Backport


------------------------------------------------------------------------
r857011 | ereslibre | 2008-09-04 14:25:05 +0200 (Thu, 04 Sep 2008) | 2 lines

Backport (Make entries that aren't checkable being grayed out (icon, title and description). Backporting in a while.)

------------------------------------------------------------------------
r857015 | ereslibre | 2008-09-04 14:30:01 +0200 (Thu, 04 Sep 2008) | 2 lines

Backport (Remove unused map)

------------------------------------------------------------------------
r857103 | dfaure | 2008-09-04 20:18:40 +0200 (Thu, 04 Sep 2008) | 2 lines

Backport r855796 and r855956 (don't lose focus when applying mainwindow settings), to fix bug 170394

------------------------------------------------------------------------
r857818 | tmcguire | 2008-09-06 15:55:58 +0200 (Sat, 06 Sep 2008) | 5 lines

Backport r856835 by tmcguire from trunk to the 4.1 branch:

Moved from KMeditor: Don't eat empty paragraphs in toHtml().


------------------------------------------------------------------------
r857918 | vkrause | 2008-09-07 01:14:57 +0200 (Sun, 07 Sep 2008) | 6 lines

Backport SVN commit 857906 by vkrause:

Use the correct pointer as argument here, otherwise the implicit cast to
bool kicks in and we emit the wrong signal.
This fixes change signal propagation within a KCModuleContainer.

------------------------------------------------------------------------
r858148 | djarvie | 2008-09-07 15:29:04 +0200 (Sun, 07 Sep 2008) | 2 lines

Fix setColor() so that it updates the colour of the displayed text

------------------------------------------------------------------------
r858160 | shaforo | 2008-09-07 15:47:33 +0200 (Sun, 07 Sep 2008) | 3 lines

back-port of 858156: use locale-dependant code page for vfat volumes


------------------------------------------------------------------------
r858313 | aacid | 2008-09-07 21:02:27 +0200 (Sun, 07 Sep 2008) | 5 lines

Backport r858311 | aacid | 2008-09-07 21:01:08 +0200 (Sun, 07 Sep 2008) | 2 lines

Fix what's this action not being added to the menu


------------------------------------------------------------------------
r858491 | dfaure | 2008-09-08 10:32:12 +0200 (Mon, 08 Sep 2008) | 5 lines

Backport 854572:
Fix an assert in KTabWidget when calling tabText() from a slot connected to currentChanged() -- because
of the order of things in the QTabBar/QTabWidget code, tabInserted() isn't called yet so m_tabNames was
empty. With unit test, since code speaks more than 3 lines of text.

------------------------------------------------------------------------
r858784 | ereslibre | 2008-09-08 19:46:03 +0200 (Mon, 08 Sep 2008) | 2 lines

Backport(Give the focus back to the parent widget if any)

------------------------------------------------------------------------
r858795 | dfaure | 2008-09-08 20:42:09 +0200 (Mon, 08 Sep 2008) | 4 lines

Fix file association bug detected by Maksim: the global mimeapps.list file had priority over the local one. Ooops.
Distros: better not install a global mimeapps.list until you get this fix.
CCMAIL: helio@kde.org

------------------------------------------------------------------------
r858869 | rpedersen | 2008-09-08 23:05:16 +0200 (Mon, 08 Sep 2008) | 2 lines

backport 858864

------------------------------------------------------------------------
r859305 | orlovich | 2008-09-10 02:00:58 +0200 (Wed, 10 Sep 2008) | 7 lines

Fix a regression in background: shorthand parsing:
parseBackgroundPositionXY was impropertly setting the out position 
when it didn't match, which incorrect classified the position of a missing 2nd 
coordinate in a shorthard as non-keyword, hence dropping the rule if the 
first one was vertical
BUG:170755

------------------------------------------------------------------------
r860005 | tmcguire | 2008-09-11 21:19:51 +0200 (Thu, 11 Sep 2008) | 9 lines

Backport r860002 by tmcguire from trunk to the 4.1 branch:

Don't eat my text when the emoticons were not installed.
This fixes mail text not being displayed in KMail when kdebase-runtime wasn't
installed.

CCBUG: 170770


------------------------------------------------------------------------
r860095 | orlovich | 2008-09-12 02:39:55 +0200 (Fri, 12 Sep 2008) | 4 lines

Don't crash if onscroll handle due to a layer scroll (e.g. due to a marquee) caused us to be detached

BUG:170880

------------------------------------------------------------------------
r860101 | orlovich | 2008-09-12 02:47:36 +0200 (Fri, 12 Sep 2008) | 3 lines

Tighten up the response parsing to not get confused and out-of-sync quite so easily.
Fixes #169556 for 4.1 branch, trunk has a better fix, with a new header parser..

------------------------------------------------------------------------
r860109 | orlovich | 2008-09-12 04:18:26 +0200 (Fri, 12 Sep 2008) | 4 lines

- Do not escape tabs in scripts (115325), cleanup related code to not have zillions of lines duplicated
- Kill strict SGML comment parsing insanity, taking a few packet-boundary bugs with it.
BUG:115325

------------------------------------------------------------------------
r860199 | aacid | 2008-09-12 12:27:37 +0200 (Fri, 12 Sep 2008) | 3 lines

Backport Revision 848350
load katepart translation catalog each time a kateglobal is created

------------------------------------------------------------------------
r860385 | shaforo | 2008-09-12 23:25:39 +0200 (Fri, 12 Sep 2008) | 2 lines

ok, use DOS code pages as people ask

------------------------------------------------------------------------
r860423 | dfaure | 2008-09-13 02:31:05 +0200 (Sat, 13 Sep 2008) | 3 lines

When strigi is >= 0.6.0, use signed char instead of char.
Strong suggestion for the future: KEEPING SOURCE COMPATIBILITY!

------------------------------------------------------------------------
r860800 | lueck | 2008-09-14 13:14:10 +0200 (Sun, 14 Sep 2008) | 1 line

EN -> DE
------------------------------------------------------------------------
r860940 | shaforo | 2008-09-14 19:32:38 +0200 (Sun, 14 Sep 2008) | 3 lines

add codepage data for Japanese, Chinese (all countries), Korean, Thai, and Vietnamese


------------------------------------------------------------------------
r861004 | grossard | 2008-09-14 22:02:05 +0200 (Sun, 14 Sep 2008) | 1 line

added entity for Stephane Guedon
------------------------------------------------------------------------
r861065 | ggarand | 2008-09-15 06:09:45 +0200 (Mon, 15 Sep 2008) | 5 lines

automatically merged revision 856458:
introduce needed CSS tokenizer states for proper contextual
#{ident|hexcolor} parsing

BUG: 170068
------------------------------------------------------------------------
r861066 | ggarand | 2008-09-15 06:10:30 +0200 (Mon, 15 Sep 2008) | 5 lines

automatically merged revision 857211:
ah, now I see what SadEagle meant... *inner* borders.
must dirty all cells when the border attribute is updated.

BUG: 167567
------------------------------------------------------------------------
r861067 | ggarand | 2008-09-15 06:11:07 +0200 (Mon, 15 Sep 2008) | 7 lines

automatically merged revision 857212:
fix inheritance of border color

patch by Maksim Orlovich and
Anatoli Papirovski <apapirovski mac dot com>

BUG: 170089
------------------------------------------------------------------------
r861068 | ggarand | 2008-09-15 06:11:41 +0200 (Mon, 15 Sep 2008) | 5 lines

automatically merged revision 857213:
adapt to change in Glazman's CSS3 selector test.

contains/begins/ends attribute selectors should not match at all
when their associated expression is empty.
------------------------------------------------------------------------
r861069 | ggarand | 2008-09-15 06:12:07 +0200 (Mon, 15 Sep 2008) | 4 lines

automatically merged revision 857214:
patch by Bradley Meck  <genisis329@gmail.com>

url() is valid CSS.
------------------------------------------------------------------------
r861070 | ggarand | 2008-09-15 06:13:03 +0200 (Mon, 15 Sep 2008) | 10 lines

automatically merged revision 857217:
fix background of the <select> form widget's popup list.

When the form widget has an associated bg image, it is assigned a
partially transparent palette to let the image show through.

That palette used to percolate to the popup list, with interesting visual
results.

BUG: 170398
------------------------------------------------------------------------
r861073 | ggarand | 2008-09-15 06:14:26 +0200 (Mon, 15 Sep 2008) | 4 lines

automatically merged revision 858966:
Simplify and guard the smoothscroll algoritm a little. 
Thanks to Manuel Mommertz for solving the bug.
CCBUG: 166870
------------------------------------------------------------------------
r861075 | ggarand | 2008-09-15 06:15:29 +0200 (Mon, 15 Sep 2008) | 3 lines

automatically merged revision 859315:
pickup that cleanup bit from WC.
No evidence of a related crash in b.k.o but it simply makes sense.
------------------------------------------------------------------------
r861080 | ggarand | 2008-09-15 06:20:07 +0200 (Mon, 15 Sep 2008) | 3 lines

automatically merged revision 859311:
Submit button with an empty-but-not-null 'value' attribute
must not display the default button text.
------------------------------------------------------------------------
r861081 | ggarand | 2008-09-15 06:20:59 +0200 (Mon, 15 Sep 2008) | 2 lines

automatically merged revision 859312:
add ecma binding for de facto standard onscroll event handler
------------------------------------------------------------------------
r861082 | ggarand | 2008-09-15 06:21:33 +0200 (Mon, 15 Sep 2008) | 5 lines

automatically merged revision 859313:
remove remaining incorrect use of hidesOverflow - use hasOverflowClip
instead. It knows the real overflow status.

fixes www.dhteumeuleu.com's thumbnail-loading in the gallery.
------------------------------------------------------------------------
r861083 | ggarand | 2008-09-15 06:22:12 +0200 (Mon, 15 Sep 2008) | 4 lines

automatically merged revision 859314:
compute proper getPropertyValue for the 'border' shorthand

adapted patch by Rob Buis.
------------------------------------------------------------------------
r861085 | ggarand | 2008-09-15 06:26:08 +0200 (Mon, 15 Sep 2008) | 3 lines

automatically merged revision 859325:
follow-up to r859311.
that's more like it..
------------------------------------------------------------------------
r861086 | ggarand | 2008-09-15 06:26:55 +0200 (Mon, 15 Sep 2008) | 4 lines

automatically merged revision 861053:
one more follow-up to r859311:

now save/restore code need to differentiate null/empty too.
------------------------------------------------------------------------
r861087 | ggarand | 2008-09-15 06:27:50 +0200 (Mon, 15 Sep 2008) | 7 lines

automatically merged revision 861054:
.doing strict dtd checking for inlines in strict mode turns out to be
 too optimistic. It breaks sites.

.no popBlock for dl/dt errors. It doesn't match what other browsers do.

BUG: 170650
------------------------------------------------------------------------
r861088 | ggarand | 2008-09-15 06:30:13 +0200 (Mon, 15 Sep 2008) | 3 lines

automatically merged revision 861056:
.tighten a bit the scroll rectangle
.always do ourselves the scrolling of external widgets.
------------------------------------------------------------------------
r861089 | ggarand | 2008-09-15 06:31:12 +0200 (Mon, 15 Sep 2008) | 7 lines

automatically merged revision 861057:
.avoid NVidia+Qt4 buffer clearing problems, by making our widgets
 have a private buffer.

.for the same reason, always fully clear our shared opacity buffers - or
 we might get artifacts on NVidia cards such as on
 www.codetch.com/screenshots/
------------------------------------------------------------------------
r861186 | ervin | 2008-09-15 13:38:07 +0200 (Mon, 15 Sep 2008) | 3 lines

Indeed unnecessary as pointed out by Jeff.
(backport to 4.1)

------------------------------------------------------------------------
r861402 | sengels | 2008-09-16 02:56:59 +0200 (Tue, 16 Sep 2008) | 2 lines

backport commits 860264, 860266, 861282
this fixes the open/save dialog issues
------------------------------------------------------------------------
r861488 | rpedersen | 2008-09-16 11:47:14 +0200 (Tue, 16 Sep 2008) | 2 lines

backport 861487

------------------------------------------------------------------------
r861763 | scripty | 2008-09-17 08:52:10 +0200 (Wed, 17 Sep 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r861920 | ereslibre | 2008-09-17 16:56:59 +0200 (Wed, 17 Sep 2008) | 2 lines

Backport revision 861919 (send mouse events to the hovered widget, so listviews for example highlight the correct item)

------------------------------------------------------------------------
r862491 | kkofler | 2008-09-19 03:13:25 +0200 (Fri, 19 Sep 2008) | 3 lines

KHTML fix: Make "stop animations" work again in Konqueror (kde#157789 KDE 4 regression).
(backport revision 862489 from trunk)
CCBUG: 157789
------------------------------------------------------------------------
r862655 | dfaure | 2008-09-19 16:55:26 +0200 (Fri, 19 Sep 2008) | 3 lines

deleteLater isn't a direct equivalent of delete this, one must disconnect
This fixes "Scheduler: BUG!! Slave  KIO::Slave(0x6ee820) / 3788  died, but is NOT in slaveList!!!"

------------------------------------------------------------------------
r862734 | dfaure | 2008-09-19 20:01:32 +0200 (Fri, 19 Sep 2008) | 7 lines

Backport SVN commit 862727:
Fix #167851 (Preview in columns view crashes dolphin)
Another KDirLister corner case: a dirlister holding the items,
and a second dirlister does openUrl(reload) (which triggers updateDirectory())
and the first lister immediately does openUrl() (which emits cached items).
The first lister would get no items, or after fixing this wrongly, would get them twice.

------------------------------------------------------------------------
r862739 | dfaure | 2008-09-19 20:25:56 +0200 (Fri, 19 Sep 2008) | 2 lines

Backport performance improvements, in particular when deleting a large number of items

------------------------------------------------------------------------
r863051 | shaforo | 2008-09-20 22:26:52 +0200 (Sat, 20 Sep 2008) | 1 line

port changes from trunk
------------------------------------------------------------------------
r863140 | rpedersen | 2008-09-21 12:05:31 +0200 (Sun, 21 Sep 2008) | 2 lines

backport 863138

------------------------------------------------------------------------
r863210 | ervin | 2008-09-21 14:52:32 +0200 (Sun, 21 Sep 2008) | 3 lines

Notify the world that the power saving status changed.
(backport)

------------------------------------------------------------------------
r863464 | habacker | 2008-09-22 09:31:18 +0200 (Mon, 22 Sep 2008) | 1 line

win32 fix of aspell data dir which is now inside the kde install tree; backported from trunk
------------------------------------------------------------------------
r863465 | habacker | 2008-09-22 09:38:08 +0200 (Mon, 22 Sep 2008) | 1 line

fixed case when aspell dictionary wasn't found, without this at least on windows this case results into application crash; backported from trunk
------------------------------------------------------------------------
r863692 | abizjak | 2008-09-22 23:21:53 +0200 (Mon, 22 Sep 2008) | 2 lines

backport r863691

------------------------------------------------------------------------
r864120 | abizjak | 2008-09-24 01:41:26 +0200 (Wed, 24 Sep 2008) | 4 lines

Pass the user's locale to HAL when using the ntfs-3g driver.
If ntfs-3g is not provided a locale, it will fail to show any files
with non-ascii characters in the name, and fail to create such files.

------------------------------------------------------------------------
r864153 | ggarand | 2008-09-24 06:06:27 +0200 (Wed, 24 Sep 2008) | 3 lines

automatically merged revision 864145:
fix parameter inversion,
patch by Alexey Proskuryakov <ap@webkit.org>
------------------------------------------------------------------------
r864155 | ggarand | 2008-09-24 06:08:05 +0200 (Wed, 24 Sep 2008) | 5 lines

automatically merged revision 864146:
replace this backingstore hack with either an update or a repaint
depending on emergency - as it would occasionally cause crashes.

BUG: 171170
------------------------------------------------------------------------
r864156 | ggarand | 2008-09-24 06:08:59 +0200 (Wed, 24 Sep 2008) | 3 lines

automatically merged revision 864147:
when submiting a form, ignore base url if 'action' attribute is empty -
adapted from a patch by Alexey Proskuryakov <ap at webkit dot org>
------------------------------------------------------------------------
r864157 | ggarand | 2008-09-24 06:09:57 +0200 (Wed, 24 Sep 2008) | 2 lines

automatically merged revision 864149:
fix kde4 regression: parsing mode for inline CSS must match the document's
------------------------------------------------------------------------
r864158 | ggarand | 2008-09-24 06:11:38 +0200 (Wed, 24 Sep 2008) | 5 lines

automatically merged revision 864150:
do not attempt to recover incorrectly written hexadecimal
colours when the parser is in strict mode.

BUG: 132581
------------------------------------------------------------------------
r864159 | ggarand | 2008-09-24 06:12:29 +0200 (Wed, 24 Sep 2008) | 2 lines

automatically merged revision 864151:
non-standard scroll event does not bubble in other engines - dont acte.
------------------------------------------------------------------------
r864182 | scripty | 2008-09-24 09:01:51 +0200 (Wed, 24 Sep 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r864393 | ereslibre | 2008-09-24 19:39:37 +0200 (Wed, 24 Sep 2008) | 3 lines

Backport (Initial patch by David Faure. This patch improves the way synchronousRun handles internally the job. It has to remove the autodeletion just in case we reenter the event loop. This is specially 
critical)

------------------------------------------------------------------------
r864470 | ggarand | 2008-09-24 22:36:11 +0200 (Wed, 24 Sep 2008) | 5 lines

automatically merged revision 864463:
introduce some heavy hacks to avoid QWidget::scroll's performance bug
on unpatched Qt.

BUG: 167739
------------------------------------------------------------------------
r864474 | sping | 2008-09-24 22:42:46 +0200 (Wed, 24 Sep 2008) | 2 lines

Backport fix for bug #164410: No longer skip every second match with <replace all> over adjacent matches
$ svn merge -c 864017 https://sping@svn.kde.org/home/kde/trunk/KDE/kdelibs/kate/utils/katesearchbar.cpp
------------------------------------------------------------------------
r864477 | pino | 2008-09-24 22:46:35 +0200 (Wed, 24 Sep 2008) | 2 lines

add the entry for the "help -> switch application language" menu item

------------------------------------------------------------------------
r864478 | pino | 2008-09-24 22:47:05 +0200 (Wed, 24 Sep 2008) | 2 lines

better

------------------------------------------------------------------------
r864535 | ggarand | 2008-09-25 01:50:50 +0200 (Thu, 25 Sep 2008) | 2 lines

automatically merged revision 864533:
most engines now support 'position: fixed' on body even in quirk mode.
------------------------------------------------------------------------
r864536 | ggarand | 2008-09-25 01:51:21 +0200 (Thu, 25 Sep 2008) | 9 lines

automatically merged revision 864534:
fix offsetTop/Left/Parent properties to be more in conformance with draft
CSSOM View module specification.

http://www.w3.org/TR/cssom-view/#offset-attributes

brings better compatibility as  a bonus.

BUG: 170091, 170055
------------------------------------------------------------------------
r864637 | mueller | 2008-09-25 12:16:31 +0200 (Thu, 25 Sep 2008) | 2 lines

KDE 4.1.2 preparations

------------------------------------------------------------------------
