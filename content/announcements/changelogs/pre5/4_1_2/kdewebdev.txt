------------------------------------------------------------------------
r854454 | mojo | 2008-08-29 15:59:56 +0200 (Fri, 29 Aug 2008) | 8 lines

Backport:

Only check html anchors if the file has the right mimetype. This fixes URLs like this, for example:

http://localhost/manual.pdf#page=5

Also as the nice side effect of avoid downloading the entire file.

------------------------------------------------------------------------
