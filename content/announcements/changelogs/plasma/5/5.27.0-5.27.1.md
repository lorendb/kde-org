---
title: Plasma 5.27.1 complete changelog
version: 5.27.1
hidden: true
plasma: true
type: fulllog
---
{{< details title="Discover" href="https://commits.kde.org/discover" >}}
+ [notifier] Fix KAboutData component name. [Commit.](http://commits.kde.org/discover/07cd960ac5e24d7d02b66dc23f8673ac2b5cc238) 
+ Fix parsing of the <Categories> tag. [Commit.](http://commits.kde.org/discover/8b8434fd440524e449fce6aa6c385f92df28c9e9) 
+ Extend CategoriesTest with checking of <Categories> tag parsing. [Commit.](http://commits.kde.org/discover/a59fb7ed5ef995656f9cfd5ca2c550ac8c36a486) 
+ Flatpak: fix build with appstream<0.16. [Commit.](http://commits.kde.org/discover/dbcb71cc2fa720cb19d9c0dfe805d404a93d3f03) 
+ Flatpak: Allow using newer appstream API to look up by bundle id. [Commit.](http://commits.kde.org/discover/847964b17f3355ad2eca49f25a9da8181ce20277) 
{{< /details >}}

{{< details title="Dr Konqi" href="https://commits.kde.org/drkonqi" >}}
+ Scroll bug description. [Commit.](http://commits.kde.org/drkonqi/74f538b6e7806f530aba3c335778839ad015046f) Fixes bug [#466180](https://bugs.kde.org/466180)
+ Make python distro and psutil modules in the gdb preable optional. [Commit.](http://commits.kde.org/drkonqi/621db2a810d4df69b81eab69916d92c2a963e292) 
+ Handle WITH_SENTRY correctly. [Commit.](http://commits.kde.org/drkonqi/4fc0d8af1da0f93a2d09b11840240d4e74b0c785) 
+ Login on field accepting. [Commit.](http://commits.kde.org/drkonqi/af0143d5565655916c85fe201af618a5aad10fc8) Fixes bug [#466109](https://bugs.kde.org/466109)
{{< /details >}}

{{< details title="flatpak-kcm" href="https://commits.kde.org/flatpak-kcm" >}}
+ DRY the code a bit. [Commit.](http://commits.kde.org/flatpak-kcm/68bc708e1f4feb9a22b4801553936658792bf69d) 
+ Use range based for loops where possible. [Commit.](http://commits.kde.org/flatpak-kcm/aec004107734786d47456dba6d9b0028c05a5cd0) 
+ Make clazy happy. [Commit.](http://commits.kde.org/flatpak-kcm/0310ebf5b058b4cb17f3c1dfa569a6d916c5959f) 
+ Typo--. [Commit.](http://commits.kde.org/flatpak-kcm/4ec79d12e714abe0e0519d7765b7e4527be82c92) 
+ Always use frontend strings but convert to backend strings when saving. [Commit.](http://commits.kde.org/flatpak-kcm/5d6a1c0536da8114f1f9ba9d341e3b419839d4e3) Fixes bug [#465818](https://bugs.kde.org/465818)
+ Add a test case to cover BUG 465818. [Commit.](http://commits.kde.org/flatpak-kcm/b95dd0a9f778f7d77d7ee3d9a5277dbdff588590) 
+ Don't needlessly define a default constructor. [Commit.](http://commits.kde.org/flatpak-kcm/6a3f4aaf2367fa977caa7fd02ef4ab27df47a773) 
+ Don't else after return. [Commit.](http://commits.kde.org/flatpak-kcm/0b8cdcd6d35c019106704e5b2d977e3d51f6b9ff) 
+ Initialize variables where possible. [Commit.](http://commits.kde.org/flatpak-kcm/d8fbfde69b373b65c710726d666707260d2078bb) 
+ Don't declare multiple variables in a single line. [Commit.](http://commits.kde.org/flatpak-kcm/2377edab09b3b7a8af80aa854bf07640f4cd19e1) 
+ Don't const primitives. [Commit.](http://commits.kde.org/flatpak-kcm/124c72f54593dd338447253b1c4fd9cb40c9ca65) 
+ Pass qstrings by reference where appropriate. [Commit.](http://commits.kde.org/flatpak-kcm/4f6e96d39ca6b260b1dd60af9a9870505983443d) 
+ Refresh git-blame-ignore-revs for latest clang-format run. [Commit.](http://commits.kde.org/flatpak-kcm/6465cb608c7b31d3672d997d209e84185ef4860f) 
+ Automatic clang-format run (clang 15). [Commit.](http://commits.kde.org/flatpak-kcm/a5965c5d5c7eb5c5060f25d35fe400d2b3a14f36) 
+ Enable clang-format. [Commit.](http://commits.kde.org/flatpak-kcm/24c57b5eb1df20bf17c50313c054ffc59fd6522d) 
+ Print a cmake feature summary. [Commit.](http://commits.kde.org/flatpak-kcm/4f165ff398994c595c6bf1016a9d54ce995b482a) 
+ Don't needlessly define default dtors. [Commit.](http://commits.kde.org/flatpak-kcm/0c924c82214f1a41f7aa88ee2eab79cef97f9361) 
+ Mark constructors explicit where appropriate. [Commit.](http://commits.kde.org/flatpak-kcm/4fc2780529dd656525ccbc65199ae6c647ad41bd) 
+ Don't declare getters as slots. [Commit.](http://commits.kde.org/flatpak-kcm/25dae617479370b9e25ebde02b94f56e558d3ca2) 
+ Sort includes. [Commit.](http://commits.kde.org/flatpak-kcm/c8d978457fb45a3e5fb06cac338361a51babaf27) 
+ Avoid creating empty fs permission entry from overrides. [Commit.](http://commits.kde.org/flatpak-kcm/148457764403d3ea051c6f857964364faac08179) 
{{< /details >}}

{{< details title="KDE GTK Config" href="https://commits.kde.org/kde-gtk-config" >}}
+ Xsettings: restrict `pidof` to own processes. [Commit.](http://commits.kde.org/kde-gtk-config/0e6021840f2ad44658266e9c2cf91916902c388e) 
+ Add support for XWayland client scaling. [Commit.](http://commits.kde.org/kde-gtk-config/2201885bed9d04c288b74a7fd894f6cb5c68b0ae) Fixes bug [#465733](https://bugs.kde.org/465733)
+ Remove unnecessary `canConvert` check. [Commit.](http://commits.kde.org/kde-gtk-config/8c62e1db94baf822f3bec24c78fd428f1513b791) 
+ Unset `Gdk/UnscaledDPI` and `Gdk/WindowScalingFactor` on Wayland. [Commit.](http://commits.kde.org/kde-gtk-config/761cf85843a250da09390d23325f0bd33dadad6b) Fixes bug [#465733](https://bugs.kde.org/465733)
{{< /details >}}

{{< details title="Info Center" href="https://commits.kde.org/kinfocenter" >}}
+ Fix typo in string. [Commit.](http://commits.kde.org/kinfocenter/e03bed8561dbfc4ba6994628ae9baf8bbe00c491) Fixes bug [#466144](https://bugs.kde.org/466144)
{{< /details >}}

{{< details title="KScreen" href="https://commits.kde.org/kscreen" >}}
+ Kcm: notify scale factor change through DBus. [Commit.](http://commits.kde.org/kscreen/164de5bc8cfde1bb214cb6d7aaad295886d968b2) 
+ Kcm: Use correct role for revert button. [Commit.](http://commits.kde.org/kscreen/06f0671a8337a1c51cc6d6b29c3b1d258c311119) Fixes bug [#465788](https://bugs.kde.org/465788)
{{< /details >}}

{{< details title="kscreenlocker" href="https://commits.kde.org/kscreenlocker" >}}
+ Install logging categories, to make them configurable via KDebugSettings. [Commit.](http://commits.kde.org/kscreenlocker/2b1a7b3c2e240e79612ff6c1da669e8a73d3fac2) 
+ CMake: Clean up whitespace. [Commit.](http://commits.kde.org/kscreenlocker/b2392f9f62a23e06267b869700fead62080c26f9) 
{{< /details >}}

{{< details title="ksystemstats" href="https://commits.kde.org/ksystemstats" >}}
+ Gpu/nvidia: lower pci id. [Commit.](http://commits.kde.org/ksystemstats/63375432b8dbba59746cbffd4a74a96764239373) Fixes bug [#462512](https://bugs.kde.org/462512)
{{< /details >}}

{{< details title="KWin" href="https://commits.kde.org/kwin" >}}
+ Effects/glide: Fix transform. [Commit.](http://commits.kde.org/kwin/e141fe80284eb2a35e0cd054c6b4dbd2fcf41b55) Fixes bug [#465849](https://bugs.kde.org/465849)
+ Aurorae: Add dummy window for working device pixel ratio. [Commit.](http://commits.kde.org/kwin/5e16c3337e266adcccce00cf698c679d5012fb48) Fixes bug [#452730](https://bugs.kde.org/452730). Fixes bug [#465790](https://bugs.kde.org/465790)
+ Tiling: Make child tiles inherit padding from parent tile. [Commit.](http://commits.kde.org/kwin/451eec2e8c314ce2d43cd8164f0ee77e8593fe82) Fixes bug [#465842](https://bugs.kde.org/465842)
+ Effects/contrast: Round device coordinates when creating geometry. [Commit.](http://commits.kde.org/kwin/c3f88fed6211c6023863a749736280733333f970) Fixes bug [#464526](https://bugs.kde.org/464526)
+ Cursorsource: fix scaling with Xwayland. [Commit.](http://commits.kde.org/kwin/e2a082594a8561a7228cfe2d6cf8020fd4c85b6a) Fixes bug [#466094](https://bugs.kde.org/466094)
+ Cursordelgate: round cursor position when rendering. [Commit.](http://commits.kde.org/kwin/699d7fdd88061c3014f0da3fbdf52f647137d211) 
+ Outputchangeset: Round passed scale. [Commit.](http://commits.kde.org/kwin/aa700a13d6392b023d3a1e66892a0d59edd792dd) Fixes bug [#465850](https://bugs.kde.org/465850)
+ Mark Window as damaged when decoration or shadow changes. [Commit.](http://commits.kde.org/kwin/bbe2d4236c8c5437bafd9e09d06e69a35e548e31) Fixes bug [#464417](https://bugs.kde.org/464417)
+ Add missing cerrno include. [Commit.](http://commits.kde.org/kwin/d9b7791f7bc61b5c851928810a45837b9b594bbd) 
+ Effects/tileseditor: Set translation domain in QML files. [Commit.](http://commits.kde.org/kwin/41f533550a8eea82540d317f12c247e20094ba2d) Fixes bug [#464572](https://bugs.kde.org/464572)
+ Wayland: Remove SeatInterfacePrivate::accumulatedCapabilities. [Commit.](http://commits.kde.org/kwin/2ea9d7d8a5575e8e72b4e285c1ebf6aaf218021d) 
+ Match pointer/keyboard/touch lifespan to Seat lifespan. [Commit.](http://commits.kde.org/kwin/cc43cd126f21e5a37424c2f41e6d7a015e918118) 
+ Add support for Lima, V3D, VC4 (based on https://github.com/OpenMandrivaAssociation/kwin/blob/master/kwin-5.21.4-add-support-for-panfrost-driver.patch made by  Bernhard Rosenkraenzer) and update list of supported devices for Panfrost. [Commit.](http://commits.kde.org/kwin/eef9bd5c4e564e9cbca6188c52cb3cee5ab9c825) 
+ X11: Drop xv-related workaround. [Commit.](http://commits.kde.org/kwin/f9add87ee15c9cdd701c3528d0c201d5b0c97208) 
+ XWayland: Don't dispatch xwayland events in QAbstractEventDispatcher sleeps. [Commit.](http://commits.kde.org/kwin/fdd37ec6f116f6bce38a93f8120965afe1e720d9) 
+ Tiling: Evacuate tiled windows from custom & quick tiling on output removal. [Commit.](http://commits.kde.org/kwin/c5df8cedd8ef0b5f1b6461e71fdec3f6a712289e) Fixes bug [#465522](https://bugs.kde.org/465522)
+ Wayland: Fix a typo in DataControlDeviceV1Interface::sendPrimarySelection(). [Commit.](http://commits.kde.org/kwin/9425aa87ab6311a22ddcc8988e855b036ddd8f60) 
+ Wayland: Version check before send_primary_selection calls. [Commit.](http://commits.kde.org/kwin/0bc6600ce88703843ca314b8b3aa96ea4e51a444) Fixes bug [#465657](https://bugs.kde.org/465657)
+ Set CXX standard for crossbuilding utility. [Commit.](http://commits.kde.org/kwin/ded282df2e682e5be98ee236a6ed2c011c6ad92e) 
+ Include missing header for std::round. [Commit.](http://commits.kde.org/kwin/a978f81eaa54eda5c77059324785594fab1f66a3) 
+ Fix button to Qt::MouseButton mapping. [Commit.](http://commits.kde.org/kwin/e0b289a6f1a665c77b1112049b3c70536770f27f) Fixes bug [#465463](https://bugs.kde.org/465463)
+ Backends/drm: set cursor again after it was hidden. [Commit.](http://commits.kde.org/kwin/5432ffb76d97f7d729e1b7dfaa8f3a724b36eac0) Fixes bug [#461181](https://bugs.kde.org/461181)
+ Screencast: Fix region screencasts top coordinate. [Commit.](http://commits.kde.org/kwin/8cddfe5be032d15b3e4c06ee9394ed0442cea607) 
{{< /details >}}

{{< details title="libkscreen" href="https://commits.kde.org/libkscreen" >}}
+ Fix potential crash setting new configs. [Commit.](http://commits.kde.org/libkscreen/d42dea00822c1cfb7478d1d55746e0750156918b) Fixes bug [#464590](https://bugs.kde.org/464590)
{{< /details >}}

{{< details title="libksysguard" href="https://commits.kde.org/libksysguard" >}}
+ Formatter: Properly extract strings to a translation catalog. [Commit.](http://commits.kde.org/libksysguard/68679d8ada54dfac5ce12491324f91d8572b789c) Fixes bug [#465281](https://bugs.kde.org/465281). Fixes bug [#465282](https://bugs.kde.org/465282)
{{< /details >}}

{{< details title="Plasma Desktop" href="https://commits.kde.org/plasma-desktop" >}}
+ Applets/taskmanager: don't honor groupPopups setting in IOTM form factor. [Commit.](http://commits.kde.org/plasma-desktop/089dee811f3d2ff3fdc0ac865aac97b1ca9c8699) Fixes bug [#464627](https://bugs.kde.org/464627)
{{< /details >}}

{{< details title="plasma-integration" href="https://commits.kde.org/plasma-integration" >}}
+ Codify runtime dependency on xdg-desktop-portal-kde. [Commit.](http://commits.kde.org/plasma-integration/bc1c5d66828429904ea9820154b72307d26a8529) Fixes bug [#466148](https://bugs.kde.org/466148)
+ Autotests: Link against KXmlGui. [Commit.](http://commits.kde.org/plasma-integration/193f9dd45200ce8468a0dec100ef7db41cc0a119) 
+ Autotests: Fix CMake code style (use some line breaks). [Commit.](http://commits.kde.org/plasma-integration/4549778f858d84ebae4b783268fd14962e588a19) 
+ Remove unused include. [Commit.](http://commits.kde.org/plasma-integration/3dc1a337d6f56506018ff89ea9cdc6136398a5b6) 
{{< /details >}}

{{< details title="plasma-mobile" href="https://commits.kde.org/plasma-mobile" >}}
+ Homescreens/halcyon: Remove drop shadow for placeholder message. [Commit.](http://commits.kde.org/plasma-mobile/3b769aabd1a8859ed0aa0ea88616c4544e4741e9) 
+ Revert "lockscreen: Refactor and lazy load notifications". [Commit.](http://commits.kde.org/plasma-mobile/e08f8f4a843b2cd7d69b053627408387aabfe214) 
+ Components: Remove splash screen close button support, due to crashing problems. [Commit.](http://commits.kde.org/plasma-mobile/b37605849795cc421361ed1663cf6781255b3f8d) 
+ Homescreens/halcyon: Fix configure screen showing up in task switcher. [Commit.](http://commits.kde.org/plasma-mobile/8a44dc53d288207e3f7b40d714a59c0f89b757c4) 
+ Quicksettings/screenrotation: Make available dbus call async. [Commit.](http://commits.kde.org/plasma-mobile/2c8819d18eee0e3e0864a4c51fd36fbb8ca417bb) 
+ Quicksettings/screenrotation: hide when not available. [Commit.](http://commits.kde.org/plasma-mobile/82871b897f621b3be381a52eed713265de46fb06) 
{{< /details >}}

{{< details title="Plasma Networkmanager (plasma-nm)" href="https://commits.kde.org/plasma-nm" >}}
+ Guard against double instantiation or double-free of PlasmaNM.NetworkModel. [Commit.](http://commits.kde.org/plasma-nm/224a1ab1f0ed1af105fa71bf59a7bac970de8407) Fixes bug [#465805](https://bugs.kde.org/465805)
+ Filter out the loopback device. [Commit.](http://commits.kde.org/plasma-nm/f8a864ebbb36866a3c25ca58ba5059b4a5272b5a) Fixes bug [#465655](https://bugs.kde.org/465655)
+ Revert "Use QWindow instead of KWindowSystem to set KeepAbove". [Commit.](http://commits.kde.org/plasma-nm/133ae5311f8553c16da63155ed06b05313988ec8) Fixes bug [#465751](https://bugs.kde.org/465751)
{{< /details >}}

{{< details title="plasma-welcome" href="https://commits.kde.org/plasma-welcome" >}}
+ Pages/PowerfulWhenNeeded: Fix grid by correctly setting cell width. [Commit.](http://commits.kde.org/plasma-welcome/02edd12674e45bcc8a2aa5c1a6fb138e3f3c774f) 
{{< /details >}}

{{< details title="Plasma Workspace" href="https://commits.kde.org/plasma-workspace" >}}
+ Sddm-wayland-session: Disable global shortcuts. [Commit.](http://commits.kde.org/plasma-workspace/ec26a10bf35511d9def5164d2e1ed822aa3d3055) 
+ Kcms/users: Port Connections to modern syntax. [Commit.](http://commits.kde.org/plasma-workspace/9529b0c6d9239ac613c874622dfd566ac42edd68) 
+ Revert "Revert "startplasma-wayland: Don't set GDK_SCALE and GDK_DPI_SCALE"". [Commit.](http://commits.kde.org/plasma-workspace/14c5b464f41f2b858453fd102ed57397755f9c26) See bug [#465733](https://bugs.kde.org/465733). See bug [#443215](https://bugs.kde.org/443215)
+ Better screen removal handling. [Commit.](http://commits.kde.org/plasma-workspace/8bc6be2f1af41274d8209272fca58ec7fe454416) Fixes bug [#465892](https://bugs.kde.org/465892)
+ Applets/digital-clock: fix logic error in tooltip. [Commit.](http://commits.kde.org/plasma-workspace/4c6c71fd43d70297eafc845e4fcdffda72edeb9b) Fixes bug [#465873](https://bugs.kde.org/465873)
+ Startplasma-wayland: allow people to override GDK_* envars. [Commit.](http://commits.kde.org/plasma-workspace/20c7e8826ca846b5155f17d628143e4546998c1c) Fixes bug [#443215](https://bugs.kde.org/443215)
+ Fix search for DWD weather data engine. [Commit.](http://commits.kde.org/plasma-workspace/68eb1ca872dd1f62f0471c5b9d9729d6eb783d76) 
+ Revert "startplasma-wayland: Don't set GDK_SCALE and GDK_DPI_SCALE". [Commit.](http://commits.kde.org/plasma-workspace/ba49bb121d7d6752b61aec29ae01a13e7978bd26) Fixes bug [#465733](https://bugs.kde.org/465733). See bug [#443215](https://bugs.kde.org/443215)
+ Applets/mediacontroller: add test for interacting with MPRIS2 interface. [Commit.](http://commits.kde.org/plasma-workspace/65add860d3a0ead003d33e2faf9f5d54c990bab7) 
+ Kcms/region_language: Down highlight delegate on press. [Commit.](http://commits.kde.org/plasma-workspace/e14e73c003fde67546f38e08f002c1a1655d70ef) 
+ If the desktopview is not deleted, don'r emit screenRemoved. [Commit.](http://commits.kde.org/plasma-workspace/e1969e8452a15f9c6b9d448fb358496e79811a50) Fixes bug [#465536](https://bugs.kde.org/465536)
+ Set LANG if we have language to glibc locale mapping. [Commit.](http://commits.kde.org/plasma-workspace/bd90d2b3cf28a53e2e27841b8b05f5b5ff99471e) Fixes bug [#464983](https://bugs.kde.org/464983)
+ Plasmacalendarintegration: Omit astronomical events from Holidays data. [Commit.](http://commits.kde.org/plasma-workspace/2a6f08ad25e75f9d2e01387b3d7e88a84def4669) Fixes bug [#465539](https://bugs.kde.org/465539)
+ Shell: read thickness from default group. [Commit.](http://commits.kde.org/plasma-workspace/8631d249baee3f0a0597e2f2195f92880e65df33) See bug [#465125](https://bugs.kde.org/465125). See bug [#464628](https://bugs.kde.org/464628)
+ Use KDE_INSTALL_LIBEXECDIR. [Commit.](http://commits.kde.org/plasma-workspace/785500edef6acab215f4d2ee923568df64cc1077) 
{{< /details >}}

{{< details title="Powerdevil" href="https://commits.kde.org/powerdevil" >}}
+ Only skip batteries if neither start or stop thresholds are available. [Commit.](http://commits.kde.org/powerdevil/eed8ef2f06f29b1c8983b3af950349b7d603bb7f) Fixes bug [#464535](https://bugs.kde.org/464535)
{{< /details >}}

{{< details title="xdg-desktop-portal-kde" href="https://commits.kde.org/xdg-desktop-portal-kde" >}}
+ Extract i18n from QML files. [Commit.](http://commits.kde.org/xdg-desktop-portal-kde/ac9f1e2b35a367e79f09239d0aa6ec328653c918) 
+ Appchooserdialog: show all apps while searching. [Commit.](http://commits.kde.org/xdg-desktop-portal-kde/9d9546963f777f0084bbac0ba32bb03285cc13da) Fixes bug [#464521](https://bugs.kde.org/464521)
{{< /details >}}

