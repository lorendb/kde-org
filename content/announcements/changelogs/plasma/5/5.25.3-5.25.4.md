---
title: Plasma 5.25.4 complete changelog
version: 5.25.4
hidden: true
plasma: true
type: fulllog
---
{{< details title="Breeze" href="https://commits.kde.org/breeze" >}}
+ Kstyle: Use menuTitle font metrics for size calculation. [Commit.](http://commits.kde.org/breeze/04d7c7edc32420f543787aa295b4e871d0353852) Fixes bug [#443805](https://bugs.kde.org/443805)
{{< /details >}}

{{< details title="Discover" href="https://commits.kde.org/discover" >}}
+ Fix crash after update. [Commit.](http://commits.kde.org/discover/4323e71fd9ed07e8cb120dd80cb5672912dc8b66) Fixes bug [#457278](https://bugs.kde.org/457278)
+ Do not change the sorting among resources in the default backend. [Commit.](http://commits.kde.org/discover/d11614d5f2c929374bd7c6f06f120f479561efb6) Fixes bug [#451667](https://bugs.kde.org/451667)
+ Sources: Give a minimum size to section headers. [Commit.](http://commits.kde.org/discover/6d26871651e6c58fdaa52b98e6a9fafcd7148d02) Fixes bug [#434349](https://bugs.kde.org/434349)
+ Fwupd: Make sure we don't crash trying to print an error. [Commit.](http://commits.kde.org/discover/4ec83193536ef0eba97b8975b5f300530e7e537f) Fixes bug [#455132](https://bugs.kde.org/455132)
+ Kns: Make KNSReview aware of its possible lack of AtticaProvider. [Commit.](http://commits.kde.org/discover/5bf1db7242da5649d17aa289633f2601ec57cc58) Fixes bug [#457145](https://bugs.kde.org/457145)
+ AppListPage: Make sure the PlaceholderMessage doesn't get in the way. [Commit.](http://commits.kde.org/discover/27fc70cdef7740ca80d9ba33673242f6de7d3135) Fixes bug [#457029](https://bugs.kde.org/457029)
+ Appstream: Do not treat spdx operators as licenses. [Commit.](http://commits.kde.org/discover/aa9192f8574e02b4c0ac80c7bd744a9e7c22b00f) 
+ Appstream: Treat unknown licences. [Commit.](http://commits.kde.org/discover/678c0f6533c5cf46d405f0d2a6c659fb67ebca5c) 
+ Kns: join into the AppStreamUtils::license party. [Commit.](http://commits.kde.org/discover/3c5a7309b2267d9c9da9f878f94061ee6377c901) 
+ Pk: Try harder to convert non-appstream packages licences. [Commit.](http://commits.kde.org/discover/8c6160822d83e0825fc49fcc4ac5af160d2fe9ec) Fixes bug [#454480](https://bugs.kde.org/454480)
+ Pk: Allow some error codes from offline updates. [Commit.](http://commits.kde.org/discover/c3495090b658337eec406ea637e66d0eb8c79936) Fixes bug [#443090](https://bugs.kde.org/443090)
+ Snap: Do not install a categories file. [Commit.](http://commits.kde.org/discover/5f08fb9a6702c50c6d91aa680c23694cca0b8ef9) Fixes bug [#456889](https://bugs.kde.org/456889)
+ Do not warn anymore about missing categories. [Commit.](http://commits.kde.org/discover/d747367d7c3f46e96b7c2bf988299bbf598bd8e3) 
+ Odrs: Make sure we don't fail when the application page is opened early. [Commit.](http://commits.kde.org/discover/55ee333e7a22893bd3cf1b1212b98504694cb928) Fixes bug [#426270](https://bugs.kde.org/426270)
+ Fix submitting usefulness. [Commit.](http://commits.kde.org/discover/9f06c84d9ecd035c730c8720ad01d2fa8d39848b) 
{{< /details >}}

{{< details title="Plasma Addons" href="https://commits.kde.org/kdeplasma-addons" >}}
+ Applets/calculator: read the answer after pressing `=`. [Commit.](http://commits.kde.org/kdeplasma-addons/aaa73ed4d3b164d99aa31d624f82ecbb0f6d8e97) 
+ Applets/calculator: move focus to the corresponding button when pressing keys. [Commit.](http://commits.kde.org/kdeplasma-addons/ca7f228e6b01b22c8b79aa9ce338acd3aff00a0e) 
+ Applets/colorpicker: press space to open color menu. [Commit.](http://commits.kde.org/kdeplasma-addons/665054c7237ab2d310ac06f76fab2aaf82b12b98) 
+ Applets/colorpicker: add name to color rectangle. [Commit.](http://commits.kde.org/kdeplasma-addons/174a478f314a196dd0710887fcac96107d722e25) 
+ Applets/fuzzy-clock: add a11y properties. [Commit.](http://commits.kde.org/kdeplasma-addons/626df45a82c38bfde4cacdce6a3f9d379c1384a9) 
+ [applets/comic] Fix configurationRequired and busy state when deactivating the last provider. [Commit.](http://commits.kde.org/kdeplasma-addons/6ea5314883abb4833949192dccd5deb6ea75e22b) 
+ [applets/comic] Update providers list after (un)installing provider. [Commit.](http://commits.kde.org/kdeplasma-addons/3ba869c3c523098a7df6875f74276de2492da3e7) 
+ Add missing include to array. [Commit.](http://commits.kde.org/kdeplasma-addons/cda2ff4bac2ba53963c7410f248eec8c25cec0c9) 
+ [applets/comic] Disable most context menu actions when comic is not ready. [Commit.](http://commits.kde.org/kdeplasma-addons/8ef610fb4ea2a5d45dbb573ac48adc569a1a5d21) Fixes bug [#406991](https://bugs.kde.org/406991)
+ Remove clipPath portion of SVG. [Commit.](http://commits.kde.org/kdeplasma-addons/3854385a81922824bf7ac3ccf4a544fb943a2b5c) Fixes bug [#399568](https://bugs.kde.org/399568)
{{< /details >}}

{{< details title="Info Center" href="https://commits.kde.org/kinfocenter" >}}
+ Add missing include on <array>. [Commit.](http://commits.kde.org/kinfocenter/c4c3d635f362907cebbacd2af074f81d13cc16f8) 
{{< /details >}}

{{< details title="KWin" href="https://commits.kde.org/kwin" >}}
+ Update QtQuick views at the start of the frame. [Commit.](http://commits.kde.org/kwin/6fd60861011ff1d57b1f1848985cac5aa8078690) 
+ Xkb: Use Ctrl+Mrys+Zenkaku_Hankaku to toggle the touchpad. [Commit.](http://commits.kde.org/kwin/9d7b7ad08102487ba233318c782e1eac576aaf59) 
+ Input: don't forward gestures used by KWin to applications. [Commit.](http://commits.kde.org/kwin/31b13af3612c305fde40ace9c8654402090136a0) Fixes bug [#455952](https://bugs.kde.org/455952)
+ Effects/private: Raise currently dragged window higher than anything. [Commit.](http://commits.kde.org/kwin/957ff80a3b37f46c8afa9aa15808c47a35385aa2) Fixes bug [#456936](https://bugs.kde.org/456936)
+ Fix activity swtiching through UserActions menu. [Commit.](http://commits.kde.org/kwin/7ccc489cb8a74166b06150624fbb9728ab25ea63) Fixes bug [#456873](https://bugs.kde.org/456873)
+ Backends/drm: fix build. [Commit.](http://commits.kde.org/kwin/dd4148050772e28dae8000d3a47704bd610eb8d0) 
+ Backends/drm: fix memory leak. [Commit.](http://commits.kde.org/kwin/cad6b100b5a4f64384592fec5471ab147f999237) 
+ Backends/drm: explicitly initialize all fields of drmModeModeInfo. [Commit.](http://commits.kde.org/kwin/2a2bb539c44c097f8e8f3624d2861d7f7197e308) 
+ Effects/colorpicker: Fix picking colors. [Commit.](http://commits.kde.org/kwin/420636a2a1300423212bda751097d267bd214d7d) Fixes bug [#454974](https://bugs.kde.org/454974)
+ Wayland/tablet_v2: Keep also the pad surface in a QPointer. [Commit.](http://commits.kde.org/kwin/ea28596a1f8b97687721f527398c69542b3940c1) Fixes bug [#456817](https://bugs.kde.org/456817)
+ Support keyboard navigation between windows across desktops. [Commit.](http://commits.kde.org/kwin/84277885b94485b5a0623629841acb3d5e02590a) Fixes bug [#456068](https://bugs.kde.org/456068)
+ Make DesktopView a FocusScope. [Commit.](http://commits.kde.org/kwin/9ac47ae068aee9b2015f7c7bc235dda958950edc) 
+ Accept keys in windowheap conditionally. [Commit.](http://commits.kde.org/kwin/9e75112967e03d30dcffb28874a2fc9f70694c9b) 
+ Accept keys in windowheap conditionally. [Commit.](http://commits.kde.org/kwin/8ca7bb1369931e3b5adaa741fd5f2caac393af36) 
+ Activation: Be liberal about the StartupWMClass. [Commit.](http://commits.kde.org/kwin/ac4fe1a8ec1c053524a2a9079a3da04ae471f40a) 
+ Activation: Fix activation notification of Xwayland clients. [Commit.](http://commits.kde.org/kwin/81ff7737294ee1e46b9d9280339134101010e51c) Fixes bug [#455265](https://bugs.kde.org/455265)
+ Activation: Simplify icon loading logic. [Commit.](http://commits.kde.org/kwin/ff3731972db67770d567ad951bf492d37e841355) 
+ X11: Fix shading with non-zero border. [Commit.](http://commits.kde.org/kwin/1c5215009865c20b18c0f3114b167080cd70a33a) Fixes bug [#450582](https://bugs.kde.org/450582)
+ Backends/drm: don't crash if importing a texture fails. [Commit.](http://commits.kde.org/kwin/ee1c68e9533cdfe364af161d0dac60d0f4625aae) See bug [#456500](https://bugs.kde.org/456500)
+ Swapping desktops: only swap windows on current activity. [Commit.](http://commits.kde.org/kwin/92866c986cccd0c40c3514b107b4837fa93af35c) Fixes bug [#386769](https://bugs.kde.org/386769)
+ Don't use Plasma-themed icons in Present Windows. [Commit.](http://commits.kde.org/kwin/f32e17313c657b711c972051d594548a578b46cf) Fixes bug [#455368](https://bugs.kde.org/455368)
+ TabBox: Fix loading a different switcher after one has failed. [Commit.](http://commits.kde.org/kwin/ebdab44c31efe4ad760951e5811e635089e51918) Fixes bug [#445455](https://bugs.kde.org/445455)
+ Backends/drm: handle broken legacy drivers better. [Commit.](http://commits.kde.org/kwin/f3c1facdc477a18ff5c528972ffef5d09a3a6a35) Fixes bug [#453860](https://bugs.kde.org/453860). See bug [#456306](https://bugs.kde.org/456306)
+ Wayland/drmlease: split up DrmLeaseV1Interface::deny. [Commit.](http://commits.kde.org/kwin/cfb8140f7a2f78d160901b895acedc9e1628930b) 
+ Wayland/drmlease: correct DrmLeaseDeviceV1Interface::setDrmMaster. [Commit.](http://commits.kde.org/kwin/75cb0d8807aa2d099ee497707629fd62cfeb8028) 
+ Drmlease: send device done event correctly. [Commit.](http://commits.kde.org/kwin/fced7e33b34489537a5d18f11add55199746ccea) 
{{< /details >}}

{{< details title="oxygen-sounds" href="https://commits.kde.org/oxygen-sounds" >}}
+ Update kf5 version requirement to 5.94. [Commit.](http://commits.kde.org/oxygen-sounds/a8786bf95003bde03df699e016a9ce510f86c502) 
{{< /details >}}

{{< details title="Plasma Desktop" href="https://commits.kde.org/plasma-desktop" >}}
+ Fix mouse settings not being loaded when a mouse is connected. [Commit.](http://commits.kde.org/plasma-desktop/74701a740d8b61af3296417be447ae613b0c8f23) Fixes bug [#435113](https://bugs.kde.org/435113)
+ Applets/kimpanel: add accessible properties. [Commit.](http://commits.kde.org/plasma-desktop/502bc173936d43b5514fa211410d77bc1215fdf8) 
+ Applets/kimpanel: add keyboard navigation support. [Commit.](http://commits.kde.org/plasma-desktop/e377519b502e0c25caae0f16dd0ede5d06855cb7) 
+ Applets/kickoff: add text to allow screen reader to read button name. [Commit.](http://commits.kde.org/plasma-desktop/620d640e5f1e6ab7fde6965b6fd09600f290c501) 
+ Applets/taskmanager: use `model.display` in accessible properties. [Commit.](http://commits.kde.org/plasma-desktop/a4fbd4b8e48666939c0d01bf5fbdf829880605c3) 
+ [applets/pager] Fix button group management on configuration page. [Commit.](http://commits.kde.org/plasma-desktop/805967f00b6bd9deb97a5938977190d34f58da80) Fixes bug [#456525](https://bugs.kde.org/456525)
+ Applets/taskmanager: press `Esc` to close group dialog. [Commit.](http://commits.kde.org/plasma-desktop/55796327a3c63a95a34c9b29c66d0829a88f98b1) 
+ [kcms/landingpage] Strip whitespace at the beginning of a telemetry description. [Commit.](http://commits.kde.org/plasma-desktop/c569505c1b84638353a899d0d5e8b80708211631) 
{{< /details >}}

{{< details title="Plasma Firewall" href="https://commits.kde.org/plasma-firewall" >}}
+ Un-squash advanced rule editor layout. [Commit.](http://commits.kde.org/plasma-firewall/74375115f2ca2c1c6b24bb1df540e1481ed9b309) Fixes bug [#456603](https://bugs.kde.org/456603)
{{< /details >}}

{{< details title="Plasma Audio Volume Control" href="https://commits.kde.org/plasma-pa" >}}
+ VolumeMonitor: Don't set stream on source output monitor. [Commit.](http://commits.kde.org/plasma-pa/c043001f48380da8ab89030d7c1d7323ef3d3560) 
{{< /details >}}

{{< details title="Plasma Workspace" href="https://commits.kde.org/plasma-workspace" >}}
+ KRunner: Set location before showing. [Commit.](http://commits.kde.org/plasma-workspace/eb44145d5797037348462b5e0cae309d20e52d3b) Fixes bug [#447096](https://bugs.kde.org/447096)
+ Wallpaper/slideshow: shouldn't display "current" item in image list. [Commit.](http://commits.kde.org/plasma-workspace/dfefe6139433dd9ef27209d25c528dd720baffdb) Fixes bug [#457327](https://bugs.kde.org/457327)
+ Disable toplevel fixed positions on Wayland. [Commit.](http://commits.kde.org/plasma-workspace/2e6ef9ff35ac5a53864b944592a621e7ea89670a) 
+ Runners/helprunner: Fix launching of plasmasearch kcm with args. [Commit.](http://commits.kde.org/plasma-workspace/2fdaebc00660557f0326158e183a164fcfb870d1) 
+ [kicker/kickoff] Fix urls for search results. [Commit.](http://commits.kde.org/plasma-workspace/518426c4a049c185957a3b9daaa0f9545a0b4169) Fixes bug [#456984](https://bugs.kde.org/456984)
+ Applets/icon: fix invalid `Accessible.description`. [Commit.](http://commits.kde.org/plasma-workspace/cc351474fe4d142f0bae50786570cad7a09de406) 
+ Applets/devicenotifier: improve accessible properties. [Commit.](http://commits.kde.org/plasma-workspace/0d07a25a7e56b9b74fdf1be015c9f4bfc20c88bf) 
+ Applets/batterymonitor: add accessible properties to slider. [Commit.](http://commits.kde.org/plasma-workspace/6f1537e529f44ad276b5c067969846e25a4ad62a) 
+ [FIX] Unable to remove manually added wallpaper. [Commit.](http://commits.kde.org/plasma-workspace/d6d47393bab32dc60b43e0eeac16c035000a0358) Fixes bug [#457019](https://bugs.kde.org/457019)
+ Kcms/colors: make sure the preview uses the right colours. [Commit.](http://commits.kde.org/plasma-workspace/21905cfe068326ccc5763b373ec5e6c796f1586d) Fixes bug [#456648](https://bugs.kde.org/456648)
+ Don't break configuration when saving layouts. [Commit.](http://commits.kde.org/plasma-workspace/b06fc87d5abf4fe6e8c633355d190c7c1e3eeccd) 
+ [Notifications] When there is no thumbnail available, use file icon as drag pixmap. [Commit.](http://commits.kde.org/plasma-workspace/afcf59b005a82d5e0896b2fbff59c56cd8524c31) 
+ [Notifications] Ignore stopped job in job aggregator. [Commit.](http://commits.kde.org/plasma-workspace/5c5b1ca2b998a618ebf7580c51111bfc38da2e78) 
+ [applet/{analog,digital}-clock] Use `onPressed: wasExpanded = ...` idiom. [Commit.](http://commits.kde.org/plasma-workspace/b37f46d5743a5412f2cd539b9a549f44c80b992f) 
+ Fix non-functional lockscreen due to bad cherry-pick. [Commit.](http://commits.kde.org/plasma-workspace/bfb47cb202784733618e53e1088e8676c2815456) Fixes bug [#456639](https://bugs.kde.org/456639)
{{< /details >}}

{{< details title="Powerdevil" href="https://commits.kde.org/powerdevil" >}}
+ Update battery notifications if they remain open. [Commit.](http://commits.kde.org/powerdevil/ee3d3ea6e0d3019c6585608d3a0d14b71556dc7b) 
+ When battery drains, show a notification even when AC is plugged in. [Commit.](http://commits.kde.org/powerdevil/8011d652d398896d023088232669b24d9129871b) 
{{< /details >}}

{{< details title="System Settings" href="https://commits.kde.org/systemsettings" >}}
+ Fix showing translations for kinfocenter. [Commit.](http://commits.kde.org/systemsettings/d00fac532b581ee97d8ce527d45fd3e25f1f1753) Fixes bug [#453835](https://bugs.kde.org/453835)
{{< /details >}}

