---
aliases:
- /announcements/plasma-5.20.4-5.20.5-changelog
hidden: true
plasma: true
title: Plasma 5.20.5 complete changelog
type: fulllog
version: 5.20.5
---

{{< details title="bluedevil" href="https://commits.kde.org/bluedevil" >}}
+ Show only paired devices in KCM and applet. [Commit.](http://commits.kde.org/bluedevil/de1681fbe2ed6ca502c72c2fe78bcd3df91ca332) Fixes bug [#422383](https://bugs.kde.org/422383). Fixes bug [#416322](https://bugs.kde.org/416322)
{{< /details >}}

{{< details title="discover" href="https://commits.kde.org/discover" >}}
+ Pk: Have the notifier listen to the correct for offline events. [Commit.](http://commits.kde.org/discover/9e9e8dbd484ac2eb13b4b189e2e810330d1dc3be) 
+ ApplicationPage: Fix license button tooltip. [Commit.](http://commits.kde.org/discover/996233cef8e9cb66f08484b0d9107755ffd23ad7) 
{{< /details >}}

{{< details title="kde-gtk-config" href="https://commits.kde.org/kde-gtk-config" >}}
+ Set correct installation directory for kconf_update script. [Commit.](http://commits.kde.org/kde-gtk-config/dc5d5404e8bf614ef0b7652ec6818b3558403eba) 
+ Use only GTK Module for managing window decorations CSS. [Commit.](http://commits.kde.org/kde-gtk-config/fb0bb26b2f879fc5e2bc6feafe0fa4fe7b278bea) Fixes bug [#428322](https://bugs.kde.org/428322). Fixes bug [#428120](https://bugs.kde.org/428120). Fixes bug [#428842](https://bugs.kde.org/428842)
{{< /details >}}

{{< details title="kdeplasma-addons" href="https://commits.kde.org/kdeplasma-addons" >}}
+ Fix weather widget crash when going to Units tab for the second time. [Commit.](http://commits.kde.org/kdeplasma-addons/1dbb185725e365d3f375740eb3ddcc00afbf4cd1) Fixes bug [#419709](https://bugs.kde.org/419709)
+ The comic applet previously attempted to use the url associated with the. [Commit.](http://commits.kde.org/kdeplasma-addons/b5c50cfb94bd00752bba3a37192968333d5bfbe6) 
{{< /details >}}

{{< details title="kwin" href="https://commits.kde.org/kwin" >}}
+ Scene: Fix window pixmap traversal order. [Commit.](http://commits.kde.org/kwin/e891537d157aadf67320854a680c55216d30a294) 
+ Screencasting: fix build. [Commit.](http://commits.kde.org/kwin/28e1421c1e9ab2132430de6373b4501d298e5a9a) 
+ Screencast: support BGRx format for backwards compatibility with WebRTC. [Commit.](http://commits.kde.org/kwin/baa3a4c79185088346fe3c9a456b503a8eb80de8) 
{{< /details >}}

{{< details title="libksysguard" href="https://commits.kde.org/libksysguard" >}}
+ Always emit sensorMetaDataChanged() when receiving a metaData update. [Commit.](http://commits.kde.org/libksysguard/58fa686a201beabe7c1cf6e6fa9f8bbf19eefe7d) 
{{< /details >}}

{{< details title="plasma-desktop" href="https://commits.kde.org/plasma-desktop" >}}
+ Revert: bf7d64b3 . Fixes panel height adjusting fails when the panel is on top / left. [Commit.](http://commits.kde.org/plasma-desktop/687935970717d9bb57b364ee95baae57d7f1b266) Fixes bug [#430050](https://bugs.kde.org/430050)
+ Reparse the key repeat rate config when we try to load it. [Commit.](http://commits.kde.org/plasma-desktop/35c2ec86680f0feedaaeecb3b54bf2b9ddce6541) Fixes bug [#418175](https://bugs.kde.org/418175)
+ Fix the order of the actions of the emojier. [Commit.](http://commits.kde.org/plasma-desktop/366ce555b95d97fd31856c9e7e6bf9e57b619605) 
+ [kcms/users]: Make sure avatar grid doesn't escape bounds. [Commit.](http://commits.kde.org/plasma-desktop/05c7572bdc10f25cd21cf1a8e52a4d1b7a5dc0d4) Fixes bug [#428899](https://bugs.kde.org/428899)
+ Use Instantiator to create drawer actions to improve startup time. [Commit.](http://commits.kde.org/plasma-desktop/bbf7543bf5a99887c7f2944134b6bdaa279735a2) See bug [#429855](https://bugs.kde.org/429855)
+ [kcms/users] Avoid race condition on startup. [Commit.](http://commits.kde.org/plasma-desktop/036fd9d18cef1d33f330ae91b9b272089493caa3) Fixes bug [#429314](https://bugs.kde.org/429314)
{{< /details >}}

{{< details title="plasma-disks" href="https://commits.kde.org/plasma-disks" >}}
+ Bump smartctl timeout in helper. [Commit.](http://commits.kde.org/plasma-disks/3930245874ead2dead32b04b2af9442bf2f04141) Fixes bug [#428844](https://bugs.kde.org/428844)
{{< /details >}}

{{< details title="plasma-nm" href="https://commits.kde.org/plasma-nm" >}}
+ Fix password entry jumping to different networks with wifi scanning, by pausing the scan when appropriate. [Commit.](http://commits.kde.org/plasma-nm/21a410ff77049e996df5fdc35215c4b30d893ccc) 
{{< /details >}}

{{< details title="plasma-pa" href="https://commits.kde.org/plasma-pa" >}}
+ [kcm] Read text color from proper theme. [Commit.](http://commits.kde.org/plasma-pa/099e925c879ece8f90734ea66c0878bc174c9608) 
{{< /details >}}

{{< details title="plasma-workspace" href="https://commits.kde.org/plasma-workspace" >}}
+ Recent Documents Runner: Strip trailing slash in subtext. [Commit.](http://commits.kde.org/plasma-workspace/29f8c8818053a02890e1c63547ef20245187b13c) 
+ [applets/systemtray] Fix missing expander arrow. [Commit.](http://commits.kde.org/plasma-workspace/184cc5c3f9373e43cf8d55c415b7078ab375c756) Fixes bug [#430569](https://bugs.kde.org/430569)
+ [Notifications] Check popup being null. [Commit.](http://commits.kde.org/plasma-workspace/341da657b4badac4441a0074a0ee62550945d3a9) 
+ Move keyboard positioning in the keyboard itself. [Commit.](http://commits.kde.org/plasma-workspace/8c267852a56c74c14a471d266f87ac867b8276d9) Fixes bug [#427934](https://bugs.kde.org/427934)
{{< /details >}}