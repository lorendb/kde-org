---
qtversion: 5.15.2
date: 2022-12-10
layout: framework
libCount: 83
---


### BluezQt

* Expose battery of a DeclarativeDevice via property
* Fix QML plugin when building project statically (bug 459340)

### Breeze Icons

* Add SimpleScreenRecorder icon (bug 412490)
* application-x-executable: make symbolic icon follow color scheme
* Add touchscreen icons to devices and preferences (bug 461562)

### Extra CMake Modules

* Add ZSHAUTOCOMPLETEDIR to KDEInstallDirs
* Fix ECMQmlModule for static plugins (bug 459340)
* Watch for language changes to load the new appropiate qm files

### KDE Doxygen Tools

* Update CMakeLists.txt - The majority of frameworks use KF_VERSION to indicate current version

### KCodecs

* Fix KCharset::codecForName deprecation version
* Deprecate QTextCodec public api

### KConfig

* Apply existing  QScreen::name() workaround for Windows to new code (bug 429943)
* Fix parser treating empty IntList default as a list of single zero element: [0]

### KConfigWidgets

* KHamburgerMenu: Reset when menu bar contents change
* [KRecentFilesAction] Fix broken recent files list (bug 461448)

### KCoreAddons

* [KJobTrackerInterface] qRegisterMetatype KJob::Unit

### KDeclarative

* Deprecate unused parts of KWindowSystem import
* SimpleKCM: Account for flickable's margins when deciding to show separators
* Deprecate QIconItem
* Use const/let for JS variables as appropriate
* Sync header and footer separator code to SimpleKCM (bug 461435)

### KDED

* Watch for icons directory changes from KDED (bug 455702)

### KDELibs 4 Support

* Fix dependency name

### KDesignerPlugin

* Fix localization files install path

### KDESU

* Include sys/prctl.h only on Linux

### KFileMetaData

* FFmpegExtrator: Try stream metadata if no global metadata is available
* office2007extractor: Extract text document line count
* office2007extractor: Update docx to include more properties
* Add FictionBook2 extractor

### KGlobalAccel

* Fix key events for modifiers
* Fix dialog's text
* Set correct version for kglobalacceld

### KDE GUI Addons

* KeySequenceRecorder: Stop tracking recordings when we stop recording (bug 462100)
* Make KColorUtils::mix account for alpha properly
* Don't build/install the geo: URI handler on Android

### KI18n

* kcountrysubdivisiontest fix "Missing reference in range-for"
* Set the LANG to an actual valid locale

### KIdleTime

* Port KIdleTime to ext_idle_notifier_v1

### KImageFormats

* Fix missing DCI-P3 color space set
* raw: LibRaw_QIODevice::read: fixed possible partial reading of an item
* PSD multichannel testcases
* Support to MCH with 4+ channels (treat as CMYK)
* avif: Check if encoder/decoder is available in capabilities()
* Fix condition for installing desktop files

### KIO

* kproxydlg.ui the web page https://konqueror.org does not have a FAQ section
* Remove default Search items from Places panel
* Ignore finished() from the timeoutSpecialCommand (bug 459980)
* KCoreUrlNavigator: fix header include path, add backward-compat headers
* Deprecate SlaveBase in favour of WorkerBase
* Fix header install location for KFileFilter
* Wrap IdleSlave in visibility deprecation macros
* Introduce KFileFilter class to parse and store filters
* Deprecate ForwardingSlaveBase in favour of ForwardingWorkerBase
* Introduce a new ForwardingWorkerBase
* KUrlNavigatorPlacesSelector: Use protocol icon as fallback
* Deprecate KProtocolInfo::slaveHandlesNotify()
* KIO::createDefaultJobUiDelegate Add missing @since
* Deprecate TCPSlaveBase in favour of TCPWorkerBase
* KProtocolInfoTest: adapt check for zip protocol support to JSON metadata
* Deprecate left-over no-op method SimpleJob::storeSSLSessionFromJob(...)
* Deprecate ThumbCreator and its variants
* RenameDialog: all widgets should be parented to the dialog itself
* Add KProtocolManager::workerProtocol() to match new lingo
* Add Scheduler methods for workers on hold to match new lingo
* Add KProtocolInfo::maxWorkers()/maxWorkersPerHost() to match new lingo
* Emit deprecation warning for ioslave_defaults.h
* Rename http_slave_defaults.h to http_worker_defaults.h, following port

### Kirigami

* InlineMessage: Fix corner case glitch when component is sized almost enough to fix the label
* Add ToolTip for action in PasswordField
* ImageColors: load source image asynchronously
* ImageColors: reset palette when source image is null
* ImageColors: use OpenMP to count pixels in parallel
* ImageColors: support url as source
* OverlaySheet: Fix gaps in footer
* CardsGridView: Do not set Infinity as an int value
* ApplicationHeader: Fix not loading
* Fix many memory leaks caused by `Qt.createComponent`
* UrlButton: improve accessible properties when text is different from url
* LinkButton: allow focus and add keyboard navigation
* More checks when not to diplay fabs
* PageRow: Introduce ability to automatically pop invisible pages
* icon: account window dpi when calculating actualSize
* Fix pushing pagerow to layers
* DefaultListItemBackground: remove unused leadingWidth property
* CardsGridView: remove unnecessary code
* ActionTextField: allow to focus on action icons
* ActionTextField: show native shortcut text in tooltip
* ActionTextField: fix StandardKey not working
* src/controls/About*.qml correct web page for KDE donations
* Adjust to qrc file changes from https://invent.kde.org/frameworks/extra-cmake-modules/-/merge_requests/303
* Card: By default, decorate the card background as pressed if checked
* AboutItem: Make "Report a Bug" button also be a link
* AboutItem: Add Donate link
* ActionButton: Fix broken attached properties due to import changes
* Revert "icon: `itemSize` should be `size`"
* Add alias Kirigami for units calls where missing
* ContextIcon: add missing namespace for Kirigami
* OverlayDrawer + ContextDrawerActionItem: add missing namespace for Units
* UrlButton: import Kirigami for the base class

### KItemModels

* Also remove the custom target in BUILD_TESTING
* Make use of ecm_add_qml_module()

### KNewStuff

* Fix static compilation of KF5NewStuffWidgets module (bug 459125)
* Perform initialization in initialization list

### KNotification

* KNotification: Add hints property

### KQuickCharts

* Add CLASSNAME in ecm_add_qml_module method call

### KRunner

* QueryMatch: Avoid unneeded QVariant conversion to string when setting data
* cppcheck: Do not shadow outer functions with local variables
* cppcheck: Make constructors explicit
* Fix crash when deleting RunnerManager while jobs are running (bug 448825)

### KService

* KApplicationTrader: New method setPreferredService

### KTextEditor

* Convert part metadata to JSON
* Kate:TextLine: switch to std::shared_ptr
* Fix setting default mark when ctrl is pressed
* Fix caret with inline notes
* Add "Character" the "Insert Tab" action name
* Fix condition for installing desktop file

### KTextWidgets

* Fix double delete crash

### KWayland

* client: Add F_SEAL_SHRINK seal to shm pool backing file
* client: Expose concrete xdg-shell class implementations

### KWidgetsAddons

* Extend KRecentFilesMenu

### KWindowSystem

* Deprecate KWindowSystem::demandAttention
* Deprecate KWindowSystem::setIcons
* Move X11-specific KWindowSystem functions to new KX11Extras class
* xcb/kwindoweffects: Fix stripes when dpr is not an integer
* Deprecate KWindowSystem::setUserTime
* Deprecate KWindowSystem::setBlockingCompositing
* Deprecate KWindowSystem::icccmCompliantMappingState
* Deprecate KWindowSystem::lowerWindow
* Deprecate KWindowSystem::constrainViewportRelativePosition

### KXMLGUI

* Better/corrected wording as suggested by Felix Ernst
* Added a doc note how to get a menu's action

### Plasma Framework

* PC3: allow touch scrolling even when tablet mode is off (bug 461016)
* Import ManagedTextureNode from KDeclarative
* Expose minimum drawing size for FrameSvg and FrameSvgItem
* Fix the left element of dialogs background
* pc3: Simplify Button{Focus,Hover,Shadow}.qml
* pc3: Simplify Flat/RaisedButtonBackground
* extracomponents: Only create a button in PlaceholderMessage if needed
* pc3: Don't create an SVG in TextField just to check if a certain element exists
* Expose FrameSvg::hasElement on FrameSvgItem
* FrameSvg: Replace a bunch of check-and-fetch with fetch-and-validate
* extracomponents: Use attached property for ActionTextField tooltip
* FrameSvg: Store a few unchanging strings in updateSizes as static const
* FrameSvg: Use a single QString buffer when building names to look up in the SVG
* Rework Plasma theme mask corners and outlines (bug 417511)
* Support horizontal scroll in containmentactions
* appletquickitem 'ownLayout' local variable has same name as one of the class members
* IconItem: Fix icons after context loss events
* svgitem: do not upscale svg when using fractional scaling (bug 461682)
* Remove workaround for QTBUG-70481
* PC3: remove obsolete workaround in TextArea
* ToolTip: Do not show when empty
* declarative/core: Use two opacity nodes for fading in IconItem (bug 432999)
* availableScreenRect only for valid screens
* appletinterface: Expose the QAction::Priority enum values
* Handle tooltips with disabled (negative) timeout properly (bug 397336)

### Prison

* Fix copy/pasted license header, this needs to be MIT

### Purpose

* Don't delete config when job finishes

### QQC2StyleBridge

* TextArea: allow touch scrolling even when tablet mode is off (bug 461016)
* Redraw KQuickStyleItem when DPR changes
* Switch: Improve background contrast when in unchecked state
* styleitem: Don't destroy and recreate the style on every instance creation
* Fix blurry controls in QQuickWidget
* DislogButtonBox: Fix url value comparison with non-strict ==
* ToolTip: Do not show tooltips without text
* BusyIndicator: Synchronize to implementation in PlasmaComponents3

### Solid

* upower: Support UP_DEVICE_KIND_BLUETOOTH_GENERIC
* imobiledevice: Check error returned by idevice_new (bug 448329)
* UDisks2::DeviceBackend add udi in trace when failing to to get prop
* imobiledevice: Handle events in correct thread
* imobiledevice: Don't call udiPrefix() from constructor

### Syntax Highlighting

* TOML: add number prefix, inf, nan and more datetime format ; fix multi-strings closing (bug 462178)
* Python: add pyi extension (python interface)
* Javascript: remove previousDibling keyword (probably a typo for previousSibling)
* Add GPS Exchange Format (GPX) type
* CSS family: add properties, functions and fr unit ; remove FPWD and old proposed properties
* install a index.katesyntax when QRC_SYNTAX is OFF
* Make the Rust language mode use the cstyle indenter
* C23: missing wN length modifiers with d and i in printf_like
* CMake: remove duplicate <list> and <context>
* CMake: optimize by adding DetectIdentifier and DetectSpaces (~20% faster)
* CMake: replace some <WordDetect> with <keyword>
* Add syntax definition for CSV and TSV
* JSON: small optimization on number regex
* Go: improved support for literal numbers, add unicode escape characters and new predefined type (bug 459291)
* Dynamic RegExpr has its own type (~2.2% faster)
* XML: replace a dynamic StringDetect with DetectIdentifier contextually equivalent
* XML: character < in an ENTITY is highlighted as an error

### Security information

The released code has been GPG-signed using the following key:
pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure <faure@kde.org>
Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB
