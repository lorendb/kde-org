---
aliases:
- ../pinephone-plasma-mobile-edition
title: Introducing the PinePhone - KDE Community edition
date: 2020-11-15T12:00:00+01:00
screenshots:
  - url: 20201110_092718.jpg
    name: The hardware
  - url: plasma.png
    name: "Plasma mobile homescreen"
  - url: weather.png
    name: KWeather, Plasma mobile weather application
  - url: pp_calculator.png
    name: Kalk, a calculator application
  - url: pp_camera.png
    name: Megapixels, a camera application
  - url: pp_calindori.png
    name: Calindori, a calendar application
  - url: pp_kclock.png
    name: KClock
  - url: pp_buho.png
    name: Buho, a note taking application
  - url: pp_kongress.png
    name: Kongress
  - url: pp_okular01.png
    name: Okular Mobile, a universal document viewer
  - url: pp_angelfish.png
    name: Angelfish, a web browser
  - url: https://nxos.org/wp-content/uploads/2020/11/nota_1_2-min-1024x751.png
    name: Nota, a text editor
  - url: https://nxos.org/wp-content/uploads/2020/11/pix-1024x805.png
    name: Pix, another image viewer
  - url: pp_folders.png
    name: Index, a file manager
  - url: https://nxos.org/wp-content/uploads/2020/11/vvave-1024x724.png
    name: VVave, a music player
custom_about: true
custom_contact: true
images:
  - /announcements/plasma-mobile/pinephone-plasma-mobile-edition/preview.png
hide_title: true
---
{{< img alt="Pinephone KDE edition" src="preview.png" class="img-fluid" >}}

## Experience the future of KDE's open mobile platform

[KDE](https://kde.org/) and [Pine64](https://www.pine64.org/) are announcing today the imminent availability of the new [PinePhone - KDE Community edition](https://www.pine64.org/). This Pine64 PinePhone gives you a taste of where free mobile devices and software platforms are headed.

The PinePhone - KDE Community edition includes most of the essential features a smartphone user would expect and its functionalities increase day by day. You can follow the progress of the development of apps and features in the [Plasma Mobile blog](https://www.plasma-mobile.org/blog/).

[Plasma Mobile](https://www.plasma-mobile.org/) is a direct descendant from KDE's successful [Plasma desktop](https://kde.org/plasma-desktop/). The same underlying technologies drive both environments and apps like [KDE Connect](https://kdeconnect.kde.org/) that lets you connect phones and desktops, the [Okular](https://apps.kde.org/en/okular) document reader, the [VVave](https://vvave.kde.org/) music player, and others, are available on both desktop and mobile.

Thanks to projects like [Kirigami](https://develop.kde.org/frameworks/kirigami//) and [Maui](https://mauikit.org/), developers can write apps that, not only run in multiple environments but that also gracefully adapt by growing into landscape format when displayed on workstation screen and shrinking to portrait mode on phones. Developers are rapidly populating Plasma Mobile with essential programs, such as web browsers, clocks, calendars, weather apps and games, all of which are being deployed on all platforms, regardless of the layout.

The idea of having mobile devices that can display a full workstation desktop when connected to a monitor, keyboard and mouse, has been around for years and both the KDE Community and Pine64 have been working to make it a reality.

The PinePhone handset itself is also ready for convergence. You can use any USB-C dock to connect it to extra USB devices (mouse, keyboard, storage), external monitors or even to a wired network. In fact, the 3GB version of the PinePhone already comes with such a dock that provides two extra USB ports, a full-sized HD video port and an RJ45 port.

Talking of hardware, the PinePhone's kill switches, located under the back cover and above the removable battery, allow you to deactivate the modem, WiFi/Bluetooth, microphone and cameras, and are especially designed to help preserve your privacy. This matches very well [KDE's Community vision](https://community.kde.org/KDE/Vision) of "[a] world in which everyone has control over their digital life and enjoys freedom and privacy".

The PinePhone - KDE Community edition is also reasonably priced, allowing you to enjoy a device on which you can run Plasma Mobile during the next years. You can be one of the pioneers to test and mold with your feedback the mobile system of the future.

If you are interested in developing, you will be getting early access to an ambitious Free Software mobile platform and you can help contribute to Plasma Mobile and get a headway designing new apps that you can test and deploy right now.

### Specs

* Allwinner A64 Quad Core SoC with Mali 400 MP2 GPU
* 2GB/3GB of LPDDR3 RAM
* 5.95″ LCD 1440×720, 18:9 aspect ratio (hardened glass)
* Bootable micro SD
* 16GB/32GB eMMC
* HD Digital Video Out
* USB Type C (Power, Data and Video Out)
* Quectel EG-25G with worldwide bands
* WiFi: 802.11 b/g/n, single-band, hotspot capable
* Bluetooth: 4.0, A2DP
* GNSS: GPS, GPS-A, GLONASS
* Vibrator
* RGB status LED
* Selfie and Main camera (2/5Mpx respectively)
* Main Camera: Single OV6540, 5MP, 1/4″, LED Flash
* Selfie Camera: Single GC2035, 2MP, f/2.8, 1/5″
* Sensors: accelerator, gyro, proximity, compass, barometer, ambient light
* 3 External Switches: up, down and power
* HW switches: LTE/GNSS, WiFi, Microphone, Speaker, Cameras
* Samsung J7 form-factor 3000mAh battery
* The case is matte black finished plastic
* Headphone Jack

To find out more on how to order your PinePhone - KDE Community edition, visit [Pine64's site](https://www.pine64.org/).

{{< screenshots name="screenshots" >}}

### Press Contact

For more information, high resolution photographs and detailed specifications, please get in touch emailing [press@kde.org](mailto:press@kde.org) or [info@pine64.org](mailto:info@pine64.org).
