---
title: KDE for Scientists
description: For Scientists Using KDE Applications.
layout: scientists
images:
- /for/resources/forscientists-thumb.png
sassFiles:
- /scss/for/scientists.scss
other:
 - name: PyMOL
   description: Molecular visualization system
   link: https://pymol.org/2
   img: https://raw.githubusercontent.com/schrodinger/pymol-open-source/master/data/pymol/icons/icon2.svg
 - name: Coq
   description: Formal proof management system
   link: https://coq.inria.fr/
   img: https://coq.inria.fr/files/barron_logo.png
 - name: FreeCAD
   description: Parametric 3D CAD modeler
   link: https://www.freecadweb.org/
   img: /for/resources/freecad-logo.svg
 - name: Scilab
   description: Numerical computation
   link: https://www.scilab.org/
   img: /for/resources/scilab-logo.png

cantor:
 - description: |
     ### Rich command processing

     Cantor allows you to run commands from your favorite mathematical programming language assisted by syntax highlighting and tab-completion for functions, reserved words and variables. See the output of your work directly in the worksheet or export to PDF, LaTeX, plain-text, and Jupyter notebooks!
   src: https://cantor.kde.org/assets/img/processing.gif

 - description: |
     ### Quick variables management

     Manage all your variables from the built-in side panel. See the values, size, and type of variables utilized in the environment at a glance. In addition, it is possible to change the variables values from the side panel.
   src: https://cantor.kde.org/assets/img/variables.gif

plasma:
 - src: /for/resources/color-picker.png
   name: Color picker applet
   description: Capture colors from your screen and keep track of them

 - src: /for/resources/krita-dark-theme.png
   name: Neutral color support
   description: Plasma allows you to use a desaturated dark theme.

 - src: /for/resources/preview-dolphin.png
   name: Preview support for all your images
   description: Preview your artworks Krita files directly from the file manager and image viewer
---

{{< container class="text-center text-small pt-0 labplot" >}}

## LabPlot - Scientific plotting and data analysis

{{< for/app-links download="https://labplot.kde.org/download/" learn="https://labplot.kde.org/" centered="true" >}}

LabPlot is a professional free and open source program for data scientists. It provides high-quality data visualization and plotting with few clicks. It's a reliable and easy data analysis and statistics tool that requires no coding.

![LabPlot tools](/for/resources/labplot.png)

{{< /container >}}

{{< section class="text-small cantor py-5" >}}

## Cantor

Cantor is an application that lets you use your favorite mathematical programming language from within an easy-to-use worksheet interface.

Currently, Cantor supports 10 backends: Julia, KAlgebra, Lua, Maxima, Octave, Python, Qalculate, R, Sage, and Scilab.

{{< for/app-links download="https://cantor.kde.org/download/" learn="https://cantor.kde.org/" dark="true" >}}

![](https://cantor.kde.org/assets/img/screenshot.png)

{{< for/feature features="cantor" >}}

{{< /section >}}

{{< section class="py-5 text-small" >}}

## Powered by Plasma
### ALBA Synchrotron in Barcelona, Spain

Plasma runs on the computers in one of Europe’s largest research facilities powering the majority of control desktop.

> Currently, the control room runs Debian 9 with Plasma. Almost everything in the controls section runs Linux.

> We are a scientific paper factory, so we use Kile and Okular all the time. In the controls section, we use Kate and KWrite quite often. Of course, there are also the Emacs people and the Vim users...

[Sergi Blanch-Torné](https://dot.kde.org/2019/07/19/powered-plasma-alba-synchrotron-barcelona-spain)

![Computers in the control room, powered by Plasma. Photo by Sergi Blanch-Torné.](https://dot.kde.org/sites/dot.kde.org/files/plasma-alba-controlroom-computers.jpg)

[Read the full interview on KDE News website](https://dot.kde.org/2019/07/19/powered-plasma-alba-synchrotron-barcelona-spain)

### Nasa Control Room

Nasa uses KDE 4.10 in their control room, during the InSight landing on Mars stream.

![In the wild, KDE on NASA Control Room](https://25years.kde.org/assets/img/gallery/inthewild_nasa.jpg)

### NERC's Space Geodesy Facility

The facility fires lasers at satellites to check their position to a high degree of precision. Scientists use a wide range of KDE software products on their workstations, including Plasma and Konsole. ([Source](https://youtu.be/vdvIY0CJaXw))

{{< video src="/for/resources/observatoir.mp4" loop="true" muted="true" autoplay="true" controls="false" >}}

## LIGO Hanford Observatory

KDE 4 at LIGO (Laser Interferometer Gravitational-Wave Observatory) Hanford Observatory. ([Source](https://twitter.com/LIGOWA/status/1068552370013913089))

![KDE 4 at LIGO Hanford Observatory](https://25years.kde.org/assets/img/gallery/inthewild_ligo.jpg)

{{< /section >}}

{{< section class="rkward py-5 text-small text-center" >}}

## RKWard

RKWard is an easy-to-use and easily extensible IDE/GUI for R.

{{< for/app-links learn="https://rkward.kde.org/" centered="true" >}}

![](https://rkward.kde.org/assets/img/screenshot_main.png)

### Features

- Spreadsheet-like data editor
- Syntax highlighting, code folding and code completion
- Data importing (e.g. SPSS, Stata and CSV)
- Plot previewing and browsable history
- R package management
- Workspace browser
- GUI dialogs for all kinds of statistics and plots

{{< /section >}}

{{< section class="py-5" >}}

## KStars

KStars is freely licensed, open source, cross-platform Astronomy Software created by KDE.

KStars provides an accurate graphical simulation of the night sky from any location on Earth, and at any date and time. The display includes up to 100 million stars, 13,000 deep-sky objects, all 8 planets, the Sun and Moon, and thousands of comets, asteroids, supernovae, and satellites.

For students and teachers, it supports adjustable simulation speeds in order to view phenomena that happen over long timescales, the KStars Astrocalculator to predict conjunctions, and many common astronomical calculations.

For astronomy aficionados, it comes with a database of commonly available telescopes and the capacity of controlling your kit directly from inside the program.

{{< for/app-links learn="https://apps.kde.org/kstars/" >}}

![Screenshot of KStars showing the Milky Way](/for/resources/kstars.png)

{{< /section >}}

{{< section class="kile py-5" >}}

## Kile

Kile is a user-friendly TeX/LaTeX editor that compiles, converts and shows your document with one click. It's optimized for writing (La)Tex documents and auto-completes (La)TeX commands.

Navigation is also easy, as Kile constructs a list of all the chapters in your document and then lets you use the list to jump to the corresponding section.

You can also click in the viewer and jump to the corresponding LaTeX line in the editor, or jump from the editor to the corresponding page in the viewer.

{{< for/app-links learn="https://apps.kde.org/kile/" dark="true" >}}

![Screenshot of Kile](/for/resources/kile.png)

{{< /section >}}

{{< section class="cantor py-5" >}}

## KBibTex

KBibTeX is a reference management application which can be used to collect TeX/LaTeX bibliographies and export them in various formats. It provides integration with Kile and LyX.

KBibText supports importing data from various bibliography file formats such as BibTeX, RIS and ISI, and exporting data to PDF, PostScript, RTF, and HTML. Additionally, KBibTex searches for bibliography entries in online databases (e.g. Google Scholar, ACM, IEEE, arXiv, etc.).

{{< for/app-links learn="https://apps.kde.org/kbibtex/" dark="true" >}}

![Screenshot of KBibTex](https://cdn.kde.org/screenshots/kbibtex/kbibtex-kf5.png)

{{< /section >}}

{{< container class="text-small" >}}

## Other open source applications for you

Here are more applications from other open source organizations to complement your workflow.
Also check out our [education-related applications](https://apps.kde.org/categories/education/).

{{< link-boxes >}}

{{< /container >}}

