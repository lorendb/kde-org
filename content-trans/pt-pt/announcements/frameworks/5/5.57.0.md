---
aliases:
- ../../kde-frameworks-5.57.0
date: 2019-04-13
layout: framework
libCount: 70
---
### Attica

- Aceitação de qualquer estado de HTTP entre 100 e 109 como benigno

### Baloo

- [DocumentIdDB] Silenciar as mensagens de depuração sem erros, avisar em caso de erros
- [baloosearch] Permitir a definição de uma hora ao usar p.ex. o 'mtime'
- [indexcleaner] Evitar remover as pastas incluídas abaixo das excluídas
- [MTimeDB] Correcção da pesquisa para o intervalo LessEqual (menor ou igual)
- [MTimeDB] Correcção da pesquisa quando a gama de horas devolver um conjunto vazio
- Tratamento de asserções/erros no MTimeDB
- Protecção contra itens-pai inválidos na IdTreeDB
- Remoção do documento do MTimeDB/DocumentTimeDB mesmo quando o 'timestamp' (data/hora) é 0
- Ser mais preciso com a detecção de tipos MIME (erro 403902)
- [timeline] Transformação do URL em canónico
- [timeline] Correcção das chamadas SlaveBase::finished() em falta ou no local errado
- [balooshow] Resultado de informação básica de ficheiros para diversas extensões
- [timeline] Correcção de aviso e adição de item UDS em falta para o "."
- [balooctl] Redução do nível de encadeamento para os argumentos do 'addOption', limpezas
- Reacção às actualizações de configuração dentro da indexação (erro 373430)
- Correcção de regressão ao abrir a BD no modo para leitura-escrita (erro 405317)
- [balooctl] Limpeza dos espaços em branco finais
- [motor] Resolução de estoiros no código, reversão da mudança de nome do Transaction::abort()
- Harmonização do tratamento de sublinhados no processador de pesquisas
- Motor do Baloo: tratar cada código de não-sucesso como um erro (erro 403720)

### BluezQt

- Passagem da interface Media para o Adapter
- Manager: Não necessitar da interface Media1 para a inicialização (erro 405478)
- Device: Verificar a localização do objecto no espaço removido das interfaces (erro 403289)

### Ícones do Brisa

- Adição dos ícones "notifications" e "notifications-disabled" (erro 406121)
- Tornar o 'start-here-kde' também disponível em 'start-here-kde-plasma'
- Ícone de Junção do Sublime
- Dar ao 'applications-games' e ao 'input-gaming' um pouco mais de contrate no Brisa Escuro
- Fazer com que o 24px fique realmente com 24px
- Adição dos ícones 'preferences-desktop-theme-applications' e 'preferences-desktop-theme-windowdecorations'
- Adição de ligações simbólicas do "preferences-desktop-theme" para o "preferences-desktop-theme-applications"
- Remoção do 'preferences-desktop-theme' na preparação para o transformar numa ligação simbólica
- Adição dos ícones 'collapse/expand-all', 'window-shade/unshade' (erro 404344)
- Melhoria na consistência do 'window-*' e adição de mais alguns
- Mudança do 'go-bottom/first/last/top' para ficar um pocuo mais parecidos com os 'media-skip*'
- Mudança das ligações simbólicas 'go-up/down-search' para apontarem para o 'go-up/down'
- Melhoria no alinhamento da grelha de pontos do 'go-up/down/next/previous/jump'
- Mudança do estilo dos ícones 'media-skip*' e 'media-seek*'
- Forçar um novo estilo de ícones de silêncio em todos os ícones de acções

### Módulos Extra do CMake

- Reactivação da configuração do QT_PLUGIN_PATH
- ecm_add_wayland_client_protocol: Melhoria das mensagens de erro
- ECMGeneratePkgConfigFile: tornar todas as variáveis dependentes do ${prefix}
- Adição de um novo módulo de pesquisa do UDev
- ECMGeneratePkgConfigFile: adição das variáveis usadas pelo 'pkg_check_modules'
- Reposição da retrocompatibilidade do FindFontconfig para o 'plasma-desktop'
- Adição do módulo de pesquisa do Fontconfig

### Integração da Plataforma

- uso de um ícone mais específico e apropriado para o Plasma para a categoria do Plasma
- uso do ícone do Plasma como ícone para a categoria de notificações do Plasma

### Ferramentas de Doxygen do KDE

- Actualização dos URL's para usarem o HTTPS

### KArchive

- Correcção de estoiro no KArchive::findOrCreate com ficheiros danificados
- Correcção de leitura de memória não inicializada no KZip
- Adição do Q_OBJECT ao KFilterDev

### KCMUtils

- [KCModuleLoader] Passagem dos argumentos ao KQuickAddons::ConfigModule criado
- Passagem do foco para a barra-filha de pesquisa quando o KPluginSelector está em primeiro plano (erro 399516)
- Melhoria nas mensagens de erro do KCM
- Adição de verificação na execução de que as páginas são KCM's no KCMultiDialog (erro 405440)

### KCompletion

- Não estoirar ao definir o novo campo de edição numa lista editável

### KConfig

- Adição da capacidade Notify ao 'revertToDefault'
- apontar o README para a página da Wiki
- kconfig_compiler: novos argumentos HeaderExtension &amp; SourceExtension para o 'kcfgc'
- [kconf_update] passagem de tecnologia própria de registo de mensagens para o qCDebug
- Remoção da referência do 'const KConfigIniBackend::BufferFragment &amp;'
- Macro KCONFIG_ADD_KCFG_FILES: garantir que uma mudança no File= do 'kcfg' é detectada

### KCoreAddons

- Correcção do "* xpto *" - não queremos colocar este texto a negrito
- Correcção do Erro 401996 - se carregar no URL Web do contacto =&gt; o URL incompleto fica seleccionado (erro 401996)
- Impressão do 'strerror' quando o 'inotify' falhar (razão típica: "demasiados ficheiros abertos")

### KDBusAddons

- Conversão de dois 'connects' antigos para novos

### KDeclarative

- [GridViewKCM] Correcção do cálculo da largura implícita
- passagem da grelha para um ficheiro separado
- Evitar valores fraccionários nos tamanhos e alinhamentos do GridDelegate

### Suporte para a KDELibs 4

- Remoção dos módulos de pesquisa oferecidos pelo ECM

### KDocTools

- Actualização das traduções para Ucraniano
- Actualizações para Catalão
- Entidades 'it': actualização dos URL's para usar o HTTPS
- Actualização dos URL's para usarem o HTTPS
- Uso das traduções para Indonésio
- Actualização do visual para ficar mais parecido com o kde.org
- Adição dos ficheiros necessários para usar a língua Indonésia nativa para toda a documentação em Indonésio

### KFileMetaData

- Implementação do suporte para a escrita das informações de classificação no escritor da Taglib
- implementação de mais marcas no 'taglibwriter'
- Remodelação do 'taglibwriter' para usar a interface 'property'
- Teste da extracção do 'ffmpeg' para usar o utilitário de tipos MIME (erro 399650)
- Proposta do Stefan Bruns como responsável de manutenção do KFileMetaData
- Declaração do PropertyInfo como QMetaType
- Salvaguarda contra ficheiros inválidos
- [TagLibExtractor] Uso do tipo MIME correcto em caso de herança
- Adição de um utilitário para determinar o tipo MIME-pai que é suportado actualmente
- [taglibextractor] Teste da extracção de propriedades com vários valores
- Geração de um cabeçalho para o novo MimeUtils
- Uso de função do Qt para a formatação de listas de cadeias de caracteres
- Correcção da apresentação regional dos números nas propriedades
- Verificação dos tipos MIME para todos os ficheiros de exemplo existentes, bem como a adição de mais alguns
- Adição de utilitário para determinar o tipo MIME com base no conteúdo e na extensão (erro 403902)
- Adição do suporte para a extracção de dados dos ficheiros OGG e TS (erro 399650)
- [ffmpegextractor] Adição de caso de testes de Vídeos Matroska (erro 403902)
- Remodelação da extracção da Taglib para usar a interface genérica do PropertyMap (erro 403902)
- [ExtractorCollection] Carregamento posterior dos 'plugins' de extracção
- Correcção da extracção da propriedade de proporções de tamanho
- Aumento da precisão da propriedade de 'taxa de imagens'

### KHolidays

- Organização das categorias de feriados polacos

### KI18n

- Devolução de um erro legível se o Qt5Widgets for necessário mas não for encontrado

### KIconThemes

- Correcção do preenchimento do ícone que não correspondia de facto ao tamanho pedido (erro 396990)

### KImageFormats

- ora:kra: qstrcmp -&gt; memcmp
- Correcção do RGBHandler::canRead
- xcf: Não estoirar com ficheiros com modos de camadas não suportados

### KIO

- Substituição do currentDateTimeUtc().toTime_t() por currentSecsSinceEpoch()
- Substituição do QDateTime::to_Time_t/from_Time_t por to/fromSecsSinceEpoch
- Melhoria dos ícones dos botões da janela do executável (erro 406090)
- [KDirOperator] Mostrar a Árvore Detalhada por omissão
- KFileItem: invocação do stat() a pedido, adição da opção SkipMimeTypeDetermination
- KIOExec: correcção do erro quando o URL remoto não tem um nome de ficheiro
- KFileWidget - Na gravação de um único ficheiro, se carregar no Enter/Return no KDirOperator irá despoletar o slotOk (erro 385189)
- [KDynamicJobTracker] Uso da interface de DBus gerada
- [KFileWidget] Ao gravar, realçar o nome do ficheiro depois de carregar no ficheiro existente também quando usar o duplo-click
- Não criar miniaturas para cofres encriptados (erro 404750)
- Correcção da mudança de nomes das pastas em WebDAV se o KeepAlive estiver desligado
- Mostrar uma lista das marcas no PlacesView (erro 182367)
- Janela de confirmação para Apagar/Enviar para o ALixo: Correcção de título enganador
- Apresentação do ficheiro/localização correctos na mensagem de erro "demasiado grande para fat32" (erro 405360)
- Frase da mensagem de erro com GiB, não GB (erro 405445)
- openwithdialog: uso da opção 'recursive' no filtro 'proxy'
- Remoção de URL's a serem lidos quando a tarefa de listagem está completa (erro 383534)
- [CopyJob] Tratar o URL como modificado quando mudar o nome do ficheiro como resolução de conflitos
- Passagem da localização do ficheiro local para o KFileSystemType::fileSystemType()
- Correcção da mudança de nomes para maiúsculas/minúsculas num sistema de ficheiros sem distinção
- Correcção dos avisos "Invalid URL: QUrl("algum.txt")" na janela para Gravar (erro 373119)
- Correcção de estoiro ao mover os ficheiros
- Correcção da verificação de ligações simbólicas escondidas em pontos de montagens NTFS (erro 402738)
- Tornar a sobreposição de ficheiros um pouco mais segura (erro 125102)

### Kirigami

- correcção do 'implicitWidth' dos 'listItems'
- entropia de Shannon para adivinhar um ícone monocromático
- Evitar o desaparecimento da área de contexto
- remoção do 'actionmenuitembase'
- não tentar obter a versão nas compilações estáticas
- [Tratamento Mnemónico] Substituição apenas da primeira ocorrência
- sincronização quando qualquer propriedade do modelo mudar
- uso do 'icon.name' no avançar/recuar
- correcção das barras de ferramentas das camadas
- Correcção de erros nos ficheiros de exemplo do Kirigami
- Adição de um componente SearchField e PasswordField
- correcção do tratamento dos ícones (erro 404714)
- [InlineMessage] Não desenhar sombras em torno da mensagem
- actualização imediata em caso de mudança da ordem
- correcção do formato da área de navegação
- nunca mostrar a barra de ferramentas quando o item actual pedir para não o fazer
- gestão da acção recuar/avançar também no filtro
- suportar os botões do rato para avançar/recuar
- Adição de uma inicialização posterior dos submenus
- correcção das barras de ferramentas das camadas
- kirigami_package_breeze_icons: Pesquisar também nos ícones de tamanho 16px
- Correcção da compilação baseada no Qmake
- obter a propriedade associada do item correcto
- correcção da lógica para mostrar a barra de ferramentas
- possibilidade de desactivação da barra de ferramentas para as páginas da camada
- mostrar sempre a barra de ferramentas global nos modos globais
- emissão do evento 'contextualActionsAboutToShow'
- algum espaço à direita do título
- actualização imediata quando mudar a visibilidade
- ActionTextField: Colocação adequada das acções
- TopPadding e BottomPadding
- o texto nas imagens precisa sempre de ser branco (erro 394960)
- recorte da folha de camadas (erro 402280)
- evitar mudar o pai da OverlaySheet para ColumnView
- uso de um 'qpointer' para a instância do tema (erro 404505)
- esconder a navegação nas páginas que não precisam de uma barra de ferramentas (erro 404481)
- não tentar substituir a propriedade 'enabled' (erro 404114)
- Possibilidade de cabeçalhos e rodapés personalizados no ContextDrawer (erro 404978)

### KJobWidgets

- [KUiServerJobTracker] Actualização do 'destUrl' antes de terminar a tarefa

### KNewStuff

- Mudança dos URL's para HTTPS
- Actualização da ligação para o projecto 'fsearch'
- Tratamento dos comandos OCS não suportados e não votar em excesso (erro 391111)
- Nova localização para os ficheiros KNSRC
- [knewstuff] Remoção de método obsoleto no qt5.13

### KNotification

- [KStatusNotifierItem] Enviar a sugestão do 'desktop-entry'
- Permitir a definição de sugestões personalizadas para as notificações

### KNotifyConfig

- Possibilidade de selecção apenas dos ficheiros de áudio suportados (erro 405470)

### Plataforma KPackage

- Correcção da pesquisa dos alvos das ferramentas no ambiente de Docker para Android
- Adição do suporte de compilação multi-plataforma do 'kpackagetool5'

### KService

- Adição do X-GNOME-UsesNotifications como chave reconhecida
- Adição da versão mínima do Bison para a 2.4.1, devido ao %code

### KTextEditor

- Correcção: aplicar correctamente as cores do texto do esquema escolhido (erro 398758)
- DocumentPrivate: Adição da opção "Auto Recarregar o Documento" ao menu Ver (erro 384384)
- DocumentPrivate: Suporte para definir o dicionário na selecção em bloco
- Correcção do Texto Palavras &amp; Caracteres na barra de estado do Kate
- Correcção do Minimap com o estilo QtCurve
- KateStatusBar: Mostrar o botão do cadeado na legenda 'modificado' quando estiver no modo apenas para leitura
- DocumentPrivate: Ignorar as aspas automáticas quando estas parecem já balanceadas (erro 382960)
- Adição da interface Variable ao KTextEditor::Editor
- relaxar o código para só falhar as asserções na versão de depuração e funcionar na versão final
- garantia de compatibilidade com as configurações antigas
- mais uso da interface de configuração genérica
- simplificação do QString KateDocumentConfig::eolString()
- transferência da configuração do Sonnet para a configuração do KTextEditor
- garantia de que não existem lacunas nas chaves de configuração
- conversão de mais coisas para a interface de configuração genérica
- mais usos da interface de configuração genérica
- interface de configuração genérica
- Não estoirar com ficheiros de realce de sintaxe mal formatados
- IconBorder: Aceitação de eventos de 'drag&amp;drop' (erro 405280)
- ViewPrivate: Tornar a desmarcação com as teclas de cursores mais fácil (erro 296500)
- Correcção da apresentação da árvore de sugestões de argumentos num ecrã não-primário
- Migração de alguns métodos obsoletos
- Reposição da mensagem da pesquisa a dar a volta para o seu tipo e posição anteriores (erro 398731)
- ViewPrivate: Tornar a opção 'Aplicar a Mudança de Linha' mais confortável (erro 381985)
- ModeBase::goToPos: Garantia de que o destino do salto é válido (erro 377200)
- ViInputMode: Remoção dos atributos de texto não suportados da barra de estado
- KateStatusBar: Adição do botão de dicionário
- adição de exemplo para o problema da altura da linha

### KWidgetsAddons

- Tornar o KFontRequester mais consistente
- Actualização do kcharselect-data para o Unicode 12.0

### KWindowSystem

- Envio do borrão/contraste do fundo nos pixels do dispositivo (error 404923)

### NetworkManagerQt

- WireGuard: mudança para que funcione a codificação/descodificação das senhas do mapa
- Adição do suporte em falta para o Wireguard na classe de configuração de base
- Wireguard: tratamento da chave privada como senhas
- Wireguard: a propriedade 'peers' deverão ser do tipo NMVariantMapList
- Adição do suporte para o tipo de ligação Wireguard
- ActiveConnection: adição do sinal 'stateChangedReason' onde possamos ver a razão da mudança de estado

### Plataforma do Plasma

- [AppletInterface] Verificação do Corona antes de aceder ao mesmo
- [Dialog] Não encaminhar o evento de passagem do rato quando não existir mais nada para onde propagar
- [Menu] Correcção do evento 'triggered'
- Redução da importância de alguma informação de depuração para que possam ser vistos avisos reais
- [Lista ComboBox dos Componentes do Plasma] Correcção do 'textColor' (cor do texto)
- FrameSvgItem: captura das mudanças de margens do FrameSvg também para fora dos métodos próprios
- Adição do Theme::blurBehindEnabled()
- FrameSvgItem: correcção do textureRect para os sub-itens lado-a-lado não encolherem até ao 0
- Correcção do fundo da janela do Brisa com o Qt 5.12.2 (erro 405548)
- Remoção de estoiro no 'plasmashell'
- [Item de Ícones] Limpar também o ícone da imagem ao usar o SVG do Plasma (erro 405298)
- a altura do campo de texto baseada apenas em texto simples (erro 399155)
- associação ao 'alternateBackgroundColor'

### Purpose

- Adição do 'plugin' de SMS's do KDE Connect

### QQC2StyleBridge

- o estilo do ambiente de trabalho Plasma suporta a coloração de ícones
- [SpinBox] Melhoria do comportamento da roda do rato
- adição de algum preenchimento nas ToolBars
- correcção dos ícones do RoundButton
- preenchimento com base na barra de deslocamento para todos os delegados
- verificação se uma área deslizável tem as margens da barra de deslocamento em conta

### Solid

- Possibilidade de compilar sem o UDev no Linux
- Só obter o 'clearTextPath' quando for usado

### Realce de Sintaxe

- Adição da definição de sintaxe da linguagem Elm ao realce de sintaxe
- AppArmor &amp; SELinux: remoção de indentação nos ficheiros XML
- Doxygen: não usar a cor preta nas marcas
- Permitir mudanças de contexto de fim-de-linha nas linhas vazias (erro 405903)
- Correcção da dobragem do 'endRegion' nas regras com o 'beginRegion+endRegion' (uso de length=0) (erro 405585)
- Adição de extensões para o realce do Groovy (erro 403072)
- Adição do ficheiro de realce de sintaxe para Smali
- Adição do "." como 'weakDeliminator' no ficheiro de sintaxe do Octave
- Logcat: correcção de cor do dsError com underline="0"
- correcção de estoiro no realce com ficheiros de HL com problemas
- guarda das bibliotecas-alvo para a versão anterior do CMake (erro 404835)

### Informação de segurança

O código lançado foi assinado com GPG, usando a seguinte chave: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Impressão digital da chave primária: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB
