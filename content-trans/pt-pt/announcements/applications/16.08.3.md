---
aliases:
- ../announce-applications-16.08.3
changelog: true
date: 2016-11-10
description: O KDE Lança as Aplicações do KDE 16.08.3
layout: application
title: O KDE Lança as Aplicações do KDE 16.08.3
version: 16.08.3
---
10 de Novembro de 2016. Hoje o KDE lançou a terceira actualização de estabilidade para as <a href='../16.08.0'>Aplicações do KDE 16.08</a>. Esta versão contém apenas correcções de erros e actualizações de traduções, pelo que será uma actualização segura e agradável para todos.

As mais de 20 correcções de erros registadas incluem as melhorias no 'kdepim', 'ark', 'okteta', 'umbrello', entre outros.

Esta versão também inclui as versões de Suporte de Longo Prazo da Plataforma de Desenvolvimento do KDE 4.14.26.
