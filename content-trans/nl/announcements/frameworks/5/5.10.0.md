---
aliases:
- ../../kde-frameworks-5.10.0
date: '2015-05-08'
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### KActivities

- (geen log met wijzigingen geleverd)

### KConfig

- QML-proof klassen genereren met de kconfigcompiler

### KCoreAddons

- Nieuwe cmake-macro kcoreaddons_add_plugin om gemakkelijker op KPluginLoader gebaseerde plug-ins te maken.

### KDeclarative

- Crash in textuurcache gerepareerd.
- en andere reparaties

### KGlobalAccel

- Nieuwe methode globalShortcut toevoegen die de sneltoets ophaalt zoals gedefinieerd in globale instellingen.

### KIdleTime

- Voorkomen dat kidletime crasht op het platform wayland

### KIO

- KPropertiesDialog::KPropertiesDialog(urls) en KPropertiesDialog::showDialog(urls) toegevoegd.
- Asynchrone op QIODevice gebaseerd ophalen van gegevens voor KIO::storedPut en KIO::AccessManager::put.
- Condities met teruggegeven waarde van QFile::rename repareren (bug 343329)
- KIO::suggestName gerepareerd om betere namen te suggereren (bug 341773)
- kioexec: pad voor te beschrijven locatie voor kurl gerepareerd (bug 343329)
- Bladwijzers alleen opslaan in user-places.xbel (bug 345174)
- Item in RecentDocuments dupliceren als twee verschillende bestanden dezelfde naam hebben
- Betere foutmelding als een enkel bestand te groot is voor de prullenbak (bug 332692)
- KDirLister crash repareren bij doorverwijzen wanneer het slot openURL aanroept

### KNewStuff

- Nieuwe set klassen, genaamd KMoreTools en gerelateerd. KMoreTools helpt bij het toevoegen van tips over externe hulpmiddelen die potentieel nog niet zijn geïnstalleerd. Verder maakt het lange menu's korter door een hoofdmenu en meer secties te bieden die ook door de gebruiker zijn in te stellen.

### KNotifications

- KNotifications repareren wanneer deze gebruikt wordt met Ubuntu's NotifyOSD (bug 345973)
- Start geen bijwerken van meldingen bij instellen van dezelfde eigenschappen (bug 345973)
- Introduceer LoopSound-vlag waarmee meldingen een geluid herhaald af kunnen spelen als zij dat nodig hebben (bug 346148)
- Geen crash als meldingen geen widget hebben

### KPackage

- Een KPackage::findPackages functie toevoegen vergelijkbaar met KPluginLoader::findPlugins

### KPeople

- KPluginFactory gebruiken voor beschikbaar maken van de plug-ins, in plaats van KService (behouden voor compatibiliteit).

### KService

- Fout opsplitsen van pad van item (bug 344614)

### KWallet

- Migratieagent controleert nu ook of de oude portefeuille leeg is voor het starten (bug 346498)

### KWidgetsAddons

- KDateTimeEdit: reparatie zodat invoer van de gebruiker ook echt wordt geregistreerd. Dubbele marges repareren.
- KFontRequester: selecteren van alleen niet-proportionele lettertypen repareren

### KWindowSystem

- Geen afhankelijkheid van QX11Info in KXUtils::createPixmapFromHandle (bug 346496)
- nieuwe methode NETWinInfo::xcbConnection() -&gt; xcb_connection_t*

### KXmlGui

- Sneltoets repareren wanneer secondaire sneltoets is ingesteld (bug 345411)
- Werk lijst met bugzilla producten/componenten bij voor rapporteren van bugs (bug 346559)
- Globale sneltoetsen: sta ook het instellen van de alternatieve sneltoets toe

### NetworkManagerQt

- De geïnstalleerde headers zijn nu georganiseerd zoals alle andere frameworks.

### Plasma framework

- PlasmaComponents.Menu ondersteunt nu secties
- KPluginLoader gebruiken in plaats van ksycoca voor laden van C++ dataengines
- Neem visualParent rotatie in beschouwing in popupPosition (bug 345787)

### Sonnet

- Probeer niet te accentueren als er geen spellingcontrole is gevonden. Dit zou leiden tot een oneindige loop waarbij de timer rehighlighRequest steeds afvuurt.

### Frameworkintegratie

- Repareer eigen bestandsdialogen uit widgets QFileDialog: ** Bestandsdialogen geopend met exec() en zonder ouder werden geopend, maar elke interactie met de gebruiker was geblokkeerd op een manier waarbij geen bestand  geselecteerd kon worden of de dialoog gesloten. ** Bestandsdialogen geopend met open() of show() met een ouder werden helemaal niet geopend.

U kunt discussiëren en ideeën delen over deze uitgave in de section voor commentaar van <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>het artikel in the dot</a>.
