---
aliases:
- ../announce-applications-17.04.3
changelog: true
date: 2017-07-13
description: KDE lanza las Aplicaciones de KDE 17.04.3
layout: application
title: KDE lanza las Aplicaciones de KDE 17.04.3
version: 17.04.3
---
Hoy, 13 de julio de 2017, KDE ha lanzado la tercera actualización de estabilización para las <a href='../17.04.0'>Aplicaciones 17.04</a>. Esta versión solo contiene soluciones de errores y actualizaciones de traducciones, por lo que será una actualización agradable y segura para todo el mundo.

Entre las más de 25 correcciones de errores registradas, se incluyen mejoras en kdepim, dolphin, dragonplayer, kdenlive y umbrello, entre otras aplicaciones.

También se incluye la versión de la Plataforma de desarrollo de KDE 4.14.34 que contará con asistencia a largo plazo.
