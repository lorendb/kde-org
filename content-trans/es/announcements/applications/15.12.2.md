---
aliases:
- ../announce-applications-15.12.2
changelog: true
date: 2016-02-16
description: KDE lanza las Aplicaciones de KDE 15.12.2
layout: application
title: KDE lanza las Aplicaciones de KDE 15.12.2
version: 15.12.2
---
Hoy, 16 de febrero de 2016, KDE ha lanzado la segunda actualización de estabilización para las <a href='../15.12.0'>KDE Aplicaciones 15.12</a>. Esta versión solo contiene soluciones de errores y actualizaciones de traducciones, por lo que será una actualización agradable y segura para todo el mundo.

Entre las más de 30 correcciones de errores registradas, se incluyen mejoras en kdelibs, kdepim, kdenlive, marble, konsole, spectacle, akonadi, ark y umbrello, entre otras aplicaciones.

También se incluye la versión de la Plataforma de desarrollo de KDE 4.14.17 que contará con asistencia a largo plazo.
