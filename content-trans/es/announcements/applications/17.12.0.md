---
aliases:
- ../announce-applications-17.12.0
date: 2017-12-14
description: KDE lanza las Aplicaciones de KDE 17.12.0
layout: application
title: KDE lanza las Aplicaciones de KDE 17.12.0
version: 17.12.0
---
14 de diciembre de 2017. Publicadas las Aplicaciones de KDE 17.12.0.

Trabajamos continuamente en la mejora del software incluido en nuestras series de Aplicaciones de KDE y esperamos que encuentre útiles todas las nuevas mejoras y correcciones de errores.

### Novedades de las Aplicaciones de KDE 17.12

#### Sistema

{{<figure src="/announcements/applications/17.12.0/dolphin1712.gif" width="600px" >}}

<a href='https://www.kde.org/applications/system/dolphin/'>Dolphin</a>, nuestro gestor de archivos, puede ahora guardar búsquedas y limitar la búsqueda a solo carpetas. El cambio de nombre de archivos es ahora más fácil: solo tiene que hacer doble clic en el nombre de archivo. Ahora tiene más información sobre los archivos, ya que la fecha de modificación y la URL de origen de los archivos descargados se muestran en el panel de información. Además, se han introducido nuevas columnas de género, tasa de bits y año de lanzamiento.

#### Gráficos

Nuestro potente visor de documentos <a href='https://www.kde.org/applications/graphics/okular/'>Okular</a> se puede usar ahora en pantallas HiDPI y permite ver documentos en lenguaje Markdown. Además, la representación de documentos que se cargan lentamente se muestra ahora de forma progresiva. También existe una opción para compartir el documento por correo electrónico.

El visor de imágenes <a href='https://www.kde.org/applications/graphics/gwenview/'>Gwenview</a> puede ahora abrir y resaltar imágenes en el gestor de archivos, el zum es más suave, la navegación con el teclado se ha mejorado y ahora es compatible con los formatos FITS y Truevision TGA. Las imágenes están protegidas ahora para evitar que se eliminen accidentalmente con la tecla «Suprimir» cuando no están seleccionadas.

#### Multimedia

<a href='https://www.kde.org/applications/multimedia/kdenlive/'>Kdenlive</a> usa ahora menos memoria al manejar proyectos de vídeo que incluyen muchas imágenes, se han refinado los perfiles de proxy predeterminados y se ha corregido un error molesto relacionado con el salto de un segundo hacia adelante cuando se reproduce hacia atrás.

#### Utilidades

{{<figure src="/announcements/applications/17.12.0/kate1712.png" width="600px" >}}

Se ha mejorado el uso de ZIP en el motor «libzip» de <a href='https://www.kde.org/applications/utilities/ark/'>Ark</a>. <a href='https://www.kde.org/applications/utilities/kate/'>Kate</a> tiene un nuevo <a href='https://frinring.wordpress.com/2017/09/25/ktexteditorpreviewplugin-0-1-0/'>complemento de vista previa</a> que le permite mostrar una vista previa en vivo del documento de texto en el formato final, aplicando cualquier complemento KParts disponible (por ejemplo, para Markdown, SVG, gráficos Dot, interfaz de usuario de Qt, o parches). Este complemento también funciona en <a href='https://www.kde.org/applications/development/kdevelop/'>KDevelop</a>.

#### Desarrollo

{{<figure src="/announcements/applications/17.12.0/kuiviewer1712.png" width="600px" >}}

<a href='https://www.kde.org/applications/development/kompare/'>Kompare</a> proporciona ahora un menú de contexto en el área de diferencias, lo que permite un rápido acceso a las acciones de navegación y de modificación. Para los desarrolladores resultará de utilidad la nueva vista previa de objetos de interfaz gráfica de KUIViewer en el panel de los archivos de interfaz gráfica de Qt (widgets, diálogos, etc.). Ahora también se permite el uso de la API de emisión de KParts.

#### Oficina

El equipo de <a href='https://www.kde.org/applications/office/kontact'>Kontact</a> ha trabajado duro para realizar mejoras. La mayor parte del trabajo se ha dedicado a modernizar el código fuente, aunque también se ha mejorado la visualización de mensajes cifrados y ahora se puede usar «text/pgp» y <a href='https://phabricator.kde.org/D8395'>Apple® Wallet Pass</a>. También existe una nueva opción para seleccionar la carpeta IMAP durante la configuración de las vacaciones, una nueva advertencia de KMail cuando se vuelve a abrir un mensaje y la identidad o el transporte de correo no es el mismo, una nueva <a href='https://micreabog.wordpress.com/2017/10/05/akonadi-ews-resource-now-part-of-kde-pim/'>implementación de Microsoft® Exchange™</a>, implementación para Nylas Mail y una importación mejorada de Geary en el asistente de importación de Akonadi, además de diversas correcciones de fallos y mejoras generales.

#### Juegos

{{<figure src="/announcements/applications/17.12.0/ktuberling1712.jpg" width="600px" >}}

<a href='https://www.kde.org/applications/games/ktuberling/'>KTuberling</a> puede alcanzar una audiencia más amplia, ya que se ha <a href='https://tsdgeos.blogspot.nl/2017/10/kde-edu-sprint-in-berlin.html'>portado a Android</a>. <a href='https://www.kde.org/applications/games/kolf/'>Kolf</a>, <a href='https://www.kde.org/applications/games/ksirk/'>KsirK</a> y <a href='https://www.kde.org/applications/games/palapeli/'>Palapeli</a> han completado la transición de los juegos de KDE a Frameworks 5.

### Más migraciones a KDE Frameworks 5

También se han portado más aplicaciones basadas en kdelibs4 a KDE Frameworks 5. Entre ellas se incluyen el reproductor de música <a href='https://www.kde.org/applications/multimedia/juk/'>JuK</a>, el gestor de descargas <a href='https://www.kde.org/applications/internet/kget/'>KGet</a>, <a href='https://www.kde.org/applications/multimedia/kmix/'>KMix</a>, utilidades como <a href='https://www.kde.org/applications/utilities/sweeper/'>Sweeper</a> y <a href='https://www.kde.org/applications/utilities/kmouth/'>KMouth</a>, <a href='https://www.kde.org/applications/development/kimagemapeditor/'>KImageMapEditor</a> y el esclavo de Zeroconf. ¡Nuestro agradecimiento por el duro trabajo para los desarrolladores que han dedicado su tiempo y su esfuerzo de forma voluntaria para que esto sea posible!

### Aplicaciones que se han movido a su propio calendario de lanzamientos

<a href='https://www.kde.org/applications/education/kstars/'>KStars</a> posee ahora su propio calendario de lanzamientos; consulte este <a href='https://knro.blogspot.de'>blog de desarrollador</a> para leer sus anuncios. Merece la pena tener en cuenta que algunas aplicaciones, como Kopete y Blogilo, <a href='https://community.kde.org/Applications/17.12_Release_Notes#Tarballs_that_we_do_not_ship_anymore'>ya no se publican</a> junto al resto de Aplicaciones, ya que aún no se han portado a KDE Frameworks 5 o no se mantienen de forma activa en este momento.

### A la caza de errores

Se han resuelto más de 110 errores en aplicaciones, que incluyen la suite Kontact, Ark, Dolphin, Gwenview, K3b, Kate, Kdenlive, Konsole, Okular y Umbrello, entre otras.

### Registro de cambios completo

Si desea leer más sobre los cambios de este lanzamiento, <a href='/announcements/changelogs/applications/17.12.0'>consulte el registro de cambios completo</a>. Aunque es un poco intimidatorio debido a su amplitud, el registro de cambios puede ser un modo excelente para conocer el trabajo interno de KDE y descubrir aplicaciones y funciones que tal vez no sabía que tenía a su disposición.
