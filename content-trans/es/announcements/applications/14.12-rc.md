---
aliases:
- ../announce-applications-14.12-rc
custom_spread_install: true
date: '2014-11-27'
description: KDE lanza la versión candidata de las Aplicaciones 14.12.
layout: application
title: KDE lanza la candidata a versión final para las Aplicaciones 4.12
---
Hoy, 27 de noviembre de 2014, KDE ha lanzado la candidata a versión final de las nuevas Aplicaciones. Una vez congeladas las dependencias y las funcionalidades, el equipo de KDE se ha centrado en corregir errores y en pulir aún más la versión.

Debido a las numerosas aplicaciones que se basan en KDE Frameworks 5, es necesario probar la versión 14.12 de manera exhaustiva con el fin de mantener y mejorar la calidad y la experiencia del usuario. Los usuarios reales son críticos para mantener la alta calidad de KDE porque los desarrolladores no pueden probar todas las configuraciones posibles. Contamos con ustedes para ayudar a encontrar errores lo antes posible de forma que se puedan corregir antes de la versión final. Considere la idea de unirse al equipo instalando la candidata a versión final <a href='https://bugs.kde.org/'>e informando de cualquier error que encuentre</a>.

#### Instalación de paquetes binarios de las aplicaciones de la candidata a versión final de KDE 14.12.

<em>Paquetes</em>. Algunos distribuidores del SO Linux/UNIX tienen la gentileza de proporcionar paquetes binarios de la candidata a versión final de 14.12 (internamente, 14.11.97) para algunas versiones de sus distribuciones y en otros casos han sido voluntarios de la comunidad los que lo han hecho posible. En las próximas semanas estarán disponibles paquetes binarios adicionales, así como actualizaciones de los paquetes disponibles en este momento.

<em>Ubicación de paquetes</em>. Para obtener una lista actual de los paquetes binarios disponibles de los que el Equipo de Lanzamiento de KDE tiene conocimiento, visite la <a href='http://community.kde.org/KDE_SC/Binary_Packages'>Wiki de la Comunidad</a>.

#### Compilación de las aplicaciones de la candidata a versión final de KDE 14.12

La totalidad del código fuente de la candidata a versión final de las Aplicaciones 14.12 se puede <a href='http://download.kde.org/unstable/applications/14.11.97/src/'>descargar libremente</a>. Dispone de instrucciones sobre cómo compilar e instalar 14.11.97 en la <a href='/info/applications/applications-14.11.97'>página de información sobre la candidata a versión final de las aplicaciones de KDE 4.11.95</a>.
