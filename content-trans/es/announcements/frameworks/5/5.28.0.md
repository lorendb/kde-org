---
aliases:
- ../../kde-frameworks-5.28.0
date: 2016-11-15
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Nueva framework: resaltado de sintaxis

Motor de resaltado de sintaxis para las definiciones sintácticas de Kate

Esta es una implementación independiente del motor de resaltado sintáctico de Kate. Está pensado como un bloque de construcción para editores de texto así como para representación sencilla de texto resaltado (por ejemplo, como HTML), permitiendo integración con un editor personalizado y proporcionando una subclase de QSyntaxHighlighter lista para usar.

### Iconos Brisa

- Se han actualizado iconos de acciones de kstars (error 364981)
- Brisa oscuro se lista como Brisa en el archivo «.themes» equivocado de las Preferencias del sistema (error 370213).

### Módulos CMake adicionales

- Hacer que «KDECMakeSettings» funcione con «KDE_INSTALL_DIRS_NO_DEPRECATED».
- No requerir dependencias de enlazado de Python para ECM.
- Añadir el módulo «PythonModuleGeneration».

### KActivitiesStats

- Ignorar estados de enlace al ordenar el modelo «UsedResources» y «LinkedResources».

### Herramientas KDE Doxygen

- [CSS] Revertir los cambios realizados por doxygen 1.8.12.
- Añadir el archivo «doxygenlayout».
- Actualizar el modo de definir nombres de grupo.

### KAuth

- Asegurarse de que se puede realizar más de una solicitud.
- Asegurarse de conocer el avance leyendo el resultado del programa.

### KConfig

- Asegurarse de que no se rompe la compilación con anteriores unidades rotas.
- No producir un error fatal cuando no se analiza correctamente el campo «File».

### KCoreAddons

- Mostrar URL incorrecta.
- Cargar avatares de usuario desde «AccountsServicePath», si existe (error 370362).

### KDeclarative

- [QtQuickRendererSettings] Corregir que el valor predeterminado esté vacío en lugar de «false».

### Soporte de KDELibs 4

- Hacer que la bandera de Francia use realmente todo el mapa de bits.

### KDocTools

- Corregir «checkXML5 genera archivos HTML en el directorio de trabajo para docbooks válidos» (error 371987).

### KIconThemes

- Permitir el uso de factores de escalado no enteros en «kiconengine» (error 366451).

### KIdleTime

- Se ha desactivado el envío masivo de mensajes «esperando» a la salida de la consola.

### KImageFormats

- imageformats/kra.h: «overrides» para los métodos «capabilities()» y «create()» de «KraPlugin».

### KIO

- Se ha corregido el formato de fecha HTTP enviado por «kio_http» para que use siempre la configuración local de C (error 372005).
- KACL: Corregir fugas de memoria detectadas por ASAN.
- Corregir fugas de memoria en «KIO::Scheduler» detectadas por ASAN.
- Se ha eliminado el botón de borrado duplicado (error 369377).
- Corregir las entradas de ejecución automática cuando no existe «/usr/local/share/applications» (error 371194).
- [KOpenWithDialog] Ocultar la cabecera de «TreeView».
- Sanear el tamaño del búfer de nombres de enlaces simbólicos (error 369275).
- Finalizar correctamente los «DropJobs» cuando no se emite «triggered» (error 363936).
- ClipboardUpdater: Se ha corregido otro fallo en Wayland (error 359883).
- ClipboardUpdater: Se ha corregido un fallo en Wayland (error 370520).
- Permitir el uso de factores de escalado no enteros en «KFileDelegate» (error 366451).
- kntlm: Distinguir entre «NULL» y dominio vacío.
- No mostrar el diálogo de sobrescritura si el nombre de archivo está vacío.
- kioexec: Usar nombres de archivos amigables.
- Corregir el propietario del foco si se cambia la URL antes de mostrar el widget.
- Importante mejora de rendimiento al desactivar las vistas previas en el diálogo de archivos (fallo 346403)

### KItemModels

- Añadir enlaces de Python.

### KJS

- Exportar «FunctionObjectImp», usado por el depurador de «khtml».

### KNewStuff

- Separar roles de orden y filtros.
- Permitir que se puedan consultar las entradas instaladas

### KNotification

- No deshacer la referencia a un objeto al que no hemos hecho referencia cuando la notificación no tiene ninguna acción.
- «KNotification» ya no falla al usarla en una «QGuiApplication» cuando no está funcionando ningún servicio de notificación (error 370667).
- Se han corregido bloqueos en «NotifyByAudio».

### Framework KPackage

- Asegurarse de que estamos buscando metadatos JSON y del escritorio.
- Protegerse contra la destrucción de Q_GLOBAL_STATIC al salir de la aplicación
- Se ha corregido un puntero descontrolado en «KPackageJob» (error 369935).
- Eliminar el descubrimiento asociado a una tecla al eliminar una definición.
- Generar el icono en el archivo «appstream».

### KPty

- Usar «ulog-helper» en FreeBSD en lugar de «utempter».
- Buscar más a fondo «utempter» usando también el prefijo básico de cmake.
- Solucionar temporalmente los fallos de «find_program ( utempter ...)».
- Usar la ruta de ECM para encontrar el binario «utempter», más fiable que el simple prefijo «cmake».

### KRunner

- i18n: Manejo de cadenas en archivos «kdevtemplate».

### KTextEditor

- Brisa oscuro: Oscurecer el color de fondo de la línea actual para una mejor legibilidad (error 371042).
- Instrucciones de «Dockerfile» ordenadas.
- Brisa (oscuro): Hacer que los comentarios sean un poco más claros para una mejor legibilidad (error 371042).
- Se han corregido el «CStyle» y los sangradores de C++/boost cuando están activados los paréntesis automáticos (error 370715).
- Se ha añadido el «modeline» 'auto-brackets'.
- Se ha corregido la inserción de texto tras el final de archivo (caso raro).
- Se han corregido los archivos no válidos de resaltado de XML
- Maxima: Eliminar loa colores codificados a mano, corregir la etiqueta de «itemData».
- Se han añadido las definiciones de sintaxis de OBJ, PLY y STL.
- Se ha añadido el resaltado de sintaxis para Praat

### KUnitConversion

- Nuevas unidades térmicas y eléctricas y función de conveniencia de unidades.

### Framework KWallet

- Si «Gpgmepp» no se encuentra, intentar usar «KF5Gpgmepp».
- Usar «Gpgmepp» de GpgME-1.7.0.

### KWayland

- Reubicación mejorada de la exportación de CMake.
- [herramientas] Se ha corregido la generación de «wayland_pointer_p.h».
- [herramientas] Generar métodos «eventQueue» solo para las clases globales.
- [servidor] Corregir el bloqueo al actualizar la superficie del teclado enfocada.
- [servidor] Se ha corregido un posible bloqueo en la creación de «DataDevice».
- [servidor] Asegurarnos de que tenemos una «DataSource» en el «DataDevice» en «setSelection».
- [herramientas/generador] Mejorar la destrucción de recursos del lado del servidor.
- Añadir solicitud para tener el foco en un «PlasmaShellSurface» con el rol de panel.
- Permitir que el panel se pueda ocultar automáticamente en la interfaz «PlasmaShellSurface».
- Permitir el paso de un «QIcon» genérico a través de la interfaz «PlasmaWindow».
- [servidor] Implementar la propiedad de ventana genérica en «QtSurfaceExtension».
- [cliente] Añadir métodos para obtener «ShellSurface» de una «QWindow».
- [servidor] Enviar eventos de punteros a todos los recursos «wl_pointer» de un cliente.
- [servidor] No llamar a «wl_data_source_send_send» si «DataSource» está sin enlazar.
- [servidor] Usar «deleteLater» cuando se destruye una «ClientConnection» (error 370232).
- Implementar el uso de protocolo de punteros relativos.
- [servidor] Cancelar la selección anterior de «SeatInterface::setSelection».
- [servidor] Enviar eventos de teclado a todos los recursos «wl_keyboard» de un cliente.

### KWidgetsAddons

- Mover «kcharselect-generate-datafile.py» al subdirectorio «src».
- Importar el script «kcharselect-generate-datafile.py» con el historial.
- Eliminar sección desactualizada.
- Añadir nota de copyright y de permiso en Unicode.
- Corregir una advertencia: Falta un «override».
- Agregar bloques SMP de símbolos.
- Se han corregido las referencias «Ver también».
- Añadir bloques Unicode que faltaban; mejorar el orden (error 298010).
- Añadir categorías de caracteres al archivo de datos.
- Actualizar las categorías Unicode en el script de generación del archivo de datos.
- Ajustar al archivo de generación del archivo de datos para que pueda analizar archivos de datos Unicode 5.2.0.
- Reenviar la corrección del puerto para generar traducciones.
- Dejar que el script para generar el archivo de datos de «kcharselect» también escriba un esqueleto para traducción.
- Añadir el script para generar el archivo de datos para «KCharSelect».
- Nueva aplicación «KCharSelect» (usando el widget «kcharselect» de «kdelibs»).

### KWindowSystem

- Reubicación mejorada de la exportación de CMake.
- Se ha implementado «desktopFileName» en «NETWinInfo»

### KXMLGUI

- Permitir el uso del nuevo estilo de «connect» en KActionCollection::add<a href="">Action</a>.

### ModemManagerQt

- Se ha corregido el directorio de inclusión en el archivo «pri».

### NetworkManagerQt

- Se ha corregido el directorio de inclusión en el archivo «pri».
- Se ha corregido un error de «moc» debido al uso de «Q_ENUMS» en un «namespace» con la rama 5.8 de Qt.

### Framework de Plasma

- Asegurarse de que el OSD no tenga el indicador de diálogo (error 370433).
- Definir las propiedades de contexto antes de volver a cargar el «qml» (error 371763).
- No volver a analizar el archivo de metadatos si ya está cargado.
- Se ha corregido un cuelgue en «qmlplugindump» cuando no hay ninguna QApplication disponible
- No mostrar el menú «Alternativas» de forma predeterminada.
- Nuevo «bool» para usar la señal «activated» como conmutable o expandida (error 367685).
- Correcciones para compilar «plasma-framework» con Qt 5.5.
- [PluginLoader] Usar el operador &lt;&lt; en «finalArgs» en lugar de una lista de inicialización.
- Usar kwayland para las sombras y para posicionar los diálogos.
- Iconos restantes que faltaban y mejoras de red.
- Mover «availableScreenRect/Region» a «AppletInterface».
- No cargar acciones de contenedores para contenedores incrustados (bandejas del sistema).
- Actualizar la visibilidad de la entrada del menú de alternativas de las miniaplicaciones bajo demanda.

### Solid

- Corregir el orden inestable de los resultados de la consulta una vez más.
- Añadir una opción de CMake para cambiar entre los gestores HAL y UDisks en FreeBSD.
- Hacer que el motor UDisks2 compile en FreeBSD (y, posiblemente, en otros UNIX).
- Windows: No mostrar diálogos de error (error 371012).

### Información de seguridad

El código publicado se ha firmado con GPG usando la siguiente clave: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt;. Huella digital de la clave primaria: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB.

Puede comentar esta versión y compartir sus ideas en la sección de comentarios de <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>el artículo de dot</a>.
