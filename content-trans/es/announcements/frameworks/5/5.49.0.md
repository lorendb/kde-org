---
aliases:
- ../../kde-frameworks-5.49.0
date: 2018-08-11
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Attica

- Don't instantiate a QStringRef into a QString only to search in a QStringList
- Define elements when they're declared

### Baloo

- [tags_kio] Fix multiple filename copies
- Revert "[tags_kio] Use UDS_URL instead of UDS_TARGET_URL."
- [tags_kio] Use UDS_URL instead of UDS_TARGET_URL
- [tags_kio] Query target filepaths instead of appending paths to the file UDS entry
- Support special URLs for finding files of a certain type
- Avoid manipulation of lists with quadratic complexity
- Use non deprecated fastInsert in baloo

### Iconos Brisa

- Se ha añadido el icono «drive-optical» (error 396432).

### Módulos CMake adicionales

- Android: Don't hardcode a random version of the Android SDK
- ECMOptionalAddSubdirectory: Provide a bit more detail
- Fix variable definition check
- Change the 'since' version
- Improve ECMAddAppIconMacro

### Integración con Frameworks

- Respetar BUILD_TESTING.

### KActivities

- Respetar BUILD_TESTING.

### KArchive

- Respetar BUILD_TESTING.

### KAuth

- Avoid warnings for PolkitQt5-1 headers
- Respetar BUILD_TESTING.

### KBookmarks

- Respetar BUILD_TESTING.

### KCodecs

- Respetar BUILD_TESTING.

### KCompletion

- Respetar BUILD_TESTING.

### KConfig

- Respetar BUILD_TESTING.

### KConfigWidgets

- Respetar BUILD_TESTING.

### KCoreAddons

- Fix overflow in rounding code (bug 397008)
- API dox: remove not-to-be-there ":"s behind "@note"
- API dox: talk about nullptr, not 0
- KFormat: Replace unicode literal with unicode codepoint to fix MSVC build
- KFormat: correct @since tag for new KFormat::formatValue
- KFormat: Allow usage of quantities beyond bytes and seconds
- Correct KFormat::formatBytes examples
- Respetar BUILD_TESTING.

### KCrash

- Respetar BUILD_TESTING.

### KDBusAddons

- Don't block forever in ensureKdeinitRunning
- Respetar BUILD_TESTING.

### KDeclarative

- ensure we are always writing in the engine's root context
- Mejor legibilidad.
- Improve API docs a bit
- Respetar BUILD_TESTING.

### Soporte de KDELibs 4

- Corregir «qtplugins» en «KStandardDirs».

### KDocTools

- Respetar BUILD_TESTING.

### KEmoticons

- Respetar BUILD_TESTING.

### KFileMetaData

- API dox: add @file to functions-only header to have doxygen cover those

### KGlobalAccel

- Respetar BUILD_TESTING.

### Complementos KDE GUI

- Respetar BUILD_TESTING.

### KHolidays

- Se ha instalado la cabecera del cálculo del amanecer/atardecer.
- Added leap year day as (cultural) holiday for Norway
- Added ‘name’ entry for Norwegian holiday files
- Se han añadido descripciones para los archivos de festividades noruegas.
- more Japanese holiday updates from phanect
- Se han actualizado «holiday_jp_ja» y «holiday_jp-en_us» (error 365241).

### KI18n

- Reuse function that already does the same
- Fix the catalog handling and locale detection on Android
- Readability, skip no-op statements
- Fix KCatalog::translate when translation is same as original text
- a file has been renamed
- Let ki18n macro file name follow style of other find_package related files
- Se ha corregido la comprobación de configuración para «_nl_msg_cat_cntr».
- Don't generate files in the source directory
- libintl: Determine if _nl_msg_cat_cntr exists before use (bug 365917)
- Fix the binary-factory builds

### KIconThemes

- Respetar BUILD_TESTING.

### KIO

- Install kio related kdebugsettings category file
- rename private header to _p.h
- Remove custom icon selection for trash (bug 391200)
- Top-align labels in properties dialog
- Present error dialog when user tries to create directory named "." or ".." (bug 387449)
- API dox: talk about nullptr, not 0
- kcoredirlister lstItems benchmark
- [KSambaShare] Check file that's changed before reloading
- [KDirOperator] Use alternating background colors for multi-column views
- avoid memory leak in slave jobs (bug 396651)
- SlaveInterface: deprecate setConnection/connection, nobody can use them anyway
- Slightly faster UDS constructor
- [KFilePlacesModel] Support pretty baloosearch URLs
- Remove projects.kde.org web shortcut
- Switch KIO::convertSize() to KFormat::formatByteSize()
- Use non deprecated fastInsert in file.cpp (first of many to come)
- Replace Gitorious web shortcut by GitLab
- Don't show confirmation dialog for Trash action by default (bug 385492)
- Don't ask for passwords in kfilewidgettest

### Kirigami

- support dynamically adding and removing title (bug 396417)
- introduce actionsVisible (bug 396413)
- adapt margins when scrollbar appears/disappear
- better management of the size (bug 396983)
- Optimise setting up the palette
- AbstractApplciationItem shouldn't have its own size, only implicit
- new signals pagePushed/pageRemoved
- Se ha corregido la lógica.
- add ScenePosition element (bug 396877)
- No need to emit the intermediary palette for every state
- hide-&gt;show
- Collapsible Sidebar Mode
- kirigami_package_breeze_icons: don't treat lists as elements (bug 396626)
- fix search/replace regexp (bug 396294)
- animating a color produces a rather unpleasant effect (bug 389534)
- color focused item for keyboard navigation
- remove quit shortcut
- Remove long-time deprecated Encoding=UTF-8 from desktop format file
- Se ha corregido el tamaño de la barra de herramientas (error 396521).
- Se ha corregido el tamaño de las asas.
- Respetar BUILD_TESTING.
- Show icons for actions that have an icon source rather than an icon name

### KItemViews

- Respetar BUILD_TESTING.

### KConfigWidgets

- Respetar BUILD_TESTING.

### KJS

- Respetar BUILD_TESTING.

### KMediaPlayer

- Respetar BUILD_TESTING.

### KNewStuff

- Remove long-time deprecated Encoding=UTF-8 from desktop format files
- Change default sort order in the download dialog to Rating
- Fix DownloadDialog window margins to meet general theme margins
- Restore accidentally removed qCDebug
- Use the right QSharedPointer API
- Handle empty preview lists

### KNotification

- Respetar BUILD_TESTING.

### Framework KPackage

- Respetar BUILD_TESTING.

### KParts

- API dox: talk about nullptr, not 0

### KPeople

- Respetar BUILD_TESTING.

### KPlotting

- Respetar BUILD_TESTING.

### KPty

- Respetar BUILD_TESTING.

### KRunner

- Respetar BUILD_TESTING.

### KService

- API dox: talk about nullptr, not 0
- Require out-of-source build
- Add subseq operator to match sub-sequences

### KTextEditor

- proper fix for the raw string indenting auto-quoting
- fix indenter to cope with new syntax file in syntaxhighlighting framework
- adjust test to new state in syntax-highlighting repository
- Show "Search wrapped" message in center of view for better visibility
- fix warning, just use isNull()
- Extender la API de guiones.
- fix segfault on rare cases where empty vector occurs for word count
- enforce clear of scrollbar preview on document clear (bug 374630)

### KTextWidgets

- API docs: partial revert of earlier commit, didn't really work
- KFindDialog: give the lineedit focus when showing a reused dialog
- KFind: reiniciar el contador cuando se cambia el patrón (por ejemplo, en el diálogo para buscar).
- Respetar BUILD_TESTING.

### KUnitConversion

- Respetar BUILD_TESTING.

### Framework KWallet

- Respetar BUILD_TESTING.

### KWayland

- Cleanup RemoteAccess buffers on aboutToBeUnbound instead of object destruction
- Support cursor hints on locked pointer
- Reduce unnecessary long wait times on failing signal spies
- Fix selection and seat auto tests
- Replace remaining V5 compat global includes
- Add XDG WM Base support to our XDGShell API
- Make XDGShellV5 co-compilable with XDGWMBase

### KWidgetsAddons

- Fix KTimeComboBox input mask for AM/PM times (bug 361764)
- Respetar BUILD_TESTING.

### KWindowSystem

- Respetar BUILD_TESTING.

### KXMLGUI

- Hacer que «KMainWindow» no guarde las preferencias del widget de forma incorrecta (error 395988).
- Respetar BUILD_TESTING.

### KXmlRpcClient

- Respetar BUILD_TESTING.

### Framework de Plasma

- if an applet is invalid, it has immediately UiReadyConstraint
- [Plasma PluginLoader] Cache plugins during startup
- Fix fading node when one textured is atlassed
- [Interfaz de contenedores] No cargar acciones de contenedores con la restricción del modo kiosko «plasma/containment_actions».
- Respetar BUILD_TESTING.

### Prison

- Fix Mixed to Upper mode latching in Aztec code generation

### Purpose

- Make sure debugging for kf5.kio.core.copyjob is disabled for the test
- Revert "test: A more "atomic" way of checking for the signal to happen"
- test: A more "atomic" way of checking for the signal to happen
- Add bluetooth plugin
- [Telegram] Don't wait for Telegram to be closed
- Prepare to use Arc's status colours in the revision drop-down list
- Respetar BUILD_TESTING.

### QQC2StyleBridge

- Improve sizing of menus (bug 396841)
- Remove double comparison
- Port away from string-based connects
- check for valid icon

### Solid

- Respetar BUILD_TESTING.

### Sonnet

- Sonnet: setLanguage should schedule a rehighlight if highlight is enabled
- Use the current hunspell API

### Resaltado de sintaxis

- CoffeeScript: fix templates in embedded JavaScript code &amp; add escapes
- Exclude this in Definition::includedDefinitions()
- Use in-class member initialization where possible
- Añadir funciones para acceder a las palabras clave.
- Add Definition::::formats()
- Add QVector&lt;Definition&gt; Definition::includedDefinitions() const
- Add Theme::TextStyle Format::textStyle() const;
- C++: fix standard floating-point literals (bug 389693)
- CSS: update syntax and fix some errors
- C++: update for c++20 and fix some syntax errors
- CoffeeScript &amp; JavaScript: fix member objects. Add .ts extension in JS (bug 366797)
- Lua: fix multi-line string (bug 395515)
- RPM Spec: add MIME type
- Python: fix escapes in quoted-comments (bug 386685)
- haskell.xml: don't highlight Prelude data constructors differently from others
- haskell.xml: remove types from "prelude function" section
- haskell.xml: highlight promoted data constructors
- haskell.xml: add keywords family, forall, pattern
- Respetar BUILD_TESTING.

### ThreadWeaver

- Respetar BUILD_TESTING.

### Información de seguridad

El código publicado se ha firmado con GPG usando la siguiente clave: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt;. Huella digital de la clave primaria: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB.

Puede comentar esta versión y compartir sus ideas en la sección de comentarios de <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>el artículo de dot</a>.
