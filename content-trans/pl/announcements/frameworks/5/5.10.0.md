---
aliases:
- ../../kde-frameworks-5.10.0
date: '2015-05-08'
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### KAktywności

- (brak dziennika zmian)

### KConfig

- Tworzenie klas QML-proof przy użyciu kconfigcompiler

### KCoreAddons

- Nowe marko cmake kcoreaddons_add_plugin do łatwiejszego tworzenia wtyczek opartych na KPluginLoader.

### KDeclarative

- Usunięto usterkę w pamięci podręcznej tekstur.
- i inne poprawki

### KGlobalAccel

- Dodano nową metodę globalShortcut, która pobiera skróty tak jak zostało to wskazane w ustawieniach globalnych.

### KIdleTime

- Usunięto awarię kidletime na platformie wayland

### KIO

- Dodano KPropertiesDialog::KPropertiesDialog(urls) oraz KPropertiesDialog::showDialog(urls).
- Asynchronicznie, oparte na QIODevice pobieranie danych dla KIO::storedPut oraz KIO::AccessManager::put.
- Naprawiono warunki dla wartości zwracanej QFile::rename (błąd 343329)
- Naprawiono KIO::suggestName, tak aby podawało lepsze nazwy (błąd 341773)
- kioexec: Naprawiono ścieżkę dla miejsc zapisywalnych dla kurl (błąd 343329)
- Przechowuj zakładki tylko w user-places.xbel (błąd 345174)
- Powiel wpis ostatnich dokumentów, jeśli dwa różne pliki mają tę samą nazwę
- Lepsza wiadomość błędu, jeśli pojedynczy plik jest za duży na kosz (błąd 332692)
- Usunięto usterkę KDirLister po przekierowaniu w chwili wywołania przez slot openURL

### KNewStuff

- Nowy zestaw klas zwany KMoreTools. KMoreTools pomaga w dodawaniu wskazówek o zewnętrznych narzędziach, które prawdopodobnie nie są jeszcze zainstalowane. Dodatkowo, czyni długie menu krótszymi dostarczając sekcję główną i więcej sekcji, co jest także do ustawienia po stronie użytkownika.

### KNotifications

- Naprawiono KNotifications przy użyciu Ubuntu NotifyOSD (błąd 345973)
- Nie wyzwalaj uaktualnienia w powiadomieniach przy ustawianiu tych samych właściwości (błąd 345973)
- Wprowadzono flagę LoopSound umożliwiającym odgrywanie dźwięków w pętli przez powiadomienia (błąd 346148)
- Bez usterki, gdy powiadomienie nie ma elementu interfejsu

### KPackage

- Dodano funkcję KPackage::findPackages podobną do KPluginLoader::findPlugins

### KLudzie

- Używaj KPluginFactory do uruchamiania wtyczek, zamiast KService (zachowane dla zgodności).

### KService

- Poprawiono zły podział ścieżki pola wprowadzania (błąd 344614)

### Portfel

- Usługa przenoszenia teraz sprawdza czy stary portfel jest pusty zanim zacznie (błąd 346498)

### KWidgetsAddons

- KDateTimeEdit: Działania użytkownika na wejściu są rejestrowane. Naprawiono podwójne marginesy Fix double margins.
- KFontRequester: naprawiono wybieranie czcionek tylko o stałej szerokości

### KWindowSystem

- Nie polegaj na QX11Info w KXUtils::createPixmapFromHandle (błąd 346496)
- nowa metoda NETWinInfo::xcbConnection() -&gt; xcb_connection_t*

### KXmlGui

- Naprawiono skróty przy ustawieniu drugiego skrótu (błąd 345411)
- Uaktualniono listę bugzilli produktów/składników do zgłaszania błędów (błąd 346559)
- Globalne skróty: umożliwiono ustawienie także skrótu alternatywnego

### ZarządzanieSieciąQt

- Zainstalowane nagłówki są teraz zorganizowane tak samo jak wszystkie inne szkielety.

### Szkielety Plazmy

- Składniki Plazmy. Menu obsługuje sekcje.
- Używaj KPluginLoader zamiast ksycoca do wczytywania silników danych C++
- Rozważ obrót visualParent w popupPosition (błąd 345787)

### Sonnet

- Nie próbuj podświetlać, jeśli nie znaleziono sprawdzania pisowni. Spowoduje to niekończącą się pętlę z ciągłym wyzwalaniem czasomierza rehighlighRequest.

### Integracja Szkieletów

- Naprawiono natywne okna dialogowe plików z elementów interfejsów QFileDialog: ** Okna dialogowe plików otwarte poprzez exec() i bez rodzica były otwierane, lecz działania użytkownika były blokowane w ten sposób, że nie można było wybrać ani pliku, ani katalogu, ani nie można było zamknąć tego okna. ** Okna dialogowe plików otwarte poprzez open() lub show() z rodzicem nie były w ogóle otwierane.

Możesz przedyskutować i podzielić się pomysłami dotyczącymi tego wydania w dziale komentarzy <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artykułu dot</a>.
