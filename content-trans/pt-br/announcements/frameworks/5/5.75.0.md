---
aliases:
- ../../kde-frameworks-5.75.0
date: 2020-10-10
layout: framework
libCount: 70
qtversion: 5.12
---
### Baloo

+ [AdvancedQueryParser] Alívio do processamento do texto que termina em parêntesis
+ [AdvancedQueryParser] Alívio do processamento do texto que termina com um comparador
+ [AdvancedQueryParser] Correcção de acesso fora dos limites caso o último carácter seja um comparador
+ [Term] Substituição do construtor Term::Private com valores predefinidos
+ [BasicIndexingJob] Atalho para a obtenção do XAttr nos ficheiros sem atributos
+ [extractor] Correcção do tipo de documento no resultado da extracção
+ Mudança da licença do ficheiro para LGPL-2.0-ou-posterior

### BluezQt

+ Adição da propriedade 'rfkill' ao gestor
+ Adição da propriedade 'status' ao 'rfkill'
+ Registo do Rfkill para o QML
+ Exportação do Rfkill
+ Suporte do fornecimento dos dados do serviço para publicidade LE

### Ícones Breeze

+ Adição de novo ícone genérico de "comportamento"
+ Fazer com que a validação do ícone dependa da geração do ícone apenas se estiver activa
+ Substituição de programa em Bash do ícone a 24px por um programa em Python
+ Uso de iconografia com estilo de bandeiras para o 'view-calendar-holiday'
+ Adição do logótipo do Plasma Nano
+ Adição do 'application-x-kmymoney'
+ Adição do ícone do KMyMoney

### Módulos extra do CMake

+ Correcção do 'fetch-translations' para os URL's do Invent
+ Inclusão do FeatureSummary e dos módulos de pesquisa
+ Introdução de verificação de plausibilidade para licenças sem limites
+ Adição do CheckAtomic.cmake
+ Correcção da configuração com o 'pthread' no Android a 32 bits
+ Adição do parâmetro RENAME do ecm_generate_dbus_service_file
+ Correcção do 'find_library' no Android com o NDK &lt; 22
+ Ordenação explícita das listas de versões do Android
+ Gravação do Android {min,target,compile}Sdk em variáveis

### Ferramentas Doxygen do KDE

+ Melhorias nas licenças
+ Correcção do api.kde.org em dispositivos móveis
+ Transformação do api.kde.org numa PWA

### KArchive

+ Mudança da licença do ficheiro para LGPL-2.0-ou-posterior

### KAuth

+ uso de nova variável de instalação (erro 415938)
+ Marcação do David Edmundson como responsável de manutenção do KAuth

### KCalendarCore

+ Mudança da licença do ficheiro para LGPL-2.0-ou-posterior

### KCMUtils

+ Remoção do truque de tratamento dos eventos interiores numa página (erro 423080)

### KCompletion

+ Mudança da licença dos ficheiros para LGPL-2.0-ou-posterior

### KConfig

+ CMake: Activar também o SKIP_AUTOUIC nos ficheiros gerados
+ Uso da ordem inversa no KDesktopFile::locateLocal parar percorrer os locais de configuração genéricos

### KConfigWidgets

+ Correcção do 'isDefault' que fazia com que o KCModule não actualizasse o seu estado predefinido de forma adequada
+ [kcolorscheme]: Adição do 'isColorSetSupported' para verificar se um esquema de cores tem um dado conjunto de cores
+ [kcolorscheme] Ler de forma adequada as cores Inactivas personalizadas para os fundos

### KContacts

+ Mudança do ficheiro de licença obsoleto para o LGPL-2.0-apenas
+ Mudança da licença dos ficheiros para LGPL-2.0-ou-posterior

### KCoreAddons

+ KJob: emissão dos eventos result() e finished() no máximo uma vez
+ Adição do método de leitura protegido KJob::isFinished()
+ Descontinuação do KRandomSequence em detrimento do QRandomGenerator
+ Inicialização da variável na classe de cabeçalho + passagem de ponteiros/variáveis para constantes
+ reforço dos testes baseados em mensagens face ao ambiente (erro 387006)
+ simplificação do teste de vigilância do 'qrc' (erro 387006)
+ contagem de referências e remoção de instâncias do KDirWatchPrivate (erro 423928)
+ Descontinuação do KBackup::backupFile() devido à perda da funcionalidade
+ Descontinuação do KBackup::rcsBackupFile(...) devido à inexistência de utilizadores conhecidos

### KDBusAddons

+ Mudança da licença dos ficheiros para LGPL-2.0-ou-posterior

### KDeclarative

+ O QML do I18n foi adicionado no KF 5.17
+ Mudança da licença dos ficheiros para LGPL-2.0-ou-posterior
+ Bloquear os atalhos ao gravar as sequências de teclas (erro 425979)
+ Adição do SettingHighlighter como uma versão manual do realce feito pelo SettingStateBinding

### KDELibs 4 Support

+ KStandardDirs: resolver sempre as ligações simbólicas para os ficheiros de configuração

### KHolidays

+ Remoção dos comentários de campos de descrição para os ficheiros de feriados mt_*
+ Adição dos feriados nacionais de Malta tanto em Inglês (en-gb) como em Maltês (mt)

### KI18n

+ Mudança da licença do ficheiro para LGPL-2.0-ou-posterior

### KIO

+ KUrlNavigator: usar sempre o "desktop:/", não o "desktop:"
+ Suporte da sintaxe 'bang' do DuckDuckGo nos Atalhos da Web (erro 374637)
+ KNewFileMenu: o KIO::mostLocalUrl é útil apenas com protocolos :local
+ Descontinuação do KIO::pixmapForUrl
+ kio_trash: remoção da verificação desnecessariamente restrita (erro 76380)
+ OpenUrlJob: tratamento consistente de todos os programas de texto (erro 425177)
+ KProcessRunner: mais meta-dados do 'systemd'
+ KDirOperator: não invocar o 'setCurrentItem' para um URL vazio (erro 425163)
+ KNewFileMenu: correcção da criação de uma nova pasta cujo nome começa por ':' (erro 425396)
+ StatJob: clarificar que o 'mostLocalUrl' funciona apenas com protocolos :locais
+ Documentação em como adicionar novos papéis "aleatórios" no kfileplacesmodel.h
+ Remoção do antigo truque do 'kio_fonts' no KCoreDirLister, onde o nome da máquina era retirado de forma incorrecta
+ KUrlCompletion: acomodação dos protocolos ":local" que usam um nome de máquina no URL
+ Divisão do código do método 'addServiceActionsTo' em métodos mais pequenos
+ [kio] ERRO: Permitir aspas na janela para abrir/gravar (erro 185433)
+ StatJob: cancelar a tarefa se o URL for inválido (erro 426367)
+ Ligar os 'slots' explicitamente em vez de usar ligações automáticas
+ Fazer com que a API do 'filesharingpage' funcione de facto

### Kirigami

+ AbstractApplicationHeader: 'anchors.fill' em vez de âncoras dependentes da posição
+ Melhoria da aparência e consistência do GlobalDrawerActionItem
+ Remoção da indentação do formulário nos formatos estreitos
+ Reversão da funcionalidade para "permitir a personalização das cores do cabeçalho"
+ possibilitar a personalização das cores do cabeçalho
+ Adição de '@since' em falta para as propriedades da área pintada
+ Introdução das propriedades 'paintedWidth/paintedHeight' do estilo do Image do QtQuick
+ Adição de uma propriedade de imagem de substituição do ícone (para fins de contingência)
+ Remoção do comportamento personalizado do 'implicitWidth' e 'implicitHeight' do Icon
+ Guarda de um ponteiro potencialmente nulo (o que costuma ser de facto frequente)
+ 'setStatus' protegido
+ Reintrodução da propriedade 'status'
+ Suporte para os fornecedores de imagens do tipo ImageResponse e Texture no Kirigami::Icon
+ Avisar as pessoas para não usarem o ScrollView no ScrollablePage
+ Reversão de "mostrar sempre o separador"
+ fazer com que o modo móvel suporte delegações de títulos personalizados
+ Esconder a linha separadora da área de navegação se o formato dos botões estiver visível mas se tiver largura zero (erro 426738)
+ [ícone] Considerar um ícone inválido quando a origem é um URL vazio
+ Modificação do Units.fontMetrics para usar de facto o FontMetrics
+ Adição da propriedade Kirigami.FormData.labelAlignment
+ mostrar sempre o separador
+ Uso das cores do Header para o AbstractApplicationHeader do computador
+ Uso do contexto do componente ao criar métodos delegados do ToolBarLayout
+ Interromper e apagar os incubadores ao apagar o ToolBarLayoutDelegate
+ Remoção das acções e métodos delegados do ToolBarLayout quando são destruídos (erro 425670)
+ Substituição do uso de uma conversão de ponteiro à C no 'sizegroup'
+ as constantes binárias são uma extensão do C++14
+ sizegroup: Correcção dos avisos onde o enumerado não é tratado
+ sizegroup: Correcção das ligações com 3 argumentos
+ sizegroup: Adição de CONSTANTE ao sinal
+ Correcção de alguns casos onde se usam ciclos de intervalos com contentores de Qt não constantes
+ Restringir a altura do botão na barra de ferramentas global
+ Mostrar um separador entre as área de navegação e os ícones à esquerda
+ Uso do KDE_INSTALL_TARGETS_DEFAULT_ARGS
+ Dimensionamento do ApplicationHeaders com o SizeGroup
+ Introdução do SizeGroup
+ Correcção: fazer com que o indicador de actualização apareça por cima dos cabeçalhos da lista
+ pôr as folhas sobrepostas sobre as gavetas

### KItemModels

+ ignorar o 'sourceDataChanged' com índices inválidos
+ Suporte para os nós "fechados" do KDescendantProxyModel

### KNewStuff

+ Acompanhamento manual do ciclo de vida dos nossos módulos internos do KPackage
+ Actualização das versões das actualizações de KPackage's "falsos" (erro 427201)
+ Não usar o parâmetro predefinido quando não for necessário
+ Correcção de estoiro ao instalar os 'kpackages' (erro 426732)
+ Detectar quando a 'cache' muda e reagir de acordo com isso
+ Correcção da actualização  do item se o número de versão estiver vazio (erro 417510)
+ Mudança de ponteiro para constante + inicialização de variável no ficheiro de inclusão
+ Mudança da licença do ficheiro para LGPL-2.0-ou-posterior
+ Aceitar a sugestão de aquisição da manutenção

### KNotification

+ Baixar a versão do 'plugin' de Android até que esteja disponível um novo Gradle
+ Uso das versões do Android SDK do ECM
+ Descontinuação do construtor do KNotification com um parâmetro 'widget'

### Plataforma KPackage

+ Correcção das notificações do DBus quando instalar/actualizar
+ Mudança da licença do ficheiro para LGPL-2.0-ou-posterior

### KParts

+ Instalação dos ficheiros de definição de tipos de serviços krop &amp; krwp com um tipo correspondente ao nome do ficheiro

### KQuickCharts

+ Evitar um ciclo de associações dentro do Legend
+ Remoção da verificação do GLES3 no SDFShader (erro 426458)

### KRunner

+ Adição da propriedade 'matchRegex' para evitar o lançamento desnecessário de tarefas
+ Permitir a definição de acções no QueryMatch
+ Permitir definir acções individuais para as correspondências do módulo de execução do D-Bus
+ Mudança da licença dos ficheiros para LGPL-2.0-ou-posterior
+ Adição da propriedade de número mínimo de letras
+ Ter em consideração a variável de ambiente XDG_DATA_HOME nas pastas de instalação de modelos
+ Melhoria nas mensagens de erro dos módulos de execução do D-Bus
+ Início da emissão dos avisos de migração dos meta-dados durante a execução

### KService

+ reposição do 'disableAutoRebuild' do 'brink' (erro 423931)

### KTextEditor

+ [kateprinter] Migração dos métodos obsoletos do QPrinter
+ Não criar memória temporária para detectar o tipo MIME de um ficheiro gravado localmente
+ evitar um bloqueio devido ao carregamento do dicionário e trigramas na primeira escrita
+ [Modo VI] Mostrar sempre os 'buffers' a-z em minúsculas
+ [KateFadeEffect] emissão de hideAnimationFinished() quando o desvanecimento por uma aparição
+ garantia de um contorno perfeito ao nível do pixel mesmo em desenhos à escala
+ garantia de que é pintado o separador do contorno sobre todas as outras coisas, como os destaques de dobragem
+ passagem do separador de entre o contorno de ícones e os números de linha para entre a barra e o texto
+ [Modo VI] Correcção do comportamento dos registos numerados
+ [Modo VI] Colocação do texto apagado no registo adequado
+ Reposição do comportamento da pesquisa na selecção quando não está disponível nenhuma selecção
+ não existem mais ficheiros LGPL-2.1-apenas ou LGPL-3.0-apenas
+ Mudança da licença dos ficheiros para LGPL-2.0-ou-posterior
+ uso de método de modificação não necessário para algumas cores do tema
+ O 5.75 será por uma vez incompatível, usando por omissão a 'Selecção Automática do Tema de Cores' nos temas
+ alteração do esquema =&gt; tema no código para evitar confusões
+ redução do cabeçalho proposto da licença para o estado actual
+ garantia de que se fica sempre com algum tema válido
+ melhoria da janela Copiar...
+ correcção de mais alguns nomes novo =&gt; cópia
+ adição de alguns KMessageWidget que sugerem que os temas apenas para leitura sejam copiados, mudança de nome =&gt; cópia
+ desactivação da edição de temas apenas para leitura
+ a gravação do estilo específico de realce substitui o trabalho, sendo gravadas apenas as diferenças
+ simplificação da criação de atributos; as cores transparentes agora são devidamente tratadas no Format
+ tentar limitar a exportação dos atributos das mudanças, sendo que está parcialmente funcional, com o problema que ainda são exportados os nomes errados das definições incluídas
+ iniciar o cálculo das predefinições 'reais' com base no tema actual e dos formatos sem substituições do estilo no realce
+ correcção da acção de reposição
+ armazenamento das substituições específicas da sintaxe, sendo que de momento só são guardadas todas as coisas que foram carregadas na árvore
+ começar a trabalhar nas substituições específicas do realce de sintaxe, sendo que de momento aparecem apenas os estilos que um dado realce realmente possui
+ permitir a gravação das alterações face ao estilo predefinido
+ permitir a gravação das mudanças de cores
+ implementação da exportação do tema: cópia simples de ficheiro
+ não existe uma importação/exportação específica do realce, o que não faz sentido com o novo formato .theme
+ implementação da importação de ficheiros .theme
+ trabalho na criação &amp; remoção de temas, sendo que a criação irá copiar o tema actual como ponto de partida
+ uso das cores do tema em todo o lado
+ remoção de mais código antigo do esquema em detrimento do KSyntaxHighlighting::Theme
+ começar a usar as cores definidas pelo tema, sem existir qualquer lógica nova quanto a isso
+ inicialização do 'm_pasteSelection' e aumento da versão do ficheiro UI
+ adição de atalho para colar a selecção do rato
+ evitar o 'setTheme', sendo que podemos passar simplesmente o tema às funções auxiliares
+ correcção da dica, sendo que será reposta a predefinição do tema
+ exportação da configuração predefinida dos estilos para um tema em JSON
+ início do trabalho de exportação para JSON do tema, activada ao usar a extensão .theme na janela de exportação
+ mudança de 'Usar o Tema de Cores do KDE' para 'Usar as Cores Predefinidas' que é o efeito actual de facto
+ não fornecer o tema vazio 'KDE' por omissão
+ suporte da selecção automática do tema correcto de cores actual do Qt/KDE
+ conversão dos nomes antigos dos temas para os novos, uso de um novo ficheiro de configuração e transferência apenas uma vez dos dados
+ mudança da configuração de tipos de letra para a aparência, mudança do nome esquema =&gt; tema de cores
+ remoção do nome fixo do tema predefinido; uso dos acessores do KSyntaxHighlighting
+ carregamento das cores de contingência do tema
+ não incluir as cores incorporadas de todo
+ uso da função correcta para pesquisar o tema
+ as cores do editor são agora usadas a partir do Theme
+ uso do enumerado KSyntaxHighlighting::Theme::EditorColorRole
+ tratamento da transição Normal =&gt; Predefinido
+ primeiro passo: carregamento da lista de temas do KSyntaxHighlighting, sendo que agora existem como esquemas conhecidos no KTextEditor

### KUnitConversion

+ Uso de maiúsculas na Eficiência de Combustível

### KWayland

+ Não colocar em 'cache' o iterador QList::end() se for usado o erase()

### KWidgetsAddons

+ kviewstateserializer.cpp - guardas contra estoiros no restoreScrollBarState()

### KWindowSystem

+ Mudança da licença dos ficheiros para ser compatível com o LGPL-2.1

### KXMLGUI

+ [kmainwindow] Não apagar elementos de um 'kconfiggroup' inválido (erro 427236)
+ Não sobrepor as janelas principais ao abrir instâncias adicionais (erro 426725)
+ [kmainwindow] Não criar janelas nativas para janelas que não sejam do nível de topo (erro 424024)
+ KAboutApplicationDialog: avoid empty "Libraries" tab if HideKdeVersion is set
+ Mostrar o código da língua para aleḿ do nome (traduzido) na janela de mudança da língua da aplicação
+ Descontinuação do KShortcutsEditor::undoChanges() em detrimento do novo undo()
+ Tratamento do duplo fecho na janela principal (erro 416728)

### Plasma Framework

+ Correcção do plasmoidheading.svgz que estava a ser instalado no local errado (erro 426537)
+ Fornecimento de um valor de 'lastModified' no ThemeTest
+ Detecção de que estamos à procura de um elemento vazio e saída antecipada nesse caso
+ Mudança para que as Tooltip's do PlasmaComponents3 usem o estilo típico das dicas (erro 424506)
+ Uso das cores do Header nos cabeçalhos PlasmoidHeading
+ Mudança do tipo de alívio do movimento animado da TabBar do PC2 para OutCubic
+ Adição do suporte para o conjunto de cores do Tooltip
+ Correcção dos ícones do Button/ToolButton do PC3 que nem sempre mostra o conjunto de cores correcto (erro 426556)
+ Garantia de que o FrameSvg usa a data/hora 'lastModified' ao usar a 'cache' (erro 426674)
+ Garantia de que se tem uma data/hora 'lastModified' válida quando é invocado o método 'setImagePath'
+ Descontinuação da data/hora 'lastModified' a 0 no Theme::findInCache (erro 426674)
+ Adaptação da importação do QQC2 para o novo esquema de versões
+ [windowthumbnail] Verificação se existe o GLContext relevante, e não qualquer um
+ Adição da importação do PlasmaCore em falta no ButtonRow.qml
+ Correcção de mais alguns erros de referências no PlasmaExtras.ListItem
+ Correcção de erro no 'implicitBackgroundWidth' do PlasmaExtras.ListItem
+ Chamar ao modo de edição "Modo de Edição"
+ correcção de um TypeError no QueryDialog.qml
+ Correcção de um ReferenceError do PlasmaCore no Button.qml

### QQC2StyleBridge

+ Usar também a cor do texto realçado quando o 'checkDelegate' estiver realçado (erro 427022)
+ Respeitar o 'click' na barra de deslocamento para saltar para a posição indicada (erro 412685)
+ Não herdar cores no estilo de barras de ferramentas por omissão
+ Mudança da licença do ficheiro para LGPL-2.0-ou-posterior
+ Uso das cores do cabeçalho apenas nas barras de ferramentas do cabeçalho
+ Mudança da declaração do conjunto de cores para um local onde possa ser substituído
+ Uso das cores do Header no estilo do ToolBar
+ adição da propriedade 'isItem' em falta, que é necessária nas árvores

### Sonnet

+ Redução da versão dos resultados dos trigramas

### Realce de sintaxe

+ AppArmor: correcção da expressão regular de detecção dos locais
+ AppArmor: actualização do realce para o AppArmor 3.0
+ 'cache' das cores nas conversões de 'rgb' para 'ansi256colors' (acelera o carregamento do Markdown)
+ SELinux: uso das palavras-chave 'include'
+ Legendas do SubRip &amp; Logcat: pequenas melhorias
+ gerador do doxygenlua.xml
+ Correcção das fórmulas de LaTeX do Doxygen (erro 426466)
+ uso do Q_ASSERT como no resto da plataforma + correcção de validações
+ mudança do --format-trace para --syntax-trace
+ aplicação de um estilo nas regiões
+ seguimento dos contextos e regiões
+ uso da cor de fundo do editor por omissão
+ Realce de sintaxe ANSI
+ Adição de 'copyright' e actualização da cor do separador no tema Radical
+ Actualização da cor do separador nos temas Solares
+ Melhoria da cor do separador e do contorno dos ícones nos temas Ayu, Nord e Vim Escuro
+ tornar a cor do separador menos intrusiva
+ importação do esquema do Kate para temas feita por Juraj Oravec
+ secção mais destacada sobre as licenças, que remete para a nossa cópia do MIT.txt
+ primeiro modelo de gerador de base16, https://github.com/chriskempson/base16
+ adição de informação de licença adequada para todos os temas
+ Adição do tema de cores Radical
+ Adição do tema de cores Nord
+ melhoria do caso de apresentação de temas para mostrar mais estilos
+ Adição dos temas Gruvbox Claro e Escuro, com a licença do MIT
+ Adição do tema de cores Ayu (com variantes claras, escuras e miragem)
+ Adição de alternativa POSIX para a atribuição simples de variáveis
+ ferramentas para gerar um grafo a partir de um ficheiro de sintaxe
+ correcção da conversão automática do QRgb indefinido == 0 para "preto" em vez de "transparente"
+ Adição de ficheiros de exemplo do 'changelog' e 'control' da Debian
+ adição do tema de cores Drácula

### Informações de segurança

O código lançado foi assinado com GPG, usando a seguinte chave: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Impressão digital da chave primária: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB
