---
aliases:
- ../../kde-frameworks-5.68.0
date: 2020-03-07
layout: framework
libCount: 70
---
### Baloo

- [ModifiedFileIndexer] Corrige as verificações de tempo para arquivos novos
- [ModifiedFileIndexer] Omite a execução do BasicIndexingJob quando não solicitado
- Sincroniza o IndexerConfig ao sair (bug 417127)
- [FileIndexScheduler] Força a avaliação do indexerState após suspender/resumir

### BluezQt

- Corrige erros no commit de porte da QRegularExpression

### Ícones Breeze

- Adiciona o ícone network-wireless-hotspot
- Move os ícones de painel do Telegram para a categoria status
- [breeze-icons] Adiciona os ícones do telegram-desktop na área de notificação (bug 417583)
- [breeze-icons] Novo ícone do Telegram em 48px
- Adiciona ícone RSS em ação
- Remove ícones do Telegram em 48px
- Correção para certificar que a validação não foi feita me paralelo à geração
- Novo ícone/logotipo do yakuake
- Corrige inconsistências e duplicatas nos ícones network-wired/wireless
- Corrige valores de cores de texto antigas para ícones osd-*
- Instala apenas ícones gerados apenas se eles forem gerados
- Colocar aspas em todos os caminhos para garantir que os sistemas CI funcionem
- Define -e no script gerador para ele retornar adequadamente os erros
- Compilação: corrige a compilação quando o prefixo de instalação não for gravável pelo usuário
- Correção no novo gerador 24px para usar bash ao invés de sh
- Gerar automaticamente também links simbólicos de compatibilidade 24@2x
- Gerar automaticamente ícones monocromáticos 24px
- Adicionar ícones que estavam apenas em actions/24 no actions/22
- Definir a escala do documento para 1.0 para todos os ícones actions/22
- Adicionar novos ícones <code>smiley-add</code>
- Consistência dos ícones 'shapes' e 'shape-choose' com os outros ícones '-shape'
- Consistência do 'smiley-shape' com os outros ícones '-shape'
- Consistência dos ícones 'flower-shape' e 'hexagon-choose' com os outros ícones '-shape'
- Substituição do &lt;use/&gt; por &lt;path/&gt; no muondiscover.svg
- Adição dos ícones de estado: data-error, data-warning, data-information
- Adição de ícone para o org.kde.Ikona
- adição de ícone do vvave
- adição de ícone do puremaps
- Unificação da aparência de todos os ícones que contêm 🚫 (sinal de não)
- Novo ícone para o KTimeTracker (erro 410708)
- Optimização dos ícones do KTrip e do KDE Itinerary
- actualização dos ícones 'travel-family'

### Módulos extra do CMake

- Suporte para o NDK r20 e o Qt 5.14
- Carregamento dos ficheiros QM dos URL's assets: no Android
- Adição do ecm_qt_install_logging_categories &amp; ecm_qt_export_logging_category
- ECMGeneratePriFile: remoção de problemas com utilizações do LIB_NAME sem ser um nome de alvo
- ECMGeneratePriFile: Correcção das configurações estáticas

### Integração do Framework

- [KStyle] Configuração da cor das KMessageWidgets com a correcta do esquema de cores actual

### KActivities

- Correcção de problema ao descobrir as pastas de inclusão do Boost
- Uso dos métodos expostos no DBus para mudar de actividades na CLI

### KAuth

- [KAuth] Adição do suporte para os detalhes da acção na infra-estrutura do Polkit1
- [policy-gen] Correcção do código para usar de facto o grupo de captura correcto
- Eliminação da infra-estrutura do Policykit
- [polkit-1] Simplificação da pesquisa da existência da acção do Polkit1Backend
- [polkit-1] Devolução de um estado de erro no 'actionStatus' se ocorreu um erro
- Cálculo do KAuthAction::isValid a pedido

### KBookmarks

- Mudança de nomes das acções por questões de consistência

### KCalendarCore

- Actualização da 'cache' de visibilidade quando a visibilidade do portátil muda

### KCMUtils

- Verificação do 'activeModule' antes de o usar (erro 417396)

### KConfig

- [KConfigGui] Limpeza da propriedade de tipos de letra 'styleName' nos estilos de letra Regular (erro 378523)
- Correcção da geração de código para os itens com min/max (erro 418146)
- KConfigSkeletonItem : possibilidade de definir um KconfigGroup para ler e gravar os itens em grupos encadeados
- Correcção da propriedade gerada is&lt;NomePropriedade&gt;Immutable
- Adição do setNotifyFunction ao KPropertySkeletonItem
- Adição de um  is&lt;NomePropriedade&gt;Immutable para saber se uma dada propriedade é imutável

### KConfigWidgets

- Mudança de "Redisplay" (Mostrar de Novo) para "Refresh" (Actualizar)

### KCoreAddons

- adição de uma sugestão que o QIcon pode ser usado como um logótipo do programa

### KDBusAddons

- Descontinuação do KDBusConnectionPool

### KDeclarative

- Exposição do sinal 'capture' no KeySequenceItem
- Correcção do tamanho do cabeçalho no GridViewKCM (erro 417347)
- Possibilidade de uma classe derivada de ManagedConfigModule registar de forma explícita o KCoreConfigSkeleton
- Possibilidade de usar o KPropertySkeletonItem no ManagedConfigModule

### KDED

- Adição de uma opção --replace ao kded5

### Complementos da interface KDE

- [UrlHandler] Tratamento da abertura da documentação 'online' para os módulos KCM
- [KColorUtils] Mudança da gama de matiz do getHcy() para [0.0, 1.0)

### KHolidays

- Atualização dos feriados japoneses
- holiday_jp_ja - correcção ortográfica do Dia Nacional da Fundação (erro 417498)

### KI18n

- Suporte para o Qt 5.14 no Android

### KInit

- Mudança do kwrapper/kshell para desencadear o klauncher5 se necessário

### KIO

- [KFileFilterCombo] Não adicionar um QMimeType inválido ao filtro de tipos MIME (erro 417355)
- [src/kcms/*] Substituição do 'foreach' (obsoleto) por 'for' baseado em 'range'/'index'
- KIO::iconNameForUrl(): tratamento do caso de um ficheiro/pasta sob o trash:/
- [krun] Partilha da implementação do 'runService' e do 'runApplication'
- [krun] Eliminação do suporte do KToolInvocation no KRun::runService
- Melhoria do KDirModel para evitar mostrar o '+' se não existirem sub-pastas
- Correcção do Konsole em execução no Wayland (erro 408497)
- KIO::iconNameForUrl: correcção da pesquisa pelos ícones de protocolo do KDE (erro 417069)
- Correcção da capitalização do item "ligação básica"
- Mudança do "AutoSkip" (Ignorar Automaticamente) para "Skip All" (Ignorar Tudo) (erro 416964)
- Correcção de fuga de memória no KUrlNavigatorPlacesSelector::updateMenu
- IO-Slave 'file': parar de copiar assim que o IO-Slave for morto
- [KOpenWithDialog] Selecção automática do resultado caso o filtro de modelos só tenha uma correspondência (erro 400725)

### Kirigami

- Mostrar uma dica com o URL completo para o botão de URL com texto extravasado
- Possibilidade de ter barras de ferramentas com recolha nas páginas deslizantes também nos rodapés
- Correcção do comportamento do PrivateActionToolButton com o 'showText' vs IconOnly
- Correcção do ActionToolBar/PrivateActionToolButton em conjunto com a Action do QQC2
- Mudança no item de menu assinalado para estar sempre dentro do intervalo
- Detecção dos eventos de mudança de língua, encaminhando-os para o motor de QML
- Suporte para o Qt 5.14 no Android
- não ter folhas sobrepostas sob o cabeçalho da página
- usar um substituto quando o ícone não conseguir ser carregado
- Ligações em falta para os ficheiros de código do 'pagepool'
- Ícone: correcção do desenho dos URL's image: em PPP's elevados (erro 417647)
- Não estoirar quando a largura ou a altura do ícone é igual a 0 (erro 417844)
- correcção das margens no OverlaySheet
- [examples/simplechatapp] Configurar sempre o 'isMenu' como 'true' (verdadeiro)
- [RFC] Redução do tamanho dos cabeçalhos de Nível 1 e aumento do preenchimento de espaço à esquerda para os títulos das páginas
- sincronização adequada das sugestões de tamanho com a máquina de estado (erro 417351)
- Adição do suporte para 'plugins' de temas estáticos da plataforma
- alinhamento correcto do 'headerParent' quando existir uma barra de deslocamento
- Correcção do cálculo da largura da barra de páginas
- Adição do PagePoolAction ao ficheiro QRC
- possibilidade de estilos de barras de ferramentas em dispositivos móveis
- Mudança da documentação da API para reflectir que o Kirigami não é só uma plataforma móvel

### KItemModels

- KRearrangeColumnsProxyModel: desactivação temporária da validação devido a um erro no QTreeView
- KRearrangeColumnsProxyModel: reposição do setSourceColumns()
- Passagem do SortFilterProxyModel do Plasma para o 'plugin' de QML do KItemModel

### KJS

- Exposição de funcionalidades de gestão da avaliação de tempos-limite na API pública

### KNewStuff

- Correcção da delegação ao carregar apenas numa miniatura (erro 418368)
- Correcção do deslocamento na página de EntryDetails (erro 418191)
- Não invocar o 'delete' a dobrar no CommentsModel (erro 417802)
- Cobertura também do 'plugin' do QtQuick no ficheiro de categorias instalado
- Uso do catálogo de traduções correcto para apresentar as mesmas
- Correcção do formato básico e do título do fecho da Dialog do KNSQuick (erro 414682)

### KNotification

- Disponibilização do 'kstatusnotifieritem' sem o 'dbus'
- Adaptação da numeração de acções no Android para funcionar como no KNotifications
- Remoção do Kai-Uwe como responsável de manutenção do 'knotifications'
- Limpar sempre o HTML caso o servidor não o suporte
- [android] Emissão do 'defaultActivated' quando tocar na notificação

### KPeople

- Correcção da geração do ficheiro PRI

### KQuickCharts

- Não imprimir erros sobre papéis inválidos quando o 'roleName' não estiver definido
- Uso de plataforma sem ecrã nos testes em Windows
- Remoção da transferência da validação do GLSL no texto da validação
- Correcção de erro de validação no desenho de gráficos de linhas
- Actualização do desenho de perfis de base do gráfico de linhas por questões de compatibilidade
- Adição de comentário sobre a verificação dos limites
- LineChart: Adição de suporte para a verificação dos limites mín/máx do Y
- Adição da função 'sdf_rectangle' à biblioteca SDF
- [gráfico de linhas] Protecção contra divisões por 0
- Gráficos de linhas: Redução do número de pontos por segmento
- Não perder pontos no fim de um gráfico de linhas

### Kross

- O Qt5::UiTools não é opcional neste módulo

### KService

- Novo mecanismo de pesquisa nas aplicações: KApplicationTrader

### KTextEditor

- Adição de uma opção para quebrar de forma dinâmica dentro das palavras
- KateModeMenuList: não sobrepor a barra de deslocamento

### KWayland

- Adição das localizações no DBus do menu da aplicação para a interface 'org_kde_plasma_window'
- Registry: não destruir a chamada de retorno no 'globalsync'
- [surface] Correcção da posição do 'buffer' ao associar 'buffers' às superfícies

### KWidgetsAddons

- [KMessageWidget] Possibilidade de o estilo mudar a nossa paleta de cores
- [KMessageWidget] Desenho com o QPainter em vez de usar uma folha de estilo
- Redução ligeira do tamanho do cabeçalho de nível 1

### ModemManagerQt

- Remoção da geração &amp; instalação dos ficheiros .pri do QMake por estar com problemas de momento

### NetworkManagerQt

- Suporte ao SAE no securityTypeFromConnectionSetting
- Remoção da geração &amp; instalação dos ficheiros .pri do QMake por estar com problemas de momento

### Ícones do Oxygen

- Suporte dos ícones 'data-error/warning/information' também nos tamanhos 32,46,64,128
- Adição do item de acção "plugins" para corresponder aos ícones do Brisa
- Adição dos ícones de estado: data-error, data-warning, data-information

### Plasma Framework

- Botões: possibilidade de ajustar a escala dos ícones
- Tentativa de aplicação do esquema de cores do tema actual nos QIcons (erro 417780)
- Dialog: fim da ligação aos sinais do QWindow no destrutor
- Correcção de fuga de memória no ConfigView e Dialog
- correcção das sugestões de tamanho das legendas dos botões
- validação de que as sugestões de tamanho são inteiras e pares
- suporte para o icon.width/height (erro 417514)
- Remoção de cores fixas (erro 417511)
- Construção do NullEngine com o KPluginMetaData() (erro 417548)
- Redução ligeira do tamanho do cabeçalho de nível 1
- Centrar na vertical o ícone/imagem da dica
- suporte para a propriedade 'display' nos Buttons
- Não avisar no caso de meta-dados inválidos do 'plugin' (erro 412464)
- as dicas têm sempre um grupo de cores normal
- [Testes] Mudança do radiobutton3.qml para usar o PC3
- Optimização do código ao largar ficheiros no ecrã (erro 415917)

### Prison

- Correcção do ficheiro .pri para não falhar com inclusões em CamelCase
- Correcção do ficheiro .pri para ter o nome QtGui do qmake como dependência

### Purpose

- Remodelação do 'plugin' da Nextcloud
- Eliminação do suporte para o Twitter

### QQC2StyleBridge

- ScrollView: Uso da altura da barra de deslocamento como preenchimento inferior e não como largura

### Solid

- Correcção da lógica invertida no IOKitStorage::isRemovable

### Sonnet

- Correcção de estoiro à saída

### Realce de sintaxe

- Correcção de estoiro de memória devido a pilhas de contextos demasiado grandes
- Actualização geral do realce de sintaxe do CartoCSS
- Adição do realce de sintaxe para os ficheiros Properties do Java
- TypeScript: adição dos campos privados e importações/exportações apenas de tipos, assim como algumas outras correcções
- Adição da extensão FCMacro do FreeCAD para a definição de realce do Python
- Atualizações para o CMake 3.17
- C++: palavra-chave constinit e sintaxe std::format para textos. Melhorias no formato printf
- Spec RPM: várias melhorias
- Realce do makefile: corrige os nomes das variáveis nas condicionais "else" (bug 417379)
- Adição do realce de sintaxe para o Solidity
- Pequenas melhorias em alguns arquivos XML
- Realce do makefile: adiciona substituições (bug 416685)

### Informações de segurança

O código lançado foi assinado com GPG, usando a seguinte chave: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Impressão digital da chave primária: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB
