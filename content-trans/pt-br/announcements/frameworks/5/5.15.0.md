---
aliases:
- ../../kde-frameworks-5.15.0
date: 2015-10-10
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Baloo

- Correção do tratamento do limite/início no SearchStore::exec
- Recriação do índice do Baloo
- Configuração do balooctl: adição de opções para definir/visualizar o <i>onlyBasicIndexing</i>
- Migração de verificação do balooctl para funcionar com novas arquiteturas (erro <a href='https://bugs.kde.org/show_bug.cgi?id=353011'>353011</a>)
- FileContentIndexer: Correção da emissão duplicada do <i>filePath</i>
- UnindexedFileIterator: O <i>mtime</i> é um 'quint32' e não 'quint64'
- Transaction: Correção de outro erro de digitação do Dbi
- Transaction: Correção do documentMTime() e do documentCTime(), que usavam o Dbis errado
- Transaction::checkPostingDbInTermsDb: Otimização do código
- Correção dos avisos do D-Bus
- Balooctl: Adição do comando <i>checkDb</i>
- Configuração do balooctl: Adição do "filtro de exclusão"
- KF5Baloo: Garantia de que as interfaces de D-Bus são geradas antes do uso. (erro <a href='https://bugs.kde.org/show_bug.cgi?id=353308'>353308</a>)
- Evitar o uso do QByteArray::fromRawData
- Remoção do <i>baloo-monitor</i> do Baloo
- TagListJob: Emissão de erro quando não for possível abrir o banco de dados
- Não ignorar os sub-termos, caso não seja encontrado
- Limpeza de código para a falha do Baloo::File::load(), caso não consiga abrir a BD
- O <i>balooctl</i> passa a usar o IndexerConfig, em vez de manipular o <i>baloofilerc</i> diretamente
- Melhoria na internacionalização do <i>balooshow</i>
- O <i>balooshow</i> agora falha normalmente se não for possível abrir o banco de dados.
- Falha do Baloo::File::load() se não abrir o banco de dados. (erro <a href='https://bugs.kde.org/show_bug.cgi?id=353049'>353049</a>)
- IndexerConfig: Adição do método refresh()
- inotify: Não simular um evento 'closedWrite' após a movimentação sem 'cookie'
- ExtractorProcess: Remoção da linha extra no fim do 'filePath'
- baloo_file_extractor: Chamada do QProcess::close antes de destruir o QProcess
- baloomonitorplugin/balooctl: Internacionalização do estado da indexação.
- BalooCtl: Adição da opção 'config'
- Tornar o <i>baloosearch</i> mais apresentável
- Remoção dos arquivos vazios do EventMonitor
- BalooShow: Mostrar mais informações quando os IDs não corresponderem
- BalooShow: Quando chamado sem uma verificação de ID, caso o ID esteja correto
- Adição de uma classe FileInfo
- Adição de verificações de erros em diversos pontos, para que o Baloo não falhe se estiver desativado. (erro <a href='https://bugs.kde.org/show_bug.cgi?id=352454'>352454</a>)
- Correção do Baloo que não respeitava a opção de configuração "apenas a indexação básica"
- Monitor: Obtenção do tempo restante na inicialização
- Uso de chamadas a métodos reais no MainAdaptor, em vez do QMetaObject::invokeMethod
- Adição da interface org.kde.baloo ao objeto raiz, por motivo de compatibilidade
- Correção da string de data apresentada na barra de endereços, devido à migração para o QDate
- Adição de um atraso ao fim de cada arquivo em vez de cada lote
- Remoção da dependência do Qt::Widgets no <i>baloo_file</i>
- Remoção de código não utilizado do <i>baloo_file_extractor</i>
- Adição do monitor do Baloo ou do plugin QML experimental
- Tornar a "pesquisa do tempo restante" segura em multitarefa
- kioslaves: Adição da substituição ausente para funções virtuais
- Extractor: Definição do <i>applicationData</i> após a construção do aplicativo
- Query: Implementação do suporte do 'offset'
- Balooctl: Adição da opções --version e --help (erro <a href='https://bugs.kde.org/show_bug.cgi?id=351645'>351645</a>)
- Remoção do suporte do KAuth para aumentar o monitoramento máximo do <i>inotify</i> quando a quantidade é muito baixa (erro <a href='https://bugs.kde.org/show_bug.cgi?id=351602'>351602</a>)

### BluezQt

- Correção da falha do <i>fakebluez</i> no <i>obexmanagertest</i> com o ASAN
- Declaração prévia de todas as classes exportadas no types.h
- ObexTransfer: Apresentação de erro quando a sessão de transferência é removida
- Utils: Armazenamento de ponteiros para as instâncias dos gerenciadores
- ObexTransfer: Apresentação de erro quando o org.bluez.obex falha

### Módulos extra do CMake

- Atualização do cache de ícones do GTK ao instalar os ícones.
- Remoção da solução alternativa para atrasar a execução no Android
- ECMEnableSanitizers: O item de sanidade indefinido é suportado pelo gcc 4.9
- Desativação da detecção do X11, XCB, etc., no OS X
- Pesquisa pelos arquivos no prefixo instalado em vez do caminho do prefixo
- Uso do Qt5 para indicar qual o prefixo de instalação do Qt5
- Adição da definição ANDROID no qsystemdetection.h, conforme necessidade.

### Integração do Framework

- Correção de problemas aleatórios na caixa de diálogo de arquivos que impediam o seu aparecimento. (erro <a href='https://bugs.kde.org/show_bug.cgi?id=350758'>350758</a>)

### KActivities

- Uso de uma função de correspondência personalizada em vez do <i>glob</i> do SQLite. (erro <a href='https://bugs.kde.org/show_bug.cgi?id=352574'>352574</a>)
- Correção de problemas na adição de um novo recurso ao modelo

### KCodecs

- Correção de falha no UnicodeGroupProber::HandleData com textos strings curtas

### KConfig

- Marcação do <i>kconfig-compiler</i> como ferramenta não-gráfica

### KCoreAddons

- KShell::splitArgs: Apenas o espaço em ASCII é um separador, não o espaço em Unicode U+3000 (erro <a href='https://bugs.kde.org/show_bug.cgi?id=345140'>345140</a>)
- KDirWatch: Correção da falha quando um destrutor estático e global usa o KDirWatch::self() (erro <a href='https://bugs.kde.org/show_bug.cgi?id=353080'>353080</a>)
- Correção de falha quando o KDirWatch é usado no Q_GLOBAL_STATIC
- KDirWatch: Correção da segurança em <i>threads</i>
- Esclarecimento de como definir os argumentos do construtor do KAboutData

### KCrash

- KCrash: Passagem do <i>cwd</i> ao <i>kdeinit</i> quando o aplicativo é reiniciado automaticamente com o <i>kdeinit</i> (erro <a href='https://bugs.kde.org/show_bug.cgi?id=337760'>337760</a>)
- Adição do KCrash::initialize(), para que os aplicativos e o plugin da plataforma possam ativar o KCrash de forma explícita
- Desativação do ASAN, se estiver ativo

### KDeclarative

- Pequenas melhorias no ColumnProxyModel
- Possibilidade de os aplicativos conhecerem a localização da <i>homeDir</i>
- Retirada do EventForge do contêiner desktop
- Incluída a propriedade <i>enabled</i> (ativo) no QIconItem

### KDED

- kded: Simplificação da lógica em torno do sycoca; basta chamar o <i>ensureCacheValid</i>

### KDELibs 4 Support

- Chamada do <i>newInstance</i> a partir do filho na primeira invocação
- Uso das definições do <i>kdewin</i>
- Não tentar encontrar o X11 no WIN32
- cmake: Correção da verificação da versão da <i>Taglib</i> no FindTaglib.cmake

### KDesignerPlugin

- O <i>moc</i> do Qt não consegue lidar com as macros (QT_VERSION_CHECK)

### KDESU

- kWarning -&gt; qWarning

### KFileMetaData

- Implementação dos metadados do usuário no Windows

### Complementos da interface KDE

- Não pesquisar pelo X11/XCB também faz sentido no WIN32

### KHTML

- Substituição do std::auto_ptr por std::unique_ptr
- khtml-filter: Eliminação das regras que contêm funcionalidades especiais do AdBlock que ainda não são suportadas
- khtml-filter: Reordenação do código, sem alterações funcionais
- khtml-filter: Ignorar expressões regulares com opções, por não haver suporte para elas
- khtml-filter: Correção da detecção do separador das opções do <i>adblock</i>
- khtml-filter: Limpeza dos espaços em branco finais
- khtml-filter: Não descartar linhas que comecem com '&amp;', por não serem caracteres especiais do <i>adblock</i>

### KI18n

- Remoção dos iteradores restritos para o MSVC, para permitir a compilação do <i>ki18n</i>

### KIO

- KFileWidget: O argumento <i>parent</i> (pai) deverá ser 0 por padrão, como em todos os widgets
- Certeza de que o tamanho da lista de <i>bytes</i> que foi descarregada na estrutura é grande o suficiente, antes de calcular o <i>targetInfo</i>, caso contrário, será acessada a memória que não nos pertence
- Correção do uso do <i>Qurl</i> ao chamar o QFileDialog::getExistingDirectory()
- Atualização da lista de dispositivos do Solid, antes de efetuar uma pesquisa no <i>kio_trash</i>
- Permissão do uso de <i>trash:</i>, além do <i>trash:/</i> como URL do <i>listDir</i> (que chama o <i>listRoot</i>) (erro <a href='https://bugs.kde.org/show_bug.cgi?id=353181'>353181</a>)
- KProtocolManager: Correção de um bloqueio ao usar o EnvVarProxy (erro <a href='https://bugs.kde.org/show_bug.cgi?id=350890'>350890</a>)
- Não tentar encontrar o X11 no WIN32
- KBuildSycocaProgressDialog: Uso do indicador de ocupado incorporado no Qt (erro <a href='https://bugs.kde.org/show_bug.cgi?id=158672'>158672</a>)
- KBuildSycocaProgressDialog: Execução do <i>kbuildsycoca5</i> com o QProcess
- KPropertiesDialog: Correção do caso em que o <i>~/.local</i> é um link simbólico, comparação de caminhos canônicos
- Adição do suporte para compartilhamento de rede no <i>kio_trash</i> (erro <a href='https://bugs.kde.org/show_bug.cgi?id=177023'>177023</a>)
- Conexão aos sinais do QDialogButtonBox, não do <i>QDialog</i> (erro <a href='https://bugs.kde.org/show_bug.cgi?id=352770'>352770</a>)
- KCM de Cookies: Atualização dos nomes de D-Bus para o <i>kded5</i>
- Uso de arquivos JSON diretamente, em vez do <i>kcoreaddons_desktop_to_json()</i>

### KNotification

- Não enviar um sinal de atualização de notificação duplicado
- Reprocessamento da configuração da notificação apenas se tiver sido alterada
- Não tentar encontrar o X11 no WIN32

### KNotifyConfig

- Modificação do método de carregamento de padrões
- Envio do nome do aplicativo, cuja configuração foi atualizada em conjunto com o sinal de D-Bus
- Adição de método para reverter o <i>kconfigwidget</i> aos valores padrão
- Não sincronizar a configuração <i>n</i> vezes na gravação

### KService

- Uso da data mais recente na subpasta como data da pasta de recursos
- KSycoca: Armazenamento do <i>mtime</i> para cada pasta de origem, de forma a detectar alterações (erro <a href='https://bugs.kde.org/show_bug.cgi?id=353036'>353036</a>)
- KServiceTypeProfile: Remoção da criação de <i>factory</i> desnecessária (erro <a href='https://bugs.kde.org/show_bug.cgi?id=353360'>353360</a>)
- Simplificação e otimização do KServiceTest::initTestCase
- Tornar o nome de instalação do arquivo <i>applications.menu</i> uma variável em cache do CMake
- KSycoca: O ensureCacheValid() só deverá criar o BD se ele não existir
- KSycoca: Fazer com que o banco de dados funcione após o recente código de verificação de datas
- KSycoca: Alteração do nome do arquivo do BD para incluir a linguagem e o SHA1 das pastas de onde foi criado
- KSycoca: Inclusão do <i>ensureCacheValid()</i> na API pública
- KSycoca: Adição de um ponteiro <i>q</i> para remover mais utilizações de <i>singletons</i>
- KSycoca: Remoção de todos os métodos <i>self()</i> para as <i>factories</i>, guardando-os no KSycoca
- KBuildSycoca: Remoção da gravação do arquivo <i>ksycoca5stamp</i>
- KBuildSycoca: Uso do <i>qCWarning</i> em vez do <i>fprintf(stderr, ...)</i> ou qWarning
- KSycoca: Reconstrução do <i>ksycoca</i> num processo em vez de executar o <i>kbuildsycoca5</i>
- KSycoca: Transferência de todo o código do <i>kbuildsycoca</i> para a biblioteca, exceto o <i>main()</i>
- Otimização do KSycoca: Apenas monitorar o arquivo se o aplicativo se conectar ao <i>databaseChanged()</i>
- Correção dos vazamentos de memória na classe <i>KBuildSycoca</i>
- KSycoca: Substituição da notificação de D-Bus com o monitoramento de arquivos usando o <i>KDirWatch</i>
- kbuildsycoca: A opção --nosignal tornou-se obsoleta
- KBuildSycoca: Substituição do bloqueio baseado no D-Bus por um arquivo de bloqueio
- Não falhar se encontrar informações inválidas do plugin
- Renomeação dos cabeçalhos para <i>_p.h</i> como preparação para a transferência do código para a biblioteca <i>kservice</i>
- Transferência do <i>checkGlobalHeader()</i> para dentro do <i>KBuildSycoca::recreate()</i>
- Remoção do código do --checkstamps e do --nocheckfiles

### KTextEditor

- Validação de mais expressões regulares
- Correção de expressões regulares nos arquivos HL (erro <a href='https://bugs.kde.org/show_bug.cgi?id=352662'>352662</a>)
- Sincronização do realce de OCaml com o estado do https://code.google.com/p/vincent-hugot-projects/, antes de o Google Code ter fechado, e algumas pequenas correções de erros
- Adição de quebra de linha (erro <a href='https://bugs.kde.org/show_bug.cgi?id=352258'>352258</a>)
- Validação da linha antes de chamar o código de dobragem (erro <a href='https://bugs.kde.org/show_bug.cgi?id=339894'>339894</a>)
- Correção de problemas na contagem de palavras do Kate, integrando com o <i>DocumentPrivate</i> em vez do <i>Document</i> (erro <a href='https://bugs.kde.org/show_bug.cgi?id=353258'>353258</a>)
- Atualização do realce de sintaxe do Kconfig: adição dos novos operadores do Linux 4.2
- Sincronização com a ramificação KDE/4.14 do Kate
- Minimapa: Correção do manipulador da barra de rolagem que não era desenhada com as marcações desligadas (erro <a href='https://bugs.kde.org/show_bug.cgi?id=352641'>352641</a>)
- Sintaxe: Adição da opção <i>git-user</i> para o <i>kdesrc-buildrc</i>

### KWallet Framework

- Não é mais fechado automaticamente na última utilização

### KWidgetsAddons

- Correção do aviso C4138 (MSVC): '*/' encontrado fora do comentário

### KWindowSystem

- Execução de uma cópia completa do QByteArray get_stringlist_reply
- Permitir a interação com vários servidores X nas classes NETWM.
- [xcb] Considerar os modificadores no KKeyServer como inicializados em plataformas != x11
- Mudança do KKeyserver (X11) para registro de eventos por categorias

### KXMLGUI

- Possibilidade de importar/exportar esquemas de atalhos de forma simétrica

### NetworkManagerQt

- Correção das introspecções, sendo que o LastSeen deverá estar no AccessPoint e não na ActiveConnection

### Plasma Framework

- Ocultar a janela de dicas quando o cursor entrar em uma ToolTipArea inativa
- Se o arquivo <i>desktop</i> tiver Icon=/foo.svgz, usar esse arquivo do pacote
- Adição de um tipo de arquivo "screenshot" (imagem) nos pacotes
- Considerar o <i>devicepixelration</i> na barra de rolagem independente
- Nenhum efeito ao passar em telas sensíveis ao toque/móveis
- Uso das margens SVG do campo de edição no cálculo do <i>sizeHint</i>
- Não escurecer o ícone de animação nas dicas do Plasma
- Correção do texto em reticências do botão
- Os menus de contexto dos miniaplicativos dentro de um painel já não se sobrepõem mais ao miniaplicativo
- Simplificação da obtenção da lista de aplicativos associados no <i>AssociatedApplicationManager</i>

### Sonnet

- Correção do ID do plugin do Hunspell para um carregamento adequado
- Suporte da compilação estática no Windows, adição do caminho dos dicionários do Hunspell e do LibreOffice no Windows
- Não assumir dicionários do Hunspell codificados em UTF-8. (erro <a href='https://bugs.kde.org/show_bug.cgi?id=353133'>353133</a>)
- Correção do Highlighter::setCurrentLanguage() para o caso em que o idioma anterior era inválido (erro <a href='https://bugs.kde.org/show_bug.cgi?id=349151'>349151</a>)
- Suporte do /usr/share/hunspell como caminho dos dicionários
- Plugin baseado no <i>NSSpellChecker</i>

Você pode discutir e compartilhar ideias sobre esta versão na seção de comentários do <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artigo do Dot</a>.
