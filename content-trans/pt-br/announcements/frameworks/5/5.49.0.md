---
aliases:
- ../../kde-frameworks-5.49.0
date: 2018-08-11
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Attica

- Não instanciar uma QStringRef numa QString apenas para procurar numa QStringList
- Definir os elementos quando são declarados

### Baloo

- [tags_kio] Correcção de várias cópias do nome do ficheiro
- Reversão do "[tags_kio] Usar o UDS_URL em vez do UDS_TARGET_URL."
- [tags_kio] Usar o UDS_URL em vez do UDS_TARGET_URL
- [tags_kio] Pesquisa dos locais de destino dos ficheiros em vez de adicionar os locais ao elemento UDS do ficheiro
- Suporte para URL's especiais na pesquisa de ficheiros por um determinado tipo
- Evitar a manipulação de listas com complexidade quadrática
- Uso do 'fastInsert' não obsoleto no Baloo

### Ícones Breeze

- Adição do ícone <code>drive-optical</code> (erro 396432)

### Módulos extra do CMake

- Android: Não usar uma versão aleatória fixa do SDK do Android
- ECMOptionalAddSubdirectory: Fornecimento de mais alguns detalhes
- Correcção da verificação da definição da variável
- Modificação da versão 'since'
- Melhoria do ECMAddAppIconMacro

### Integração do Framework

- Respeito do BUILD_TESTING

### KActivities

- Respeito do BUILD_TESTING

### KArchive

- Respeito do BUILD_TESTING

### KAuth

- Eliminação de avisos dos ficheiros de inclusão do PolkitQt5-1
- Respeito do BUILD_TESTING

### KBookmarks

- Respeito do BUILD_TESTING

### KCodecs

- Respeito do BUILD_TESTING

### KCompletion

- Respeito do BUILD_TESTING

### KConfig

- Respeito do BUILD_TESTING

### KConfigWidgets

- Respeito do BUILD_TESTING

### KCoreAddons

- Correcção de ultrapassagem no código de arredondamento (erro 397008)
- Documentação da API: remoção dos ":" desnecessários atrás do "@note"
- Documentação da API: referência ao 'nullptr', não ao 0
- KFormat: Substituição do literal Unicode com o código em Unicode para corrigir a compilação com o MSVC
- KFormat: correcção da marca @since para o novo KFormat::formatValue
- KFormat: Possibilidade do uso de quantidades para além de 'bytes' e segundos
- Correcção dos exemplos do KFormat::formatBytes
- Respeito do BUILD_TESTING

### KCrash

- Respeito do BUILD_TESTING

### KDBusAddons

- Não bloquear para sempre no 'ensureKdeinitRunning'
- Respeito do BUILD_TESTING

### KDeclarative

- Garantia de que se está sempre a escrever no contexto de topo do motor
- Melhor legibilidade
- Pequenas melhorias na documentação da API
- Respeito do BUILD_TESTING

### KDELibs 4 Support

- Correcção do 'qtplugins' no KStandardDirs

### KDocTools

- Respeito do BUILD_TESTING

### KEmoticons

- Respeito do BUILD_TESTING

### KFileMetaData

- Documentação da API: adição do @file ao ficheiro de inclusão apenas com funções para que o Doxygen também faça referência a elas

### KGlobalAccel

- Respeito do BUILD_TESTING

### Complementos da interface KDE

- Respeito do BUILD_TESTING

### KHolidays

- Instalação do cabeçalho de cálculo do nascer ou pôr do Sol
- Adição do dia do ano bissexto como feriado (cultural) na Noruega
- Adição do elemento ‘name’ ao ficheiros de feriados da Noruega
- Adição de descrições para os ficheiros de feriados Noruegueses
- Mais actualizações de feriados Japoneses do 'phanect'
- holiday_jp_ja, holiday_jp-en_us - actualizações (erro 365241)

### KI18n

- Reutilização de uma função que já faz o mesmo
- Correcção do tratamento do catálogo e detecção da região no Android
- Legibilidade - ignorar instruções sem-operação
- Correcção do KCatalog::translate quando a tradução é igual ao texto original
- Um ficheiro mudou de nome
- Possibilidade de o nome do ficheiro de macros do 'ki18n' seguir o estilo dos outros ficheiros relacionados com o 'find_package'
- Correcção da verificação do 'configure' para o '_nl_msg_cat_cntr'
- Não gerar ficheiros na pasta de código
- libintl: Validação se o '_nl_msg_cat_cntr' existe antes de o usar (erro 365917)
- Correcção das compilações de binários de fábrica

### KIconThemes

- Respeito do BUILD_TESTING

### KIO

- Instalação do ficheiro de categorias do 'kdebugsettings' relacionado com o 'kio'
- Mudança de nome do ficheiro de inclusão privado para '_p.h'
- Remoção da selecção de ícones personalizados para o lixo (erro 391200)
- Alinhamento ao topo das legendas na janela de propriedades
- Apresentação de uma janela de erro quando o utilizador tentar criar uma pasta chamada "." ou ".." (erro 387449)
- Documentação da API: referência ao 'nullptr', não ao 0
- Teste de performance do 'lstItems' do kcoredirlister
- [KSambaShare] Verificar o ficheiro que foi alterado antes de recarregar
- [KDirOperator] Uso de cores de fundo alternadas para as vistas multi-colunas
- Eliminação de fugas de memórias nas tarefas dos 'slaves' (erro 396651)
- SlaveInterface: descontinuação do 'setConnection/connection', já que ninguém as pode usar de facto
- Construtor de UDS ligeiramente mais rápido
- [KFilePlacesModel] Suporte para URL's bonitos do 'baloosearch'
- Remoção do atalho Web para o projects.kde.org
- Mudança do KIO::convertSize() para o KFormat::formatByteSize()
- Uso do método não-obsoleto 'fastInsert' no 'file.cpp' (primeira de muitas alterações)
- Substituição do atalho Web do Gitorious pelo GitLab
- Não mostrar a janela de confirmação para a acção 'Enviar para o Lixo' por omissão (erro 385492)
- Não pedir senhas no 'kfilewidgettest'

### Kirigami

- suporte para adicionar ou remover de forma dinâmica o título (erro 396417)
- Introdução do 'actionsVisible' (erro 396413)
- Adaptação das margens quando a barra de deslocamento aparece/desaparece
- melhor gestão do tamanho (erro 396983)
- Optimização da configuração da paleta
- O AbstractApplciationItem não deveria ter o seu próprio tamanho, devendo ser implícito
- Novos eventos 'pagePushed/pageRemoved'
- correcção da lógica
- Adição do elemento ScenePosition (erro 396877)
- Sem necessidade de emitir a paleta intermédia para todos os estados
- esconder-&gt;mostrar
- Modo de Barra Lateral Extensível
- kirigami_package_breeze_icons: não tratar as listas como elementos (erro 396626)
- correcção da expressão regular na pesquisa/substituição (erro 396294)
- a animação de uma cor produz um efeito relativamente desagradável (erro 389534)
- item focado na cor para a navegação com o teclado
- eliminação do atalho para sair
- Remoção do item obsoleto - já há algum tempo - 'Encoding=UTF-8' do formato de ficheiros '.desktop'
- correcção do tamanho da barra de ferramentas (erro 396521)
- correcção do tratamento do dimensionamento
- Respeito do BUILD_TESTING
- Mostrar ícones para as acções que têm a localização de um ícone em vez de um nome para o mesmo

### KItemViews

- Respeito do BUILD_TESTING

### KJobWidgets

- Respeito do BUILD_TESTING

### KJS

- Respeito do BUILD_TESTING

### KMediaPlayer

- Respeito do BUILD_TESTING

### KNewStuff

- Remoção do item obsoleto - já há algum tempo - 'Encoding=UTF-8' do formato de ficheiros '.desktop'
- Mudança da ordenação predefinida na janela de transferências para a Classificação
- Correção para fazer as margens de janela do DownloadDialog seguirem as margens do tema geral
- Reposição de um 'qCDebug' removido sem querer
- Uso da API correcta do QSharedPointer
- Tratamento das listas de antevisão vazias

### KNotification

- Respeito do BUILD_TESTING

### Plataforma KPackage

- Respeito do BUILD_TESTING

### KParts

- Documentação da API: referência ao 'nullptr', não ao 0

### KPeople

- Respeito do BUILD_TESTING

### KPlotting

- Respeito do BUILD_TESTING

### KPty

- Respeito do BUILD_TESTING

### KRunner

- Respeito do BUILD_TESTING

### KService

- Documentação da API: referência ao 'nullptr', não ao 0
- Obrigação do uso de uma compilação fora-do-código
- Adição do operador 'subseq' para corresponder às sub-sequências

### KTextEditor

- correcção adequada para a citação automática na indentação de textos em bruto
- correcção da indentação para respeitar o novo ficheiro da sintaxe na plataforma 'syntaxhighlighting'
- Ajuste dos testes para o novo estado no repositório de realces de sintaxe
- Apresentação da mensagem "A pesquisa deu a volta" no centro da janela, para melhor visibilidade
- Correcção de aviso, usando apenas o 'isNull()'
- Alargamento da API de Programação
- Correcção de erro de segmentação em casos raros em que ocorre um vector vazio na contagem de palavras
- Forçar a limpeza da antevisão da barra de deslocamento na limpeza do documento (erro 374630)

### KTextWidgets

- Documentação da API: reversão parcial da modificação anterior, já que não funcionava de facto
- KFindDialog: colocar o campo de texto em primeiro plano ao mostrar uma janela reutilizada
- KFind: limpeza da contagem ao mudar o padrão (p.ex. na janela de pesquisa)
- Respeito do BUILD_TESTING

### KUnitConversion

- Respeito do BUILD_TESTING

### KWallet Framework

- Respeito do BUILD_TESTING

### KWayland

- Limpeza dos elementos do RemoteAccess no 'aboutToBeUnbound' em vez de destruir o objecto
- Suporte às dicas do cursor num cursor bloqueado
- Redução de tempos de espera desnecessários nas sondas de eventos/sinais com problemas
- Correcção da selecção e testes-automáticos do local
- Substituição das inclusões globais de compatibilidade com o V5
- Adição do suporte de base do WM do XDG na nossa API do XDGShell
- Possibilidade de compilação do XDGShellV5 em conjunto com o XDGWMBase

### KWidgetsAddons

- Correcção da máscara de entrada do KTimeComboBox para as horas AM/PM (erro 361764)
- Respeito do BUILD_TESTING

### KWindowSystem

- Respeito do BUILD_TESTING

### KXMLGUI

- Correcção do KMainWindow a gravar incorrectamente as definições do elemento (erro 395988)
- Respeito do BUILD_TESTING

### KXmlRpcClient

- Respeito do BUILD_TESTING

### Plasma Framework

- Se um dado elemento for inválido, terá um UiReadyConstraint imediato
- [PluginLoader do Plasma] 'Cache' dos 'plugins' durante o arranque
- Correcção do nó em desvanecimento quando é mapeada uma textura
- [Containment] Não carregar as acções do contentor com a restrição do KIOSK 'plasma/containment_actions'
- Respeito do BUILD_TESTING

### Prison

- Correcção do modo Misto para Maiúsculas na geração de código em Aztec

### Purpose

- Validação de que a depuração do 'kf5.kio.core.copyjob' está desactivada no teste
- Reversão do "teste: Uma forma mais "atómica" de verificar a ocorrência do sinal"
- teste: Uma forma mais "atómica" de verificar a ocorrência do sinal
- Adição do 'plugin' do Bluetooth
- [Telegram] Não esperar pelo fecho do Telegram
- Preparação para o uso das cores de estado do Arc na lista de revisões
- Respeito do BUILD_TESTING

### QQC2StyleBridge

- Melhoria no dimensionamento dos menus (erro 396841)
- Remoção de comparação dupla
- Migração dos 'connects' antigos baseados em texto
- verificação do ícone válido

### Solid

- Respeito do BUILD_TESTING

### Sonnet

- Sonnet: o 'setLanguage' deve agendar uma nova invocação do realce se o mesmo estiver activo
- Uso da API actual do 'hunspell'

### Realce de sintaxe

- CoffeeScript: correcção dos modelos no código incorporado em JavaScript &amp; adição de sequências de escape
- Exclusão desta no Definition::includedDefinitions()
- Uso da inicialização de membros dentro da classe sempre que possível
- adição de funções para aceder às palavras-chave
- Adição do Definition::::formats()
- Adição da constante QVector&lt;Definition&gt; Definition::includedDefinitions()
- Adição da constante Theme::TextStyle Format::textStyle();
- C++: correcção dos literais em vírgula-flutuante padrão (erro 389693)
- CSS: actualização da sintaxe e correcção de alguns erros
- C++: actualização para o c++20 e correcção de alguns erros de sintaxe
- CoffeeScript &amp; JavaScript: correcção dos objectos-membros. Adição da extensão .ts ao JS (erro 366797)
- Lua: correcção de textos multi-linhas (erro 395515)
- Spec do RPM: adição do tipo MIME
- Python: correcção das sequências de escape nos comentários entre aspas (erro 386685)
- haskell.xml: não realçar os construtores de dados do Prelude de forma diferente dos outros
- haskell.xml: remoção dos tipos da secção "prelude function"
- haskell.xml: realce dos construtores de dados promovidos
- haskell.xml: adição das palavras-chave 'family', 'forall', 'pattern'
- Respeito do BUILD_TESTING

### ThreadWeaver

- Respeito do BUILD_TESTING

### Informações de segurança

O código lançado foi assinado com GPG, usando a seguinte chave: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Impressão digital da chave primária: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB

Você pode discutir e compartilhar ideias sobre esta versão na seção de comentários do <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artigo do Dot</a>.
