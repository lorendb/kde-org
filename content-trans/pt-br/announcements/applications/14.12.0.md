---
aliases:
- ../announce-applications-14.12.0
changelog: true
date: '2014-12-17'
description: O KDE Lança as Aplicações do KDE 14.12.
layout: application
title: O KDE Lança as Aplicações do KDE 14.12
version: 14.12.0
---
17 de Dezembro de 2014. Hoje o KDE lançou as Aplicações do KDE 14.12. Esta versão inclui novas funcionalidades e correcções de erros para mais de uma centena de aplicações. A maioria destas aplicações baseiam-se na Plataforma de Desenvolvimento do KDE 4; algumas já foram convertidas para as novas <a href='https://dot.kde.org/2013/09/25/frameworks-5'>Plataformas do KDE 5</a>, um conjunto de bibliotecas modulares que se baseiam no Qt5, a última versão desta plataforma aplicacional multi-plataforma.

A <a href='http://api.kde.org/4.x-api/kdegraphics-apidocs/libs/libkface/libkface/html/index.html'>Libkface</a> é nova nesta versão; é uma biblioteca que permite a detecção e reconhecimento de caras em fotografias.

O lançamento inclui as primeiras versões baseadas nas Plataformas do KDE 5 do <a href='http://www.kate-editor.org'>Kate</a> e do <a href='https://www.kde.org/applications/utilities/kwrite/'>KWrite</a>, <a href='https://www.kde.org/applications/system/konsole/s'>Konsole</a>, <a href='https://www.kde.org/applications/graphics/gwenview/s'>Gwenview</a>, <a href='http://edu.kde.org/kalgebras'>KAlgebra</a>, <a href='http://edu.kde.org/kanagram'>Kanagram</a>, <a href='http://edu.kde.org/khangman'>KHangman</a>, <a href='%[8]'>Kig</a>, <a href='http://edu.kde.org/parley'>Parley</a>, <a href='https://www.kde.org/applications/development/kapptemplate/'>KAppTemplate</a> e <a href='https://www.kde.org/applications/utilities/okteta/'>Okteta</a>. Algumas bibliotecas estão também prontas para serem usadas nas Plataformas do KDE 5: a analitza and a libkeduvocdocument.

O <a href='http://kontact.kde.org'>Pacote Kontact</a> está agora em Suporte de Longo Prazo na versão 4.14, enquanto os programadores estão a usar as suas energias para o transpor para as Plataformas do KDE 5

Algumas das novas funcionalidades desta versão incluem:

+ O <a href='http://edu.kde.org/kalgebra'>KAlgebra</a> tem uma nova versão para Android, graças às Plataformas do KDE 5, e consegue agora <a href='http://www.proli.net/2014/09/18/touching-mathematics/'>imprimir os seus gráficos em 3D</a>
+ O <a href='http://edu.kde.org/kgeography'>KGeography</a> tem um novo mapa para o Bihar.
+ O visualizador de documentos <a href='http://okular.kde.org'>Okular</a> agora tem suporte para a pesquisa inversa de latex-synctex nos ficheiros DVI e tem algumas pequenas melhorias no suporte para o ePub.
+ O <a href='http://umbrello.kde.org'>Umbrello</a> -- o modelador de UML -- tem muitas novas funcionalidades que são demasiado numerosas para apresentar aqui.

A versão de abril do KDE Applications 15.04 irá incluir muitas funcionalidades novas, assim como mais aplicativos baseados no modular KDE Frameworks 5.
