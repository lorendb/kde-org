---
aliases:
- ../../kde-frameworks-5.21.0
date: 2016-04-09
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
Nova infraestrutura: KActivitiesStats, unha biblioteca para acceder aos datos de estatísticas de uso recollidos polo xestor de actividades de KDE.

### Todas as infraestruturas

Agora requírese Qt ≥ 5.4, é dicir, xa non se fornece compatibilidade con Qt 5.3.

### Attica

- Engadir «const variant» a un método de obtención

### Baloo

- Centralizar o tamaño dos lotes na configuración
- Retirar o código que bloquea a indexación de ficheiro text/plain sen extensión .txt (fallo 358098)
- Comprobar tanto o nome de ficheiro como o contido para determinar o tipo MIME (fallo 353512)

### BluezQt

- ObexManager: Dividir as mensaxes de erro para obxectos que faltan

### Iconas de Breeze

- engadir iconas de Lokalize de Breeze
- sincronizar as iconas de aplicacións entre Breeze e Breeze Escuro
- actualizar as iconas do tema e retirar a icona application-system, corrixir os grupos de Kicker
- engadir compatibilidade cos complementos XPI de Firefox (fallo 359913)
- actualizar a icona de Okular á correcta
- engadir compatibilidade coa icona de aplicación de ktnef
- engadir iconas de kmenueditor, kmouse e knotes
- cambiar a icona de silencio do volume de son para usar - para silencio en vez de usar só cor vermella (fallo 360953)
- engadir compatibilidade co tipo MIME de Djvu (fallo 360136)
- engadir unha ligazón en vez de unha entrada dupla
- engadir a icona ms-shortcut para gnucash (fallo 360776)
- cambiar o fondo do escritorio ao xenérico
- actualizar as iconas para usar un fondo de escritorio xenérico
- engadir unha icona para Konqueror (fallo 360304)
- engadir a icona process-working para animacións de progreso en KDE (fallo 360304)
- engadir unha icona de instalar software e unha icona de actualizar software coa cor correcta
- engadir iconas de emblemas de engadir e retirar para a selección de Dolphin, engadir unha icona de montar
- Retirar a folla de estilo das iconas dos miniaplicativos analogclock e kickerdash
- sincronizar os temas Breeze e Breeze Escuro (fallo 360294)

### Módulos adicionais de CMake

- Corrixir que _ecm_update_iconcache só actualice o lugar de instalación
- Reverter «ECMQtDeclareLoggingCategory: Incluír &lt;QDebug&gt; co ficheiro xerado»

### Integración de infraestruturas

- Usar o código de standardIcon de QCommonStyle como reserva
- Definir un tempo límite predeterminado de peche do menú

### KActivities

- Retiráronse comprobacións de compilador agora que todas as infraestruturas requiren c++11
- Retirouse ResourceModel de QML xa que KAStats::ResultModel o superou
- Inserir nun QFlatSet baleiro devolveu un iterador incorrecto

### KCodecs

- Simplificar o código (qCount → std::count, isprint caseiro → QChar::isPrint)
- detección de codificación: corrixir a quebra por mal uso de isprint (fallo 357341)
- Corrixir unha quebra debida a unha variábel sen inicializar (fallo 357341)

### KCompletion

- KCompletionBox: forzar unha xanela sen marco e non definir o foco
- KCompletionBox *non* debería ser un consello

### KConfig

- Engadir a posibilidade de obter os lugares de QStandardPaths dentro de ficheiros de escritorio

### KCoreAddons

- Corrixir kcoreaddons_desktop_to_json() en Windows
- src/lib/CMakeLists.txt - corrixir a ligazón cunha biblioteca de fíos
- Engadir bosquexos para permitir a compilación en Android

### KDBusAddons

- Evitar examinar a interface de D-Bus cando non a usamos

### KDeclarative

- uso uniforme de std::numeric_limits
- [DeclarativeDragArea] Non sobrepor o «texto» dos dados MIME

### Compatibilidade coa versión 4 de KDELibs

- Corrixir unha ligazón obsoleta no docbook de kdebugdialog5 
- Impedir fugas de Qt5::Network como bibliotecas necesarias para o resto de ConfigureChecks

### KDESU

- Definir os macros de funcionalidades para activar a construción co libc de musl

### KEmoticons

- KEmoticons: corrixir unha quebra cando loadProvider falla por algún motivo

### KGlobalAccel

- Permitir matar kglobalaccel5 de maneira axeitada, corrixindo un apagado moi lento

### KI18n

- Usar os idiomas de configuracións rexionais de sistema de Qt como reserva fóra de UNIX

### KInit

- Limpar e facer cambios internos na migración a XCB de KLauncher

### KIO

- FavIconsCache: sincronizar tras escribir para que outras aplicacións o vexan e para evitar unha quebra ao destruír
- Corrixir moitos problemas de fíos en KUrlCompletion
- Corrixir unha quebra no diálogo de cambio de nome (fallo 360488)
- KOpenWithDialog: mellorar o texto do título e a descrición da xanela (fallo 359233)
- Permitir despregar escravos de entrada e saída cunha mellor compatibilidade con varias plataformas incluíndo a información dos protocolos nos metadatos de complemento

### KItemModels

- KSelectionProxyModel: simplificar a xestión da retirada de filas, simplificar a lóxica de cancelación de selección
- KSelectionProxyModel: Só crear de novo a asociación ao retirar cando se necesite (fallo 352369)
- KSelectionProxyModel: só baleirar as asociacións de firstChild para a raíz
- KSelectionProxyModel: asegurarse de que se usan os sinais correctos ao retirar o último seleccionado
- Facer DynamicTreeModel buscábel por rol de visualización

### KNewStuff

- Non quebrar se faltan ficheiros .desktop ou hai ficheiros .desktop rotos

### KNotification

- Xestionar os clics co botón esquerdo en iconas antigas da área de notificacións (fallo 358589)
- Só usar a marca X11BypassWindowManagerHint na plataforma X11

### Infraestrutura de paquetes

- Cargar os paquetes tras instalalos
- non fallar se o paquete existe e está actualizado
- Engadir Package::cryptographicHash(QCryptographicHash::Algorithm)

### KPeople

- Definir o URI de contacto como URI de persoa en PersonData cando non existe unha persoa
- Definir o nome da conexión de base de datos

### KRunner

- Importar o modelo de executor de KAppTemplate

### KService

- Corrixir o novo aviso de kbuildsycoca cando in tipo MIME herda dunha referencia
- Corrixir a xestión de x-scheme-handler/* en mimeapps.list
- Corrixir a xestión de x-scheme-handler/* na análise de mimeapps.list (fallo 358159)

### KTextEditor

- Reverter «Páxina de configuración de abrir e gardar: Usar o termo «Cartafol» en vez de «Directorio»»
- forzar UTF-8
- Páxina de configuración de abrir e gardar: Usar o termo «Cartafol» en vez de «Directorio»
- kateschemaconfig.cpp: usar os filtros correctos cos diálogos de abrir e gardar (fallo 343327)
- c.xml: usar o estilo predeterminado para palabras clave de control de fluxo
- isocpp.xml: usar o estilo predeterminado «dsControlFlow» para as palabras clave de control de fluxo
- c/isocpp: engadir máis tipos estándar de C
- KateRenderer::lineHeight() devolve un enteiro
- impresión: usar o tamaño de letra do esquema de impresión seleccionado (fallo 356110)
- aceleración de cmake.xml: Usar WordDetect en vez de RegExpr
- Cambiar a anchura das tabulacións de 4 a 8
- Corrixir o cambio de cor do número de liña actual
- Corrixir a selección do elemento de completado co rato (fallo 307052)
- Engadir salientado de sintaxe para gcode
- Corrixir o pintado do fondo da selección de MiniMap
- Corrixir a codificación de gap.xml (usar UTF-8)
- Corrixir os bloques de comentarios aniñados (fallo 358692)

### KWidgetsAddons

- Ter en conta as marxes do contido ao calcular os consellos de tamaño

### KXMLGUI

- Corrixir a perda das accións conectadas nas barras de ferramentas de edición

### NetworkManagerQt

- ConnectionSettings: Límite de tempo de ping da inicialización da pasarela
- Novos tipos de conexión TunSetting e Tun
- Crear dispositivos para todos os tipos coñecidos

### Iconas de Oxygen

- Instalar index.theme no mesmo directorio no que estivo sempre
- Instalar en oxygen/base/ para que as iconas movidas das aplicacións non entren en conflito coa versión instalada por eses aplicacións
- Replicar as ligazóns simbólicas de breeze-icons
- Engadir novas iconas emblem-added e emblem-remove para sincronizar con Breeze

### Infraestrutura de Plasma

- [calendario] Corrixir que o miniaplicativo de calendario non baleire a selección ao agochar (fallo 360683)
- actualizar a icona de son para usar a folla de estilos
- actualizar a icona de silenciar o son (fallo 360953)
- Corrixir a creación forzada de miniaplicativos cando Plasma é inmutábel
- [Nodo esvaéndose] Non mesturar a opacidade por separado (fallo 355894)
- [Svg] Non analizar de novo a configuración como resposta a Theme::applicationPaletteChanged
- Dialog: definir os estados de SkipTaskbar e Pager antes de mostrar a xanela (fallo 332024)
- Introducir de novo a propiedade busy en Applet
- Asegurarse de que o ficheiro de exportación de PlasmaQuick se atopa correctamente
- Non importar disposicións que non existen
- Permitir que un miniaplicativo ofreza un obxecto de probas
- Substituír QMenu::exec por QMenu::popup
- FrameSvg: Corrixir os punteiros soltos en sharedFrames cando o tema cambia
- IconItem: Planificar unha actualización de mapas de píxeles cando a xanela cambia
- IconItem: animar o cambio de activo e activado incluso cando as animacións estean desactivadas
- DaysModel: Converter «update» nunha rañura
- [Elemento de icona] Non animar desde o mapa de píxeles anterior se era invisíbel
- [Elemento de icona] Non chamar a loadPixmap en setColorGroup
- [Miniaplicativo] Non sobrescribir a marca «Persistent» da notificación de desfacer
- Permitir sobrepoñer a opción de mutabilidade de Plasma na creación do contedor
- Engadir icon/titleChanged
- Eliminar a dependencia de QtScript
- A cabeceira de plasmaquick_export.h is está no cartafol plasmaquick
- Instalar algunhas cabeceiras de plasmaquick

Pode comentar e compartir ideas sobre esta versión na sección de comentarios do <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artigo do Dot</a>.
