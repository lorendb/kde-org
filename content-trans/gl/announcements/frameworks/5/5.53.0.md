---
aliases:
- ../../kde-frameworks-5.53.0
date: 2018-12-09
layout: framework
libCount: 70
---
### Baloo

- Corrixir as buscar de puntuacións de 10 (5 estrelas) (fallo 357960)
- Evitar escribir datos sen cambios en bases de datos de termos
- Non engadir Type::Document, Presentation e Spreadsheet dúas veces para documentos de MS Office
- Preparar KCrash ben de verdade
- Asegurarse de que só hai unha MTime por documento na MTimeDB
- [Extractor] Usar a serialización de QDataStream en vez de a cociñada
- [Extractor] Substituír o noso propio xestor de entrada e saída por QDataStream, capturar HUP

### Iconas de Breeze

- Engadir iconas para application-vnd.appimage e x-iso9660-appimage
- Engadir unha icona de dialog-warning de 22 px (fallo 397983)
- Corrixir o ángulo e a marxe de dialog-ok-apply de 32 px (fallo 393608)
- Cambiar as cores de icona monocroma principais para que coincidan coas novas cores das HIG
- Cambiar as iconas de acción archive-* para representar arquivos (fallo 399253)
- Engadir ligazóns simbólicas de help-browser aos directorios 16 px e 22 px (fallo 400852)
- Engadir novas iconas de ordenamento xenéricas; cambiar o nome das iconas de ordenamento existentes
- Engadir a versión de root de drive-harddisk (fallo 399307)

### Módulos adicionais de CMake

- Novo módulo: FindLibExiv2.cmake

### Integración de infraestruturas

- Engadir a opción BUILD_KPACKAGE_INSTALL_HANDLERS para saltar os xestores de instalación de construción

### Ferramentas de Doxygen de KDE

- Engadir un indicador de actividade durante a nova busca e facer a nova busca asíncrona (fallo 379281)
- Normalizar todas as rutas de entrada coa función os.path.normpath (fallo 392428)

### KCMUtils

- Aliñamentos perfectos entre títulos de KCM de QML e QWidget
- Engadir contexto á conexión de kcmodule a lambdas (fallo 397894)

### KCoreAddons

- Permitir usar KAboutData, License e Person desde QML
- Corrixir a quebra se o directorio XDG_CACHE_HOME é pequeno de máis ou está sen espazo (fallo 339829)

### DNS-SD de KDE

- agora instala kdnssd_version.h para comprobar a versión da biblioteca
- non deixar en memoria o resolvedor en remoteservice
- evitar unha condición de carreira entre sinais de avahi
- corrixir para macOS

### KFileMetaData

- Reavivar a propiedade «Description» para os metadatos de DublinCore
- engadir a propiedade description a KFileMetaData
- [KFileMetaData] Engadir un extractor de DSC que se axuste a Postscript (incrustado)
- [ExtractorCollection] Evitar a dependencia de kcoreaddons para probas de integración continua
- Engadir a taglibextractor compatibilidade con ficheiros speex
- engadir dúas fontes máis de Internet sobre información de etiquetas
- simplificar a xestión de etiquetas id3
- [XmlExtractor] Usar QXmlStreamReader para un mellor rendemento

### KIO

- Corrixir a aserción ao limpar ligazóns simbólicas en PreviewJob
- Engadir a posibilidade de ter un atallo de teclado para crear un ficheiro
- [KUrlNavigator] Activar de novo co clic do botón central do rato (fallo 386453)
- Retirar fornecedores de busca que xa non están dispoñíbeis
- Migrar máis fornecedores de busca a HTTPS
- Exportar KFilePlaceEditDialog de novo (fallo 376619)
- Restaurar a compatibilidade con sendfile
- [ioslaves/trash] Xestionar as ligazóns simbólicas rotas en directorios subordinados eliminados (fallo 400990)
- [RenameDialog] Corrixir a disposición cando se usa a marca NoRename
- Engadir o @since que faltaba en KFilePlacesModel::TagsType
- [KDirOperator] Usar a nova icona <code>view-sort</code> icon para o selector de orde
- Usar HTTPS para todos os fornecedores de busca que o permitan
- Desactivar a opción de desmontar para / ou /home (fallo 399659)
- [KFilePlaceEditDialog] Corrixir a garda de inclusión
- [Panel de lugares] Usar a nova icona <code>folder-root</code> para o elemento «Raíz»
- [KSambaShare] Permitir probar o analizador de «net usershare info»
- Darlle aos diálogos de ficheiro un botón de menú de «Ordenar por» na barra de ferramentas

### Kirigami

- DelegateRecycler: non crear un novo propertiesTracker para cada delegado
- Mover a páxina «Sobre» de Discover a Kirigami
- Agochar o caixón contextual cando hai unha barra de ferramentas global
- asegurarse de que todos os elementos están dispostos (fallo 400671)
- cambiar o índice ao premer, non ao facer clic (fallo 400518)
- novos tamaños de texto para Headings
- os caixóns das barras laterais non moven cabeceiras nin pés globais

### KNewStuff

- Engadir sinais de erros útiles a nivel programático

### KNotification

- Cambiar o nome de NotifyByFlatpak por NotifyByPortal
- Portal de notificacións: permitir mapas de píxeles nas notificacións

### Infraestrutura KPackage

- Non xerar datos de AppStream para ficheiros aos que lles falta unha descrición (fallo 400431)
- Capturar os metadatos dos paquetes antes do comezo da instalación

### KRunner

- Ao aproveitar executores ao cargar de novo, cargar de novo a súa configuración (fallo 399621)

### KTextEditor

- Permitir prioridades de definición de sintaxe negativas
- Expoñer a funcionalidade de «Conmutar o comentario» a través do menú de ferramentas e o atallo predeterminado (fallo 387654)
- Corrixir as linguaxes agochadas no menú de modo
- SpellCheckBar: Usar DictionaryComboBox en vez dun simple QComboBox
- KTextEditor::ViewPrivate: Evitar o aviso «Solicitouse un texto de intervalo incorrecto»
- Android: xa non fai falla definir log2
- desconectar contextmenu de todos os receptores de aboutToXXContextMenu (fallo 401069)
- Introducir AbstractAnnotationItemDelegate para que o consumidor teña máis control

### KUnitConversion

- Actualizouse coas unidades industriais de petróleo (fallo 388074)

### KWayland

- Xerar automaticamente un ficheiro de rexistro e corrixir o ficheiro das categorías
- Engadir VirtualDesktops a PlasmaWindowModel
- Actualizar a proba de PlasmaWindowModel para reflectir os cambios de VirtualDesktop
- Limpar windowInterface nas probas antes de destruír windowManagement
- Eliminar o elemento correcto en removeDesktop
- Limpar a entrada de lista do xestor de escritorios virtuais no destrutor de PlasmaVirtualDesktop
- Versión correcta da interface PlasmaVirtualDesktop que acaba de engadirse
- [servidor] Consello de contido de entrada de texto e versión por protocolo de propósito
- [servidor] Poñer activación e desactivación de entrada de texto, activar ou desactivar retrochamadas en clases subordinadas
- [servidor] Poñer «set» arredor da retrochamada de texto con uint na clase v0
- [servidor] Poñer algunhas respostas exclusivas de v0 de entrada de texto na clase v0

### KWidgetsAddons

- Engadir a API de nivel de Kirigami.Heading

### KXMLGUI

- Actualizar o texto «Sobre KDE»

### NetworkManagerQt

- Corrixiuse un fallo (erro?) na configuración de IPv4 e IPv6
- Engadir as opcións ovs-bridge e ovs-interface
- Actualizar a configuración de Ip-tunnel
- Engadir proxy e configuración de usuario
- Engadir a opción de IpTunnel
- Agora podemos construír sempre a proba da opción de TUN
- Engadir opcións de IPv6 que faltaban
- Escoitar as interfaces de D-Bus engadidas en vez de servizos rexistrados (fallo 400359)

### Infraestrutura de Plasma

- mesmas funcionalidades en Menu que no estilo de Desktop
- Qt 5.9 volveuse a versión mínima requirida
- Recuperar unha liña de CMakeLists.txt eliminada (accidentalmente?)
- Consistencia 100% co tamaño de cabeceira de Kirigami
- aparencia máis homoxénea con cabeceiras de Kirigami
- Instalar a versión procesada das importacións privadas
- Controis de selección de texto para móbiles
- Actualizar os esquemas de cores de Breeze Claro e Breeze Escuro
- Corrixíronse varias fugas de memoria (grazas a ASAN)

### Purpose

- Complemento de Phabricator: usar a orde diff.rev. de Arcanist (fallo 401565)
- Fornecer un título para JobDialog
- Permitir a JobDialog obter un mellor tamaño inicial (fallo 400873)
- Permitir que menudemo comparta URL distintos
- Usar QQC2 para JobDialog (fallo 400997)

### QQC2StyleBridge

- tamaño de elemento consistente con QWidgets
- corrixir o tamaño do menú
- asegurarse de que os flickables son pixelAligned
- Compatibilidade con aplicacións baseados en QGuiApplication (fallo 396287)
- controis de texto de área táctil
- Definir o tamaño segundo a anchura e altura de icona indicadas
- Respectar a propiedade «flat» dos botóns
- Corrixir un problema cando só hai un elemento no menú (fallo 400517)

### Solid

- Corrixir o cambio de icona do disco raíz para que non cambie outras iconas de maneira errónea

### Sonnet

- DictionaryComboBoxTest: engadir un estirón para evitar expandir o botón de envorcar

### Realce da sintaxe

- BrightScript: Permitir que sub non teña nome
- Engadir un ficheiro de realce para as trazas de depuración de Wayland
- Engadir salientado de sintaxe para TypeScript e React con TypeScript
- Rust e Yacc/Bison: mellorar os comentarios
- Prolog e Lua: corrixir o indicador de intérprete
- Corrixir a carga de idioma tras incluír palabras clave deste idioma noutro ficheiro
- Engadir sintaxe de BrightScript
- debchangelog: engadir Disco Dingo

### Información de seguranza

The released code has been GPG-signed using the following key: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB

Pode comentar e compartir ideas sobre esta versión na sección de comentarios do <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artigo do Dot</a>.
