---
aliases:
- ../announce-applications-15.04.1
changelog: true
date: '2015-05-12'
description: KDE Ships Applications 15.04.1.
layout: application
title: KDE publica a versión 15.04.1 das aplicacións de KDE
version: 15.04.1
---
May 12, 2015. Today KDE released the first stability update for <a href='../15.04.0'>KDE Applications 15.04</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

As máis de 50 correccións de erros inclúen melloras en, entre outros, KDE Libs, KDE PIM, Kdenlive, Okular, Marble e Umbrello.

This release also includes Long Term Support versions of Plasma Workspaces 4.11.19, KDE Development Platform 4.14.8 and the Kontact Suite 4.14.8.
