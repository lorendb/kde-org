---
aliases:
- ../announce-applications-17.12.2
changelog: true
date: 2018-02-08
description: KDE에서 KDE 프로그램 17.12.2 출시
layout: application
title: KDE에서 KDE 프로그램 17.12.2 출시
version: 17.12.2
---
February 8, 2018. Today KDE released the second stability update for <a href='../17.12.0'>KDE Applications 17.12</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

Kontact, Dolphin, Gwenview, KGet, Okular 등에 25개 이상의 버그 수정 및 기능 개선이 있었습니다.
