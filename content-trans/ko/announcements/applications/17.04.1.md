---
aliases:
- ../announce-applications-17.04.1
changelog: true
date: 2017-05-11
description: KDE에서 KDE 프로그램 17.04.1 출시
layout: application
title: KDE에서 KDE 프로그램 17.04.1 출시
version: 17.04.1
---
May 11, 2017. Today KDE released the first stability update for <a href='../17.04.0'>KDE Applications 17.04</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

kdepim, dolphin, gwenview, kate, kdenlive 등에 20개 이상의 버그 수정 및 기능 개선이 있었습니다.

This release also includes Long Term Support version of KDE Development Platform 4.14.32.
