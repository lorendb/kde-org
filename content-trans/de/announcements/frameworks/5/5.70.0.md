---
aliases:
- ../../kde-frameworks-5.70.0
date: 2020-05-02
layout: framework
libCount: 70
---
### Baloo

- [FileWatch] Remove redundant watchIndexedFolders() slot
- [ModifiedFileIndexer] Clarify a comment
- [FileWatch] Fix watch updates on config changes
- [KInotify] Fix path matching when removing watches
- [Extractor] Use categorized logging
- Use KFileMetaData for XAttr support instead of private reimplementation
- Revert "add Baloo DBus signals for moved or removed files"
- [QML Monitor] Show remaining time as soon as possible
- [FileContentIndexer] Fix state update and signal order
- [Monitor] Fix monitor state and signal ordering
- [Extractor] Fix progress reporting
- [Coding] Avoid recurrent detach and size checks
- [baloo_file] Remove KAboutData from baloo_file
- [Searchstore] Reserve space for phrase query terms
- [SearchStore] Allow querying exact matches for non-properties
- [PhraseAndIterator] Remove temporary arrays when checking matches
- [Extractor] Better balance idle and busy mode
- [Extractor] Fix idle monitoring
- [Extractor] Remove IdleStateMonitor wrapper class
- [OrpostingIterator] Allow skipping elements, implement skipTo
- [PhraseAndIterator] Replace recursive next() implementation
- [AndPostingIterator] Replace recursive next() implementation
- [PostingIterator] Make sure skipTo also works for first element
- rename and export newBatchTime signal in filecontentindexer
- [SearchStore] Handle double values in property queries
- [AdvancedQueryParser] Move semantic handling of tokens to SearchStore
- [Inotify] Remove not-so-OptimizedByteArray
- [Inotify] Remove dead/duplicate code
- [QueryParser] Replace single-use helper with std::none_of

### Breeze Icons

- Move document corner fold to top right in two icons
- Add 16px konversation icon
- correct vscode icon name
- Add 16px Vvave icon
- add alligator icon
- Add preferences-desktop-tablet and preferences-desktop-touchpad icons
- Update links in README.md
- build: quote source directory path
- Allow building from a read-only source location
- Add expand/collapse icons to accompany existing expand-all/collapse-all icons
- Add auth-sim-locked and auth-sim-missing
- Add sim card device icons
- Add rotation icons
- Add 16px System Settings icon
- Change ButtonFocus to Highlight
- Improve the look of kcachegrind
- Remove border from format-border-set-* icons

### Extra CMake-Module

- android: include the architecture on the apk name
- ECMAddQch: fix use of quotation marks with PREDEFINED in doxygen config
- Adapt FindKF5 to stricter checks in newer find_package_handle_standard_args
- ECMAddQch: help doxygen to handle Q_DECLARE_FLAGS, so such types get docs
- Fix wayland scanner warnings
- ECM: attempt to fix KDEInstallDirsTest.relative_or_absolute on Windows

### KDE Doxygen Tools

- Fix missing whitespace after "Platform(s):" on front page
- Fix use of quotation marks for PREDEFINED entries in Doxygile.global
- Teach doxygen about Q_DECL_EQ_DEFAULT &amp; Q_DECL_EQ_DELETE
- Add drawer on mobile and clean code
- Teach doxygen about Q_DECLARE_FLAGS, so such types can be documented
- Port to Aether Bootstrap 4
- Redo api.kde.org to look more like Aether

### KBookmarks

- Always create actioncollection
- [KBookMarksMenu] Set objectName for newBookmarkFolderAction

### KCMUtils

- KSettings::Dialog: add support for KPluginInfos without a KService
- Small optimization: call kcmServices() only once
- Downgrade warning about old-style KCM to qDebug, until KF6
- Use ecm_setup_qtplugin_macro_names

### KConfig

- kconfig_compiler : generate kconfig settings with subgroup
- Fix some compiler warnings
- Add force save behavior to KEntryMap
- Add standard shortcut for "Show/Hide Hidden Files" (bug 262551)

### KContacts

- Align description in metainfo.yaml with the one of README.md

### KCoreAddons

- API dox: use ulong typedef with Q_PROPERTY(percent) to avoid doxygen bug
- API dox: document Q_DECLARE_FLAGS-based flags
- Mark ancient KLibFactory typedef as deprecated
- [KJobUiDelegate] Add AutoHandlingEnabled flag

### KCrash

- Drop klauncher usage from KCrash

### KDeclarative

- Properly name the content of the kcmcontrols project
- Tweak kcmcontrols docs
- Add startCapture method
- [KeySequenceHelper] Work around Meta modifier behavior
- Also release the window in the destructor

### KDED

- Port KToolInvocation::kdeinitExecWait to QProcess
- Drop delayed second phase

### KHolidays

- Nicaragua Holidays
- Taiwanese holidays
- Updated Romanian holidays

### KI18n

- KI18N_WRAP_UI macro: set SKIP_AUTOUIC property on ui file and gen. header

### KIconThemes

- Add note about porting loadMimeTypeIcon

### KImageFormats

- Add support for modern Gimp images/XCF files

### KIO

- [RenameDialog] Add an arrow indicating direction from src to dest (bug 268600)
- KIO_SILENT Adjust API docs to match reality
- Move handling of untrusted programs to ApplicationLauncherJob
- Move check for invalid service from KDesktopFileActions to ApplicationLauncherJob
- Detect executables without +x permission in $PATH to improve error message (bug 415567)
- Make the HTML file template more useful (bug 419935)
- Add JobUiDelegate constructor with AutoErrorHandling flag and window
- Fix cache directory calculation when adding to trash
- File protocol: ensure KIO::StatAcl works without implicitly depending on KIO::StatBasic
- Add KIO::StatRecursiveSize detail value so kio_trash only does this on demand
- CopyJob: when stat'ing the dest, use StatBasic
- [KFileBookMarkHandler] Port to new KBookmarkMenu-5.69
- Mark KStatusBarOfflineIndicator as deprecated
- Replace KLocalSocket with QLocalSocket
- Avoid crash in release mode after the warning about unexpected child items (bug 390288)
- Docu: remove mention of non-existing signal
- [renamedialog] Replace KIconLoader usage with QIcon::fromTheme
- kio_trash: Add size, modification, access and create date for trash:/ (bug 413091)
- [KDirOperator] Use new "Show/Hide Hidden Files" standard shortcut (bug 262551)
- Show previews on encrypted filesystems (bug 411919)
- [KPropertiesDialog] Disable changing remote dir icons (bug 205954)
- [KPropertiesDialog] Fix QLayout warning
- API dox: document more of the default property values of KUrlRequester
- Fix DirectorySizeJob so it doesn't depend on listing order
- KRun: fix assert when failing to start an application

### Kirigami

- Introduce Theme::smallFont
- Make BasicListItem more useful by giving it a subtitle property
- Less segfaulty PageRouterAttached
- PageRouter: find parents of items better
- Remove unused QtConcurrent from colorutils
- PlaceholderMessage: Remove Plasma units usage
- Allow PlaceholderMessage to be text-less
- vertically center sheets if they don't have a scrollbar (bug 419804)
- Account for top and bottom margin in default card height
- Various fixes to new Cards (bug 420406)
- Icon: improve icon rendering on multi-screen multi-dpi setups
- Fix error in PlaceholderMessage: actions are disabled, not hidden
- Introduce PlaceholderMessage component
- Hotfix: fix bad typing in FormLayout array functions
- Hotfix for SwipeListItem: use Array.prototype.*.call
- Hotfix: use Array.prototype.some.call in ContextDrawer
- Hotfix for D28666: use Array.prototype.*.call instead of invoking functions on 'list' objects
- Add missing m_sourceChanged variable
- Use ShadowedRectangle for Card backgrounds (bug 415526)
- Update the visibility check for ActionToolbar by checking width with less-"equal"
- Couple of 'trivial' fixes for broken code
- never close when the click is inside the sheet contents (bug 419691)
- sheet must be under other popups (bug 419930)
- Add PageRouter component
- Add ColorUtils
- Allow setting separate corner radii for ShadowedRectangle
- Remove the STATIC_LIBRARY option to fix static builds

### KJobWidgets

- Add KDialogJobUiDelegate(KJobUiDelegate::Flags) constructor

### KJS

- Implement UString operator= to make gcc happy
- Silence compiler warning about copy of non-trivial data

### KNewStuff

- KNewStuff: Fix file path and process call (bug 420312)
- KNewStuff: port from KRun::runApplication to KIO::ApplicationLauncherJob
- Replace Vokoscreen with VokoscreenNG (bug 416460)
- Introduce more user-visible error reporting for installations (bug 418466)

### KNotification

- Implement updating of notifications on Android
- Handle multi-line and rich-text notifications on Android
- Add KNotificationJobUiDelegate(KJobUiDelegate::Flags) constructor
- [KNotificationJobUiDelegate] Append "Failed" for error messages

### KNotifyConfig

- Consistently use knotify-config.h to pass in flags about Canberra/Phonon

### KParts

- Add StatusBarExtension(KParts::Part *) overloaded constructor

### KPlotting

- Port foreach (deprecated) to range for

### KRunner

- DBus Runner: Add service property to request actions once (bug 420311)
- Print a warning if runner is incompatible with KRunner

### KService

- Deprecate KPluginInfo::service(), since the constructor with a KService is deprecated

### KTextEditor

- fix drag'n'drop on left side border widget (bug 420048)
- Store and fetch complete view config in and from session config
- Revert premature port to unreleased Qt 5.15 which changed meanwhile

### KTextWidgets

- [NestedListHelper] Fix indentation of selection, add tests
- [NestedListHelper] Improve indentation code
- [KRichTextEdit] Make sure headings don't mess with undo stack
- [KRichTextEdit] Fix scroll jumping around when horizontal rule is added (bug 195828)
- [KRichTextWidget] Remove ancient workaround and fix regression (commit 1d1eb6f)
- [KRichTextWidget] Add support for headings
- [KRichTextEdit] Always treat key press as single modification in undo stack (bug 256001)
- [findreplace] Handle searching for WholeWordsOnly in Regex mode

### KUnitConversion

- Add imperial gallon and US pint (bug 341072)
- Add Icelandic Krona to currencies

### KWayland

- [Wayland] Add to PlasmaWindowManagement protocol windows stacking order
- [server] Add some sub-surface life cycle signals

### KWidgetsAddons

- [KFontChooser] Remove NoFixedCheckBox DisplayFlag, redundant
- [KFontChooser] Add new DisplayFlag; modify how flags are used
- [KFontChooser] Make styleIdentifier() more precise by adding font styleName (bug 420287)
- [KFontRequester] Port from QFontDialog to KFontChooserDialog
- [KMimeTypeChooser] Add the ability to filter the treeview with a QSFPM (bug 245637)
- [KFontChooser] Make the code slightly more readable
- [KFontChooser] Add a checkbox to toggle showing only monospaced fonts
- Remove not necessary include

### KWindowSystem

- Print meaningful warning when there is no QGuiApplication

### KXMLGUI

- [KRichTextEditor] Add support for headings
- [KKeySequenceWidget] Work around Meta modifier behavior

### NetworkManagerQt

- Replace foreach with range-for

### Plasma Framework

- [PlasmaCore.IconItem] Regression: fix crash on source change (bug 420801)
- [PlasmaCore.IconItem] Refactor source handling for different types
- Make applet tooltip text spacing consistent
- [ExpandableListItem] make it touch-friendly
- [ExpandableListItem] Use more semantically correct expand and collapse icons
- Fix PC3 BusyIndicator binding loop
- [ExpandableListItem] Add new showDefaultActionButtonWhenBusy option
- Remove rounded borders to plasmoidHeading
- [ExpandableListItem] Add itemCollapsed signal and don't emit itemExpanded when collapsed
- Add readmes clarifying state of plasma component versions
- [configview] Simplify code / workaround Qt5.15 crash
- Create ExpandableListItem
- Make animation durations consistent with Kirigami values

### QQC2StyleBridge

- Detect QQC2 version at build time with actual detection
- [ComboBox] Use transparent dimmer

### Solid

- [Solid] Port foreach to range/index for
- [FakeCdrom] Add a new UnknownMediumType enumerator to MediumType
- [FstabWatcher] Fix loosing of fstab watcher
- [Fstab] Do not emit deviceAdded twice on fstab/mtab changes

### Syntax Highlighting

- debchangelog: add Groovy Gorilla
- Update Logtalk language syntax support
- TypeScript: add the "awaited" type operator

### Sicherheitsinformation

Der veröffentlichte Quelltext wurde mit GPG mit folgendem Schlüssel signiert: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Primärer Fingerprint des Schlüssels: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB
