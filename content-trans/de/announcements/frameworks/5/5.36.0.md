---
aliases:
- ../../kde-frameworks-5.36.0
date: 2017-07-08
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
All frameworks: Option to build &amp; install QCH file with the public API dox

### Baloo

- Use FindInotify.cmake to decide whether inotify is available

### Breeze Icons

- Do not depend on bash unnecessarily, and do not validate icons by default

### Extra CMake-Module

- FindQHelpGenerator: avoid picking up Qt4 version
- ECMAddQch: fail hard if needed tools are not present, to avoid surprises
- Drop perl as dep for ecm_add_qch, not needed/used
- Scan the whole install folder for qml dependencies
- New: ECMAddQch, for generating qch &amp; doxygen tag files
- Fix KDEInstallDirsTest.relative_or_absolute_usr, avoid Qt paths being used

### KAuth

- Check error status after every PolKitAuthority usage

### KBookmarks

- Emit errors when keditbookmarks is missing (bug 303830)

### KConfig

- Fix for CMake 3.9

### KCoreAddons

- Use FindInotify.cmake to decide whether inotify is available

### KDeclarative

- KKeySequenceItem: make it possible to record Ctrl+Num+1 as a shortcut
- Start drag with press and hold on touch events (bug 368698)
- Don't rely on QQuickWindow delivering QEvent::Ungrab as mouseUngrabEvent (as it no longer does in Qt 5.8+) (bug 380354)

### KDELibs 4 Support

- Search for KEmoticons, which is a dependency per the CMake config.cmake.in (bug 381839)

### KFileMetaData

- Add an extractor using qtmultimedia

### KI18n

- Make sure that the tsfiles target is generated

### KIconThemes

- More details about deploying icon themes on Mac &amp; MSWin
- Change panel icon size default to 48

### KIO

- [KNewFileMenu] Hide "Link To Device" menu if it would be empty (bug 381479)
- Use KIO::rename instead of KIO::moveAs in setData (bug 380898)
- Fix drop menu position on Wayland
- KUrlRequester: Set NOTIFY signal to textChanged() for text property
- [KOpenWithDialog] HTML-escape file name
- KCoreDirLister::cachedItemForUrl: don't create the cache if it didn't exist
- Use "data" as filename when copying data urls (bug 379093)

### KNewStuff

- Fix incorrect error detection for missing knsrc files
- Expose and use Engine's page size variable
- Make it possible to use QXmlStreamReader to read a KNS registry file

### KPackage-Framework

- Added kpackage-genericqml.desktop

### KTextEditor

- Fix cpu usage spiking after showing vi command bar (bug 376504)
- Fix jumpy scrollbar-dragging when mini-map is enabled
- Jump to the clicked scrollbar position when minim-map is enabled (bug 368589)

### KWidgetsAddons

- Update kcharselect-data to Unicode 10.0

### KXMLGUI

- KKeySequenceWidget: make it possible to record Ctrl+Num+1 as a shortcut (bug 183458)
- Revert "When building menu hyerarchies, parent menus to their containers"
- Revert "use transientparent directly"

### NetworkManagerQt

- WiredSetting: wake on lan properties were backported to NM 1.0.6
- WiredSetting: metered property was backported to NM 1.0.6
- Add new properties to many settings classes
- Device: add device statistics
- Add IpTunnel device
- WiredDevice: add information about required NM version for s390SubChannels property
- TeamDevice: add new config property (since NM 1.4.0)
- Wired device: add s390SubChannels property
- Update introspections (NM 1.8.0)

### Plasma Framework

- Make sure size is final after showEvent
- Fix vlc tray icon margins and color scheme
- Set Containments to have focus within the view (bug 381124)
- generate the old key before updating enabledborders (bug 378508)
- show show password button also if empty text (bug 378277)
- Emit usedPrefixChanged when prefix is empty

### Solid

- cmake: build udisks2 backend on FreeBSD only when enabled

### Syntax Highlighting

- Highlight .julius files as JavaScript
- Haskell: Add all language pragmas as keywords
- CMake: OR/AND not highlighted after expr in () (bug 360656)
- Makefile: Remove invalid keyword entries in makefile.xml
- indexer: Improve error reporting
- HTML syntax file version update
- Angular modifiers in HTML attributes added
- Update test reference data following the changes of the previous commit
- Bug 376979 - angle brackets in doxygen comments break syntax highlighting

### ThreadWeaver

- Work-around MSVC2017 compiler bug

### Sicherheitsinformation

Der veröffentlichte Quelltext wurde mit GPG mit folgendem Schlüssel signiert: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Primärer Fingerprint des Schlüssels: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB

Sie können im Kommentarabschnitt des <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>Dot-Artikels</a> über diese Veröffentlichung diskutieren und Ideen einbringen.
