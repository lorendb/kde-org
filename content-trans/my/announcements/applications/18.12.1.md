---
aliases:
- ../announce-applications-18.12.1
changelog: true
date: 2019-01-10
description: KDE Ships Applications 18.12.1.
layout: application
major_version: '18.12'
title: ကေဒီအီးသည် ကေဒီအီးအပ္ပလီကေးရှင်းများ ၁၈.၁၂.၁ တင်ပို့ခဲ့သည်
version: 18.12.1
---
{{% i18n_date %}}

Today KDE released the first stability update for <a href='../18.12.0'>KDE Applications 18.12</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

About 20 recorded bugfixes include improvements to Kontact, Cantor, Dolphin, JuK, Kdenlive, Konsole, Okular, among others.

တိုးတက်မှုများ -

- Akregator now works with WebEngine from Qt 5.11 or newer
- Sorting columns in the JuK music player has been fixed
- Konsole renders box-drawing characters correctly again
