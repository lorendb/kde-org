---
aliases:
- ../../kde-frameworks-5.64.0
date: 2019-11-10
layout: framework
libCount: 70
---
### Attica

- Lägg till några std::move i set-funktioner

### Baloo

- Kompilera med Qt 5.15
- Använd propertymap för att lagra egenskaper i Baloo::Result
- Lägg till fristående konverteringsfunktioner för PropertyMap till Json och vice versa
- [Databas] Arbeta om hantering av miljöflaggor
- Ersätt rekursion i FilteredDirIterator med snurriteration

### Breeze-ikoner

- Centrera de icke-fyrkantiga 64 bildpunkters audio Mime-typ ikonerna (fel 393550)
- Ta bort kvarleva av nepomuk ikon
- Flytta färgrik 32 bildpunkters help-about ikon till åtgärder (fel 396626)
- Förbättra draw ikoner (fel 399665)
- Ta bort nepomuk ikon
- Fyll i området för musens mittenknapp
- Lägg till folder-recent, utöka klockas visare i folder-temp
- Använd en korrektare och lämpligare visuell metafor för "hämta heta nyheter" ikon (fel 400500)
- Uppdatera elisa ikon
- Använd css istället för scss som utdataformat
- Rätta återgivning av 22 bildpunkters edit-opacity ikon
- Lägg till edit-opacity ikoner (fel 408283)
- Ikoner för blåsigt väder (fel 412718)
- Rätta felaktiga marginaler i 16/22 bildpunkters mediaikoner
- Använd textfärg istället för markeringsfärg för betyg/stjärnemblem
- Lägg till draw-arrow ikoner (fel 408283)
- Lägg till draw-highlight åtgärdsikoner (fel 408283)
- Lägg till PATH/LD_LIBRARY_PATH i anrop av qrcAlias
- Lägg till applications-network ikon för att byta namn på Internet-kategori till Network
- Lägg till edit-line-width ikoner (fel 408283)

### Extra CMake-moduler

- Ange inte C/C++ standarder om redan angivna
- Använd modernt sätt att ange C/CXX-standard
- Höj CMake krav till 3.5
- ECMAddQch: stöd PREDEFINED_MACROS/BLANK_MACROS med blanktecken och citationstecken

### Integrering med ramverk

- Lägg till standardikoner för att stödja alla poster i QDialogButtonBox (fel 398973)
- säkerställ att winId() inte anropas för icke-inbyggda grafiska komponenter (fel 412675)

### KActivitiesStats

- tester: rätta misslyckat bygge för makron
- Windows MSVC kompileringsrättning
- Lägg till en åtkomstfunktion för att få en QUrl från en ResultSet::Result

### KArchive

- Rätta minnesläcka i KXzFilter::init
- Rätta null pekarreferens när extraktion misslyckas
- decodeBCJ2: Rätta assert med felaktiga filer
- KXzFilter::Private: ta bort oanvända egenskaper
- K7Zip: Rätta minnesanvändning i readAndDecodePackedStreams

### KCalendarCore

- Lägg också till libical version
- Definiera explicit kopieringskonstruktor för journal

### KCMUtils

- Visa navigeringsknappar villkorligt i sidhuvudet för inställningsmoduler med flera sidor
- använd inte en egen höjd för sidhuvud (fel 404396)
- lägg till extra inkludering
- Rätta minnesläcka för KQuickAddons::ConfigModule-objekt (fel 412998)
- [KCModuleLoader] Visa fel när inläsning av QML misslyckas

### KConfig

- kconfig_compiler: Flytta KSharedConfig::Ptr när de används
- Gör så att det kompilerar med qt5.15 utan metod som avråds från
- Exponera isImmutable för introspektion (t.ex. QML)
- Lägg till bekvämlighet för förvalda/ändrade tillstånd i KCoreConfigSkeleton
- Gör så att kconfig_compiler genererar konstruktorer med valfritt argument för överliggande objekt
- Gör preferences() en öppen funktion

### KConfigWidgets

- Undvik överlagring av KCModule::changed

### KContacts

- Installera översättningar

### KCoreAddons

- KProcessInfoList -- lägg till gränssnitt proclist för FreeBSD

### KDeclarative

- Använd anslutning kontrollerad vid kompileringstillfälle
- Gör slot settingChanged() skyddad
- Få KQuickAddons::ConfigModule att exponera om vi är i förvalt tillstånd
- Greppa tangentbordet när KeySequenceItem spelar in
- Lägg till ManagedConfigModule
- Ta bort föråldrade kommentarer om [Se] expansion
- [ConfigModule] Exponera komponentstatus och felsträng för mainUi

### Stöd för KDELibs 4

- KLocale dokumentation av programmeringsgränssnitt: gör det enklare att hitta hur man konverterar kod från att använda det

### KDocTools

- man: använd &lt;arg&gt; istället för &lt;group&gt;

### KFileMetaData

- Rätta krasch i skrivsamling och uppstädning

### KHTML

- Utöka KHtmlView::print() att använda en fördefinierad instans av QPrinter (fel 405011)

### KI18n

- Lägg till KLocalizedString::untranslatedText
- Ersätt alla qWarning och relaterade anrop med kategoriserad loggning

### KIconThemes

- Rätta användning av de nya avrådningsmakrona för assignIconsToContextMenu
- Avråd från KIconTheme::assignIconsToContextMenu

### KIO

- Const och signatur för nyintroducerade SlaveBase::configValue
- Konvertera till QSslError-variant av KSslInfoDialog
- Konvertera KSSLD interna delar från KSslError till QSslError
- Gör SSL fel som inte går att ignorera explicita
- aktivera också automatisk KIO_ASSERT_SLAVE_STATES för byggen från git
- Konvertera (det mesta av) KSslInfoDialog från KSslError till QSslError
- kio_http: undvik dubbel Content-Type och Depth när använd av KDAV
- Konvertera KSSLD D-Bus gränssnitt från KSslError till QSslError
- Ersätt användning av SlaveBase::config()-&gt;readEntry med SlaveBase::configValue
- Ta bort två oanvända medlemsvariabler som använder KSslError
- Undvik skicka KDirNotify::emitFilesAdded när emptytrashjob avslutas
- Avråd från KTcpSocket-baserad variant av SslUi::askIgnoreSslErrors
- Behandla "application/x-ms-dos-executable" som körbar på alla plattformar (fel 412694)
- Ersätt användning av SlaveBase::config() med SlaveBase::mapConfig()
- ftptest: ersätt logger-colors med logger
- [SlaveBase] Använd QMap istället för KConfig för att lagra inställning av I/O-slav
- Konvertera KSslErrorUiData till QSslError
- undanta katalogen ioslaves från dokumentation av programmeringsgränssnitt
- ftptest: markera att överskrivning utan överskrivningsflagga inte längre misslyckas
- ftptest: omstrukturera demonstart till dess egen hjälpfunktion
- [SslUi] Lägg till dokumentation av programmeringsgränssnittet för askIgnoreSslErrors()
- anse för tillfället att ftpd inte krävs
- konvertera ftp-slav till nytt felrapporteringssystem
- Rätta inläsning av proxy-inställningar
- Implementera KSslCertificateRule med QSslError istället för KSslError
- Konvertera (det mesta av) gränssnittet i KSslCertificateRule till QSslError
- Konvertera KSslCertificateManager till QSslError
- Uppdatera test kfileplacesviewtest efter D7446

### Kirigami

- Säkerställ att GlobalDrawer topContent alltid förblir överst (fel 389533)
- färglägg när musen hålls över bara när läget mer än en sida (fel 410673)
- Byt namn på Okular Active till Okular Mobile
- Objekt har aktivt fokus under flik när de inte är i en vy (fel 407524)
- Tillåt att contextualActions flyter in i sidhuvudets verktygsrad
- Rätta felaktig kreditmodell för Kirigami.AboutPage
- Visa inte sammanhangslåda om alla åtgärder är osynliga
- Rätta Kirigami mallbild
- låt behållare vara tomma på borttagna objekt
- begränsa storlek för dragmarginaler
- Rätta att menyers verktygsknapp visas när ingen låda är tillgänglig
- Inaktivera att dra global låda i menyläge
- Visa verktygstipstext för menyalternativ
- Varna inte om LayoutDirection i SearchField
- Kontrollera aktiveringstillstånd riktigt för Action i ActionToolBar-knappar
- Använd åtgärdsegenskapen i MenuItem direkt i ActionMenuItem
- Tillåt att globala lådor blir en meny om det önskas
- Var mer explicit om åtgärdsegenskapernas typer

### KItemViews

- [RFC] Förena stil för nya Kirigami.ListSectionHeader och CategoryDrawer

### KJS

- Bättre meddelande för intervallfel i String.prototype.repeat(count)
- Förenkla tolkning av numeriska litteraler
- Tolka JS binära litteraler
- Detektera avkortade hexadecimala och oktala litteraler
- Stöd för nya standardsättet att ange oktala litteraler
- Samling av regressionstester tagna från khtmltests-arkivet

### KNewStuff

- Säkerställ att egenskapen changedEntries propageras riktigt
- Rätta hämtning av KNSCore::Cache när Engine initieras (fel 408716)

### KNotification

- [KStatusNotifierItem] Tillåt vänsterklick när menyn är null (fel 365105)
- Ta bort stöd för Growl
- Lägg till och aktivera stöd för Notification Center i macOS

### KPeople

- Laga bygge: begränsa DISABLE_DEPRECATED för KService till &lt; 5.0

### KService

- Gör så att det kompilerar med qt5.15 utan metod som avråds från

### KTextEditor

- KateModeMenuList: förbättra radbrytning
- rätta krasch (fel 413474)
- fler ok anlände, mer v2+
- fler ok anlände, mer v2+
- lägg till tips i copyright sidhuvud
- inga icke-triviala kodrader återstår här förutom folk som gick med på v2+ =&gt; v2+
- inga icke-triviala kodrader återstår här för upphovsmän som inte längre svarar, v2+
- fler filer v2+, eftersom upphovsmännen har gett sina ok, se kwrite-devel@kde.org
- fler filer v2+, eftersom upphovsmännen har gett sina ok, se kwrite-devel@kde.org
- fler filer v2+, eftersom upphovsmännen har gett sina ok, se kwrite-devel@kde.org
- fler filer v2+, eftersom upphovsmännen har gett sina ok, se kwrite-devel@kde.org
- fler filer v2+, eftersom upphovsmännen har gett sina ok, se kwrite-devel@kde.org
- fler filer v2+, eftersom upphovsmännen har gett sina ok, se kwrite-devel@kde.org
- katedocument.h är redan v2+
- ok av loh.tar, sars, lgplv2+
- ändra licens till lgplv2+, fick ok från sven + michal
- ändra licens till lgplv2+, fick ok från sven + michal
- alla filer med SPDX-License-Identifier är lgplv2+
- klargör licens, lägg till SPDX-License-Identifier
- klargör licens, lägg till SPDX-License-Identifier
- klargör licens, lägg till SPDX-License-Identifier
- klargör licens, lägg till SPDX-License-Identifier
- klargör licens, lägg till SPDX-License-Identifier
- klargör licens, lägg till SPDX-License-Identifier
- uppdatera licens, dh är i lgplv2+ avsnittet i relicensecheck.pl
- uppdatera licens, dh är i lgplv2+ avsnittet i relicensecheck.pl
- klargör licens, lägg till SPDX-License-Identifier
- klargör licens, lägg till SPDX-License-Identifier
- klargör licens, lägg till SPDX-License-Identifier
- klargör licens, lägg till SPDX-License-Identifier
- klargör licens, lägg till SPDX-License-Identifier
- klargör licens, lägg till SPDX-License-Identifier
- klargör licens, lägg till SPDX-License-Identifier
- lgplv2.1+ =&gt; lgplv2+
- klargör licens, lägg till SPDX-License-Identifier
- klargör licens, lägg till SPDX-License-Identifier
- klargör licens, lägg till SPDX-License-Identifier
- klargör licens, lägg till SPDX-License-Identifier
- klargör licens, lägg till SPDX-License-Identifier
- klargör licens, lägg till SPDX-License-Identifier
- klargör licens, lägg till SPDX-License-Identifier
- klargör licens, lägg till SPDX-License-Identifier
- klargör licens, lägg till SPDX-License-Identifier
- klargör licens, lägg till SPDX-License-Identifier
- sidhuvudet innehåller ingen bara gpl v2-logik sedan länge
- klargör licens, lägg till SPDX-License-Identifier
- klargör licens, lägg till SPDX-License-Identifier
- klargör licens, lägg till SPDX-License-Identifier
- klargör licens, lägg till SPDX-License-Identifier
- klargör licens, lägg till SPDX-License-Identifier
- klargör licens, 'michalhumpula' =&gt; ['gplv23', 'lgplv23', 'gplv2+', 'lgplv2+', '+eV' ]
- lägg till saknat s (fel 413158)
- KateModeMenuList: tvinga den vertikala positionen ovanför knappen
- better: självständiga sidhuvuden
- gruppera inkluderingar för semantik
- sortera inkluderingar

### KTextWidgets

- Ta bort anrop till KIconTheme::assignIconsToContextMenu som inte längre behövs

### Kwayland

- FakeInput: lägg till stöd för tangentbordsnedtryckning och uppsläppning
- Rätta icke-heltals skalningskopiering när OutputChangeSet skapas

### KXMLGUI

- rätta förvald genvägsdetektering

### NetworkManagerQt

- Lägg till SAE behörighetskontroll använd av WPA3

### Plasma ramverk

- avbilda disabledTextColor till ColorScope
- lägg till DisabledTextColor i Theme
- [PC3/button] Avkorta alltid text
- Förbättra menyalternativ för panelinställningar
- [icons/media.svg] Lägg till 16 och 32 bildpunkters ikoner, uppdatera stil
- [PlasmaComponents3] Rätta bakgrund för kontrollerbar verktygsknapp

### Prison

- Rätta minneshantering i datamatrix

### Syfte

- i18n: Lägg till uteslutningstecken för åtgärdsobjekt (X-Purpose-ActionDisplay)

### QQC2StyleBridge

- Tilldela inte kombinationsrutans currentIndex eftersom det gör att bindning går sönder
- Lyssna på när programstil ändras

### Solid

- Bygg inte statiska bibliotek när BUILD_TESTING=OFF

### Syntaxfärgläggning

- VHDL: alla nyckelord är okänsliga (fel 413409)
- Lägg till strängundantagstecken i PowerShell syntax
- Modelines: rätta kommentarslut
- Meson: fler inbyggda funktioner och lägg till inbyggda medlemsfunktioner
- debchangelog: lägg till Focal Fossa
- Uppdateringar från CMake 3.16
- Meson: Lägg till ett kommentaravsnitt för kommentera och avkommentera med Kate
- TypeScript: uppdatera grammatik och rättningar

### ThreadWeaver

- Kompilera med Qt 5.15

### Säkerhetsinformation

Den utgivna koden GPG-signerad genom att använda följande nyckel: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Primärt nyckelfingeravtryck: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB
