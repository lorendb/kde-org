---
aliases:
- ../announce-applications-17.12.0
date: 2017-12-14
description: KDE levererar KDE-program 17.12.0
layout: application
title: KDE levererar KDE-program 17.12.0
version: 17.12.0
---
14:e december, 2017. Idag ger KDE ut KDE-program 17.12.0.

Vi arbetar kontinuerligt på att förbättra programvaran som ingår i vår KDE-programserie, och vi hoppas att du finner alla nya förbättringar och felrättningar användbara.

### Vad är nytt i KDE-program 17.12

#### System

{{<figure src="/announcements/applications/17.12.0/dolphin1712.gif" width="600px" >}}

<a href='https://www.kde.org/applications/system/dolphin/'>Dolphin</a>, vår filhanterare, kan nu spara sökningar och begränsa sökningar till bara kataloger. Det är nu enklare att byta namn på filer: dubbelklicka helt enkelt på filnamnet. Mer filinformation finns nu till hands, eftersom ändringsdatum och ursprunglig webbadress för nerladdade filer nu visas i informationspanelen. Dessutom har kolumnerna Genre, Bithastighet och Utgivningsår introducerats.

#### Grafik

Vår kraftfulla dokumentvisare <a href='https://www.kde.org/applications/graphics/okular/'>Okular</a> har fått stöd för bildskärmar med hög upplösning och Markdown-språket, och återgivning av dokument som tar lång tid att läsas in visas nu progressivt. Ett alternativ att dela ett dokument via e-post är nu tillgängligt.

Bildvisaren <a href='https://www.kde.org/applications/graphics/gwenview/'>Gwenview</a> kan nu öppna och markera bilder i filhanteraren, zoomning är smidigare, tangentbordsnavigering har förbättrats, och formaten FITS och Truevision TGA stöds nu. Bilder är nu skyddade från borttagning av misstag med tangenten Delete när de inte är markerade.

#### Multimedia

<a href='https://www.kde.org/applications/multimedia/kdenlive/'>Kdenlive</a> använder nu mindre minne när videoprojekt som inkluderar många bilder hanteras, ersättningsstandardprofiler har förfinats, och ett irriterande fel som hade att göra med att gå en sekund framåt vid uppspelning baklänges har rättats.

#### Verktyg

{{<figure src="/announcements/applications/17.12.0/kate1712.png" width="600px" >}}

Stödet för zip i <a href='https://www.kde.org/applications/utilities/ark/'>Ark</a> med gränssnittet libzip har förbättrats. <a href='https://www.kde.org/applications/utilities/kate/'>Kate</a> har ett nytt <a href='https://frinring.wordpress.com/2017/09/25/ktexteditorpreviewplugin-0-1-0/'>insticksprogram för förhandsgranskning</a> som låter dig direkt se en förhandsgranskning av ett textdokument med slutligt format, genom användning av tillgängliga KParts-insticksprogram (t.ex. för Markdown, SVG, Dot graph, Qt UI eller programfixar). Insticksprogrammet fungerar också i <a href='https://www.kde.org/applications/development/kdevelop/'>KDevelop</a>.

#### Utveckling

{{<figure src="/announcements/applications/17.12.0/kuiviewer1712.png" width="600px" >}}

<a href='https://www.kde.org/applications/development/kompare/'>Kompare</a> erbjuder nu en sammanhangsberoende meny i jämförelseområdet, vilket ger snabbare åtkomst till navigerings- eller ändringsåtgärder. Om du är en utvecklare, kan du finna att den nya förhandsgranskningen av UI-objekt i fönsterrutan beskrivna av Qt UI-filer (grafiska komponenter, dialogrutor, etc.) är användbar. Den stöder nu också KParts strömmande programmeringsgränssnitt.

#### Kontor

<a href='https://www.kde.org/applications/office/kontact'>Kontact</a>-gruppen har arbetat hårt med att förbättra och förfina. En stor del av arbetet har varit att modernisera koden, men användare kommer att märka att visningen av krypterade brev har förbättrats och stöd har lagts till för text/pgp och <a href='https://phabricator.kde.org/D8395'>Apple® Wallet Pass</a>. Nu finns det ett alternativ att välja IMAP-korg vid semesterinställning, en ny varning i Kmail när ett brev öppnas igen och identiteten eller brevöverföringen inte är likadan, nytt stöd för <a href='https://micreabog.wordpress.com/2017/10/05/akonadi-ews-resource-now-part-of-kde-pim/'>Microsoft® Exchange™</a>, stöd för Nylas Mail och förbättrad Geary-import i Akonadis importguide, samt diverse andra felrättningar och allmänna förbättringar.

#### Spel

{{<figure src="/announcements/applications/17.12.0/ktuberling1712.jpg" width="600px" >}}

<a href='https://www.kde.org/applications/games/ktuberling/'>Potatismannen</a> kan nu nå en bredare publik, eftersom det har <a href='https://tsdgeos.blogspot.nl/2017/10/kde-edu-sprint-in-berlin.html'>konverterats till Android</a>. <a href='https://www.kde.org/applications/games/kolf/'>Kolf</a>, <a href='https://www.kde.org/applications/games/ksirk/'>KsirK</a> och <a href='https://www.kde.org/applications/games/palapeli/'>Palapeli</a> har gjort konverteringen av KDE-spelen till Ramverk 5 färdig.

### Mer konvertering till KDE Ramverk 5

Ännu fler program som var baserade på kdelibs4 som nu har konverterats till KDE Ramverk 5. De omfattar musikspelaren <a href='https://www.kde.org/applications/multimedia/juk/'>JuK</a>, nerladdningshanteraren <a href='https://www.kde.org/applications/internet/kget/'>KGet</a>, <a href='https://www.kde.org/applications/multimedia/kmix/'>KMix</a> verktyg såsom <a href='https://www.kde.org/applications/utilities/sweeper/'>Sweeper</a> och <a href='https://www.kde.org/applications/utilities/kmouth/'>KMouth</a>, och <a href='https://www.kde.org/applications/development/kimagemapeditor/'>KImageMapEditor</a> och I/O-slaven Zeroconf. Många tack till de hårt arbetande utvecklarna som bidragit med sin tid och sitt arbete för att göra detta möjligt!

### Program går till sina egna utgivningstidplaner

<a href='https://www.kde.org/applications/education/kstars/'>KStars</a> har nu en egen utgivningstidplan: Titta på den här <a href='https://knro.blogspot.de'>utvecklarbloggen</a> för kungörelser. Det är värt att notera att flera program såsom Kopete och Blogilo <a href='https://community.kde.org/Applications/17.12_Release_Notes#Tarballs_that_we_do_not_ship_anymore'>inte längre levereras</a> med programserien, eftersom de inte ännu har konverterats till KDE Ramverk 5, eller inte aktivt underhålls för närvarande.

### Felutplåning

Mer än 110 fel har rättats i program, inklusive Kontact-sviten, Ark, Dolphin, Gwenview, K3b, Kate, Kdenlive, Konsole, Okular, Umbrello med flera.

### Fullständig ändringslogg

Om du vill läsa mer om ändringarna i utgåvan, <a href='/announcements/changelogs/applications/17.12.0'>gå över till den fullständiga ändringsloggen</a>. Även om den kan vara lite avskräckande på grund av sin omfattning, kan ändringsloggen bli ett utmärkt sätt att lära sig något om KDE:s interna funktioner och upptäcka program och funktionalitet som du inte ens visste fanns.
