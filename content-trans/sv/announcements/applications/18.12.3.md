---
aliases:
- ../announce-applications-18.12.3
changelog: true
date: 2019-03-07
description: KDE levererar Program 18.12.2.
layout: application
major_version: '18.12'
release: applications-18.12.3
title: KDE levererar KDE-program 18.12.3
version: 18.12.3
---
{{% i18n_date %}}

Idag ger KDE ut den tredje stabilitetsuppdateringen av <a href='../18.12.0'>KDE-program 18.12</a>. Utgåvan innehåller bara felrättningar och översättningsuppdateringar, och är därmed en säker och behaglig uppdatering för alla.

Fler än tjugo registrerade felrättningar omfattar förbättringar av bland annat Kontact, Ark, Cantor, Dolphin, Filelight, JuK, Lokalize och Umbrello.

Förbättringar omfattar:

- Inläsning av .tar.zstd arkiv i Ark har rättats
- Dolphin kraschar inte längre när en Plasma-aktivitet stoppas
- Att byta till en annan partition kan inte längre krascha Filelight
