---
aliases:
- ../announce-applications-16.04.1
changelog: true
date: 2016-05-10
description: KDE distribueix les aplicacions 16.04.1 del KDE
layout: application
title: KDE distribueix les aplicacions 16.04.1 del KDE
version: 16.04.1
---
10 de maig de 2016. Avui KDE distribueix la primera actualització d'estabilització per a les <a href='../16.04.0'>aplicacions 16.04 del KDE</a>. Aquesta publicació només conté esmenes d'errors i actualitzacions de traduccions, proporcionant una actualització segura i millor per a tothom.

Hi ha més de 25 esmenes registrades d'errors que inclouen millores al Kdepim, Ark, Kate, Dolphin, Kdenlive, Lokalize i Spectacle entre altres.

Aquest llançament també inclou les versions de suport a llarg termini de la plataforma de desenvolupament KDE 4.14.20.
