---
aliases:
- ../announce-applications-15.12.0
changelog: true
date: 2015-12-16
description: Es distribueixen les aplicacions 15.12 del KDE.
layout: application
release: applications-15.12.0
title: KDE distribueix les aplicacions 15.12.0 del KDE
version: 15.12.0
---
16 de desembre de 2015. Avui KDE anuncia el llançament de les aplicacions 15.12 del KDE.

El KDE es complau d'anunciar el llançament 15.14 de les aplicacions del KDE, l'actualització de desembre de 2015 de les aplicacions del KDE. Aquest llançament aporta una aplicació nova i funcionalitats addicionals i esmenes d'errors a totes les aplicacions existents. L'equip està tractant de donar la millor qualitat a l'escriptori i a aquestes aplicacions. Així que esperem comptar amb vós per enviar els vostres comentaris.

En aquest llançament, s'han actualitzat una gran part d'aplicacions per a utilitzar els <a href='https://dot.kde.org/2013/09/25/frameworks-5'>Frameworks 5 del KDE</a> nous, incloent-hi l'<a href='https://edu.kde.org/applications/all/artikulate/'>Artikulate</a>, el <a href='https://www.kde.org/applications/system/krfb/'>Krfb</a>, el <a href='https://www.kde.org/applications/system/ksystemlog/'>KSystemLog</a>, el <a href='https://games.kde.org/game.php?game=klickety'>Klickety</a> i més jocs del KDE, a banda de la interfície de connectors d'imatge del KDE (KIPI) i les seves biblioteques d'implementació. Això fa que el nombre total d'aplicacions que usen els Frameworks 5 del KDE sigui 126.

### Una addició nova Spectacular

Després de 14 anys de formar part del KDE, el KSnapshot s'ha retirat i substituït per una aplicació nova de captura de pantalla, l'Spectacle.

Amb noves funcionalitats i una IU completament nova, l'Spectacle fa que les captures de pantalla siguin tan fàcils i discretes com poden ser. Com a complement del qual es podia fer amb el KSnapshot, amb l'Spectacle es poden prendre captures de pantalla compostes de menús emergents junt amb les seves finestres pare, o prendre captures de pantalla de la pantalla sencera (o la finestra actualment activa) sense ni iniciar l'Spectacle, senzillament usant les dreceres de teclat Maj + Impr Pant i Meta + Impr Pant respectivament.

També s'ha optimitzat enèrgicament el temps d'inici de l'aplicació, per a minimitzar completament el retard de temps entre l'inici de l'aplicació i la captura de la imatge.

### Afinament pertot arreu

Diverses aplicacions s'han afinat significativament en aquest cicle, addicionalment a les esmenes d'estabilitat i errors.

El <a href='https://userbase.kde.org/Kdenlive'>Kdenlive</a>, l'editor de vídeo no lineal, ha rebut esmenes grans a la seva interfície d'usuari. Ara es pot copiar i enganxar elements a la línia de temps, i canviar la transparència fàcilment en una pista concreta. Els colors d'icones s'ajusten automàticament al tema principal de la IU, facilitant la seva visió.

{{<figure src="/announcements/applications/15.12.0/ark1512.png" class="text-center" width="600px" caption=`L'Ark ara pot mostrar els comentaris dels ZIP`>}}

L'<a href='https://www.kde.org/applications/utilities/ark/'>Ark</a>, el gestor d'arxius, ara pot mostrar comentaris incrustats en arxius ZIP i RAR. S'ha millorat la implementació per als caràcters Unicode als noms de fitxer en els arxius ZIP, i ara es poden detectar arxius corromputs i recuperar-ne tantes dades com sigui possible. També es poden obrir fitxers arxivats en la vostra aplicació predeterminada, i s'ha esmenat l'acció d'arrossegar i deixar anar a l'escriptori, així com la vista prèvia dels fitxers XML.

### Tots els jocs i cap treball

{{<figure src="/announcements/applications/15.12.0/ksudoku1512.png" class="text-center" width="600px" caption=`Nova pantalla de benvinguda del KSudoku (al Mac OS X)`>}}

Els desenvolupadors dels jocs del KDE han estat treballant de valent durant els darrers mesos per optimitzar els jocs per a una experiència més fluida i enriquidora, i s'ha aconseguit material nou en aquesta àrea perquè els usuaris el gaudeixin.

En el <a href='https://www.kde.org/applications/games/kgoldrunner/'>KGoldrunner</a>, s'han afegit dos conjunts nous de nivells, un que permet cavar mentre es cau i l'altre no. S'han afegit solucions per a diversos conjunts de nivells existents. Per afegir dificultat, ara s'ha inhabilitat cavar mentre es cau en alguns conjunts antics de nivells.

En el <a href='https://www.kde.org/applications/games/ksudoku/'>KSudoku</a>, ara es poden imprimir els puzles Mathdoku i Killer Sudoku. La disposició multicolumna nova en la pantalla de benvinguda del KSudoku facilita veure més dels molts tipus de puzle disponibles, i les columnes s'ajusten automàticament quan es redimensiona la finestra. S'ha fet el mateix en el Palapeli, facilitant la visió de més col·leccions de trencaclosques a la vegada.

També s'han inclòs esmenes d'estabilitat per a jocs com el KNavalBattle, el Klickety, el <a href='https://www.kde.org/applications/games/kshisen/'>KShisen</a> i el <a href='https://www.kde.org/applications/games/ktuberling/'>KTuberling</a>, i ara l'experiència general ha millorat força. El KTuberling, el Klickety i el KNavalBattle també s'han actualitzat per usar els Frameworks 5 nous del KDE.

### Esmenes importants

Us molesta quan la vostra aplicació preferida falla en el moment més inoportú? A nosaltres també, i per solucionar això s'ha treballat durament per esmenar molts errors, però probablement encara queda algun desapercebut, així que no oblideu <a href='https://bugs.kde.org'>informar-los</a>!

En el gestor de fitxers <a href='https://www.kde.org/applications/system/dolphin/'>Dolphin</a>, s'han inclòs diverses esmenes per estabilitat i algunes esmenes per fer els desplaçaments més suaus. En el <a href='https://www.kde.org/applications/education/kanagram/'>Kanagram</a>, s'ha esmenat un problema molest amb el text blanc sobre fons de pantalla blancs. En el <a href='https://www.kde.org/applications/utilities/kate/'>Kate</a>, s'ha intentat esmenar una fallada que passava en aturar, i addicionalment s'ha netejat la IU i s'ha afegit un netejador nou de memòria cau.

El <a href='https://userbase.kde.org/Kontact'>conjunt de programes Kontact</a> ha rebut moltes funcionalitats afegides, esmenes grans i optimitzacions de rendiment. De fet, hi ha hagut tant desenvolupament en aquest cicle que ha saltat a la versió número 5.1. L'equip està treballant de valent, i espera els vostres comentaris.

### Avançant

Com a part de l'esforç per a modernitzar l'oferta, s'han eliminat diverses aplicacions del KDE i ja no es tornaran a publicar dins de les aplicacions del KDE 15.12

S'han eliminat 4 aplicacions de la publicació - Amor, KTux, KSnapshot i SuperKaramba. Com s'ha dit abans, el KSnapshot s'ha substituït per l'Spectacle, i essencialment el Plasma substitueix el SuperKaramba com a motor de ginys. S'han eliminat els estalvis de pantalla autònoms perquè el bloqueig de pantalla es gestiona de manera molt diferent en els escriptoris moderns.

També s'han eliminat 3 paquets de material gràfic (kde-base-artwork, kde-wallpapers i kdeartwork); el seu contingut no ha canviat en molt temps.

### Registre complet de canvis
