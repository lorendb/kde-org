---
aliases:
- ../announce-applications-17.04.2
changelog: true
date: 2017-06-08
description: KDE distribueix les aplicacions 17.04.2 del KDE
layout: application
title: KDE distribueix les aplicacions 17.04.2 del KDE
version: 17.04.2
---
8 de juny de 2017. Avui KDE distribueix la segona actualització d'estabilització per a les <a href='../17.04.0'>aplicacions 17.04 del KDE</a>. Aquesta publicació només conté esmenes d'errors i actualitzacions de traduccions, proporcionant una actualització segura i millor per a tothom.

Hi ha més de 15 esmenes registrades d'errors que inclouen millores al Kdepim, Ark, Dolphin, Gwenview, Kdenlive, entre d'altres.

Aquest llançament també inclou les versions de suport a llarg termini de la plataforma de desenvolupament KDE 4.14.33.
