---
aliases:
- ../../kde-frameworks-5.45.0
date: 2018-04-14
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Attica

- Estableix explícitament el tipus de contingut als formularis de dades

### Baloo

- Simplifica els termes dels operadors &amp;&amp; i ||
- No recupera l'ID del document ID per entrades omeses del resultat
- No recupera repetidament el «mtime» de la base de dades en ordenar
- No exporta «databasesanitizer» de manera predeterminada
- Baloodb: afegeix un missatge experimental
- Presenta l'eina «baloodb» de la CLI
- Presenta la classe «sanitizer»
- [FileIndexerConfig] Retarda l'ompliment de les carpetes fins que realment s'usin
- src/kioslaves/search/CMakeLists.txt - enllaça a Qt5Network seguint els canvis al KIO
- Balooctl: «checkDb» també hauria de verificar el darrer URL conegut del «documentId»
- Balooctl monitor: espera al servei en reprendre

### Icones Brisa

- Afegeix la icona «window-pin» (error 385170)
- Reanomena les icones de 64px afegides per l'Elisa
- Canvia les icones de 32px de la barreja de la llista de reproducció i repetició
- Icones que manquen dels Missatges en línia (error 392391)
- Icona nova per al reproductor de música Elisa
- Afegeix icones per l'estat dels suports
- Elimina el marc al voltant de les icones d'acció dels suports
- Afegeix les icones «media-playlist-append» i «play»
- Afegeix «view-media-album-cover» per al Babe

### Mòduls extres del CMake

- Fa ús de la infraestructura de la línia de treball principal del CMake per detectar la cadena d'eines del compilador
- API dox: esmena diverses línies «code-block» per tenir línies buides abans/després
- Afegeix ECMSetupQtPluginMacroNames
- Proporciona «androiddeployqt» amb tots els camins de prefix
- Inclou el «stdcpp-path» en el fitxer Json
- Resol els enllaços simbòlics als camins d'importació QML
- Proporciona camins d'importació a «androiddeployqt»

### Integració del marc de treball

- kpackage-install-handlers/kns/CMakeLists.txt - enllaç a Qt::Xml seguint els canvis a «knewstuff»

### KActivitiesStats

- No assumeix que el SQLite funciona i no finalitza en cas d'error

### Eines de Doxygen del KDE

- Primer cerca «qhelpgenerator-qt5» per a la generació de l'ajuda

### KArchive

- karchive, kzip: intenta gestionar els fitxers duplicats d'una manera millor
- Usa «nullptr» per passar un apuntador nul a «crc32»

### KCMUtils

- Fa possible sol·licitar per programa un mòdul de configuració de connector
- Per consistència, usa X-KDE-ServiceTypes en lloc de ServiceTypes
- Afegeix «X-KDE-OnlyShowOnQtPlatforms» a la definició «servicetype» del «KCModule»

### KCoreAddons

- KTextToHTML: retorna quan l'URL està buit
- Neteja «m_inotify_wd_to_entry» abans d'invalidar els apuntadors Entry (error 390214)

### KDeclarative

- Estableix QQmlEngine només un cop al QmlObject

### KDED

- Afegeix X-KDE-OnlyShowOnQtPlatforms a la definició del «servicetype» KDEDModule

### KDocTools

- Afegeix les entitats Elisa, Markdown, KParts, DOT, SVG a «general.entities»
- customization/ru: esmena la traducció de «underCCBYSA4.docbook» i «underFDL.docbook»
- Esmena un duplicat a «lgpl-notice/gpl-notice/fdl-notice»
- customization/ru: traducció de «fdl-notice.docbook»
- Canvia l'ortografia del KWave, sol·licitada pel mantenidor

### KFileMetaData

- taglibextractor: Refactoritza per a millorar-ne la lectura

### KGlobalAccel

- No fer l'asserció si s'usa incorrectament des de D-Bus (error 389375)

### KHolidays

- holidays/plan2/holiday_in_en-gb - actualitza el calendari de festius per a l'Índia (error 392503)
- Aquest paquet no estava actualitzat. Potser un problema amb l'script
- Refà els fitxers de festius per Alemanya (error 373686)
- Dona format al README.md tal com esperen les eines (amb una secció «Introduction»)

### KHTML

- Evita demanar un protocol buit

### KI18n

- S'assegura que «ki18n» pot construir les seves pròpies traduccions
- No invoca PythonInterp.cmake a KF5I18NMacros
- Fa possible generar fitxers «po» en paral·lel
- Crea un constructor per KLocalizedStringPrivate

### KIconThemes

- Fa acurat el comentari d'exportació de KIconEngine
- Evita un error en temps d'execució de l'ASAN

### KInit

- Elimina l'autorització temporal de l'IdleSlave

### KIO

- Assegura que s'ha definit el model quan es crida «resetResizing»
- «pwd.h» no existeix al Windows
- Elimina les entrades predeterminades «Desats recentment aquest mes» i «Desats recentment el darrer mes»
- Té el KIO construït per l'Android
- Desactiva temporalment la instal·lació del fitxer ajudant «kauth» del «ioslave» i del fitxer de política
- Gestiona les indicacions de confirmació d'operació amb privilegis al SlaveBase en lloc del KIO::Job
- Millora la coherència de la IU «Obre amb» mostrant sempre les aplicacions de dalt en línia
- Esmena una fallada quan el dispositiu emet lectura preparada després de finalitzar el treball
- Ressalta els elements seleccionats en mostrar la carpeta pare des del diàleg obre/desa (error 392330)
- Admet fitxers ocults de NTFS
- Per consistència, usa X-KDE-ServiceTypes en lloc de ServiceTypes
- Esmena una asserció a «concatPaths» en enganxar un camí complet a la línia d'edició del KFileWidget
- [KPropertiesDialog] Implementa la pestanya Suma de verificació per a qualsevol camí local (error 392100)
- [KFilePlacesView] Només crida a KDiskFreeSpaceInfo si és necessari
- FileUndoManager: no suprimeix fitxers locals no existents
- [KProtocolInfoFactory] No neteja la memòria cau si s'acaba de construir
- No intenta cercar una icona per un URL relatiu (p. ex. «~»)
- Usa un URL correcte d'element per al menú contextual «Crea nou» (error 387387)
- Soluciona la majoria de casos de paràmetre incorrecte a «findProtocol»
- KUrlCompletion: retorn anticipat si l'URL no és vàlid com «:/»
- No intenta cercar una icona per un URL buit

### Kirigami

- Icones més grans en mode mòbil
- Força una mida del contingut a l'element d'estil del fons
- Afegeix una pàgina d'exemple del tipus InlineMessage i d'aplicació Gallery
- Heurística millor per l'acoloriment selectiu
- Fa que funcioni realment la càrrega de SVG locals
- Admet també el mètode de càrrega d'icones d'Android
- Usa una estratègia d'acoloriment similar als antics estils diferents
- [Card] Usa una implementació pròpia de «findIndex»
- Mata les transferències de xarxa si es canvia la icona mentre s'executa
- Primer prototipus d'un reciclador delegat
- Permet que els clients OverlaySheet ometin el botó integrat de tancament
- Components per Cards
- Esmena la mida de l'ActionButton
- Fa que les «passiveNotifications» durin més, perquè els usuaris puguin realment llegir-les
- Elimina la dependència no utilitzada del QQC1
- Disposició de ToolbarApplicationHeader
- Fa possible mostrar el títol encara que tingui accions CTX

### KNewStuff

- Vota realment en fer clic a les estrelles a la vista de llista (error 391112)

### Framework del KPackage

- Intenta solucionar la construcció al FreeBSD
- Usa Qt5::rcc en lloc de cercar l'executable
- Usa NO_DEFAULT_PATH per assegurar que es recull l'ordre correcta
- Cerca també executables «rcc» prefixats
- Defineix el component per a la generació correcta del QRC
- Esmena la generació del paquet binari del RCC
- Genera el fitxer «rcc» cada vegada, en temps d'instal·lació
- Fa que els components «org.kde.» incloguin un URL de donació
- Marca com a no obsolet el «kpackage_install_package» per al «plasma_install_package»

### KPeople

- Exposa PersonData::phoneNumber al QML

### Kross

- No cal el requisit de «kdoctools»

### KService

- API dox: Per consistència, usa X-KDE-ServiceTypes en lloc de ServiceTypes

### KTextEditor

- Fa possible construir el «KTextEditor» al gcc 4.9 del NDK a l'Android
- Evita un error en temps d'execució de l'ASAN: el desplaçament d'exponent -1 és negatiu
- Optimització de TextLineData::attribute
- No calcula «attribute()» dues vegades
- Reverteix l'esmena: La vista salta quan el desplaçament després del final del document està actiu (error 391838)
- No contamina l'historial del porta-retalls amb duplicats

### KWayland

- Afegeix la interfície d'Accés Remot al KWayland
- [servidor] Afegeix la implementació de la semàntica de marcs del Pointer versió 5 (error 389189)

### KWidgetsAddons

- KColorButtonTest: elimina el codi de tasca pendent
- ktooltipwidget: Sostreu els marges de la mida disponible
- [KAcceleratorManager] Només estableix «iconText()» si realment ha canviat (error 391002)
- ktooltipwidget: Evita la visualització fora de pantalla
- KCapacityBar: defineix l'estat de QStyle::State_Horizontal
- Sincronitza amb els canvis de KColorScheme
- ktooltipwidget: Esmena el posicionament del consell d'eina (error 388583)

### KWindowSystem

- Afegeix «SkipSwitcher» a l'API
- [xcb] Esmena la implementació de _NET_WM_FULLSCREEN_MONITORS (error 391960)
- Redueix el temps de congelació del «plasmashell»

### ModemManagerQt

- CMake: no activa «libnm-util» com a trobat quan es troba el ModemManager

### NetworkManagerQt

- Exporta els directoris d'inclusions del NetworkManager
- Inicia requerint NM 1.0.0
- Dispositiu: defineix StateChangeReason i MeteredStatus com a Q_ENUMs
- Esmena la conversió dels indicadors AccessPoint a capacitats

### Frameworks del Plasma

- Plantilles de fons de pantalla: estableix el color de fons per assegurar el contrast a contingut de text de mostra
- Afegeix una plantilla per al fons de pantalla del Plasma amb una extensió QML
- [ToolTipArea] Afegeix el senyal «aboutToShow»
- WindowThumbnail: Usa un escalat correcte de la gamma
- WindowThumbnail: Usa filtratge de textura «mipmap» (error 390457)
- Elimina entrades X-Plasma-RemoteLocation no usades
- Plantilles: elimina X-Plasma-DefaultSize no usat de les metadades de la miniaplicació
- Per consistència, usa X-KDE-ServiceTypes en lloc de ServiceTypes
- Plantilles: elimina les entrades X-Plasma-Requires-* no usades de les metadades de la miniaplicació
- Elimina àncores d'element en una disposició
- Redueix el temps de congelació del «plasmashell»
- Precarrega només després que el contenidor emeti «uiReadyChanged»
- Esmena el trencament d'un quadre combinat (error 392026)
- Soluciona l'escalat de text amb factors d'escala no enters quan està establert PLASMA_USE_QT_SCALING=1 (error 356446)
- Icones noves per dispositius desconnectats/desactivats
- [Dialog] Permet definir «outputOnly» per al diàleg «NoBackground»
- [ToolTip] Comprova el nom de fitxer al gestor del KDirWatch
- Desactiva per ara l'avís d'obsolescència de «kpackage_install_packag»
- [Tema Brisa del Plasma] Aplica «currentColorFix.sh» a les icones canviades de suports
- [Tema Brisa del Plasma] Afegeix icones amb cercles d'estat dels suports
- Elimina els marcs al voltant dels botons dels suports
- [Window Thumbnail] Permet usar la textura «atlas»
- [Dialog] Elimina les ara obsoletes crides KWindowSystem::setState
- Permet les textures Atlas a FadingNode
- Esmena el fragment FadingMaterial amb el perfil del nucli

### QQC2StyleBridge

- Soluciona el renderitzat quan està desactivat
- Disposició millorada
- Implementació experimental per als mnemònics automàtics
- Assegura que es té en compte la mida de l'element en fer l'estil
- Esmena la renderització del tipus de lletra per a no HiDPI i factors d'escala enters (error 391780)
- Esmena els colors de les icones amb conjunts de colors
- Esmena els colors de les icones per als botons d'eina

### Solid

- El Solid ara pot consultar les bateries a p. ex. comandaments de joc i palanques de control sense fils
- Usa les enumeracions UP introduïdes recentment
- Afegeix els dispositius «gaming_input» i altres a Battery
- Afegeix l'enumeració dels dispositius de bateria
- [UDevManager] Consulta també explícitament per les càmeres
- [UDevManager] Filtra ja per subsistema abans de consultar (error 391738)

### Sonnet

- No imposa l'ús del client predeterminat, en selecciona un que admeti el llenguatge sol·licitat.
- Inclou cadenes de substitució a la llista de suggeriments
- Implementa NSSpellCheckerDict::addPersonal()
- NSSpellCheckerDict::suggest() retorna una llista de suggeriments
- Inicialitza el llenguatge del NSSpellChecker a NSSpellCheckerDict
- Implementa la categoria de registre del NSSpellChecker
- NSSpellChecker requereix AppKit
- Mou NSSpellCheckerClient::reliability() fora de la línia
- Usa el testimoni preferit de la plataforma Mac
- Usa el directori correcte a on cercar trigrames al directori de construcció del Windows

### Ressaltat de la sintaxi

- Fa possible construir completament el projecte amb compilació creuada
- Redissenya el generador de sintaxi del CMake
- Optimitza el ressaltat de Bash, Cisco, Clipper, Coffee, Gap, Haml, Haskell
- Afegeix el ressaltat de sintaxi per als fitxers MIB

### Informació de seguretat

El codi publicat s'ha signat amb GPG usant la clau següent: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Empremta digital de la clau primària: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB

Podeu debatre i compartir idees quant a aquest llançament en la secció de comentaris en <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>l'article del Dot</a>.
