---
date: 2013-08-14
hidden: true
title: KDE Application 4.11 introduce grandi passi avanti nella gestione delle informazioni
  personali (PIM) e miglioramenti globali
---
Il gestore dei file Dolphin ha ricevuto molte piccole correzioni e ottimizzazioni in questa versione. Il caricamento di grosse cartelle è stato accelerato e richiede fino al 30&#37; di memoria in meno. L'uso intensivo di disco e CPU viene prevenuto caricando solo le anteprime degli elementi vicini a quelli visibili. Ci sono stati molti altri miglioramenti; ad esempio sono stati corretti molti bug che coinvolgevano le cartelle espanse nella Vista dettagliata, nessuna icona segnaposto di tipo &quot;sconosciuto&quot; sarà mostrata quando si accede ad una cartella, e il clic del pulsante centrale su un archivio ora apre una nuova scheda con il contenuto dell'archivio, creando un'esperienza utente globalmente più coerente.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/send-later.png" caption=`Il nuovo processo di invio posticipato in Kontact` width="600px">}}

## Miglioramenti alla suite Kontact

Per la suite Kontact l'attenzione si è concentrata nuovamente sulla stabilità, sulle prestazioni e sull'uso della memoria. L'importazione di cartelle, il passaggio tra mappe, il recupero della posta, la marcatura o lo spostamento di molti messaggi e il tempo di avvio sono stati tutti migliorati negli ultimi sei mesi. Vedi <a href='http://blogs.kde.org/2013/07/18/memory-usage-improvements-411'>questo blog</a> per i dettagli. La <a href='http://www.aegiap.eu/kdeblog/2013/07/news-in-kdepim-4-11-archive-mail-agent/'>funzionalità di archiviazione ha ricevuto varie correzioni di bug</a>, e ci sono stati miglioramenti nell'assistente di importazione della posta che permettono l'importazione delle impostazioni dal client Trojitá e una migliore importazione dagli altri programmi di posta. Puoi trovare maggiori informazioni <a href='http://www.progdan.cz/2013/07/whats-new-in-the-akonadi-world/'>qui</a>.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/kmail-archive-agent.png" caption=`L'agente di archiviazione gestisce la conservazione dei messaggi in forma compressa` width="600px">}}

Questo rilascio porta con sé delle nuove importanti funzioni. C'è un <a href='http://www.aegiap.eu/kdeblog/2013/05/news-in-kdepim-4-11-header-theme-33-grantlee-theme-generator-headerthemeeditor/'>nuovo editor di temi per le intestazioni dei messaggi di posta</a> e le immagini nella posta possono essere ridimensionate al volo. La <a href='http://www.aegiap.eu/kdeblog/2013/07/new-in-kdepim-4-11-send-later-agent/'>funzione di invio posticipato</a> permette di pianificare l'invio di messaggi di posta in orari specificati. Il supporto in KMail per i filtri di Sieve (una funzionalità di IMAP che consente di filtrare i messaggi sul server) è stato migliorato, gli utenti possono generare degli script sieve di filtro <a href='http://www.aegiap.eu/kdeblog/2013/04/news-in-kdepim-4-11-improve-sieve-support-22/'>con un'interfaccia facile da usare</a>. Per quanto riguarda la sicurezza KMail introduce il &quot;rilevamento delle truffe&quot; automatico , <a href='http://www.aegiap.eu/kdeblog/2013/04/news-in-kdepim-4-11-scam-detection/'>mostrando un avviso</a> quando un messaggio contiene tipici trucchi di &quot;phishing&quot;. Adesso ricevi una <a href='http://www.aegiap.eu/kdeblog/2013/06/news-in-kdepim-4-11-new-mail-notifier/'>notifica informatica</a> quando arriva un nuovo messaggio di posta e infine, ma non meno importante, il programma di scrittura di blog Blogilo si presenta con un editor HTML notevolmente migliorato basato su QtWebKit.

## Supporto più esteso per i linguaggi in Kate

L'editor di testi avanzato Kate introduce nuove estensioni: Python (2 e 3), JavaScript e JQuery, Django e XML. Essi introducono nuove funzioni come completamento automatico statico e dinamico, controllo della sintassi, inserimento di frammenti di codice e l'abilità di rientrare automaticamente il codice XML con una scorciatoia di tastiera. Ma c'è altro per gli amici di Python: una console Python che fornisce informazioni dettagliate su un file sorgente aperto. Sono state effettuati alcuni piccoli miglioramenti nell'interfaccia utente, come <a href='http://kate-editor.org/2013/04/02/kate-search-replace-highlighting-in-kde-4-11/'>delle nuove notifiche passive per la ricerca</a>, <a href='http://kate-editor.org/2013/03/16/kate-vim-mode-papercuts-bonus-emscripten-qt-stuff/'>ottimizzazioni alla modalità VIM</a> e <a href='http://kate-editor.org/2013/03/27/new-text-folding-in-kate-git-master/'>una nuova funzionalità di raggruppamento del testo</a>.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/kstars.png" caption=`KStars mostra i prossimi eventi interessanti visibili dalla tua posizione` width="600px">}}

## Miglioramenti alle altre applicazioni

Nell'area dei giochi e della didattica sono arrivate piccole e grandi nuove funzioni e ottimizzazioni. I dattilografi in erba possono apprezzare il supporto per la scrittura da destra a sinistra in KTouch, mentre l'amico degli osservatori delle stelle KStars ora dispone di uno strumento che mostra gli eventi interessanti visibili nella tua area. Gli strumenti matematici Rocs, Kig, Cantor e KAlgebra hanno tutti ricevuto attenzione e supportano ulteriori motori e calcoli. E il gioco KJumpingCube adesso fornisce una tavola da gioco più grande, nuovi livelli di abilità, tempi di risposta più rapidi e un'interfaccia utente migliorata.

Il programma di disegno di base Kolourpaint può gestire il formato di immagini WebP, mentre il visualizzatore universale di documenti Okular dispone di strumenti di revisione configurabili e introduce il supporto per annullare/rifare le operazioni nei moduli e nelle annotazioni. Il riproduttore/gestore di tag audio Juk supporta la riproduzione e la modifica dei metadati del nuovo formato audio Ogg Opus (questa funzione richiede però il supporto di Ogg Opus anche nel driver audio e nella libraria TagLib).

#### Installare le applicazioni KDE

Il software KDE, incluse tutte le sue librerie e le sue applicazioni, è disponibile liberamente e gratuitamente sotto licenze Open Source. Il software KDE funziona su varie configurazioni hardware e architetture CPU come ARM e x86, su vari sistemi operativi e funziona con ogni tipo di gestore di finestre o ambiente desktop. Oltre a Linux e altri sistemi operativi basati su UNIX puoi trovare versioni per Microsoft Windows di buona parte delle applicazioni KDE sul sito del <a href='http://windows.kde.org'>software KDE per Windows</a> e versioni Apple Mac OS X sul sito del <a href='http://mac.kde.org/'>software KDE per Mac</a>. Versioni sperimentali di applicazioni KDE per varie piattaforme mobili come MeeGo, MS Windows Mobile e Symbian possono essere trovate sul web ma non sono al momento supportate. <a href='http://plasma-active.org'>Plasma Active</a> fornisce l'esperienza utente per una vasta gamma di dispositivi, come tablet e altro hardware mobile.

Il software KDE si può scaricare come sorgente e in vari formati binari da <a href='http://download.kde.org/stable/4.11.0'>download.kde.org</a> e può essere anche ottenuto su <a href='/download'>CD-ROM</a> o con qualsiasi <a href='/distributions'>principale distribuzione GNU/Linux o sistema UNIX</a> attualmente disponibile.

##### Pacchetti

Alcuni fornitori di sistemi Linux/UNIX hanno gentilmente messo a disposizione pacchetti binari di 4.11.0 per alcune versioni delle rispettive distribuzioni, e in altri casi dei volontari della comunità hanno provveduto. <br />

##### Posizione dei pacchetti

Per l'elenco aggiornato dei pacchetti binari disponibili di cui la squadra di rilascio di KDE è stata informata, visita il <a href='http://community.kde.org/KDE_SC/Binary_Packages#KDE_4.11.0'>wiki Community</a>.

Il codice sorgente completo per 4.11.0 può essere <a href='/info/4/4.11.0'>scaricato liberamente</a>. Le istruzioni su come compilare e installare il software KDE 4.11.0 sono disponibili dalla <a href='/info/4/4.11.0#binary'>pagina di informazioni di 4.11.0</a>.

#### Requisiti di sistema

Per poter ottenere il massimo da questi rilasci, raccomandiamo l'uso di una versione recente di Qt, come 4.8.4. Questo è necessario per poter assicurare un'esperienza stabile e più efficiente, dato che alcuni miglioramenti del software KDE sono stati in realtà effettuati sul framework Qt sottostante.<br /> Per usare in pieno le funzionalità del software KDE raccomandiamo inoltre di usare i driver grafici più recenti disponibili per il tuo sistema, in quanto l'esperienza utente può migliorare notevolmente, sia nelle funzionalità opzionali, sia nelle prestazioni sia nella stabilità globale.

## Oggi sono stati annunciati anche:

## <a href="../plasma"><img src="/announcements/4/4.11.0/images/plasma.png" class="app-icon" alt="The KDE Plasma Workspaces 4.11" width="64" height="64" /> Plasma Workspaces 4.11 continua ad affinare l'esperienza utente</a>

In preparazione per il supporto a lungo termine, Plasma Workspaces porta ulteriori miglioramenti alle funzionalità di base con una barra delle applicazioni più fluida, un indicatore della batteria più intelligente ed un mixer audio migliorato. L'introduzione di KScreen porta in Workspaces una gestione intelligente di monitor multipli, mentre miglioramenti su larga scala delle prestazioni combinati con piccole correzioni all'usabilità producono un'esperienza d'uso complessiva più gradevole. 

## <a href="../platform"><img src="/announcements/4/4.11.0/images/platform.png" class="app-icon" alt="The KDE Development Platform 4.11"/> KDE Platform 4.11 porta prestazioni migliori</a>

Questo rilascio di KDE Platform 4.11 continua a concentrarsi sulla stabilità. Nuove funzionalità sono in corso di implementazione in KDE Frameworks 5.0, il nostro futuro rilascio, ma in questa versione stabile siamo riusciti a far entrare delle ottimizzazioni per la nostra infrastruttura Nepomuk.
