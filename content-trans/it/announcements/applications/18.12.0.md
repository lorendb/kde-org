---
aliases:
- ../announce-applications-18.12.0
changelog: true
date: 2018-12-13
description: KDE rilascia Applications 18.12.
layout: application
release: applications-18.12.0
title: KDE rilascia KDE Applications 18.04.0
version: 18.12.0
---
{{% i18n_date %}}

KDE rilascia KDE Applications 18.12.

{{%youtube id="ALNRQiQnjpo"%}}

Lavoriamo continuamente per migliorare il software incluso nelle nostre serie di KDE Application, confidiamo che troverai utili tutti i miglioramenti e la risoluzione degli errori.

## Novità in KDE Applications 18.12

Sono stati corretti oltre 140 errori nelle applicazioni, tra cui la suite Kontact, Ark, Cantor, Dolphin, Gwenview, Kate, KmPlot, Konsole, Lokalize, Okular, Spectacle, Umbrello e altre ancora.

### Gestione dei file

{{<figure src="/announcements/applications/18.12.0/app1812_dolphin01.png" width="600px" >}}

<a href='https://www.kde.org/applications/system/dolphin/'>Dolphin</a>, il gestore di file completo di KDE:

- Nuova implementazione MTP che lo rende completamente utilizzabile negli ambienti di produzione
- Grandi miglioramenti di prestazioni nella lettura dei file su protocollo SFTP
- Per le anteprime delle miniature, le cornici e le ombre ora vengono disegnate solo per i file immagine senza trasparenza, migliorando la visibilità delle icone
- Nuove anteprime delle miniature per i documenti LibreOffice e le applicazioni AppImage
- I file video più grandi di 5 MB vengono ora visualizzati nelle miniature delle cartelle, quando l'Anteprima delle cartelle è abilitata
- Durante la lettura di CD audio, Dolphin ora è in grado di variare il bitrate CBR del codificatore MP3 e corregge la data e l'ora nei file FLAC
- Il menu «Controllo» di Dolphin ora mostra gli elementi «Crea nuovi…»' e possiede l'elemento di menu «Mostra le risorse nascoste»
- Dolphin ora si chiude quando è presente solo una scheda aperta ed è premuta la scorciatoia di tastiera standard «Chiudi scheda» (Ctrl+w)
- Dopo aver smontato un volume dal pannello Risorse, è ora possibile rimontarlo
- La vista Documenti recenti (disponibile navigando a recentdocuments:/ in Dolphin) ora mostra solo i documenti correnti, ed esclude automaticamente gli URL web
- Dolphin ora mostra un avviso prima di applicare un'operazione di rinomina per rendere nascosto un file o una cartella
- Non è più possibile, dal pannello Risorse, provare a smontare i dischi che contengono il sistema operativo attivo o la cartella home

<a href='https://www.kde.org/applications/utilities/kfind'>KFind</a>, lo strumento di ricerca di file tradizionale di KDE, ora possiede un metodo di ricerca dei metadati basato su KFileMetaData.

### Ufficio

<a href='https://www.kde.org/applications/internet/kmail/'>KMail</a>, il client completo di posta elettronico di KDE:

- KMail ora è in grado di mostrare una casella unificata di posta in arrivo
- Nuova estensione: Genera messaggi HTML dal linguaggio Markdown
- Uso di Purpose per la condivisione di testo (come posta elettronica)
- I messaggi di posta HTML ora sono leggibili, non importa quale schema dei colori sia in uso

{{<figure src="/announcements/applications/18.12.0/app1812_okular01.png" width="600px" >}}

<a href='https://www.kde.org/applications/graphics/okular/'>Okular</a>, il visore di documenti versatile di KDE:

- Nuovo strumento per le annotazioni «Typewriter», che può essere utilizzato per del testo in qualsiasi posizione
- La vista gerarchica Contenuti ora possiede la capacità di espandere e comprimere tutto, o solo un paragrafo specifico
- Comportamento di riga a capo migliorato nelle annotazioni interne
- Quando si passa il mouse su un collegamento, l'URL ora viene mostrato ogni qualvolta sia attivabile, e non solo quando si è in modalità Navigazione
- I file ePub che contengono risorse con spazi nei loro URL sono ora mostrati correttamente

### Sviluppo

{{<figure src="/announcements/applications/18.12.0/app1812_kate01.png" width="600px" >}}

<a href='https://www.kde.org/applications/utilities/kate/'>Kate</a>, l'editor di testo avanzato di KDE:

- Quando si usa il terminale incorporato, esso ora sincronizza automaticamente la cartella corrente con la posizione sul disco del documento attivo
- Il terminale incorporato di Kate ora può attivare o disattivare il fuoco utilizzando la scorciatoia di tastiera F4
- Il commutatore di schede incorporato di Kate ora mostra i percorsi completi per file con nomi simili
- I numeri di riga ora sono attivi in modo predefinito
- L'estensione Filtro di testo, incredibilmente completa e utile, è ora attivata in modo predefinito ed è più facile da imparare
- L'apertura di un documento già aperto tramite la funzionalità Apertura rapida ora ritorna a quel documento
- La funzionalità Apertura rapida non mostra più voci duplicate
- Quando si usano più Attività, i file ora sono aperti nell'Attività corretta
- Kate ora mostra tutte le icone corrette quando si avvia in GNOME utilizzando il tema delle icone GNOME

{{<figure src="/announcements/applications/18.12.0/app1812_konsole.png" width="600px" >}}

<a href='https://www.kde.org/applications/system/konsole/'>Konsole</a>, l'emulatore di terminale di KDE:

- Konsole ora supporta completamente i caratteri emoji
- Le icone delle schede inattive ora sono evidenziate quando ricevono un segnale di campanella
- I due punti finali ora non sono più considerati parti di una parola nel caso di una selezione con doppio clic, rendendo più semplice selezionare percorsi e l'output di «grep»
- Quando viene collegato un mouse coi pulsanti avanti e indietro, Konsole ora è in grado di usare quei pulsanti per passare da una scheda all'altra
- Konsole ora ha un elemento di menu per ripristinare la dimensione dei caratteri al valore predefinito del profilo, se questi sono stati ingranditi o ridotti
- Le schede ora sono più difficili da staccare accidentalmente e più veloci da riordinare
- Selezione Maiusc+clic migliorata
- Risolto il doppio clic su una riga di testo che eccede la larghezza della finestra
- La barra di ricerca si chiude di nuovo quando premi il tasto Esc

<a href='https://www.kde.org/applications/development/lokalize/'>Lokalize</a>, lo strumento di traduzione di KDE:

- Nasconde i file tradotti nella scheda del progetto
- Aggiunto supporto di base per pology, il sistema di controllo della sintassi ed del glossario
- Navigazione semplificata con ordinamento di schede e apertura di schede multipla
- Corretti errori di segmentazione dovuti ad accessi concomitanti a oggetti di database
- Corrette azioni di trascinamento e rilascio dal risultato non regolare
- Corretto un errore sulle scorciatoie diverse tra editor
- Ricerca migliorata (trova e visualizza le forme plurali)
- Ripristino della compatibilità con Windows grazie al sistema di compilazione manuale

### Accessori

<a href='https://www.kde.org/applications/graphics/gwenview/'>Gwenview</a>, il visore di immagini di KDE:

- Lo strumento «Correzione occhi rossi» è stato migliorato in usabilità
- Gwenview ora mostra una finestra di avviso quando nascondi la barra dei menu, e ti avvisa come ripristinarla

{{<figure src="/announcements/applications/18.12.0/app1812_spectacle01.png" width="600px" >}}

<a href='https://www.kde.org/applications/graphics/spectacle/'>Spectacle</a>, l'accessorio per catturare schermate di KDE:

- Spectacle ora possiede la capacità di numerare in maniera sequenziale le schermate, e passa al suo schema di nomina se cancelli il campo del nome
- Corretto il salvataggio delle immagini in un formato diverso da .png, quando si usa il comando Salva come...
- Quando si usa Spectacle per aprire una schermata in un'applicazione esterna, è ora possibile modificare e salvare l'immagine dopo averla lavorata
- Spectacle ora apre la cartella corretta quando fai clic su Strumenti > Apri cartella delle schermate
- La data e l'ora delle schermate ora riflettono la data e l'ora di creazione dell'immagine, e non del suo salvataggio
- Tutte le opzioni di salvataggio ora si trovano sulla pagina «Save»

<a href='https://www.kde.org/applications/utilities/ark/'>Ark</a>, il gestore degli archivi di KDE:

- Aggiunto supporto per il formato Zstandard (archivi tar.zst)
- Corretto il comportamento di Ark che visualizzava l'anteprima per determinati file (per es. Open Document) come fossero archivi, anziché aprirli con la corretta applicazione

### Matematica

<a href='https://www.kde.org/applications/utilities/kcalc/'>KCalc</a>, la semplice calcolatrice di KDE, ora possiede un'impostazione per ripetere più volte l'ultimo calcolo.

<a href='https://www.kde.org/applications/education/cantor/'>Cantor</a>, l'interfaccia matematica di KDE:

- Aggiunta il tipo di voce Markdown
- Evidenziazione animata della voce di comando attualmente calcolata
- Visualizzazione delle voci dei comandi in attesa (in coda ma non ancora calcolate)
- Possibilità di formattazione delle voci dei comandi (colore di sfondo, colore in primo piano, proprietà dei caratteri)
- Possibilità di inserimento di nuove voci di comandi in posizioni arbitrarie nel foglio di lavoro, posizionando il cursore nel punto desiderato e iniziando a digitare
- Per le espressioni che contengono più comandi, mostra il risultato come oggetti indipendenti nel foglio di lavoro
- Aggiunto supporto per aprire i fogli di lavoro dai percorsi relativi all'interno della console
- Aggiunto supporto per aprire più file in una shell di Cantor
- Cambiato il colore e il carattere per quando si chiedono informazioni aggiuntive, per meglio distinguere dal normale input nella voce di comando
- Aggiunte scorciatoie di tastiera per la navigazione tra i fogli di lavoro (Ctrl+PagSu, Ctrl+PagGiù)
- Aggiunta azione nel sottomenu «Visualizza» per il ripristino dello zoom
- Abilitato lo scaricamento dei progetti Cantor da store.kde.org (al momento funziona solo dal sito web)
- Apre il foglio di lavoro in modalità sola lettura se nel sistema non è disponibile il motore

<a href='https://www.kde.org/applications/education/kmplot/'>KmPlot</a>, il disegnatore di funzioni di KDE, ha corretto molti problemi:

- Risolti i nomi errati dei grafici per le derivate e gli integrali nella notazione convenzionale
- L'esportazione SVG in Kmplot ora funziona correttamente
- La funzionalità della derivata prima non usa la notazione di Lagrange
- La deselezione della funzione non a fuoco ora nasconde il suo grafico:
- Risolto un crash di Kmplot durante l'apertura ricorsiva dell'opzione «Modifica costanti» dall'editor delle funzioni
- Risolto un crash di KmPlot quando si elimina una funzione e il puntatore del mouse segue nel grafico
- Ora è possibile esportare i dati dei disegni come un qualsiasi formato immagine
