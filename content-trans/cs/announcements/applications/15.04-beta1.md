---
aliases:
- ../announce-applications-15.04-beta1
date: '2015-03-06'
description: KDE vydává Aplikace KDE 15.04 Beta 1.
layout: application
title: KDE vydává první Beta verzi Aplikací KDE 15.04
---
March 6, 2015. Today KDE released the beta of the new versions of KDE Applications. With dependency and feature freezes in place, the KDE team's focus is now on fixing bugs and further polishing.

With the various applications being based on KDE Frameworks 5, the KDE Applications 15.04 releases need a thorough testing in order to maintain and improve the quality and user experience. Actual users are critical to maintaining high KDE quality, because developers simply cannot test every possible configuration. We're counting on you to help find bugs early so they can be squashed before the final release. Please consider joining the team by installing the beta <a href='https://bugs.kde.org/'>and reporting any bugs</a>.
