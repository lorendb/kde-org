---
aliases:
- ../announce-4.11.1
date: 2013-09-03
description: KDE dobavlja Plasma Workspaces, aplikacije in platformo 4.11.1.
title: KDE dobavlja septembrske posodobitve za Plasma Workspaces, aplikacije in platformo
---
3. september 2013. Danes je KDE izdal posodobitve za svoje delovne prostore, aplikacije in razvojno platformo. Te posodobitve so prve v nizu mesečnih stabilizacijskih posodobitev niza izdaj 4.11. Kot je bilo objavljeno ob izdaji, bodo delovni prostori še naprej prejemali posodobitve v naslednjih dveh letih. Ta izdaja vsebuje samo popravke napak in posodobitve prevodov ter bo varna in prijetna posodobitev za vse.

Več kot 70 popravkov zabeleženih napak vključno z izboljšavami upravitelja oken KWin, upravitelja datotek Dolphin in drugih. Uporabniki lahko pričakujejo, da se bo namizje Plasma hitreje zaganjalo, Dolphin se bo pomikal bolj gladko, različne aplikacije in orodja pa bodo uporabljale manj pomnilnika. Izboljšave vključujejo vrnitev povleci-in-spusti iz opravilne vrstice nazaj v pager, poudarjanje in barvne popravke v Kate in VELIKO odstranjenih napakic v igri Kmahjongg. Obstajajo številni popravki stabilnosti in običajni dodatki prevodov.

Bolj popoln <a href='https://bugs.kde.org/buglist.cgi?query_format=advanced&amp;short_desc_type=allwordssubstr&amp;short_desc=&amp;long_desc_type=substring&amp;long_desc=&amp;bug_file_loc_type=allwordssubstr&amp;bug_file_loc=&amp;keywords_type=allwords&amp;keywords=&amp;bug_status=RESOLVED&amp;bug_status=VERIFIED&amp;bug_status=CLOSED&amp;emailtype1=substring&amp;email1=&amp;emailassigned_to2=1&amp;emailreporter2=1&amp;emailcc2=1&amp;emailtype2=substring&amp;email2=&amp;bugidtype=include&amp;bug_id=&amp;votes=&amp;chfieldfrom=2011-06-01&amp;chfieldto=Now&amp;chfield=cf_versionfixedin&amp;chfieldvalue=4.11.1&amp;cmdtype=doit&amp;order=Bug+Number&amp;field0-0-0=noop&amp;type0-0-0=noop&amp;value0-0-0='>seznam</a> sprememb je na voljo v KDE-jevem sledilniku težav. Za podroben seznam sprememb, ki so bile vključene v 4.11.1, lahko brskate tudi po dnevnikih Git.

Če želite prenesti izvorno kodo ali pakete za namestitev, pojdite na stran z <a href='/info/4/4.11.1'>informacijami o 4.11.1</a> . Če želite izvedeti več o različicah 4.11 KDE Workspaces, aplikacij in razvojne platforme KDE, si oglejte <a href='/announcements/4.11/'>opombe</a> k izdaji 4.11.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/send-later.png" caption=`Nov potek dela »pošlji pozneje« v Kontactu` width="600px">}}

Programska oprema KDE, vključno z vsemi knjižnicami in aplikacijami, je prosto na voljo pod odprto-kodnimi dovoljenju. Programsko opremo KDE lahko dobite kot izvorno kodo in kot različne binarne formate na <a href='http://download.kde.org/stable/4.11.1/'>download.kde.org</a> ali od katerega koli od <a href='/distributions'>glavnih dobaviteljev sistemov GNU/Linux in UNIX</a> danes.
