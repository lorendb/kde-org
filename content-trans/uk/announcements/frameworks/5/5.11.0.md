---
aliases:
- ../../kde-frameworks-5.11.0
date: 2015-06-12
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Додаткові модулі CMake

- Нові аргументи ecm_add_tests(). (вада 345797)

### Інтеграція бібліотек

- Належне значення initialDirectory для KDirSelectDialog
- Тепер перевіряється наявність схеми під час перевизначення початкового значення адреси
- У режимі FileMode::Directory приймаються лише наявні каталоги

### KActivities

(журналу змін не надано)

### KAuth

- KAUTH_HELPER_INSTALL_ABSOLUTE_DIR тепер доступна усім користувачам KAuth

### KCodecs

- KEmailAddress: додано перевантаження для extractEmailAddress і firstEmailAddress, яке повертає повідомлення про помилку.

### KCompletion

- Виправлено небажане позначення під час редагування назви файла у діалоговому вікні роботи із файлами (вада 344525)

### KConfig

- Усунуто аварійне завершення, якщо QWindow::screen() є порожнім
- Додано KConfigGui::setSessionConfig() (вада 346768)

### KCoreAddons

- Новий зручний програмний інтерфейс KPluginLoader::findPluginById()

### KDeclarative

- Підтримка створення ConfigModule з KPluginMetdata
- Виправлено події pressAndhold

### Підтримка KDELibs 4

- Використання QTemporaryFile замість жорсткого вбудовування тимчасового файла.

### KDocTools

- Оновлення перекладів
- Оновлено customization/ru
- Виправлено замінники із помилковими посиланнями

### KEmoticons

- Кешування теми у додатку інтеграції

### KGlobalAccel

- [runtime] Специфічний для платформи код пересунуто до додатків

### KIconThemes

- Оптимізовано KIconEngine::availableSizes()

### KIO

- Усунуто спробу автоматичного завершення імен користувачів і оцінку, якщо дописування на початку є непорожнім. (вада 346920)
- Використання KPluginLoader::factory(), якщо завантажується KIO::DndPopupMenuPlugin
- Виправлено жорстке блокування, якщо використовуються мережеві проксі-сервери (вада 346214)
- Виправлено KIO::suggestName, тепер суфікси назв файлів зберігаються
- Викинуто kbuildsycoca4 під час оновлення sycoca5.
- KFileWidget: у режимі лише каталогів не приймаються файли
- KIO::AccessManager: уможливлено асинхронну обробку послідовних QIODevice

### KNewStuff

- Додано новий метод fillMenuFromGroupingNames
- KMoreTools: додано багато нових групувань
- KMoreToolsMenuFactory: обробка «git-clients-and-actions»
- createMenuFromGroupingNames: параметр адреси тепер є необов'язковим

### KNotification

- Виправлено аварійне завершення у NotifyByExecute, якщо не встановлено віджет (вада 348510)
- Поліпшено обробку сповіщень під час закриття (вада 342752)
- Використання QDesktopWidget замінено на QScreen
- Забезпечення можливості використання KNotification із потоку обробки без графічного інтерфейсу

### Бібліотека Package

- Захист доступу до qpointer структури (вада 347231)

### KPeople

- Використання QTemporaryFile замість жорсткого використання /tmp.

### KPty

- Використання tcgetattr і tcsetattr, якщо можливо

### Kross

- Виправлено завантаження модулів Kross «forms» і «kdetranslation»

### KService

- Під час роботи від root зберігається значення власника файла для наявних файлів кешу (вада 342438)
- Захист від неможливості відкрити потік даних (вада 342438)
- Виправлено перевірку на некоректні права доступу під час запису файла (вада 342438)
- Виправлено запити ksycoca для псевдотипів MIME x-scheme-handler/*. (вада 347353)

### KTextEditor

- Уможливлено встановлення власних файлів підсвічування XML до katepart5/syntax сторонніми програмами і додатками, як у часи KDE 4.x
- Додано KTextEditor::Document::searchText()
- Повернуто використання KEncodingFileDialog (вада 343255)

### KTextWidgets

- Додано метод спорожнення декоратора
- Уможливлено використання нетипового декоратора sonnet
- У KTextEdit реалізовано пошук попереднього результату.
- Повторно додано підтримку синтезу тексту з мовлення

### KWidgetsAddons

- KAssistantDialog: повторно додано кнопку «Довідка», як у версії для KDELibs4

### KXMLGUI

- Додано керування сеансом для KMainWindow (вада 346768)

### NetworkManagerQt

- Викинуто підтримку WiMAX для NM 1.2.0+

### Бібліотеки Plasma

- Компоненти календаря тепер можуть показувати номери тижнів (вада 338195)
- Використання QtRendering для шрифтів у полях пароля
- Виправлено фільтрування AssociatedApplicationManager, якщо у типу MIME є альтернативний варіант (вада 340326)
- Виправлено розфарбовування тла панелі (вада 347143)
- Усунуто повідомлення «Не вдалося завантажити аплет»
- Можливість завантаження модулів KCM QML у вікнах налаштовування плазмоїдів
- Заборонено використання DataEngineStructure для аплетів
- libplasma портовано з sycoca якомога глибше
- [plasmacomponents] SectionScroller тепер відповідає ListView.section.criteria
- Смужки гортання тепер не приховуються автоматично, якщо використовується сенсорний екран (вада 347254)

### Sonnet

- Для SpellerPlugins використовується один центральний кеш.
- Зменшено кількість тимчасових отримань об'ємів пам'яті.
- Оптимізація: кеш словників не витирається під час копіювання об'єктів перевірки правопису.
- Оптимізація: усунуто потребу у викликах save(), виклик тепер відбувається один раз наприкінці, якщо це потрібно.

Обговорити цей випуск та поділитися ідеями можна у розділі коментарів до <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>статті з новиною</a>.
