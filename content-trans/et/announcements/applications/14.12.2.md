---
aliases:
- ../announce-applications-14.12.2
changelog: true
date: '2015-02-03'
description: KDE Ships Applications 14.12.2.
layout: application
title: KDE toob välja KDE rakendused 14.12.2
version: 14.12.2
---
February 3, 2015. Today KDE released the second stability update for <a href='../14.12.0'>KDE Applications 14.12</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

Rohkem kui 20 teadaoleva veaparanduse hulka kuuluvad anagrammimängu Kanagram, UML-i tööriista Umbrello, dokumendinäitaja Okular ja virtuaalse gloobuse Marble täiustused.

This release also includes Long Term Support versions of Plasma Workspaces 4.11.16, KDE Development Platform 4.14.5 and the Kontact Suite 4.14.5.
